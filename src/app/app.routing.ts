import { Routes } from '@angular/router';
import { AdminLayoutComponent } from './layouts/admin-new/admin-layout.component';

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'login-dokter',
    pathMatch: 'full'
  },
  {
    path: 'auth',
    loadChildren: './auth/login/login.module#LoginModule'
  },
  
  // start New
  {
    path: 'login-dokter',
    loadChildren: './auth/login-dokter-rj/login-dokter-rj.module#LoginDokterRJModule'
  },

  {
    path: 'menu-utama',
    component: AdminLayoutComponent,
    loadChildren: './app-dokter-rawat-jalan/menu-utama/menu-utama.module#MenuUtamaModule',
  },
  {
    path: 'pasien-rawat-jalan',
    component: AdminLayoutComponent,
    loadChildren: './app-dokter-rawat-jalan/list-pasien-rawat-jalan/list-pasien-rawat-jalan.module#ListPasienRJModule',
  },
  {
    path: 'pasien-rawat-inap',
    component: AdminLayoutComponent,
    loadChildren: './app-dokter-rawat-jalan/list-pasien-rawat-inap/list-pasien-rawat-inap.module#ListPasienRIModule',
  },
  // end New
  
  {
    path: 'forbidden',
    loadChildren: './maintenance/offline-ui/offline-ui.module#OfflineUiModule'
  },
  {
    path: '**',
    redirectTo: 'forbidden'
  }
];
