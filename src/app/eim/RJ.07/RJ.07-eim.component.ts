import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
declare var require: any;
import '../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { AppService } from "../../shared/service/app.service";
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from '../../shared/service/session.service';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { CompleterService, CompleterData, RemoteData } from 'ng2-completer';
import { DatePipe } from '@angular/common';
import { time } from 'd3';
import { DISABLED } from '@angular/forms/src/model';

@Component({
  selector: 'app-RJ.07-eim',
  templateUrl: './RJ.07-eim.component.html',
  styleUrls: [
    './RJ.07-eim.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class RJ07EIMComponent implements OnInit {

  private _serviceUrl = '/api/IgdRj07PerawatPengkajianAwal';
  public data: any;
  FormRJ: FormGroup;
  public useraccesdata: any;
  public loading = false;
  public onoff_skalaNyeri = false;
  yaNyeri: string = "";
  tidakNyeri: string = "";

  // Data Complete
  protected dataKomplit: CompleterData;

  today: number = Date.now();

  // private fieldArrayTindakan: Array<any> = [];
  // private newAttributeTindakan: any = {};

  // private fieldArrayObervasi: Array<any> = [];
  // private newAttributeObervasi: any = {};

  jenisKelamin: string = "";

  // Completer pasien
  protected pasienTerdaftar: string = "";
  protected searchPasien_arr = [];
  textSearching: string = "";

  // Completer karyawan
  protected karyawanTerdaftar: string = "";

  // Pengkajian Keperawatan

  sumberInfo: string = "";
  caraMasuk: string = "";
  asalMasuk: string = "";
  hubungiDokter: string = "";

  // Hasil pengkajian cidera/jatuh
  jwbPengkajianA: string = "";
  jwbPengkajianB: string = "";
  hasilPengkajian: string = "";

  // Skrining Nutrisi
  nilaiPenurunan: number = 0;
  nilaiPenurunanBB: number = 0;
  nilaiAsupan: number = 0;
  nilaiDiagnosa: number = 0;
  totalSkorSkriningNutrisi: number = 0;
  ketSkriningNutrisi: string = "";

  nilaiSkalaNyeri: string = "";
  sifatNyeri: string = "";
  nilaiUmur: string = "";
  public inValidFormRJ07: Boolean = false;

  private lastNoReg;
  pemberianio_arr = [];
  tindakan_arr = [];
  observasil_arr = [];


  // Pemeriksaan Dokter
  statusAlergi: string = "";

  constructor(private http: Http, private _http: HttpClient, private formatdate: AppFormatDate, private service: AppService, private route: ActivatedRoute,
    private router: Router, private session: SessionService, private completerService: CompleterService, private datePipe: DatePipe) {

    // get pasien
    this.dataService = completerService.remote(
      null,
      "ListSearch",
      "ListSearch");
    this.dataService.urlFormater(term => {
      return `api/IgdRj07PerawatPengkajianAwal/GetPasienBy/` + term;
    });
    this.dataService;
    console.log("Data Pasien: ", this.dataService);

    // get karyawan
    this.dataService1 = completerService.remote(
      null,
      "ListSearchKaryawan",
      "ListSearchKaryawan");
    this.dataService1.urlFormater(term1 => {
      return `api/IgdRj07PerawatPengkajianAwal/GetKaryawanBy/` + term1;
    });
    this.dataService1;
    console.log("Data Karyawan: ", this.dataService1);

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    //validation
    let NoReg = new FormControl('');
    let NipPerawat = new FormControl('');
    let NamaPerawat = new FormControl('');
    let KdRm = new FormControl('');
    let Poli = new FormControl('IGD');
    let Nama = new FormControl('');
    let JenKel = new FormControl('');
    let TglLahir = new FormControl('');
    let Umur = new FormControl({ value: '', disabled: true });
    let NamaDokter = new FormControl('');
    let Triase = new FormControl('');
    let JalurNafasSumbatanMerah = new FormControl(false);
    let JalurNafasBebasKuning = new FormControl(false);
    let JalurNafasAncamanKuning = new FormControl(false);
    let JalurNafasBebasHijau = new FormControl(false);
    let PernafasanHentiNafasMerah = new FormControl(false);
    let PernafasanBradipneaMerah = new FormControl(false);
    let PernafasanSianoisiMerah = new FormControl(false);
    let PernafasanTakipneaKuning = new FormControl(false);
    let PernafasanMengiKuning = new FormControl(false);
    let PernafasanFrekuensiNafasNormalHijau = new FormControl(false);
    let SirkulasiHentiJantungMerah = new FormControl(false);
    let SirkulasiNadiTidakTerabaMerah = new FormControl(false);
    let SirkulasiAkralDinginMerah = new FormControl(false);
    let SirkulasiNadiTerabaRendahKuning = new FormControl(false);
    let SirkulasiBradikardiaKuning = new FormControl(false);
    let SirkulasiTakikardiKuning = new FormControl(false);
    let SirkulasiPucatKuning = new FormControl(false);
    let SirkulasIAkralDinginKuning = new FormControl(false);
    let SirkulasiCrt2detikKuning = new FormControl(false);
    let SirkulasiNyeriDadaKuning = new FormControl(false);
    let SirkulasiNadiKuatHijau = new FormControl(false);
    let SirkulasiFrekuensiNadiNormalHijau = new FormControl(false);
    let AlasanPenggolonganTriase = new FormControl('');
    let AlasanTriaseLainnya = new FormControl({ value: '', disabled: true });
    let SumberInformasi = new FormControl('');
    // let AutoAnamnesa = new FormControl(false);
    // let HeteroAnamnesa = new FormControl(false);
    let NamaKeluarga = new FormControl({ value: '', disabled: true });
    let HubunganKeluarga = new FormControl({ value: '', disabled: true });
    let CaraMasuk = new FormControl('');
    let AlatBantuan = new FormControl({ value: '', disabled: true });
    let CaraMasukLainnya = new FormControl({ value: '', disabled: true });
    let AsalMasuk = new FormControl('');
    let TempatAsalRujukan = new FormControl({ value: '', disabled: true });
    let RiwayatPenyakitSekarang = new FormControl('');
    let RiwayatPenyakitDahulu = new FormControl('');
    let RiwayatPengobatanSebelumnya = new FormControl('');
    let FungsionalAktivitasSehariHari = new FormControl('');
    let OnOffSkalaNyeri = new FormControl('');
    let radioSkalaNyeri = new FormControl('');
    let StatusNyeri = new FormControl('');
    let ProvokatorNyeri = new FormControl('');
    let SifatNyeri = new FormControl('');
    let LokasiNyeri = new FormControl('');
    let PenjalaranNyeri = new FormControl('');
    let SkalaNyeri = new FormControl('');
    let DurasiNyeri = new FormControl('');
    let Frekuensi = new FormControl('');
    let PenghilangNyeriMinumObat = new FormControl(false);
    let PenghilangNyeriIstirahat = new FormControl(false);
    let PenghilangNyeriMendengarkanMusik = new FormControl(false);
    let PenghilangNyeriBerubahPosisi = new FormControl(false);
    let PenghilangNyeriLain = new FormControl(false);
    let NyeriHilangLain = new FormControl({ value: '', disabled: true });
    let KeseimbanganBerjalanSempoyonganLimbung = new FormControl('');
    let PerluPenopang = new FormControl('');
    let HasilPengkajianCideraJatuh = new FormControl({ value: '', disabled: true });
    // let HubungiDokter = new FormControl('');
    let WaktuHubungiDokter = new FormControl('');
    let SkorAdanyaPenurunanBeratBadan = new FormControl('');
    let SkorPenurunanBeratBadan = new FormControl({ value: '', disabled: true });
    let SkorPenurunanAsupanMakanan = new FormControl('');
    let SkorDiagnosaBerhubungandenganGizi = new FormControl('');
    let TotalSkorSkrinningNutrisi = new FormControl({ value: 0, disabled: true });
    let HasilSkrinningNutrisi = new FormControl({ value: '', disabled: true });
    let MasalahKeperawatanPenurunanKesadaran = new FormControl({ value: '', disabled: true });
    let MasalahKeperawatanKejang = new FormControl({ value: '', disabled: true });
    let MasalahKeperawatanKetidakEfektifanKebersihanJalanNafas = new FormControl({ value: '', disabled: true });
    let MasalahKeperawatanSesak = new FormControl({ value: '', disabled: true });
    let MasalahKeperawatanNyeri = new FormControl({ value: '', disabled: true });
    let MasalahKeperawatanGangguanHemodinamik = new FormControl({ value: '', disabled: true });
    let MasalahKeperawatanGangguanIntegritasKulit = new FormControl({ value: '', disabled: true });
    let MasalahKeperawatanKeseimbanganCairanElektrolit = new FormControl({ value: '', disabled: true });
    let MasalahKeperawatanPeningkatanSuhuTubuh = new FormControl({ value: '', disabled: true });
    let MasalahKeperawatanLainLain = new FormControl({ value: '', disabled: true });
    let MkevaluasiPenurunanKesadaranGcs = new FormControl({ value: '', disabled: true });
    let MkevaluasiKejang = new FormControl({ value: '', disabled: true });
    let MkevaluasiJalanNafasRonchi = new FormControl({ value: false, disabled: true });
    let MkevaluasiJalanNafasWheezing = new FormControl({ value: false, disabled: true });
    let MkevaluasiJalanNafasKrekels = new FormControl({ value: false, disabled: true });
    let MkevaluasiJalanNafasStridor = new FormControl({ value: false, disabled: true });
    let MkevaluasiSesakRr = new FormControl({ value: '', disabled: true });
    let MkevaluasiSesakRatraksi = new FormControl({ value: false, disabled: true });
    let MkevaluasiSesakSaturasi = new FormControl({ value: false, disabled: true });
    let MkevaluasiNyeri = new FormControl({ value: '', disabled: true });
    let MkevaluasiGangguanHemodinamikTekananDarah = new FormControl({ value: '', disabled: true });
    let MkevaluasiGangguanHemodinamikNadi = new FormControl({ value: '', disabled: true });
    let MkevaluasiGangguanHemodinamikAkralHangatDingin = new FormControl({ value: '', disabled: true });
    let MkevaluasiGangguanHemodinamikCrt = new FormControl({ value: '', disabled: true });
    let MkevaluasiGangguanIntegritasKulitLuka = new FormControl({ value: false, disabled: true });
    let MkevaluasiGangguanIntegritasKulitGatal = new FormControl({ value: false, disabled: true });
    let MkevaluasiGangguanIntegritasKulitMerah = new FormControl({ value: false, disabled: true });
    let MkevaluasiKeseimbanganCairanPendarahan = new FormControl({ value: false, disabled: true });
    let MkevaluasiKeseimbanganCairanIntake = new FormControl({ value: false, disabled: true });
    let MkevaluasiKeseimbanganCairanOutput = new FormControl({ value: false, disabled: true });
    let MkevaluasiSuhu = new FormControl({ value: '', disabled: true });
    let RisikoInfeksiNosokomial = new FormControl('');
    let KategoriPenyakit = new FormControl('');
    let KeluhanUtama = new FormControl('');
    let KesadaranEye = new FormControl('');
    let KesadaranVerbal = new FormControl('');
    let KesadaranMotorik = new FormControl('');
    let TekananDarahMmHg = new FormControl('');
    let NadiXMnt = new FormControl('');
    let SuhuCelcius = new FormControl('');
    let PernafasanXMnt = new FormControl('');
    let BeratBadanKg = new FormControl('');
    let TinggiBadanCm = new FormControl('');
    let SpO2 = new FormControl('');
    let PupilMmMm = new FormControl('');
    let ReflexCahaya = new FormControl('');
    let Akral = new FormControl('');
    let StatusAlergiYaTidak = new FormControl('');
    let sebutkanStatusAlergi = new FormControl('');
    let GangguanPerilaku = new FormControl('');
    let PemeriksaanFisik = new FormControl('');
    // Pemberian Infus
    let WaktuPI = new FormControl('');
    let NamaAlatPI = new FormControl('');
    let DosisPI = new FormControl('');
    let RutePI = new FormControl('');
    // Tindakan
    let WaktuT = new FormControl('');
    let TindakanT = new FormControl('');
    // Observasi Lanjutan
    let WaktuOL = new FormControl('');
    let KesadaranOL = new FormControl('');
    let TekananDarahOL = new FormControl('');
    let NadiOL = new FormControl('');
    let PernafasanOL = new FormControl('');
    let SuhuOL = new FormControl('');



    this.FormRJ = new FormGroup({
      NoReg: NoReg,
      NipPerawat: NipPerawat,
      NamaPerawat: NamaPerawat,
      KdRm: KdRm,
      Poli: Poli,
      Nama: Nama,
      JenKel: JenKel,
      TglLahir: TglLahir,
      Umur: Umur,
      NamaDokter: NamaDokter,
      Triase: Triase,
      JalurNafasSumbatanMerah: JalurNafasSumbatanMerah,
      JalurNafasBebasKuning: JalurNafasBebasKuning,
      JalurNafasAncamanKuning: JalurNafasAncamanKuning,
      JalurNafasBebasHijau: JalurNafasBebasHijau,
      PernafasanHentiNafasMerah: PernafasanHentiNafasMerah,
      PernafasanBradipneaMerah: PernafasanBradipneaMerah,
      PernafasanSianoisiMerah: PernafasanSianoisiMerah,
      PernafasanTakipneaKuning: PernafasanTakipneaKuning,
      PernafasanMengiKuning: PernafasanMengiKuning,
      PernafasanFrekuensiNafasNormalHijau: PernafasanFrekuensiNafasNormalHijau,
      SirkulasiHentiJantungMerah: SirkulasiHentiJantungMerah,
      SirkulasiNadiTidakTerabaMerah: SirkulasiNadiTidakTerabaMerah,
      SirkulasiAkralDinginMerah: SirkulasiAkralDinginMerah,
      SirkulasiNadiTerabaRendahKuning: SirkulasiNadiTerabaRendahKuning,
      SirkulasiBradikardiaKuning: SirkulasiBradikardiaKuning,
      SirkulasiTakikardiKuning: SirkulasiTakikardiKuning,
      SirkulasiPucatKuning: SirkulasiPucatKuning,
      SirkulasIAkralDinginKuning: SirkulasIAkralDinginKuning,
      SirkulasiCrt2detikKuning: SirkulasiCrt2detikKuning,
      SirkulasiNyeriDadaKuning: SirkulasiNyeriDadaKuning,
      SirkulasiNadiKuatHijau: SirkulasiNadiKuatHijau,
      SirkulasiFrekuensiNadiNormalHijau: SirkulasiFrekuensiNadiNormalHijau,
      AlasanPenggolonganTriase: AlasanPenggolonganTriase,
      AlasanTriaseLainnya: AlasanTriaseLainnya,
      SumberInformasi: SumberInformasi,
      // AutoAnamnesa: AutoAnamnesa,
      // HeteroAnamnesa: HeteroAnamnesa,
      NamaKeluarga: NamaKeluarga,
      HubunganKeluarga: HubunganKeluarga,
      CaraMasuk: CaraMasuk,
      AlatBantuan: AlatBantuan,
      CaraMasukLainnya: CaraMasukLainnya,
      AsalMasuk: AsalMasuk,
      TempatAsalRujukan: TempatAsalRujukan,
      RiwayatPenyakitSekarang: RiwayatPenyakitSekarang,
      RiwayatPenyakitDahulu: RiwayatPenyakitDahulu,
      RiwayatPengobatanSebelumnya: RiwayatPengobatanSebelumnya,
      FungsionalAktivitasSehariHari: FungsionalAktivitasSehariHari,
      radioSkalaNyeri: radioSkalaNyeri,
      OnOffSkalaNyeri: OnOffSkalaNyeri,
      StatusNyeri: StatusNyeri,
      ProvokatorNyeri: ProvokatorNyeri,
      SifatNyeri: SifatNyeri,
      LokasiNyeri: LokasiNyeri,
      PenjalaranNyeri: PenjalaranNyeri,
      SkalaNyeri: SkalaNyeri,
      DurasiNyeri: DurasiNyeri,
      Frekuensi: Frekuensi,
      PenghilangNyeriMinumObat: PenghilangNyeriMinumObat,
      PenghilangNyeriIstirahat: PenghilangNyeriIstirahat,
      PenghilangNyeriMendengarkanMusik: PenghilangNyeriMendengarkanMusik,
      PenghilangNyeriBerubahPosisi: PenghilangNyeriBerubahPosisi,
      PenghilangNyeriLain: PenghilangNyeriLain,
      NyeriHilangLain: NyeriHilangLain,
      KeseimbanganBerjalanSempoyonganLimbung: KeseimbanganBerjalanSempoyonganLimbung,
      PerluPenopang: PerluPenopang,
      HasilPengkajianCideraJatuh: HasilPengkajianCideraJatuh,
      // HubungiDokter: HubungiDokter,
      WaktuHubungiDokter: WaktuHubungiDokter,
      SkorAdanyaPenurunanBeratBadan: SkorAdanyaPenurunanBeratBadan,
      SkorPenurunanBeratBadan: SkorPenurunanBeratBadan,
      SkorPenurunanAsupanMakanan: SkorPenurunanAsupanMakanan,
      SkorDiagnosaBerhubungandenganGizi: SkorDiagnosaBerhubungandenganGizi,
      TotalSkorSkrinningNutrisi: TotalSkorSkrinningNutrisi,
      HasilSkrinningNutrisi: HasilSkrinningNutrisi,
      MasalahKeperawatanKejang: MasalahKeperawatanKejang,
      MasalahKeperawatanPenurunanKesadaran: MasalahKeperawatanPenurunanKesadaran,
      MasalahKeperawatanKetidakEfektifanKebersihanJalanNafas: MasalahKeperawatanKetidakEfektifanKebersihanJalanNafas,
      MasalahKeperawatanSesak: MasalahKeperawatanSesak,
      MasalahKeperawatanNyeri: MasalahKeperawatanNyeri,
      MasalahKeperawatanGangguanHemodinamik: MasalahKeperawatanGangguanHemodinamik,
      MasalahKeperawatanGangguanIntegritasKulit: MasalahKeperawatanGangguanIntegritasKulit,
      MasalahKeperawatanKeseimbanganCairanElektrolit: MasalahKeperawatanKeseimbanganCairanElektrolit,
      MasalahKeperawatanPeningkatanSuhuTubuh: MasalahKeperawatanPeningkatanSuhuTubuh,
      MasalahKeperawatanLainLain: MasalahKeperawatanLainLain,
      MkevaluasiPenurunanKesadaranGcs: MkevaluasiPenurunanKesadaranGcs,
      MkevaluasiKejang: MkevaluasiKejang,
      MkevaluasiJalanNafasRonchi: MkevaluasiJalanNafasRonchi,
      MkevaluasiJalanNafasWheezing: MkevaluasiJalanNafasWheezing,
      MkevaluasiJalanNafasKrekels: MkevaluasiJalanNafasKrekels,
      MkevaluasiJalanNafasStridor: MkevaluasiJalanNafasStridor,
      MkevaluasiSesakRr: MkevaluasiSesakRr,
      MkevaluasiSesakRatraksi: MkevaluasiSesakRatraksi,
      MkevaluasiSesakSaturasi: MkevaluasiSesakSaturasi,
      MkevaluasiNyeri: MkevaluasiNyeri,
      MkevaluasiGangguanHemodinamikTekananDarah: MkevaluasiGangguanHemodinamikTekananDarah,
      MkevaluasiGangguanHemodinamikNadi: MkevaluasiGangguanHemodinamikNadi,
      MkevaluasiGangguanHemodinamikAkralHangatDingin: MkevaluasiGangguanHemodinamikAkralHangatDingin,
      MkevaluasiGangguanHemodinamikCrt: MkevaluasiGangguanHemodinamikCrt,
      MkevaluasiGangguanIntegritasKulitLuka: MkevaluasiGangguanIntegritasKulitLuka,
      MkevaluasiGangguanIntegritasKulitGatal: MkevaluasiGangguanIntegritasKulitGatal,
      MkevaluasiGangguanIntegritasKulitMerah: MkevaluasiGangguanIntegritasKulitMerah,
      MkevaluasiKeseimbanganCairanPendarahan: MkevaluasiKeseimbanganCairanPendarahan,
      MkevaluasiKeseimbanganCairanIntake: MkevaluasiKeseimbanganCairanIntake,
      MkevaluasiKeseimbanganCairanOutput: MkevaluasiKeseimbanganCairanOutput,
      MkevaluasiSuhu: MkevaluasiSuhu,
      RisikoInfeksiNosokomial: RisikoInfeksiNosokomial,
      KategoriPenyakit: KategoriPenyakit,
      KeluhanUtama: KeluhanUtama,
      KesadaranEye: KesadaranEye,
      KesadaranVerbal: KesadaranVerbal,
      KesadaranMotorik: KesadaranMotorik,
      TekananDarahMmHg: TekananDarahMmHg,
      NadiXMnt: NadiXMnt,
      SuhuCelcius: SuhuCelcius,
      PernafasanXMnt: PernafasanXMnt,
      BeratBadanKg: BeratBadanKg,
      TinggiBadanCm: TinggiBadanCm,
      SpO2: SpO2,
      PupilMmMm: PupilMmMm,
      ReflexCahaya: ReflexCahaya,
      Akral: Akral,
      StatusAlergiYaTidak: StatusAlergiYaTidak,
      sebutkanStatusAlergi: sebutkanStatusAlergi,
      GangguanPerilaku: GangguanPerilaku,
      PemeriksaanFisik: PemeriksaanFisik,
      // Pemberian Infus
      WaktuPI: WaktuPI,
      NamaAlatPI: NamaAlatPI,
      DosisPI: DosisPI,
      RutePI: RutePI,
      // Tindakan
      WaktuT: WaktuT,
      TindakanT: TindakanT,
      // Observasi Lanjutan
      WaktuOL: WaktuOL,
      KesadaranOL: KesadaranOL,
      TekananDarahOL: TekananDarahOL,
      NadiOL: NadiOL,
      PernafasanOL: PernafasanOL,
      SuhuOL: SuhuOL

    });
  }

  // dataService untuk get pasien
  dataService: RemoteData;
  // dataService1 untuk get karyawan
  dataService1: RemoteData;
  ngOnInit() {
    // var date = new Date();
    // console.log(this.datePipe.transform(date, "h:mm a"));
  }

  onSelected(value) {
    let data = value["originalObject"];
    console.log("Pasien: ", data);
    console.log("Terpilih")

    // Set waktu
    let d = new Date();
    var hour = d.getHours();
    var minute = d.getMinutes();

    var times = hour + ":" + minute;
    // console.log(times);

    //validation
    let NoReg = new FormControl('');
    let KdRm = new FormControl(data.Kd_RM);
    let Poli = new FormControl('IGD');
    let Nama = new FormControl(data.Nama);
    let JenKel = new FormControl(data.JenKel);
    // Membuat kode jk
    if (data.JenKel == "Perempuan") {
      this.kdJK = "P";
    }
    else {
      this.kdJK = "L";
    }

    let TglLahir = new FormControl(this.datePipe.transform(data.Tgl_Lahir, "yyyy-MM-dd"));
    // konvert ke umur
    let { AgeFromDateString, AgeFromDate } = require('age-calculator');
    let ageFromString = new AgeFromDateString(this.datePipe.transform(data.Tgl_Lahir, "yyyy-MM-dd")).age;
    this.nilaiUmur = ageFromString;

    let Umur = new FormControl({ value: '', disabled: true });
    let NamaDokter = new FormControl('');
    let Triase = new FormControl('');
    let JalurNafasSumbatanMerah = new FormControl(false);
    let JalurNafasBebasKuning = new FormControl(false);
    let JalurNafasAncamanKuning = new FormControl(false);
    let JalurNafasBebasHijau = new FormControl(false);
    let PernafasanHentiNafasMerah = new FormControl(false);
    let PernafasanBradipneaMerah = new FormControl(false);
    let PernafasanSianoisiMerah = new FormControl(false);
    let PernafasanTakipneaKuning = new FormControl(false);
    let PernafasanMengiKuning = new FormControl(false);
    let PernafasanFrekuensiNafasNormalHijau = new FormControl(false);
    let SirkulasiHentiJantungMerah = new FormControl(false);
    let SirkulasiNadiTidakTerabaMerah = new FormControl(false);
    let SirkulasiAkralDinginMerah = new FormControl(false);
    let SirkulasiNadiTerabaRendahKuning = new FormControl(false);
    let SirkulasiBradikardiaKuning = new FormControl(false);
    let SirkulasiTakikardiKuning = new FormControl(false);
    let SirkulasiPucatKuning = new FormControl(false);
    let SirkulasIAkralDinginKuning = new FormControl(false);
    let SirkulasiCrt2detikKuning = new FormControl(false);
    let SirkulasiNyeriDadaKuning = new FormControl(false);
    let SirkulasiNadiKuatHijau = new FormControl(false);
    let SirkulasiFrekuensiNadiNormalHijau = new FormControl(false);
    let AlasanPenggolonganTriase = new FormControl('');
    let AlasanTriaseLainnya = new FormControl({ value: '', disabled: true });
    let SumberInformasi = new FormControl('');
    // let AutoAnamnesa = new FormControl(false);
    // let HeteroAnamnesa = new FormControl(false);
    let NamaKeluarga = new FormControl({ value: '', disabled: true });
    let HubunganKeluarga = new FormControl({ value: '', disabled: true });
    let CaraMasuk = new FormControl('');
    let AlatBantuan = new FormControl({ value: '', disabled: true });
    let CaraMasukLainnya = new FormControl({ value: '', disabled: true });
    let AsalMasuk = new FormControl('');
    let TempatAsalRujukan = new FormControl({ value: '', disabled: true });
    let RiwayatPenyakitSekarang = new FormControl('');
    let RiwayatPenyakitDahulu = new FormControl('');
    let RiwayatPengobatanSebelumnya = new FormControl('');
    let FungsionalAktivitasSehariHari = new FormControl('');
    let radioSkalaNyeri = new FormControl('');
    let OnOffSkalaNyeri = new FormControl('');
    let StatusNyeri = new FormControl('');
    let ProvokatorNyeri = new FormControl('');
    let SifatNyeri = new FormControl('');
    let LokasiNyeri = new FormControl('');
    let PenjalaranNyeri = new FormControl('');
    let SkalaNyeri = new FormControl('');
    let DurasiNyeri = new FormControl('');
    let Frekuensi = new FormControl();
    let PenghilangNyeriMinumObat = new FormControl(false);
    let PenghilangNyeriIstirahat = new FormControl(false);
    let PenghilangNyeriMendengarkanMusik = new FormControl(false);
    let PenghilangNyeriBerubahPosisi = new FormControl(false);
    let PenghilangNyeriLain = new FormControl(false);
    let NyeriHilangLain = new FormControl({ value: '', disabled: true });
    let KeseimbanganBerjalanSempoyonganLimbung = new FormControl('');
    let PerluPenopang = new FormControl('');
    let HasilPengkajianCideraJatuh = new FormControl({ value: '', disabled: true });
    let WaktuHubungiDokter = new FormControl(times);
    let SkorAdanyaPenurunanBeratBadan = new FormControl('');
    let SkorPenurunanBeratBadan = new FormControl({ value: '', disabled: true });
    let SkorPenurunanAsupanMakanan = new FormControl('');
    let SkorDiagnosaBerhubungandenganGizi = new FormControl('');
    let TotalSkorSkrinningNutrisi = new FormControl({ value: 0, disabled: true });
    let HasilSkrinningNutrisi = new FormControl({ value: '', disabled: true });
    let MasalahKeperawatanPenurunanKesadaran = new FormControl({ value: '', disabled: true });
    let MasalahKeperawatanKejang = new FormControl({ value: '', disabled: true });
    let MasalahKeperawatanKetidakEfektifanKebersihanJalanNafas = new FormControl({ value: '', disabled: true });
    let MasalahKeperawatanSesak = new FormControl({ value: '', disabled: true });
    let MasalahKeperawatanNyeri = new FormControl({ value: '', disabled: true });
    let MasalahKeperawatanGangguanHemodinamik = new FormControl({ value: '', disabled: true });
    let MasalahKeperawatanGangguanIntegritasKulit = new FormControl({ value: '', disabled: true });
    let MasalahKeperawatanKeseimbanganCairanElektrolit = new FormControl({ value: '', disabled: true });
    let MasalahKeperawatanPeningkatanSuhuTubuh = new FormControl({ value: '', disabled: true });
    let MasalahKeperawatanLainLain = new FormControl({ value: '', disabled: true });
    let MkevaluasiPenurunanKesadaranGcs = new FormControl({ value: '', disabled: true });
    let MkevaluasiKejang = new FormControl({ value: '', disabled: true });
    let MkevaluasiJalanNafasRonchi = new FormControl({ value: false, disabled: true });
    let MkevaluasiJalanNafasWheezing = new FormControl({ value: false, disabled: true });
    let MkevaluasiJalanNafasKrekels = new FormControl({ value: false, disabled: true });
    let MkevaluasiJalanNafasStridor = new FormControl({ value: false, disabled: true });
    let MkevaluasiSesakRr = new FormControl({ value: '', disabled: true });
    let MkevaluasiSesakRatraksi = new FormControl({ value: false, disabled: true });
    let MkevaluasiSesakSaturasi = new FormControl({ value: false, disabled: true });
    let MkevaluasiNyeri = new FormControl({ value: '', disabled: true });
    let MkevaluasiGangguanHemodinamikTekananDarah = new FormControl({ value: '', disabled: true });
    let MkevaluasiGangguanHemodinamikNadi = new FormControl({ value: '', disabled: true });
    let MkevaluasiGangguanHemodinamikAkralHangatDingin = new FormControl({ value: '', disabled: true });
    let MkevaluasiGangguanHemodinamikCrt = new FormControl({ value: '', disabled: true });
    let MkevaluasiGangguanIntegritasKulitLuka = new FormControl({ value: false, disabled: true });
    let MkevaluasiGangguanIntegritasKulitGatal = new FormControl({ value: false, disabled: true });
    let MkevaluasiGangguanIntegritasKulitMerah = new FormControl({ value: false, disabled: true });
    let MkevaluasiKeseimbanganCairanPendarahan = new FormControl({ value: false, disabled: true });
    let MkevaluasiKeseimbanganCairanIntake = new FormControl({ value: false, disabled: true });
    let MkevaluasiKeseimbanganCairanOutput = new FormControl({ value: false, disabled: true });
    let MkevaluasiSuhu = new FormControl({ value: '', disabled: true });
    let RisikoInfeksiNosokomial = new FormControl('');
    let KategoriPenyakit = new FormControl('');
    let KeluhanUtama = new FormControl('');
    let KesadaranEye = new FormControl('');
    let KesadaranVerbal = new FormControl('');
    let KesadaranMotorik = new FormControl('');
    let TekananDarahMmHg = new FormControl('');
    let NadiXMnt = new FormControl('');
    let SuhuCelcius = new FormControl('');
    let PernafasanXMnt = new FormControl('');
    let BeratBadanKg = new FormControl('');
    let TinggiBadanCm = new FormControl('');
    let SpO2 = new FormControl('');
    let PupilMmMm = new FormControl('');
    let ReflexCahaya = new FormControl('');
    let Akral = new FormControl('');
    let StatusAlergiYaTidak = new FormControl('');
    let sebutkanStatusAlergi = new FormControl('');
    let GangguanPerilaku = new FormControl('');
    let PemeriksaanFisik = new FormControl('');
    // Pemberian Infus
    // let WaktuPI = new FormControl('');
    let WaktuPI = new FormControl(times);
    let NamaAlatPI = new FormControl('');
    let DosisPI = new FormControl('');
    let RutePI = new FormControl('');
    // Tindakan
    let WaktuT = new FormControl(times);
    let TindakanT = new FormControl('');
    // Observasi Lanjutan
    let WaktuOL = new FormControl(times);
    let KesadaranOL = new FormControl('');
    let TekananDarahOL = new FormControl('');
    let NadiOL = new FormControl('');
    let PernafasanOL = new FormControl('');
    let SuhuOL = new FormControl('');

    this.FormRJ = new FormGroup({
      NoReg: NoReg,
      KdRm: KdRm,
      Poli: Poli,
      Nama: Nama,
      JenKel: JenKel,
      TglLahir: TglLahir,
      Umur: Umur,
      NamaDokter: NamaDokter,
      Triase: Triase,
      JalurNafasSumbatanMerah: JalurNafasSumbatanMerah,
      JalurNafasBebasKuning: JalurNafasBebasKuning,
      JalurNafasAncamanKuning: JalurNafasAncamanKuning,
      JalurNafasBebasHijau: JalurNafasBebasHijau,
      PernafasanHentiNafasMerah: PernafasanHentiNafasMerah,
      PernafasanBradipneaMerah: PernafasanBradipneaMerah,
      PernafasanSianoisiMerah: PernafasanSianoisiMerah,
      PernafasanTakipneaKuning: PernafasanTakipneaKuning,
      PernafasanMengiKuning: PernafasanMengiKuning,
      PernafasanFrekuensiNafasNormalHijau: PernafasanFrekuensiNafasNormalHijau,
      SirkulasiHentiJantungMerah: SirkulasiHentiJantungMerah,
      SirkulasiNadiTidakTerabaMerah: SirkulasiNadiTidakTerabaMerah,
      SirkulasiAkralDinginMerah: SirkulasiAkralDinginMerah,
      SirkulasiNadiTerabaRendahKuning: SirkulasiNadiTerabaRendahKuning,
      SirkulasiBradikardiaKuning: SirkulasiBradikardiaKuning,
      SirkulasiTakikardiKuning: SirkulasiTakikardiKuning,
      SirkulasiPucatKuning: SirkulasiPucatKuning,
      SirkulasIAkralDinginKuning: SirkulasIAkralDinginKuning,
      SirkulasiCrt2detikKuning: SirkulasiCrt2detikKuning,
      SirkulasiNyeriDadaKuning: SirkulasiNyeriDadaKuning,
      SirkulasiNadiKuatHijau: SirkulasiNadiKuatHijau,
      SirkulasiFrekuensiNadiNormalHijau: SirkulasiFrekuensiNadiNormalHijau,
      AlasanPenggolonganTriase: AlasanPenggolonganTriase,
      AlasanTriaseLainnya: AlasanTriaseLainnya,
      SumberInformasi: SumberInformasi,
      // AutoAnamnesa: AutoAnamnesa,
      // HeteroAnamnesa: HeteroAnamnesa,
      NamaKeluarga: NamaKeluarga,
      HubunganKeluarga: HubunganKeluarga,
      CaraMasuk: CaraMasuk,
      AlatBantuan: AlatBantuan,
      CaraMasukLainnya: CaraMasukLainnya,
      AsalMasuk: AsalMasuk,
      TempatAsalRujukan: TempatAsalRujukan,
      RiwayatPenyakitSekarang: RiwayatPenyakitSekarang,
      RiwayatPenyakitDahulu: RiwayatPenyakitDahulu,
      RiwayatPengobatanSebelumnya: RiwayatPengobatanSebelumnya,
      FungsionalAktivitasSehariHari: FungsionalAktivitasSehariHari,
      radioSkalaNyeri: radioSkalaNyeri,
      OnOffSkalaNyeri: OnOffSkalaNyeri,
      StatusNyeri: StatusNyeri,
      ProvokatorNyeri: ProvokatorNyeri,
      SifatNyeri: SifatNyeri,
      LokasiNyeri: LokasiNyeri,
      PenjalaranNyeri: PenjalaranNyeri,
      SkalaNyeri: SkalaNyeri,
      DurasiNyeri: DurasiNyeri,
      Frekuensi: Frekuensi,
      PenghilangNyeriMinumObat: PenghilangNyeriMinumObat,
      PenghilangNyeriIstirahat: PenghilangNyeriIstirahat,
      PenghilangNyeriMendengarkanMusik: PenghilangNyeriMendengarkanMusik,
      PenghilangNyeriBerubahPosisi: PenghilangNyeriBerubahPosisi,
      PenghilangNyeriLain: PenghilangNyeriLain,
      NyeriHilangLain: NyeriHilangLain,
      KeseimbanganBerjalanSempoyonganLimbung: KeseimbanganBerjalanSempoyonganLimbung,
      PerluPenopang: PerluPenopang,
      HasilPengkajianCideraJatuh: HasilPengkajianCideraJatuh,
      WaktuHubungiDokter: WaktuHubungiDokter,
      SkorAdanyaPenurunanBeratBadan: SkorAdanyaPenurunanBeratBadan,
      SkorPenurunanBeratBadan: SkorPenurunanBeratBadan,
      SkorPenurunanAsupanMakanan: SkorPenurunanAsupanMakanan,
      SkorDiagnosaBerhubungandenganGizi: SkorDiagnosaBerhubungandenganGizi,
      TotalSkorSkrinningNutrisi: TotalSkorSkrinningNutrisi,
      HasilSkrinningNutrisi: HasilSkrinningNutrisi,
      MasalahKeperawatanKejang: MasalahKeperawatanKejang,
      MasalahKeperawatanPenurunanKesadaran: MasalahKeperawatanPenurunanKesadaran,
      MasalahKeperawatanKetidakEfektifanKebersihanJalanNafas: MasalahKeperawatanKetidakEfektifanKebersihanJalanNafas,
      MasalahKeperawatanSesak: MasalahKeperawatanSesak,
      MasalahKeperawatanNyeri: MasalahKeperawatanNyeri,
      MasalahKeperawatanGangguanHemodinamik: MasalahKeperawatanGangguanHemodinamik,
      MasalahKeperawatanGangguanIntegritasKulit: MasalahKeperawatanGangguanIntegritasKulit,
      MasalahKeperawatanKeseimbanganCairanElektrolit: MasalahKeperawatanKeseimbanganCairanElektrolit,
      MasalahKeperawatanPeningkatanSuhuTubuh: MasalahKeperawatanPeningkatanSuhuTubuh,
      MasalahKeperawatanLainLain: MasalahKeperawatanLainLain,
      MkevaluasiPenurunanKesadaranGcs: MkevaluasiPenurunanKesadaranGcs,
      MkevaluasiKejang: MkevaluasiKejang,
      MkevaluasiJalanNafasRonchi: MkevaluasiJalanNafasRonchi,
      MkevaluasiJalanNafasWheezing: MkevaluasiJalanNafasWheezing,
      MkevaluasiJalanNafasKrekels: MkevaluasiJalanNafasKrekels,
      MkevaluasiJalanNafasStridor: MkevaluasiJalanNafasStridor,
      MkevaluasiSesakRr: MkevaluasiSesakRr,
      MkevaluasiSesakRatraksi: MkevaluasiSesakRatraksi,
      MkevaluasiSesakSaturasi: MkevaluasiSesakSaturasi,
      MkevaluasiNyeri: MkevaluasiNyeri,
      MkevaluasiGangguanHemodinamikTekananDarah: MkevaluasiGangguanHemodinamikTekananDarah,
      MkevaluasiGangguanHemodinamikNadi: MkevaluasiGangguanHemodinamikNadi,
      MkevaluasiGangguanHemodinamikAkralHangatDingin: MkevaluasiGangguanHemodinamikAkralHangatDingin,
      MkevaluasiGangguanHemodinamikCrt: MkevaluasiGangguanHemodinamikCrt,
      MkevaluasiGangguanIntegritasKulitLuka: MkevaluasiGangguanIntegritasKulitLuka,
      MkevaluasiGangguanIntegritasKulitGatal: MkevaluasiGangguanIntegritasKulitGatal,
      MkevaluasiGangguanIntegritasKulitMerah: MkevaluasiGangguanIntegritasKulitMerah,
      MkevaluasiKeseimbanganCairanPendarahan: MkevaluasiKeseimbanganCairanPendarahan,
      MkevaluasiKeseimbanganCairanIntake: MkevaluasiKeseimbanganCairanIntake,
      MkevaluasiKeseimbanganCairanOutput: MkevaluasiKeseimbanganCairanOutput,
      MkevaluasiSuhu: MkevaluasiSuhu,
      RisikoInfeksiNosokomial: RisikoInfeksiNosokomial,
      KategoriPenyakit: KategoriPenyakit,
      KeluhanUtama: KeluhanUtama,
      KesadaranEye: KesadaranEye,
      KesadaranVerbal: KesadaranVerbal,
      KesadaranMotorik: KesadaranMotorik,
      TekananDarahMmHg: TekananDarahMmHg,
      NadiXMnt: NadiXMnt,
      SuhuCelcius: SuhuCelcius,
      PernafasanXMnt: PernafasanXMnt,
      BeratBadanKg: BeratBadanKg,
      TinggiBadanCm: TinggiBadanCm,
      SpO2: SpO2,
      PupilMmMm: PupilMmMm,
      ReflexCahaya: ReflexCahaya,
      Akral: Akral,
      StatusAlergiYaTidak: StatusAlergiYaTidak,
      sebutkanStatusAlergi: sebutkanStatusAlergi,
      GangguanPerilaku: GangguanPerilaku,
      PemeriksaanFisik: PemeriksaanFisik,
      // Pemberian Infus
      WaktuPI: WaktuPI,
      NamaAlatPI: NamaAlatPI,
      DosisPI: DosisPI,
      RutePI: RutePI,
      // Tindakan
      WaktuT: WaktuT,
      TindakanT: TindakanT,
      // Observasi Lanjutan
      WaktuOL: WaktuOL,
      KesadaranOL: KesadaranOL,
      TekananDarahOL: TekananDarahOL,
      NadiOL: NadiOL,
      PernafasanOL: PernafasanOL,
      SuhuOL: SuhuOL
    });
  }

  nip_perawat = "";
  nama_perawat = "";
  onSelectKaryawan(value) {
    let data = value["originalObject"];

    console.log(data);
    console.log("Masuk");

    let NipPerawat = new FormControl(data.NIP);
    this.nip_perawat = data.NIP;
    console.log(this.nip_perawat);
    let NamaPerawat = new FormControl(data.NAMA);
    this.nama_perawat = data.NAMA;

    this.FormRJ = new FormGroup({
      NipPerawat: NipPerawat,
      NamaPerawat: NamaPerawat
    })
  }

  // Radio
  onChange(value) {
    console.log(value);
  }
  // Checked
  onChecked(isChecked: boolean) {
    console.log(isChecked);
  }
  // Keypress
  onSearchChange(searchValue: string) {
    console.log(searchValue);
  }

  kdJK = "";
  onChangeJenisKelamin(value) {
    this.jenisKelamin = value;
    if (this.jenisKelamin == "Perempuan") {
      this.kdJK = "P";
    }
    else {
      this.kdJK = "L";
    }
    console.log(this.kdJK);
  }


  /** Pemberian Infus */
  infus: any = [];
  addFieldPIO() {
    this.pemberianio_arr.push({
      'NoReg': this.infus["NoReg"],
      'WaktuPemberianInfusObat': this.infus["WaktuPI"],
      'NamaAlat': this.infus["NamaAlatPI"],
      'Dosis': this.infus["DosisPI"],
      'Rute': this.infus["RutePI"]
    })
    // console.log(this.pemberianio_arr);
    this.infus = [];
  }
  deleteFieldPIO(index) {
    this.pemberianio_arr.splice(index, 1);
    // console.log(this.pemberianio_arr);
  }
  /** Pemberian Infus */

  /** Tindakan */
  tindakan: any = [];
  addFieldTindakan() {
    this.tindakan_arr.push({
      'NoReg': this.tindakan["NoReg"],
      'WaktuTindakan': this.tindakan["WaktuT"],
      'Tindakan': this.tindakan["TindakanT"]
    })
    console.log(this.tindakan_arr);
    this.tindakan = [];
  }
  deleteFieldTindakan(index) {
    this.tindakan_arr.splice(index, 1);
    // console.log(this.tindakan_arr);
  }
  /** Tindakan */

  /** Obervasi Lanjutan */
  observasi: any = [];
  addFieldObervasi() {
    this.observasil_arr.push({
      'NoReg': this.observasi["NoReg"],
      'WaktuObervasi': this.observasi["WaktuOL"],
      'Kesadaran': this.observasi["KesadaranOL"],
      'TekananDarah': this.observasi["TekananDarahOL"],
      'Nadi': this.observasi["NadiOL"],
      'Pernafasan': this.observasi["PernafasanOL"],
      'Suhu': this.observasi["SuhuOL"]
    })
    console.log(this.observasil_arr);
    this.observasi = [];
  }
  deleteFieldObervasi(index) {
    this.observasil_arr.splice(index, 1);
    // console.log(this.observasil_arr);
  }

  onSelect(value) {
    // console.log(value)
  }

  onSelectTglLahir(value) {
    var ageCalculator = require('age-calculator');
    let { AgeFromDateString, AgeFromDate } = require('age-calculator');

    // Be careful: Javascript months start at 0 (so zero stands for january)

    let ageFromDate = new AgeFromDate(new Date(value)).age;
    // console.log("value from AgeFromDate", ageFromDate);
    // output: 30 (at the time of this doc)

    let ageFromString = new AgeFromDateString(value).age;
    //console.log("value from ageFromString", ageFromString);
    // output: 30 (at the time of this doc)
    this.nilaiUmur = ageFromString;
    // console.log(this.nilaiUmur);

  }

  onSelectSkalaNyeri(value) {
    this.nilaiSkalaNyeri = value;
    if (this.nilaiSkalaNyeri == "0" || this.nilaiSkalaNyeri == "1" || this.nilaiSkalaNyeri == "2" || this.nilaiSkalaNyeri == "3") {
      this.sifatNyeri = "Ringan";
      // console.log(this.sifatNyeri);
    }
    else if (this.nilaiSkalaNyeri == "4" || this.nilaiSkalaNyeri == "5" || this.nilaiSkalaNyeri == "6") {
      this.sifatNyeri = "Sedang";
      // console.log(this.sifatNyeri);
    }
    else if (this.nilaiSkalaNyeri == "7" || this.nilaiSkalaNyeri == "8" || this.nilaiSkalaNyeri == "9" || this.nilaiSkalaNyeri == "10") {
      this.sifatNyeri = "Berat";
      // console.log(this.sifatNyeri);
    }
  }

  // Alasan Triase
  onEnableTriaseLain(value) {
    console.log(value);
    this.FormRJ.controls["AlasanTriaseLainnya"].enable();
  }
  onDisableTriseLain(value) {
    console.log(value);
    this.FormRJ.controls["AlasanTriaseLainnya"].disable();
    this.FormRJ.controls["AlasanTriaseLainnya"].reset();
  }

  // Informasi didapat dari
  onEnableSumberInformasi(value) {
    console.log(value);
    this.FormRJ.controls["NamaKeluarga"].enable();
    this.FormRJ.controls["HubunganKeluarga"].enable();
  }
  onDisableSumberInformasi(value) {
    console.log(value);
    this.FormRJ.controls["NamaKeluarga"].disable();
    this.FormRJ.controls["NamaKeluarga"].reset();
    this.FormRJ.controls["HubunganKeluarga"].disable();
    this.FormRJ.controls["HubunganKeluarga"].reset();
  }

  // radio OnChange Cara masuk
  // onChangeEnableInfo(isChecked: boolean) {
  //   // console.log(isChecked);
  //   isChecked ? this.FormRJ.controls["NamaKeluarga"].enable() : this.FormRJ.controls["NamaKeluarga"].disable();
  //   isChecked ? this.FormRJ.controls["HubunganKeluarga"].enable() : this.FormRJ.controls["HubunganKeluarga"].disable();
  //   if (isChecked == false) {
  //     this.FormRJ.controls["NamaKeluarga"].reset();
  //     this.FormRJ.controls["HubunganKeluarga"].reset();
  //   }
  // }

  /**
  onChangeEnableInfo(value) {
    this.sumberInfo = value;
    // console.log(this.sumberInfo);
    this.FormRJ.controls["NamaKeluarga"].enable();
    this.FormRJ.controls["HubunganKeluarga"].enable();
  } */
  /**
  onChangeDisableInfo(value) {
    this.sumberInfo = value;
    // console.log(this.sumberInfo);
    this.FormRJ.controls["NamaKeluarga"].disable();
    this.FormRJ.controls["HubunganKeluarga"].disable();
  } */

  // radio OnChange Cara masuk
  onChangeEnableCaraMasuk(value) {
    console.log(value);
    this.caraMasuk = value;
    this.FormRJ.controls["AlatBantuan"].enable();
    this.FormRJ.controls["CaraMasukLainnya"].disable();
    this.FormRJ.controls["CaraMasukLainnya"].reset();
  }
  onChangeEnableCaraMasukLainnya(value) {
    console.log(value);
    this.caraMasuk = value;
    this.FormRJ.controls["CaraMasukLainnya"].enable();
    this.FormRJ.controls["AlatBantuan"].disable();
    this.FormRJ.controls["AlatBantuan"].reset();
  }
  onChangeDisableCaraMasuk(value) {
    console.log(value);
    this.caraMasuk = value;
    this.FormRJ.controls["AlatBantuan"].disable();
    this.FormRJ.controls["AlatBantuan"].reset();
    this.FormRJ.controls["CaraMasukLainnya"].disable();
    this.FormRJ.controls["CaraMasukLainnya"].reset();
  }

  // radio OnChange Asal masuk
  onChangeEnableAsalMasuk(value) {
    console.log(value);
    this.asalMasuk = value;
    this.FormRJ.controls["TempatAsalRujukan"].enable();
  }
  onChangeDisableAsalMasuk(value) {
    console.log(value);
    this.asalMasuk = value;
    this.FormRJ.controls["TempatAsalRujukan"].disable();
    this.FormRJ.controls["TempatAsalRujukan"].reset();
  }

  // On Off Skala Nyeri
  // onOffSkalaNyeri(isChecked: boolean) {
  //   if (isChecked == false) {
  //     this.onoff_skalaNyeri = false;
  //     this.FormRJ.controls["SifatNyeri"].reset();
  //     this.FormRJ.controls["SkalaNyeri"].reset();
  //   }
  //   else {
  //     this.onoff_skalaNyeri = true;
  //   }
  // }
  onEnableSkalaNyeri(value) {
    // console.log("Ya = " + value);
    this.yaNyeri = value;
    if (this.yaNyeri == "1") {
      this.onoff_skalaNyeri = true;
      console.log(this.onoff_skalaNyeri);
      // this.FormRJ.controls["radioSkalaNyeri"].enable();
      // this.FormRJ.controls["ProvokatorNyeri"].enable();
      // this.FormRJ.controls["LokasiNyeri"].enable();
      // this.FormRJ.controls["PenjalaranNyeri"].enable();
      // this.FormRJ.controls["DurasiNyeri"].enable();
      // this.FormRJ.controls["Frekuensi"].enable();
    }
  }
  onDisableSkalaNyeri(value) {
    // console.log("Tidak = " + value);
    this.tidakNyeri = value;
    if (this.tidakNyeri == "0") {
      this.onoff_skalaNyeri = false;
      this.sifatNyeri = "";
      this.nilaiSkalaNyeri = "";
      console.log(this.onoff_skalaNyeri);
      this.FormRJ.controls["radioSkalaNyeri"].reset();
      // this.FormRJ.controls["radioSkalaNyeri"].disable();
      this.FormRJ.controls["ProvokatorNyeri"].reset();
      // this.FormRJ.controls["ProvokatorNyeri"].disable();
      this.FormRJ.controls["SifatNyeri"].reset();
      this.FormRJ.controls["LokasiNyeri"].reset();
      // this.FormRJ.controls["LokasiNyeri"].disable();
      this.FormRJ.controls["PenjalaranNyeri"].reset();
      // this.FormRJ.controls["PenjalaranNyeri"].disable();
      this.FormRJ.controls["SkalaNyeri"].reset();
      this.FormRJ.controls["DurasiNyeri"].reset();
      // this.FormRJ.controls["DurasiNyeri"].disable();
      this.FormRJ.controls["Frekuensi"].reset();
      // this.FormRJ.controls["Frekuensi"].disable();
    }
  }

  // Hasil Pengkajian Cidera / Jatuh
  onChangePengkajianA(value) {
    this.jwbPengkajianA = value;
    if (this.jwbPengkajianA == "Tidak" && this.jwbPengkajianB == "Tidak") {
      this.hasilPengkajian = "Tidak Berisiko";
    }
    else if (this.jwbPengkajianA == "Ya" && this.jwbPengkajianB == "Tidak") {
      this.hasilPengkajian = "Risiko Rendah";
    }
    else if (this.jwbPengkajianA == "Tidak" && this.jwbPengkajianB == "Ya") {
      this.hasilPengkajian = "Risiko Rendah";
    }
    else if (this.jwbPengkajianA == "Ya" && this.jwbPengkajianB == "Ya") {
      this.hasilPengkajian = "Risiko Tinggi";
    }
  }
  onChangePengkajianB(value) {
    this.jwbPengkajianB = value;
    console.log(this.jwbPengkajianB);
    if (this.jwbPengkajianA == "Tidak" && this.jwbPengkajianB == "Tidak") {
      this.hasilPengkajian = "Tidak Berisiko";
      console.log(this.hasilPengkajian);
    }
    else if (this.jwbPengkajianA == "Ya" && this.jwbPengkajianB == "Tidak") {
      this.hasilPengkajian = "Risiko Rendah";
      console.log(this.hasilPengkajian);
    }
    else if (this.jwbPengkajianA == "Tidak" && this.jwbPengkajianB == "Ya") {
      this.hasilPengkajian = "Risiko Rendah";
      console.log(this.hasilPengkajian);
    }
    else if (this.jwbPengkajianA == "Ya" && this.jwbPengkajianB == "Ya") {
      this.hasilPengkajian = "Risiko Tinggi";
      console.log(this.hasilPengkajian);
    }
  }

  // Skrining Nutrisi
  // radio OnChange Penurunan
  valueBB = "";
  onChangeDisablePenurunan(value) {
    this.nilaiPenurunan = +value;
    this.totalSkorSkriningNutrisi = this.nilaiPenurunan + this.nilaiPenurunanBB + this.nilaiAsupan + this.nilaiDiagnosa;
    this.FormRJ.controls["SkorPenurunanBeratBadan"].disable();
    if (value == "0") {
      this.valueBB = "";
      this.onChangePenurunanBB("");
    }

    if (this.totalSkorSkriningNutrisi <= 1) {
      this.ketSkriningNutrisi = "Tidak Berisiko";
    }
    else {
      this.ketSkriningNutrisi = "Berisiko";
    }
    // console.log(this.ketSkriningNutrisi);
  }
  onChangeEnablePenurunan(value) {
    this.nilaiPenurunan = +value;
    this.totalSkorSkriningNutrisi = this.nilaiPenurunan + this.nilaiPenurunanBB + this.nilaiAsupan + this.nilaiDiagnosa;
    this.FormRJ.controls["SkorPenurunanBeratBadan"].enable();
    if (this.totalSkorSkriningNutrisi <= 1) {
      this.ketSkriningNutrisi = "Tidak Berisiko";
    }
    else {
      this.ketSkriningNutrisi = "Berisiko";
    }
    // console.log(this.ketSkriningNutrisi);
  }
  onChangePenurunanBB(value) {
    this.nilaiPenurunanBB = +value;
    this.totalSkorSkriningNutrisi = this.nilaiPenurunan + this.nilaiPenurunanBB + this.nilaiAsupan + this.nilaiDiagnosa;
    if (this.totalSkorSkriningNutrisi <= 1) {
      this.ketSkriningNutrisi = "Tidak Berisiko";
    }
    else {
      this.ketSkriningNutrisi = "Berisiko";
    }
    // console.log(this.ketSkriningNutrisi);
  }

  // Radio onChange Asupan
  onChangeAsupan(value) {
    this.nilaiAsupan = +value;
    this.totalSkorSkriningNutrisi = this.nilaiPenurunan + this.nilaiPenurunanBB + this.nilaiAsupan + this.nilaiDiagnosa;
    if (this.totalSkorSkriningNutrisi <= 1) {
      this.ketSkriningNutrisi = "Tidak Berisiko";
    }
    else {
      this.ketSkriningNutrisi = "Berisiko";
    }
    // console.log(this.ketSkriningNutrisi);
  }

  // Radio onChange Diagnosa
  onChangeDiagnosa(value) {
    this.nilaiDiagnosa = +value;
    this.totalSkorSkriningNutrisi = this.nilaiPenurunan + this.nilaiPenurunanBB + this.nilaiAsupan + this.nilaiDiagnosa;
    if (this.totalSkorSkriningNutrisi <= 1) {
      this.ketSkriningNutrisi = "Tidak Berisiko";
    }
    else {
      this.ketSkriningNutrisi = "Berisiko";
    }
    // console.log(this.ketSkriningNutrisi);
  }

  onSelectLain(isChecked: boolean) {
    isChecked ? this.FormRJ.controls["NyeriHilangLain"].enable() : this.FormRJ.controls["NyeriHilangLain"].disable();
    if (isChecked == false) {
      this.FormRJ.controls["NyeriHilangLain"].reset();
    }
  }

  // onChange radio Status Alergi
  onChangeEnableAlergi(value) {
    this.statusAlergi = value;
    this.FormRJ.controls["sebutkanStatusAlergi"].enable();
  }
  onChangeDisableAlergi(value) {
    this.statusAlergi = value;
    this.FormRJ.controls["sebutkanStatusAlergi"].disable();
  }

  // Masalah Keperawatan
  /** Penurunan Kesadaran */
  onSelectPenurunanKesadaran(isChecked: boolean) {
    isChecked ? this.FormRJ.controls["MkevaluasiPenurunanKesadaranGcs"].enable() : this.FormRJ.controls["MkevaluasiPenurunanKesadaranGcs"].disable();
    if (isChecked == false) {
      this.FormRJ.controls["MkevaluasiPenurunanKesadaranGcs"].reset();
    }
  }
  /** Kejang */
  onSelectKejang(isChecked: boolean) {
    isChecked ? this.FormRJ.controls["MkevaluasiKejang"].enable() : this.FormRJ.controls["MkevaluasiKejang"].disable();
    if (isChecked == false) {
      this.FormRJ.controls["MkevaluasiKejang"].reset();
    }
  }
  /** Ketidak efektifan / bersihan jalan nafas */
  onSelectKetidakEfektifan(isChecked: boolean) {
    isChecked ? this.FormRJ.controls["MkevaluasiJalanNafasRonchi"].enable() : this.FormRJ.controls["MkevaluasiJalanNafasRonchi"].disable();
    isChecked ? this.FormRJ.controls["MkevaluasiJalanNafasWheezing"].enable() : this.FormRJ.controls["MkevaluasiJalanNafasWheezing"].disable();
    isChecked ? this.FormRJ.controls["MkevaluasiJalanNafasKrekels"].enable() : this.FormRJ.controls["MkevaluasiJalanNafasKrekels"].disable();
    isChecked ? this.FormRJ.controls["MkevaluasiJalanNafasStridor"].enable() : this.FormRJ.controls["MkevaluasiJalanNafasStridor"].disable();
    if (isChecked == false) {
      this.FormRJ.controls["MkevaluasiJalanNafasRonchi"].reset();
      this.FormRJ.controls["MkevaluasiJalanNafasWheezing"].reset();
      this.FormRJ.controls["MkevaluasiJalanNafasKrekels"].reset();
      this.FormRJ.controls["MkevaluasiJalanNafasStridor"].reset();
    }
  }
  /** Sesak */
  onSelectSesak(isChecked: boolean) {
    isChecked ? this.FormRJ.controls["MkevaluasiSesakRr"].enable() : this.FormRJ.controls["MkevaluasiSesakRr"].disable();
    isChecked ? this.FormRJ.controls["MkevaluasiSesakRatraksi"].enable() : this.FormRJ.controls["MkevaluasiSesakRatraksi"].disable();
    isChecked ? this.FormRJ.controls["MkevaluasiSesakSaturasi"].enable() : this.FormRJ.controls["MkevaluasiSesakSaturasi"].disable();
    if (isChecked == false) {
      this.FormRJ.controls["MkevaluasiSesakRr"].reset();
      this.FormRJ.controls["MkevaluasiSesakRatraksi"].reset();
      this.FormRJ.controls["MkevaluasiSesakSaturasi"].reset();
    }
  }
  /** Nyeri */
  onSelectNyeri(isChecked: boolean) {
    console.log(isChecked);
    isChecked ? this.FormRJ.controls["MkevaluasiNyeri"].enable() : this.FormRJ.controls["MkevaluasiNyeri"].disable();
    if (isChecked == false) {
      this.FormRJ.controls["MkevaluasiNyeri"].reset();
    }
  }
  /** Gangguan hemodinamik */
  onSelectHemodinamik(isChecked: boolean) {
    isChecked ? this.FormRJ.controls["MkevaluasiGangguanHemodinamikTekananDarah"].enable() : this.FormRJ.controls["MkevaluasiGangguanHemodinamikTekananDarah"].disable();
    isChecked ? this.FormRJ.controls["MkevaluasiGangguanHemodinamikNadi"].enable() : this.FormRJ.controls["MkevaluasiGangguanHemodinamikNadi"].disable();
    isChecked ? this.FormRJ.controls["MkevaluasiGangguanHemodinamikAkralHangatDingin"].enable() : this.FormRJ.controls["MkevaluasiGangguanHemodinamikAkralHangatDingin"].disable();
    isChecked ? this.FormRJ.controls["MkevaluasiGangguanHemodinamikCrt"].enable() : this.FormRJ.controls["MkevaluasiGangguanHemodinamikCrt"].disable();
    if (isChecked == false) {
      this.FormRJ.controls["MkevaluasiGangguanHemodinamikTekananDarah"].reset();
      this.FormRJ.controls["MkevaluasiGangguanHemodinamikNadi"].reset();
      this.FormRJ.controls["MkevaluasiGangguanHemodinamikAkralHangatDingin"].reset();
      this.FormRJ.controls["MkevaluasiGangguanHemodinamikCrt"].reset();
    }
  }
  /** Gangguan integritas kulit */
  onSelectIntegritasKulit(isChecked: boolean) {
    isChecked ? this.FormRJ.controls["MkevaluasiGangguanIntegritasKulitLuka"].enable() : this.FormRJ.controls["MkevaluasiGangguanIntegritasKulitLuka"].disable();
    isChecked ? this.FormRJ.controls["MkevaluasiGangguanIntegritasKulitGatal"].enable() : this.FormRJ.controls["MkevaluasiGangguanIntegritasKulitGatal"].disable();
    isChecked ? this.FormRJ.controls["MkevaluasiGangguanIntegritasKulitMerah"].enable() : this.FormRJ.controls["MkevaluasiGangguanIntegritasKulitMerah"].disable();
    if (isChecked == false) {
      this.FormRJ.controls["MkevaluasiGangguanIntegritasKulitLuka"].reset();
      this.FormRJ.controls["MkevaluasiGangguanIntegritasKulitGatal"].reset();
      this.FormRJ.controls["MkevaluasiGangguanIntegritasKulitMerah"].reset();
    }
  }
  /** Gangguan keseimbangan cairan dan elektrolit */
  onSelectKeseimbanganCairan(isChecked: boolean) {
    isChecked ? this.FormRJ.controls["MkevaluasiKeseimbanganCairanPendarahan"].enable() : this.FormRJ.controls["MkevaluasiKeseimbanganCairanPendarahan"].disable();
    isChecked ? this.FormRJ.controls["MkevaluasiKeseimbanganCairanIntake"].enable() : this.FormRJ.controls["MkevaluasiKeseimbanganCairanIntake"].disable();
    isChecked ? this.FormRJ.controls["MkevaluasiKeseimbanganCairanOutput"].enable() : this.FormRJ.controls["MkevaluasiKeseimbanganCairanOutput"].disable();
    if (isChecked == false) {
      this.FormRJ.controls["MkevaluasiKeseimbanganCairanPendarahan"].reset();
      this.FormRJ.controls["MkevaluasiKeseimbanganCairanIntake"].reset();
      this.FormRJ.controls["MkevaluasiKeseimbanganCairanOutput"].reset();
    }
  }
  /** Peningkatan suhu tubuh */
  onSelectPeningkatanSuhu(isChecked: boolean) {
    isChecked ? this.FormRJ.controls["MkevaluasiSuhu"].enable() : this.FormRJ.controls["MkevaluasiSuhu"].disable();
    if (isChecked == false) {
      this.FormRJ.controls["MkevaluasiSuhu"].reset();
    }
  }
  /** Lain-lain */
  onSelectLainLain(isChecked: boolean) {
    isChecked ? this.FormRJ.controls["MasalahKeperawatanLainLain"].enable() : this.FormRJ.controls["MasalahKeperawatanLainLain"].disable();
    if (isChecked == false) {
      this.FormRJ.controls["MasalahKeperawatanLainLain"].reset();
    }
  }

  /** Pemberian Infus dan Obat */
  insertPemberianInfus(data) {
    var i: any;
    data['NoReg'] = this.FormRJ.value.NoReg;
    let pemberianInfusObat = JSON.stringify(data);
    this.service.post("api/IgdPemberianInfusObats", pemberianInfusObat)
      .subscribe(result1 => {
        i = JSON.parse(result1);
        this.loading = false;
      }, error => { this.service.errorserver(); this.loading = false; });
  }

  /** Tindakan */
  insertTindakan(data) {
    var i: any;
    data['NoReg'] = this.FormRJ.value.NoReg;
    let tindakan = JSON.stringify(data);
    this.service.post("api/IgdTindakans", tindakan)
      .subscribe(result2 => {
        console.log(result2)
        i = JSON.parse(result2);
        this.loading = false;
      }, error => { this.service.errorserver(); this.loading = false; });
  }

  /** Observasi Lanjutan */
  insertObservasiLanjutan(data) {
    var i: any;
    data['NoReg'] = this.FormRJ.value.NoReg;
    let oblanjutan = JSON.stringify(data);
    this.service.post("api/IgdObservasis", oblanjutan)
      .subscribe(result3 => {
        // console.log(result3)
        i = JSON.parse(result3);
        this.loading = false;
      }, error => { this.service.errorserver(); this.loading = false; });
  }

  insertFormRJ(data) {
    // console.log(data);
    var x: any;
    this.service.post(this._serviceUrl, data)
      .subscribe(result => {
        x = JSON.parse(result);

        /** Pmberian Infus */
        let infus_obat = this.pemberianio_arr;
        for (let i = 0; i < infus_obat.length; i++) {
          // console.log(infus_obat[i])
          this.insertPemberianInfus(infus_obat[i]);
        }

        /** Tindakan */
        let tindakan_ = this.tindakan_arr;
        for (let i = 0; i < tindakan_.length; i++) {
          // console.log(tindakan_[i]);
          this.insertTindakan(tindakan_[i]);
        }

        /** Observasi Lanjutan */
        let observasi_lanjutan = this.observasil_arr;
        for (let i = 0; i < observasi_lanjutan.length; i++) {
          // console.log(observasi_lanjutan[i])
          this.insertObservasiLanjutan(observasi_lanjutan[i]);
        }


        this.loading = false;
      }, error => { this.service.errorserver(); this.loading = false; });
  }

  // Input number
  preventNumbers(e) {
    console.log(e.keyCode);
    if (e.keyCode >= 48 && e.keyCode <= 57) {
      // we have a number
      return false;
    }
  }

  onSubmit() {

    var today = new Date();
    var date = today.getDate();
    var month = today.getMonth() + 1;
    var year = today.getFullYear();
    var current_date = year + '-' + month + '-' + date;
    // console.log(current_date);

    this.loading = true;
    this.FormRJ.value.KdJk = this.kdJK;
    this.FormRJ.value.WaktuPeriksa = this.formatdate.waktuJStoYMD(new Date());
    this.FormRJ.value.TglPeriksa = this.formatdate.tanggalJStoYMD(new Date());

    // //convert object to json
    this.FormRJ.value.Umur = +this.nilaiUmur;
    this.FormRJ.value.HasilPengkajianCideraJatuh = this.hasilPengkajian;
    this.FormRJ.value.TotalSkorSkrinningNutrisi = +this.totalSkorSkriningNutrisi;
    // this.FormRJ.value.SkalaNyeri = +this.nilaiSkalaNyeri;
    // this.FormRJ.value.SifatNyeri = this.sifatNyeri;
    if (this.onoff_skalaNyeri == true) {
      this.FormRJ.value.SkalaNyeri = +this.nilaiSkalaNyeri;
    }
    else {
      this.FormRJ.value.SkalaNyeri = '';
    }
    if (this.onoff_skalaNyeri == true) {
      this.FormRJ.value.SifatNyeri = this.sifatNyeri;
    }
    else {
      this.FormRJ.value.SifatNyeri = '';
    }
    this.FormRJ.value.OnOffSkalaNyeri = this.onoff_skalaNyeri;
    this.FormRJ.value.HasilSkrinningNutrisi = this.ketSkriningNutrisi;
    this.FormRJ.value.NipPerawat = this.nip_perawat;
    this.FormRJ.value.NamaPerawat = this.nama_perawat;

    // let data = JSON.stringify(this.FormRJ.value);
    // // console.log(data);

    // // post action
    // this.service.httpClientPost(this._serviceUrl, data);
    // setTimeout(() => {
    //   //redirect
    //   this.router.navigate(['/dashboard-listigd']);
    //   this.loading = false;
    // }, 2500)


    //UNTUK DAPATEIN NOREG
    var x: any;
    var noreg;
    this.service.get('api/IgdRj07PerawatPengkajianAwal/GetNoReg/' + current_date, x)
      .subscribe(result => {
        x = JSON.parse(result);
        this.loading = true;
        // console.log(x[0].NoReg);
        this.FormRJ.value.NoReg = x[0].NoReg;
        // console.log(x);
        let data = JSON.stringify(this.FormRJ.value);
        console.log(data);
        this.insertFormRJ(data);
      },
        error => {
          this.service.errorserver();
          this.loading = false;
        }
      );

    setTimeout(() => {
      swal(
        'Sukses!',
        'Data Berhasil Disimpan',
        'success'
      );
      //redirect
      this.router.navigate(['/dashboard-listigd']);
      this.loading = false;
    }, 2500)
  }

}
