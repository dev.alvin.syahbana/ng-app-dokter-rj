import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RJ07EIMComponent } from './RJ.07-eim.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { AmazingTimePickerModule } from 'amazing-time-picker'; // this line you need
import { Ng2CompleterModule } from "ng2-completer";
import { DatePipe } from '@angular/common';

export const ProfileEIMRoutes: Routes = [
  {
    path: '',
    component: RJ07EIMComponent,
    data: {
      breadcrumb: 'eim.profile.my_profile.my_profile',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProfileEIMRoutes),
    SharedModule,
    LoadingModule,
    AmazingTimePickerModule,
    Ng2CompleterModule
  ],
  declarations: [RJ07EIMComponent],
  providers: [AppService, AppFormatDate, DatePipe]
})
export class ProfileEIMModule { }
