import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangeEmailComponent } from './change-email-eim.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";


export const ChangeEmailEIMRoutes: Routes = [
    {
        path: '',
        component: ChangeEmailComponent,
        data: {
            breadcrumb: 'eim.profile.change_email.change_email',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ChangeEmailEIMRoutes),
        SharedModule
    ],
    declarations: [ChangeEmailComponent],
    providers: [AppService]
})
export class ChangeEmailEIMModule { }
