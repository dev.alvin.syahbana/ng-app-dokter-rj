import {Routes} from '@angular/router';

export const EIMRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard-eim.module#DashboardEIMModule',
        data: {
          breadcrumb: 'Dashboard'
        }
      }, 
      {
        path: 'profile',
        loadChildren: './profile/profile-eim.module#ProfileEIMModule',
        data: {
          breadcrumb: 'My Profile'
        }
      }, 
      {
        path: 'contact',
        loadChildren: './contact/contact-eim.module#ContactEIMModule',
        data: {
          breadcrumb: 'Add Contact'
        }
      }, 
      {
        path: 'aci',
        loadChildren: './aci/aci-eim.module#ACIEIMModule',
        data: {
          breadcrumb: 'ACI Application Search'
        }
      }, 
      {
        path: 'sms',
        loadChildren: './sms/sms-eim.module#SMSEIMModule',
        data: {
          breadcrumb: 'SMS Configuration'
        }
      }, 
      {
        path: 'audit',
        loadChildren: './audit/audit-eim.module#AuditEIMModule',
        data: {
          breadcrumb: 'Audit Report'
        }
      }
    ]
  }
];


