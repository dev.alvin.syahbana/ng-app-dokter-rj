import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl, Validators, NgControl } from "@angular/forms";
import * as XLSX from 'xlsx';

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
    constructor(private datepipe: DatePipe) { }
    transform(date: string): any {
        return this.datepipe.transform(new Date(date.substr(0, 10)), 'dd-MM-yyyy');
    }
}

@Pipe({ name: 'dataFilterActivities' })
export class DataFilterActivitiesPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.idRj0701.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.noRekMedis.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.waktuPeriksa.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.poli.toLowerCase().indexOf(query.toLowerCase()) > -1)
            );
        }
        return array;
    }
}

@Pipe({ name: 'dataFilterStatus' })
export class DataFilterStatusPipe {
    transform(array: any[], query: string): any {
        if (query == "Active") {
            return _.filter(array, row =>
                (row.SurveyTaken.indexOf("0") > -1) &&
                (row.activesurveylink.indexOf("Active") > -1));
        }
        else if (query == "Expired" || query == "Pending") {
            return _.filter(array, row =>
                (row.activesurveylink.indexOf(query) > -1));
        } else if (query == "Completed") {
            return _.filter(array, row =>
                (row.SurveyTaken.indexOf("1") > -1));
        }
        return array;
    }
}


@Component({
    selector: 'app-sensus-harian-IGD-eim',
    templateUrl: './sensus-harian-IGD-eim.component.html',
    styleUrls: [
        './sensus-harian-IGD-eim.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})


export class SensusHarianIGDEIMComponent implements OnInit {

    private _serviceUrl = '/api/IgdRj07PerawatPengkajianAwal';
    public studentcourse: any;
    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "tglPeriksa";
    public sortOrder: string = "desc";
    public useraccesdata: any;
    public filterStatus: string = "All";
    public title = 'dini';
    modelPopup1: NgbDateStruct;
    modelPopup2: NgbDateStruct;
    formFilter: FormGroup;
    public pilihanSort;
    public dataDitemukan = false;

    public loading = false;

    constructor(private _http: Http, private service: AppService, private formatdate: AppFormatDate, private session: SessionService, private router: Router, private parserFormatter: NgbDateParserFormatter) {

        let dariTanggal = new FormControl('', Validators.required);
        let sampaiTanggal = new FormControl('', Validators.required);

        this.formFilter = new FormGroup({
            dariTanggal: dariTanggal,
            sampaiTanggal: sampaiTanggal
        });

        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    ngOnInit() {

        this.getDataSensus();
    }

    getDataSensus() {
        this.loading = true;
        var data = '';
        this.service.httpClientGet(this._serviceUrl, data)
            .subscribe(result => {
                console.log(result);
                if (result == null) {
                    swal(
                        'Informasi!',
                        'Data tidak ditemukan.',
                        'error'
                    );
                    this.data = '';
                    this.loading = false;
                }
                else {
                    this.data = result;
                    this.loading = false;
                }
            });
        error => {
            this.service.errorserver();
            this.data = '';
            this.loading = false;
        }
    }

    ExportExcel() {
        // var date = this.service.formatDate();
        var fileName = "DataSensusIGD.xls";
        var ws = XLSX.utils.json_to_sheet(this.data);
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, fileName);
        XLSX.writeFile(wb, fileName);
    }

    dariTgl = "";
    onSelectDariTanggal(date: NgbDateStruct) {
        if (date != null) {
            this.formFilter.value.dariTanggal = date;
            this.dariTgl = this.formatdate.dateCalendarToYMD(this.formFilter.value.dariTanggal);
            console.log(this.dariTgl);
        }
    }

    sampaiTgl = "";
    onSelectSampaiTanggal(date: NgbDateStruct) {
        if (date != null) {
            this.formFilter.value.sampaiTanggal = date;
            this.sampaiTgl = this.formatdate.dateCalendarToYMD(this.formFilter.value.sampaiTanggal);
            console.log(this.sampaiTgl);
        }
    }

    onSubmit() {

        this.formFilter.controls['dariTanggal'].markAsTouched();
        this.formFilter.controls['sampaiTanggal'].markAsTouched();

        if (this.formFilter.valid) {

            this.loading = true;
            var data: any;
            this.service.httpClientGet("api/IgdRj07PerawatPengkajianAwal/FilterByTanggal/" + this.dariTgl + "/" + this.sampaiTgl + "", data)
                .subscribe(result => {
                    console.log(result);
                    if (result == null) {
                        swal(
                            'Informasi!',
                            'Data tidak ditemukan.',
                            'error'
                        );
                        this.data = '';
                        this.loading = false;
                    }
                    else {
                        this.data = result;
                        this.loading = false;
                    }
                });
            error => {
                this.service.errorserver();
                this.data = '';
                this.loading = false;
            }

        }
    }

}
