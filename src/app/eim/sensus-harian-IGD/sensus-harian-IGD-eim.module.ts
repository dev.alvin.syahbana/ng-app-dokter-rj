import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SensusHarianIGDEIMComponent } from './sensus-harian-IGD-eim.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { DataFilterActivitiesPipe,DataFilterStatusPipe,ConvertDatePipe } from './sensus-harian-IGD-eim.component';
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';


export const ProfileEIMRoutes: Routes = [
  {
    path: '',
    component: SensusHarianIGDEIMComponent,
    data: {
      breadcrumb: 'eim.profile.my_profile.my_profile',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(ProfileEIMRoutes),
      SharedModule,
      LoadingModule
    ],
    declarations: [SensusHarianIGDEIMComponent, DataFilterActivitiesPipe, DataFilterStatusPipe, ConvertDatePipe],
    providers: [AppService, AppFormatDate]
  })
export class ProfileEIMModule { }
