import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PBFDetailComponent } from './pemakaianbarangfarmasi-detail.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';
import { AppFormatDate } from "../../shared/format-date/app.format-date";

export const PBFDetailRoutes: Routes = [
  {
    path: '',
    component: PBFDetailComponent,
    data: {
      breadcrumb: 'eim.profile.my_profile.my_profile',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PBFDetailRoutes),
    SharedModule,
    LoadingModule    
  ],
  declarations: [PBFDetailComponent],
  providers: [AppService, AppFormatDate]
})
export class PBFDetailModule { }
