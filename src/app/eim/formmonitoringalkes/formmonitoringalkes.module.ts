import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RJ07EIMComponent } from './formmonitoringalkes.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";


export const ProfileEIMRoutes: Routes = [
  {
    path: '',
    component: RJ07EIMComponent,
    data: {
      breadcrumb: 'eim.profile.my_profile.my_profile',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProfileEIMRoutes),
    SharedModule
  ],
  declarations: [RJ07EIMComponent],
  providers: [AppService]
})
export class ProfileEIMModule { }
