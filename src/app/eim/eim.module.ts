import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EIMRoutes } from './eim.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(EIMRoutes),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: []
})

export class EIMModule {}
