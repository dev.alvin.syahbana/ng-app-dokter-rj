import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangePasswordEIMComponent } from './change-password-eim.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";


export const ChangePasswordEIMRoutes: Routes = [
  {
    path: '',
    component: ChangePasswordEIMComponent,
    data: {
      breadcrumb: 'eim.profile.change_password.change_password',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ChangePasswordEIMRoutes),
    SharedModule
  ],
  declarations: [ChangePasswordEIMComponent],
  providers: [AppService]
})
export class ChangePasswordEIMModule { }
