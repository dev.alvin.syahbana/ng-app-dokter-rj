import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SMSEIMComponent } from './sms-eim.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const SMSEIMRoutes: Routes = [
  {
    path: '',
    component: SMSEIMComponent,
    data: {
      breadcrumb: 'SMS Configuration',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SMSEIMRoutes),
    SharedModule
  ],
  declarations: [SMSEIMComponent]
})
export class SMSEIMModule { }
