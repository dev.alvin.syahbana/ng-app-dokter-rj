import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CustomValidators} from "ng2-validation";
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-sms-eim',
  templateUrl: './sms-eim.component.html',
  styleUrls: [
    './sms-eim.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class SMSEIMComponent implements OnInit {

  smsconfigform: FormGroup;
 
  constructor(private router: Router) {

  //validation
  let provider = new FormControl('', Validators.required);
  let accountsid = new FormControl('', Validators.required);
  let accountsid2 = new FormControl('', Validators.required);
  let hosturl = new FormControl('', Validators.required);
  let authtoken = new FormControl('', Validators.required);
  let authtoken2 = new FormControl('', Validators.required);
  let headercontenttype = new FormControl('', Validators.required);
  let from = new FormControl('', Validators.required);
  let from2 = new FormControl('', Validators.required);
  let maxcharlimit = new FormControl('', Validators.required);

  this.smsconfigform = new FormGroup({
    provider:provider,
    accountsid:accountsid,
    accountsid2:accountsid2,
    hosturl:hosturl,
    authtoken:authtoken,
    authtoken2:authtoken2,
    headercontenttype:headercontenttype,
    from:from,
    from2:from2,
    maxcharlimit:maxcharlimit
  });

  }

  ngOnInit() {
    setTimeout(() => {
       
  
    }, 1);
  }

  //submit form sms configuration
  submittedSMSconfig: boolean;
  onSubmitSMSconfig() {
    this.submittedSMSconfig = true;
    this.router.navigate(['/eim/sms']);
  }

  //success notification sms configuration
  openSuccessSwalSMSconfig() {
    swal({
      title: 'Success!',
      text: 'Your sms configuration has been added!',
      type: 'success'
    }).catch(swal.noop);
  }

}
