import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LKODetailComponent, ConvertDatePipe } from './lembarkendaliobat-detail.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';
import { AppFormatDate } from "../../shared/format-date/app.format-date";

export const LKODetailRoutes: Routes = [
  {
    path: '',
    component: LKODetailComponent,
    data: {
      breadcrumb: 'Detail RJ.07',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LKODetailRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [LKODetailComponent, ConvertDatePipe],
  providers: [AppService, AppFormatDate]
})
export class LKODetailModule { }