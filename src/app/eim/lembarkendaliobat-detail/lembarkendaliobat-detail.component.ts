import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
declare var require: any;
import '../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { AppService } from "../../shared/service/app.service";
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from '../../shared/service/session.service';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
	constructor(private datepipe: DatePipe) { }
	transform(date: string): any {
		return this.datepipe.transform(new Date(), 'dd-mm-yyyy');
	}
}

@Component({
	selector: 'app-lembarkendaliobat-detail',
	templateUrl: './lembarkendaliobat-detail.component.html',
	styleUrls: [
		'./lembarkendaliobat-detail.component.css',
		'../../../../node_modules/c3/c3.min.css',
	],
	encapsulation: ViewEncapsulation.None
})

export class LKODetailComponent implements OnInit {

	private _serviceUrl = '/api/LembarKendaliObats';
	public data: any;
	public dataObat: any;
	public dataTindakan: any;
	public dataObservasi: any;
	public loading = false;
	// Obat
	public rowsOnPageInfus: number = 10;
	public sortByInfus: string = "Tanggal";
	public sortOrderInfus: string = "asc";


	constructor(private http: Http, private _http: HttpClient, private formatdate: AppFormatDate, private service: AppService,
		private route: ActivatedRoute, private router: Router, private session: SessionService) {

	}

	ngOnInit() {
		this.getdata();
	}

	id: string;
	noreg: string;
	getdata() {
		this.loading = true;
		this.id = this.route.snapshot.params['id'];
		var data = '';
		this.service.httpClientGet(this._serviceUrl + "/" + this.id, data)
			.subscribe(result => {
				if (result == "Not found") {
					this.service.notfound();
					this.data = '';
					this.loading = false;
				}
				else {
					this.data = result;
					console.log(this.data);
					this.noreg = this.data.noReg;

					// api untuk pemberian infus
					// console.log("api/IgdPemberianInfusObats/FilterByNoReg/" + this.noreg, data)
					this.service.httpClientGet("api/LembarKendaliObatDetails/FilterObatByNoReg/" + this.noreg, data)
						.subscribe(result1 => {
							this.dataObat = result1;
							// console.log(this.dataObat);
							this.loading = false;
						},
							error => {
								this.service.errorserver();
								this.dataObat = '';
								this.loading = false;
							});

					
					this.loading = false;
				}
			},
				error => {
					this.service.errorserver();
					this.data = '';
					this.loading = false;
				});

	}
}