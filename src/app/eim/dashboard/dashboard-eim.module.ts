import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardEIMComponent } from './dashboard-eim.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const DashboardEIMRoutes: Routes = [
  {
    path: '',
    component: DashboardEIMComponent,
    data: {
      breadcrumb: 'Dashboard',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DashboardEIMRoutes),
    SharedModule
  ],
  declarations: [DashboardEIMComponent]
})
export class DashboardEIMModule { }
