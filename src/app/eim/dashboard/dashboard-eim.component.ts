import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';

@Component({
  selector: 'app-dashboard-eim',
  templateUrl: './dashboard-eim.component.html',
  styleUrls: [
    './dashboard-eim.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class DashboardEIMComponent implements OnInit {

  content = ` Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at,`;
  timeline = [
    { caption: '16 Jan', date: new Date('2014, 1, 16'), selected: true, title: 'Horizontal Timeline', content: this.content },
    { caption: '28 Feb', date: new Date('2014, 2, 28'), title: 'Event title here', content: this.content },
    { caption: '20 Mar', date: new Date('2014, 3, 20'), title: 'Event title here', content: this.content },
    { caption: '20 May', date: new Date('2014, 5, 20'), title: 'Event title here', content: this.content },
    { caption: '09 Jul', date: new Date('2014, 7, 9'), title: 'Event title here', content: this.content },
    { caption: '30 Aug', date: new Date('2014, 8, 30'), title: 'Event title here', content: this.content },
    { caption: '15 Sep', date: new Date('2014, 9, 15'), title: 'Event title here', content: this.content },
    { caption: '01 Nov', date: new Date('2014, 11, 1'), title: 'Event title here', content: this.content },
    { caption: '10 Dec', date: new Date('2014, 12, 10'), title: 'Event title here', content: this.content },
    { caption: '29 Jan', date: new Date('2015, 1, 19'), title: 'Event title here', content: this.content },
    { caption: '3 Mar', date: new Date('2015,  3,  3'), title: 'Event title here', content: this.content },
  ];

  donutChartData =  {
    chartType: 'PieChart',
    dataTable: [
      ['Task', 'Hours per Day'],
      ['Work', 11],
      ['Eat', 2],
      ['Commute', 2],
      ['Watch TV', 2],
      ['Sleep', 7]
    ],
    options: {
      height: 320,
      title: 'My Daily Activities',
      pieHole: 0.4,
      colors: ['#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f']
    },
  };
 
  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      const chart = c3.generate({
        bindto: '#chart4',
        data: {
          columns: [
            ['data1', 30, 20, 50, 40, 60, 50, 30, 20, 50, 40, 60, 50],
            ['data2', 200, 130, 90, 240, 130, 220, 200, 130, 90, 240, 130, 220], 
          ],
          xkey: ['Jan 2017', 'Feb 2017', 'Mar 2017','Jan 2017', 'Feb 2017', 'Mar 2017','Jan 2017', 'Feb 2017', 'Mar 2017','Jan 2017', 'Feb 2017', 'Mar 2017'],
          type: 'bar',
          colors: {
            data1: '#03A9F3',
            data2: '#FEC107', 
          }, 
          groups: [
            ['data1', 'data2']
          ]
        }
      });

      $('.resource-barchart1').sparkline([5, 6, 9, 7, 8, 4, 6], {
        type: 'bar',
        barWidth: '6px',
        height: '32px',
        barColor: '#1abc9c',
        tooltipClassname: 'abc'
      });

      $('.resource-barchart2').sparkline([6, 4, 8, 7, 9, 6, 5], {
        type: 'bar',
        barWidth: '6px',
        height: '32px',
        barColor: '#1abc9c',
        tooltipClassname: 'abc'
      });
  
    }, 1);
  }

}
