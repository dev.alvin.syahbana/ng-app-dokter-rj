import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
declare var require: any;
import '../../../assets/echart/echarts-all.js';
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { AppService } from "../../shared/service/app.service";
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from '../../shared/service/session.service';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { CompleterService, CompleterData, RemoteData } from 'ng2-completer';
import { DatePipe } from '@angular/common';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";



// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
	constructor(private datepipe: DatePipe) { }
	transform(date: string): any {
		return this.datepipe.transform(new Date(), 'dd-mm-yyyy');
	}
}

@Component({
  selector: 'app-pemakaian-barang-farmasi-eim',
  templateUrl: './pemakaian-barang-farmasi-eim.component.html',
  styleUrls: [
    './pemakaian-barang-farmasi-eim.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class PBFEIMComponent implements OnInit {

  private _serviceUrl = '/api/IgdRj07PerawatPengkajianAwal';
  public data: any;
  FormRJ: FormGroup;
  public useraccesdata: any;
  public loading = false;

  // Data Complete
  protected dataKomplit: CompleterData;

  today: number = Date.now();

  private fieldArray: Array<any> = [];
  private newAttribute: any = {};
  private fieldArrayTindakan: Array<any> = [];
  private newAttributeTindakan: any = {};
  private fieldArrayObervasi: Array<any> = [];
  private newAttributeObervasi: any = {};

  public inValidFormRJ07: Boolean = false;

  // Completer
  protected daftarBarang: string = "";
  protected searchPasien_arr = [];
  textSearching: string = "";

  private lastNoReg;
  kurir = [];
  PBFDetail_arr = [];


  // Pemeriksaan Dokter
  statusAlergi: string = "";
  dataService: any;

  constructor(private http: Http, private _http: HttpClient, private formatdate: AppFormatDate, private service: AppService, private route: ActivatedRoute,
    private router: Router, private session: SessionService, private completerService: CompleterService, private datePipe: DatePipe) {

    // this.dataService = completerService.remote(
    //   null,
    //   "ListSearch",
    //   "ListSearch");
    // this.dataService.urlFormater(term => {
    //   return `api/PemakaianBarangFarmasiDetails/GetBrgFarmasiBy/` + term; 
    // });
    // this.dataService;
    // console.log(this.dataService);


    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    //validation
    let NoReg = new FormControl('');
    let KdRm = new FormControl('');
    let Nama = new FormControl('');
    let Umur = new FormControl('');
    let KdDokter = new FormControl('');
    let NmDokter = new FormControl('');
    let Diagnosa = new FormControl('');
    let Tindakan = new FormControl('');
    let Tanggal = new FormControl('');
    let KdDokter1 = new FormControl('');
    let NmDokter1 = new FormControl('');
    let NipPerawat = new FormControl('');
    let NamaPerawat = new FormControl('');
    let Keterangan = new FormControl('IGD');
    // PBF
    let KodeBarangFarmasi = new FormControl('');
    let NamaBarang = new FormControl('');
    let Jumlah = new FormControl('');

    this.FormRJ = new FormGroup({
      NoReg: NoReg,
      KdRm: KdRm,
      Nama: Nama,
      Umur: Umur,
      KdDokter: KdDokter,
      NmDokter: NmDokter,
      Diagnosa: Diagnosa,
      Tindakan: Tindakan,
      Tanggal: Tanggal,
      KdDokter1: KdDokter1,
      NmDokter1: NmDokter1,
      NipPerawat: NipPerawat,
      NamaPerawat: NamaPerawat,
      Keterangan: Keterangan,
      // Pemberian Infus
      KodeBarangFarmasi: KodeBarangFarmasi,
      NamaBarang: NamaBarang,
      Jumlah: Jumlah,
    });
  }

  ngOnInit() {
    this.getDataPasien();
    // Disable pada checkbox Nyeri Hilang
  }

  id: string;
  noreg: string;
  kdrm: string;
  nama: string;
  getDataPasien(){
    this.loading = true;
		this.id = this.route.snapshot.params['id'];
		var data = '';
		this.service.httpClientGet(this._serviceUrl + "/" + this.id, data)
			.subscribe(result => {
				if (result == "Not found") {
					this.service.notfound();
					this.data = '';
					this.loading = false;
				}
				else {
					this.data = result;
					console.log(this.data);
          this.noreg = this.data.noReg;
          this.kdrm = this.data.kdRm;
          this.nama = this.data.nama;
          this.loading = false;
				}
			},
				error => {
					this.service.errorserver();
					this.data = '';
					this.loading = false;
        });
        

  }

  barangFarmasi: any = [];
  addFieldPBF() {
    this.PBFDetail_arr.push({
      'noReg': this.barangFarmasi["NoReg"],
      'kodeBarangFarmasi': this.barangFarmasi["KodeBarangFarmasi"],
      'namaBarang': this.barangFarmasi["NamaBarang"],
      'jumlah': this.barangFarmasi["Jumlah"]
    })
    // console.log(this.PBFDetail_arr)
    this.barangFarmasi = [];
  }
  deleteFieldPBF(index) {
    this.PBFDetail_arr.splice(index, 1);
    // console.log(this.PBFDetail_arr);
  }
  /** LKO Detail */

  onSelect(value) {
    // console.log(value)
  }

  tes(value){
    console.log(value)
  }
  
  onSelected(value) {
    console.log(value);
    
    // let data = value["originalObject"]

    

    // //validation 
    // let KodeBarangFarmasi = new FormControl(data.kodeBarangFarmasi);
    // let NamaBarang = new FormControl(data.namaBarang);
    
  }

  /** Pemberian Infus dan Obat */
  insertDetailPBF(data) {
    var i: any;
    data['NoReg'] = this.FormRJ.value.NoReg;
    let PBF = JSON.stringify(data);
    this.service.post("api/PemakaianBarangFarmasiDetails", PBF)
      .subscribe(result1 => {
        i = JSON.parse(result1);
        this.loading = false;
      }, error => { this.service.errorserver(); this.loading = false; });
  }

  insertFormRJ(data) {
    var x: any;
    this.service.post("api/PemakaianBarangFarmasis", data)
      .subscribe(result => {
        x = JSON.parse(result);

        /** Pmberian Infus */
        let LKO = this.PBFDetail_arr;
        for (let i = 0; i < LKO.length; i++) {
          //  console.log(LKO[i])
          this.insertDetailPBF(LKO[i]);
        }

        this.loading = false;
      }, error => { this.service.errorserver(); this.loading = false; });
  }

  onSubmit() {

    if (this.FormRJ.valid) {

      this.FormRJ.value.NoReg = this.noreg;
      this.FormRJ.value.KdRm = this.kdrm;
      this.FormRJ.value.Nama = this.nama;

      this.loading = true;
      // this.FormRJ.value.KdJk = this.kdJK;
      this.FormRJ.value.Tanggal = this.formatdate.dateJStoYMD(new Date());
    

      // //convert object to json
      // this.FormRJ.value.Umur = +this.nilaiUmur;
      // this.FormRJ.value.TotalSkorSkrinningNutrisi = +this.totalSkorSkriningNutrisi;
      // this.FormRJ.value.SkalaNyeri = +this.nilaiSkalaNyeri;
      // this.FormRJ.value.SifatNyeri = this.sifatNyeri;
      // this.FormRJ.value.HasilSkrinningNutrisi = this.ketSkriningNutrisi;
      // let data = JSON.stringify(this.FormRJ.value);
      // // console.log(data);

      // // post action
      // this.service.httpClientPost(this._serviceUrl, data);
      // setTimeout(() => {
      //   //redirect
      //   this.router.navigate(['/dashboard-listigd']);
      //   this.loading = false;
      // }, 2500)

      let data = JSON.stringify(this.FormRJ.value);
      this.insertFormRJ(data);
      setTimeout(() => {
        //redirect
        this.router.navigate(['/listpemakaianbarangfarmasi']);
        this.loading = false;
      }, 2500)
    }
  }


}
