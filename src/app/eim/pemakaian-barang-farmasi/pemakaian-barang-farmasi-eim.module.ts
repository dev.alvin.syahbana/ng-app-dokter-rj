import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PBFEIMComponent, ConvertDatePipe} from './pemakaian-barang-farmasi-eim.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { Ng2CompleterModule } from "ng2-completer";

export const PBFEIMRoutes: Routes = [
  {
    path: '',
    component: PBFEIMComponent,
    data: {
      breadcrumb: 'eim.profile.my_profile.my_profile',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PBFEIMRoutes),
    SharedModule,
    LoadingModule ,
    Ng2CompleterModule   
  ],
  declarations: [PBFEIMComponent, ConvertDatePipe],
  providers: [AppService, AppFormatDate]
})
export class PBFEIMModule { }
