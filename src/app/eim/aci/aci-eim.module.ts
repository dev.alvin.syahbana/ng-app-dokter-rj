import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ACIEIMComponent } from './aci-eim.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const ACIEIMRoutes: Routes = [
  {
    path: '',
    component: ACIEIMComponent,
    data: {
      breadcrumb: 'ACI Application Search',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ACIEIMRoutes),
    SharedModule
  ],
  declarations: [ACIEIMComponent]
})
export class ACIEIMModule { }
