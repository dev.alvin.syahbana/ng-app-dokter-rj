import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';

const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
    !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
                    ? false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
    !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
                    ? false : one.day > two.day : one.month > two.month : one.year > two.year;



const now = new Date();

@Component({
  selector: 'app-audit-eim',
  templateUrl: './audit-eim.component.html',
  styleUrls: [
    './audit-eim.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class AuditEIMComponent implements OnInit {

   
 
  constructor() { }

  ngOnInit() {
    setTimeout(() => {
       
    }, 1);
  }

  modelPopup1: NgbDateStruct;  
  modelPopup2: NgbDateStruct; 

}
