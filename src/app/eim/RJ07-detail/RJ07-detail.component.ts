import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
declare var require: any;
import '../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { AppService } from "../../shared/service/app.service";
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from '../../shared/service/session.service';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
	constructor(private datepipe: DatePipe) { }
	transform(date: string): any {
		return this.datepipe.transform(new Date(), 'dd-mm-yyyy');
	}
}

@Component({
	selector: 'app-RJ07-detail',
	templateUrl: './RJ07-detail.component.html',
	styleUrls: [
		'./RJ07-detail.component.css',
		'../../../../node_modules/c3/c3.min.css',
	],
	encapsulation: ViewEncapsulation.None
})

export class RJ07DetailComponent implements OnInit {

	private _serviceUrl = '/api/IgdRj07PerawatPengkajianAwal';
	public data: any;
	public dataInfus: any;
	public dataTindakan: any;
	public dataObservasi: any;
	public loading = false;
	// Infus
	public rowsOnPageInfus: number = 10;
	public sortByInfus: string = "waktuPemberianInfusObat";
	public sortOrderInfus: string = "asc";
	// Tindakan
	public rowsOnPageTindakan: number = 10;
	public sortByTindakan: string = "waktuTindakan";
	public sortOrderTindakan: string = "asc";
	// Tindakan
	public rowsOnPageObservasi: number = 10;
	public sortByObservasi: string = "waktuObervasi";
	public sortOrderObservasi: string = "asc";

	today: number = Date.now();
	Form: FormGroup;
	pemberianio_arr = [];
	tindakan_arr = [];
	observasil_arr = [];

	constructor(private http: Http, private _http: HttpClient, private formatdate: AppFormatDate, private service: AppService,
		private route: ActivatedRoute, private router: Router, private session: SessionService) {


		let NoReg = new FormControl('');
		// Pemberian Infus
		let WaktuPI = new FormControl('');
		let NamaAlatPI = new FormControl('');
		let DosisPI = new FormControl('');
		let RutePI = new FormControl('');
		// Tindakan
		let WaktuT = new FormControl('');
		let TindakanT = new FormControl('');
		// Observasi Lanjutan
		let WaktuOL = new FormControl('');
		let KesadaranOL = new FormControl('');
		let TekananDarahOL = new FormControl('');
		let NadiOL = new FormControl('');
		let PernafasanOL = new FormControl('');
		let SuhuOL = new FormControl('');

		this.Form = new FormGroup({
			NoReg: NoReg,
			WaktuPI: WaktuPI,
			NamaAlatPI: NamaAlatPI,
			DosisPI: DosisPI,
			RutePI: RutePI,
			WaktuT: WaktuT,
			TindakanT: TindakanT,
			WaktuOL: WaktuOL,
			KesadaranOL: KesadaranOL,
			TekananDarahOL: TekananDarahOL,
			NadiOL: NadiOL,
			PernafasanOL: PernafasanOL,
			SuhuOL: SuhuOL
		})
	}

	ngOnInit() {
		this.getdata();
	}

	id: string;
	noreg: string;
	getdata() {
		this.loading = true;
		this.id = this.route.snapshot.params['id'];
		var data = '';
		this.service.httpClientGet(this._serviceUrl + "/" + this.id, data)
			.subscribe(result => {
				if (result == "Not found") {
					this.service.notfound();
					this.data = '';
					this.loading = false;
				}
				else {
					this.data = result;
					console.log(this.data);
					this.noreg = this.data.noReg;

					// api untuk pemberian infus
					// console.log("api/IgdPemberianInfusObats/FilterByNoReg/" + this.noreg, data)
					this.service.httpClientGet("api/IgdPemberianInfusObats/FilterInfusByNoReg/" + this.noreg, data)
						.subscribe(result1 => {
							this.dataInfus = result1;
							// console.log(this.dataInfus);
							this.loading = false;
						},
							error => {
								this.service.errorserver();
								this.dataInfus = '';
								this.loading = false;
							});

					// api untuk tindakan
					// console.log("api/IgdTindakans/FilterTindakanByNoReg/" + this.noreg, data)
					this.service.httpClientGet("api/IgdTindakans/FilterTindakanByNoReg/" + this.noreg, data)
						.subscribe(result2 => {
							this.dataTindakan = result2;
							// console.log(this.dataTindakan);
							this.loading = false;
						},
							error => {
								this.service.errorserver();
								this.dataTindakan = '';
								this.loading = false;
							});

					// api untuk tindakan
					// console.log("api/IgdObservasis/FilterObservasiByNoReg/" + this.noreg, data)
					this.service.httpClientGet("api/IgdObservasis/FilterObservasiByNoReg/" + this.noreg, data)
						.subscribe(result3 => {
							this.dataObservasi = result3;
							// console.log(this.dataObservasi);
							this.loading = false;
						},
							error => {
								this.service.errorserver();
								this.dataObservasi = '';
								this.loading = false;
							});

					this.loading = false;
				}
			},
				error => {
					this.service.errorserver();
					this.data = '';
					this.loading = false;
				});

	}

	/** Pemberian Infus */
	infus: any = [];
	addFieldPIO() {
		this.pemberianio_arr.push({
			'NoReg': this.infus["NoReg"],
			'WaktuPemberianInfusObat': this.infus["WaktuPI"],
			'NamaAlat': this.infus["NamaAlatPI"],
			'Dosis': this.infus["DosisPI"],
			'Rute': this.infus["RutePI"]
		})
		// console.log(this.pemberianio_arr);
		this.infus = [];
	}
	deleteFieldPIO(index) {
		this.pemberianio_arr.splice(index, 1);
		console.log(this.pemberianio_arr);
	}
	/** Pemberian Infus */

	/** Tindakan */
	tindakan: any = [];
	addFieldTindakan() {
		this.tindakan_arr.push({
			'NoReg': this.tindakan["NoReg"],
			'WaktuTindakan': this.tindakan["WaktuT"],
			'Tindakan': this.tindakan["TindakanT"]
		})
		// console.log(this.tindakan_arr);
		this.tindakan = [];
	}
	deleteFieldTindakan(index) {
		this.tindakan_arr.splice(index, 1);
		// console.log(this.tindakan_arr);
	}
	/** Tindakan */

	/** Obervasi Lanjutan */
	observasi: any = [];
	addFieldObervasi() {
		this.observasil_arr.push({
			'NoReg': this.observasi["NoReg"],
			'WaktuObervasi': this.observasi["WaktuOL"],
			'Kesadaran': this.observasi["KesadaranOL"],
			'TekananDarah': this.observasi["TekananDarahOL"],
			'Nadi': this.observasi["NadiOL"],
			'Pernafasan': this.observasi["PernafasanOL"],
			'Suhu': this.observasi["SuhuOL"]
		})
		// console.log(this.observasil_arr);
		this.observasi = [];
	}
	deleteFieldObervasi(index) {
		this.observasil_arr.splice(index, 1);
		// console.log(this.observasil_arr);
	}

	/** Pemberian Infus dan Obat */
	insertPemberianInfus(data) {
		var i: any;
		data['NoReg'] = this.data.noReg;
		let pemberianInfusObat = JSON.stringify(data);
		this.service.post("api/IgdPemberianInfusObats", pemberianInfusObat)
			.subscribe(result1 => {
				i = JSON.parse(result1);
				this.loading = false;
			}, error => { this.service.errorserver(); this.loading = false; });
	}

	/** Tindakan */
	insertTindakan(data) {
		var i: any;
		data['NoReg'] = this.data.noReg;
		let tindakan = JSON.stringify(data);
		this.service.post("api/IgdTindakans", tindakan)
			.subscribe(result2 => {
				// console.log(result2);
				i = JSON.parse(result2);
				this.loading = false;
			}, error => { this.service.errorserver(); this.loading = false; });
	}

	/** Observasi Lanjutan */
	insertObservasiLanjutan(data) {
		var i: any;
		data['NoReg'] = this.data.noReg;
		let oblanjutan = JSON.stringify(data);
		this.service.post("api/IgdObservasis", oblanjutan)
			.subscribe(result3 => {
				// console.log(result3)
				i = JSON.parse(result3);
				this.loading = false;
			}, error => { this.service.errorserver(); this.loading = false; });
	}

	insertForm(data) {
		/** Pmberian Infus */
		let infus_obat = this.pemberianio_arr;
		for (let i = 0; i < infus_obat.length; i++) {
			// console.log(infus_obat[i])
			this.insertPemberianInfus(infus_obat[i]);
		}

		/** Tindakan */
		let tindakan_ = this.tindakan_arr;
		for (let i = 0; i < tindakan_.length; i++) {
			// console.log(tindakan_[i]);
			this.insertTindakan(tindakan_[i]);
		}

		/** Observasi Lanjutan */
		let observasi_lanjutan = this.observasil_arr;
		for (let i = 0; i < observasi_lanjutan.length; i++) {
			// console.log(observasi_lanjutan[i])
			this.insertObservasiLanjutan(observasi_lanjutan[i]);
		}
	}

	onSubmit() {
		this.loading = true;
		let data = JSON.stringify(this.Form.value);
		// console.log(data);
		this.insertForm(data);

		setTimeout(() => {
			// swal(
			// 	'Sukses!',
			// 	'Data Berhasil Disimpan',
			// 	'success'
			// );
			//redirect
			// this.router.navigate(['/dashboard-listigd']);
			location.reload();
			this.loading = false;
		}, 2000)
	}

}