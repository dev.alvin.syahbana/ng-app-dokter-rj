import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RJ07DetailComponent, ConvertDatePipe } from './RJ07-detail.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { AmazingTimePickerModule } from 'amazing-time-picker'; // this line you need

export const RJ07DetailRoutes: Routes = [
  {
    path: '',
    component: RJ07DetailComponent,
    data: {
      breadcrumb: 'Detail RJ.07',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(RJ07DetailRoutes),
    SharedModule,
    LoadingModule,
    AmazingTimePickerModule
  ],
  declarations: [RJ07DetailComponent, ConvertDatePipe],
  providers: [AppService, AppFormatDate]
})
export class RJ07DetailModule { }