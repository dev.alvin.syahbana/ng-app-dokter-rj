import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
declare var require: any;
import '../../../assets/echart/echarts-all.js';
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { AppService } from "../../shared/service/app.service";
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from '../../shared/service/session.service';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as _ from "lodash";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { Pipe, PipeTransform } from "@angular/core";
import { CompleterService, CompleterData, RemoteData } from 'ng2-completer';
import { DatePipe } from '@angular/common';

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
	constructor(private datepipe: DatePipe) { }
	transform(date: string): any {
		return this.datepipe.transform(new Date(), 'dd-mm-yyyy');
	}
}

@Component({
  selector: 'app-lembar-kendali-obat-eim',
  templateUrl: './lembar-kendali-obat-eim.component.html',
  styleUrls: [
    './lembar-kendali-obat-eim.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class LKOEIMComponent implements OnInit {

  // private _serviceUrl = '/api/LembarKendaliObats';
  private _serviceUrl = '/api/IgdRj07PerawatPengkajianAwal';
  public data: any;
  FormRJ: FormGroup;
  public useraccesdata: any;
  public loading = false;

  today: number = Date.now();

  private fieldArrayTindakan: Array<any> = [];
  private newAttributeTindakan: any = {};

  private fieldArrayObervasi: Array<any> = [];
  private newAttributeObervasi: any = {};


  public inValidFormRJ07: Boolean = false;

  private lastNoReg;
  LKODetail_arr = [];
  // dropdownListObat = [];
  // selectedItemsObat = [];
  // dropdownSettingsObat = {};
  dataService: any;

   // Data Complete
   protected dataKomplit: CompleterData;

   // Completer
  protected pasienTerdaftar: string = "";
  protected searchPasien_arr = [];
  textSearching: string = "";



  constructor(private http: Http, private _http: HttpClient, private formatdate: AppFormatDate, private service: AppService, private route: ActivatedRoute,
    private router: Router, private session: SessionService, private completerService: CompleterService, private datePipe: DatePipe) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    // this.dataService = completerService.remote(
    //   null,
    //   "ListSearch",
    //   "ListSearch");
    // this.dataService.urlFormater(term => {
    //   return `api/LembarKendaliObatDetails/GetObatBy/` + term; 
    // });
    // this.dataService;
    // console.log(this.dataService);

    //validation
    let NoReg = new FormControl('');
    let KdRm = new FormControl('');
    let Nama = new FormControl('');
    let KdKamar = new FormControl('');
    let KamarPerawatan = new FormControl('');
    let KdKelas = new FormControl('');
    let KelasPerawatan = new FormControl('');
    let KdDokter = new FormControl('');
    let NmDokter = new FormControl('');
    let KonsulDokter = new FormControl('');
    let Waktu = new FormControl('');
    let NipPerawat = new FormControl('');
    let NamaPerawat = new FormControl('');
    let Keterangan = new FormControl('IGD');
    // Detail LKO
    let WaktuLKO = new FormControl('');
    let RuanganLKO = new FormControl('');
    let KodeObat = new FormControl('');
    let NamaObat = new FormControl('');
    let JumlahObat = new FormControl('');
    let HargaObat = new FormControl('');


    this.FormRJ = new FormGroup({
      NoReg: NoReg,
      KdRm: KdRm,
      Nama: Nama,
      KdKamar: KdKamar,
      KamarPerawatan: KamarPerawatan,
      KdKelas: KdKelas,
      KelasPerawatan: KelasPerawatan,
      KdDokter: KdDokter,
      NmDokter: NmDokter,
      KonsulDokter: KonsulDokter,
      Waktu: Waktu,
      NipPerawat: NipPerawat,
      NamaPerawat: NamaPerawat,
      Keterangan: Keterangan,
      
      // Pemberian Infus
      WaktuLKO: WaktuLKO,
      RuanganLKO: RuanganLKO,
      KodeObat: KodeObat,
      NamaObat: NamaObat,
      JumlahObat: JumlahObat,
      HargaObat: HargaObat,
    

    });
  }

  ngOnInit() {
    this.getDataPasien();
    
  //   this.getDataObat();
  //   this.dropdownSettingsObat = {
  //     singleSelection: true,
  //     text: "-- Pilih --",
  //     enableSearchFilter: true,
  //     enableCheckAll: false,
  //     classes: "myclass custom-class",
  //     disabled: false,
  //     maxHeight: 120,
  //     searchAutofocus: true
  // };
  }



  
  onItemSelect(item: any) { }

  OnItemDeSelect(item: any) { }
  
  id: string;
  noreg: string;
  kdrm: string;
  nama: string;
  getDataPasien(){
    this.loading = true;
		this.id = this.route.snapshot.params['id'];
		var data = '';
		this.service.httpClientGet(this._serviceUrl + "/" + this.id, data)
			.subscribe(result => {
				if (result == "Not found") {
					this.service.notfound();
					this.data = '';
					this.loading = false;
				}
				else {
					this.data = result;
					console.log(this.data);
          this.noreg = this.data.noReg;
          this.kdrm = this.data.kdRm;
          this.nama = this.data.nama;
          this.loading = false;
				}
			},
				error => {
					this.service.errorserver();
					this.data = '';
					this.loading = false;
        });
        
               
  }


  // getDataObat(){
  //   var data: any;
  //       this.service.get('api/DaftarObats', data) 
  //           .subscribe(result => {
  //               // console.log(result);
  //               data = JSON.parse(result);
  //               if (data == "Not found") {
  //                   this.service.notfound();
  //                   this.selectedItemsObat = [];
  //               }
  //               else {
  //                   this.dropdownListObat = data.map((item) => {
  //                       return {
  //                           id: item.kodeObat,
  //                           itemName: item.namaObat
  //                       }
  //                   })
  //               }
  //           });
  //       this.selectedItemsObat = [];
  // }


  /** LKO Detail */
  obat: any = [];
  addFieldPIO() {
    this.LKODetail_arr.push({
      'noReg': this.obat["NoReg"],
      'tanggal': this.obat["WaktuLKO"],
      'ruangan': this.obat["RuanganLKO"],
      'kodeObat': this.obat["KodeObat"],
      'namaObat': this.obat["NamaObat"],
      'jumlah': this.obat["JumlahObat"],
      'harga': this.obat["HargaObat"]


      // 'NoReg': this.obat["noReg"],
      // 'WaktuLKO': this.obat["tanggal"],
      // 'RuanganLKO': this.obat["ruangan"],
      // 'NamaObat': this.obat["namaObat"],
      // 'JumlahObat': this.obat["jumlah"],
      // 'HargaObat': this.obat["harga"]
    })
    console.log(this.LKODetail_arr)
    this.obat = [];
  }
  deleteFieldPIO(index) {
    this.LKODetail_arr.splice(index, 1);
    // console.log(this.LKODetail_arr);
  }
  /** LKO Detail */

  onSelect(value) {
    // console.log(value)
  }


  /** Detail LKO */
  insertDetailLKO(data) {
    var i: any;
    data['NoReg'] = this.FormRJ.value.NoReg;
    let lembarKendaliObat = JSON.stringify(data);
    this.service.post("api/LembarKendaliObatDetails", lembarKendaliObat)
      .subscribe(result1 => {
        i = JSON.parse(result1);
        this.loading = false;
      }, error => { this.service.errorserver(); this.loading = false; });
  }

 
  insertFormRJ(data) {
    var x: any;
    this.service.post("api/LembarKendaliObats", data)
      .subscribe(result => {
        x = JSON.parse(result);

        /** Pmberian Infus */
        let LKO = this.LKODetail_arr;
        for (let i = 0; i < LKO.length; i++) {
          console.log(LKO[i])
          this.insertDetailLKO(LKO[i]);
        }

        this.loading = false;
      }, error => { this.service.errorserver(); this.loading = false; });
  }

  no_reg = "";
  nama_perawat = "";
  onSelected(value) {
    //  console.log("tess");

    
    let data = value["originalObject"]

    console.log(data);
    
    //validation 

    console.log(data.Kode_Obat);
    console.log(data.Nama_Obat);

    let KodeObat = new FormControl(data.Kode_Obat);
    let NamaObat = new FormControl(data.Nama_Obat);
    // let NoReg = new FormControl('');
    // let KdRm = new FormControl('');
    // let Nama = new FormControl('');
    // let KdKamar = new FormControl('');
    // let KamarPerawatan = new FormControl('');
    // let KdKelas = new FormControl('');
    // let KelasPerawatan = new FormControl('');
    // let KdDokter = new FormControl('');
    // let NmDokter = new FormControl('');
    // let KonsulDokter = new FormControl('');
    // let Waktu = new FormControl('');
    // let NipPerawat = new FormControl('');
    // let NamaPerawat = new FormControl('');
    // let Keterangan = new FormControl('IGD');
    // // Detail LKO
    // let WaktuLKO = new FormControl('');
    // let RuanganLKO = new FormControl('');
    // let JumlahObat = new FormControl('');
    // let HargaObat = new FormControl('');

    this.FormRJ = new FormGroup({
      KodeObat: KodeObat,
      NamaObat: NamaObat
      // NoReg: NoReg,
      // KdRm: KdRm,
      // Nama: Nama,
      // KdKamar: KdKamar,
      // KamarPerawatan: KamarPerawatan,
      // KdKelas: KdKelas,
      // KelasPerawatan: KelasPerawatan,
      // KdDokter: KdDokter,
      // NmDokter: NmDokter,
      // KonsulDokter: KonsulDokter,
      // Waktu: Waktu,
      // NipPerawat: NipPerawat,
      // NamaPerawat: NamaPerawat,
      // Keterangan: Keterangan,
      
      // // Pemberian Infus
      // WaktuLKO: WaktuLKO,
      // RuanganLKO: RuanganLKO,
      // JumlahObat: JumlahObat,
      // HargaObat: HargaObat
    });
    
  }
  
  onSubmit() {

    if (this.FormRJ.valid) {

      this.FormRJ.value.NoReg = this.noreg;
      this.FormRJ.value.KdRm = this.kdrm;
      this.FormRJ.value.Nama = this.nama;

      this.FormRJ.controls['KodeObat'].markAsTouched();
      //this.formMonObat.controls['TanggalMonitoring'].markAsTouched();
      this.FormRJ.value.Waktu = this.formatdate.waktuJStoYMD(new Date());
      this.loading = true;

      // this.FormRJ.value.KodeObat = this.selectedItemsObat[0].id;
      // this.FormRJ.value.NamaObat = this.selectedItemsObat[0].itemName;

      let data = JSON.stringify(this.FormRJ.value);
      console.log(data);
      this.insertFormRJ(data);
      setTimeout(() => {
        //redirect
        this.router.navigate(['/listlembarkendaliobat']);
        this.loading = false;
      }, 2500)
    }
  }

}
