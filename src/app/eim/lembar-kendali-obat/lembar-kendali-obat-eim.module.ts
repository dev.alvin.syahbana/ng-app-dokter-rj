import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LKOEIMComponent, ConvertDatePipe } from './lembar-kendali-obat-eim.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { Ng2CompleterModule } from "ng2-completer";

export const LKOEIMRoutes: Routes = [
  {
    path: '',
    component: LKOEIMComponent,
    data: {
      breadcrumb: 'eim.profile.my_profile.my_profile',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LKOEIMRoutes),
    SharedModule,
    LoadingModule,
    Ng2CompleterModule
  ],
  declarations: [LKOEIMComponent, ConvertDatePipe],
  providers: [AppService, AppFormatDate]
})
export class LKOEIMModule { }
