import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListPasienRIComponent, DateFormatPipe, DataFilterPipe } from './list-pasien-rawat-inap.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';
import { DatePipe } from '@angular/common';

export const ListPasienRIRoutes: Routes = [
  {
    path: '',
    component: ListPasienRIComponent,
    data: {
      breadcrumb: 'List Pasien Rawat Inap',
      icon: 'icofont-home bg-c-blue',
      status: false,
      title: 'List Pasien Rawat Inap'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ListPasienRIRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [ListPasienRIComponent, DateFormatPipe, DataFilterPipe],
  providers: [AppService]
})
export class ListPasienRIModule { }