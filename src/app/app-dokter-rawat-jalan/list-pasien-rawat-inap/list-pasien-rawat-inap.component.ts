import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import '../../../assets/echart/echarts-all.js';
import * as _ from "lodash";
import { AppService } from "../../shared/service/app.service";
import swal from 'sweetalert2';
import { SessionService } from '../../shared/service/session.service';
import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({ name: 'dateFormatPipe' })
export class DateFormatPipe implements PipeTransform {
    transform(value: string) {
        var datePipe = new DatePipe("en-US");
        value = datePipe.transform(value, 'dd-MM-yyyy');
        return value;
    }
}

@Pipe({ name: 'dataFilter' })
export class DataFilterPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.Kd_RM.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Nama.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Umur_Masuk.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.JenKel.toLowerCase().indexOf(query.toLowerCase()) > -1)
            );
        }
        return array;
    }
}

@Component({
    selector: 'app-dokter',
    templateUrl: './list-pasien-rawat-inap.component.html',
    styleUrls: [
        './list-pasien-rawat-inap.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class ListPasienRIComponent implements OnInit {

    public filterQuery: string = "";
    public dataPasienRI: any;
    public rowsOnPage: number = 5;
    public sortBy: string = "";
    public sortOrder: string = "desc";

    public useraccessdata: any;
    public loading = false;
    today: number = Date.now();

    constructor(private service: AppService, public session: SessionService) {
        let useracces = this.session.getData();
        this.useraccessdata = JSON.parse(useracces);
        console.log(this.useraccessdata);
    }

    ngOnInit() {
        this.getPasienRJ();
    }

    getPasienRJ() {
        this.loading = true;
        var param1 = this.useraccessdata.tgl_hari_ini;
        var param2 = this.useraccessdata.kode_lyn;
        var param3 = this.useraccessdata.kd_dokter;
        var param4 = this.useraccessdata.jam_praktek;

        // console.log("api/Users/ListPasienRJDokter", { tglMasuk: '2018-11-19', kdLyn: param2, kdDokter: param3, jamPraktek: param4 })
        this.service.post("api/Users/ListPasienRJDokter", { tglMasuk: param1, kdLyn: param2, kdDokter: param3, jamPraktek: param4 })
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                    this.dataPasienRI = '';
                    this.loading = false;
                }
                else {
                    this.dataPasienRI = JSON.parse(result);
                    console.log("Pasien", this.dataPasienRI);
                    this.loading = false;
                }
            });
    }
}