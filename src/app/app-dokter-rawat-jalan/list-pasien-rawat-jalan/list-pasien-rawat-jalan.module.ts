import {
  NgModule
} from '@angular/core';
import {
  CommonModule
} from '@angular/common';
import {
  ListPasienRJComponent,
  DateFormatPipe,
  DataFilterPipe,
  DataFilterPaketPipe,
  TimeFormatPipe,
  DataFilterPaketRacikPipe,
  DataFilterJenisPlyn,
  DataFilterPemeriksaanLyn,
  DataFilterRiwayatDiagnosa
} from './list-pasien-rawat-jalan.component';
import {
  RouterModule,
  Routes
} from "@angular/router";
import {
  SharedModule
} from "../../shared/shared.module";
import {
  AppService
} from "../../shared/service/app.service";
import {
  LoadingModule
} from 'ngx-loading';
import {
  DatePipe
} from '@angular/common';
import {
  Ng2CompleterModule
} from "ng2-completer";
import {
  AngularMultiSelectModule
} from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const ListPasienRJRoutes: Routes = [{
  path: '',
  component: ListPasienRJComponent,
  data: {
    breadcrumb: 'List Pasien Rawat Jalan',
    icon: 'icofont-home bg-c-blue',
    status: false,
    title: 'List Pasien Rawat Jalan'
  }
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ListPasienRJRoutes),
    SharedModule,
    LoadingModule,
    Ng2CompleterModule,
    AngularMultiSelectModule
  ],
  declarations: [ListPasienRJComponent, DateFormatPipe, DataFilterPipe, DataFilterPaketPipe, TimeFormatPipe, DataFilterPaketRacikPipe, DataFilterJenisPlyn, DataFilterPemeriksaanLyn, DataFilterRiwayatDiagnosa],
  providers: [AppService]
})
export class ListPasienRJModule {}
