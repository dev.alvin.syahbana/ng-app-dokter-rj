import {
  Component,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import '../../../assets/echart/echarts-all.js';
import * as _ from "lodash";
import {
  AppService
} from "../../shared/service/app.service";
import swal from 'sweetalert2';
import {
  SessionService
} from '../../shared/service/session.service';
import {
  Pipe,
  PipeTransform
} from '@angular/core';
import {
  DatePipe
} from '@angular/common';
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
  FormArray,
  ValidatorFn
} from "@angular/forms";
declare var require: any;
import {
  CompleterService,
  CompleterData,
  RemoteData
} from 'ng2-completer';
import {
  NgbTabChangeEvent
} from '@ng-bootstrap/ng-bootstrap';

import { environment } from '../../../environments/environment';

@Pipe({
  name: 'dateFormatPipe'
})
export class DateFormatPipe implements PipeTransform {
  transform(value: string) {
    var datePipe = new DatePipe("en-US");
    value = datePipe.transform(value, 'dd-MM-yyyy');
    return value;
  }
}

@Pipe({
  name: 'timeFormatPipe'
})
export class TimeFormatPipe implements PipeTransform {
  transform(value: string) {
    var datePipe = new DatePipe("en-US");
    value = datePipe.transform(value, 'HH:mm WIB');
    return value;
  }
}

@Pipe({
  name: 'dataFilter'
})
export class DataFilterPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.Kd_RM.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Nama.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Umur_Masuk.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.JenKel.toLowerCase().indexOf(query.toLowerCase()) > -1)
      );
    }
    return array;
  }
}

@Pipe({
  name: 'dataFilterPaketObat'
})
export class DataFilterPaketPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.Nm_Template.toLowerCase().indexOf(query.toLowerCase()) > -1)
      );
    }
    return array;
  }
}

@Pipe({
  name: 'dataFilterPaketObatRacik'
})
export class DataFilterPaketRacikPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.Nm_Template.toLowerCase().indexOf(query.toLowerCase()) > -1)
      );
    }
    return array;
  }
}

@Pipe({
  name: 'dataFilterPaketPM'
})
export class dataFilterPaketPMPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.Nm_Template.toLowerCase().indexOf(query.toLowerCase()) > -1)
      );
    }
    return array;
  }
}

@Pipe({
  name: 'dataFilterPelPunjangMedis'
})
export class DataFilterPenunjangMedisPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.PELAYANAN.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Nama.toLowerCase().indexOf(query.toLowerCase()) > -1)
      );
    }
    return array;
  }
}

@Pipe({
  name: 'dataFilterJenisPlyn'
})
export class DataFilterJenisPlyn {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.Jenis_Pelayanan.toLowerCase().indexOf(query.toLowerCase()) > -1)
      );
    }
    return array;
  }
}

@Pipe({
  name: 'dataFilterHLab'
})
export class DataFilterHLab {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.Grp.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.tgl.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.No_Trx.toLowerCase().indexOf(query.toLowerCase()) > -1)
      );
    }
    return array;
  }
}

@Pipe({
  name: 'dataFilterPemeriksaanLyn'
})
export class DataFilterPemeriksaanLyn {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.Nama.toLowerCase().indexOf(query.toLowerCase()) > -1)
      );
    }
    return array;
  }
}



@Pipe({
  name: 'dataFilterRiwayatDiagnosa'
})
export class DataFilterRiwayatDiagnosa {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row =>
        (row.Tgl_Masuk.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Trx.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.Jenis_Pelayanan.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.English_ICD.toLowerCase().indexOf(query.toLowerCase()) > -1)
      );
    }
    return array;
  }
}

@Component({
  selector: 'app-dokter',
  templateUrl: './list-pasien-rawat-jalan.component.html',
  styleUrls: [
    './list-pasien-rawat-jalan.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class ListPasienRJComponent implements OnInit {

  public filterQuery: string = "";
  public dataPasienRJ: any;
  public dataPasien: any;

  public filterPaketObatNonRacik: string = "";
  public filterPaketObatRacik: string = "";
  public filterPelayananMedis: string = "";
  public filterPaketPM: string = "";

  public apiAskep: string = environment.apiAskep;

  // public rowsOnPage: number = 10;
  public sortBy: string = "";
  public sortOrder: string = "";

  public useraccessdata: any;
  public loading = false;
  today: number = Date.now();

  public dataNonRacik = [];
  public dataPostNonRacik = [];
  public dataRacik = [];
  No_SO: any;
  dataPostApSOHeader: any;
  dataPostApSONRacik: any;

  // Paket obat Non Racik
  dataListPaketObatNonRacik: any;
  public listObatPaketNR: any;
  public dataObatNr = [];

  // Penunjang Medis
  dataListPenunjangMedis: any;
  dataListPaketPM: any;

  // Racik
  dataListPaketObatRacik: any;
  public listObatPaketR: any;
  public dataObatR = [];

  // Riwayat Diagnosa
  public filterRiwayatDiagnosa: string = "";

  // Data Complete
  protected dokterTerdaftar: string = "";
  // dataService untuk get dokter
  dataService: RemoteData; // utk non racik
  obatStr: string = '';
  dataServiceRacik: RemoteData; // utk racik
  dataServListRacikSave: RemoteData;
  dataServListRacikEdit: RemoteData;
  obatStrRacik: string = '';

  dataDiagnosaUtama: RemoteData; // utk diagnosa
  strDiagUtama: string = '';
  dataDiagnosaTambahan: RemoteData; // utk diagnosa
  strDiagTambahan: string = '';

  dataDiagnosaUtamaDetail: RemoteData; // utk diagnosa
  strDiagUtamaDetail: string = '';
  dataDiagnosaTambahanDetail: RemoteData; // utk diagnosa
  strDiagTambahanDetail: string = '';

  // ALKES
  public dataAlkesObat = [];
  dataServiceAlkesObat: RemoteData; // utk Alkes Obat
  obatStrAlkesObat: string = '';

  // kondisi paket obat 
  public paketObatNORacik = true;
  public paketObatRacik = false;
  public paketAlkes = false;
  public paketPM = false;
  public paketTind = false;
  public riwayatLab = false;

  // kondisi paket obat 
  public editPaketObatNORacik = true;
  public editPaketObatRacik = false;
  public editPaketAlkes = false;
  public editPaketPM = false;

  // Alkes
  dataAlkesObatByNoReg: any;

  // Penunjang Medis
  dataJenisPlyn: any;
  public filterJenisPlyn: string = "";

  // Hasil Lab
  dataHLab: any;
  dataHLabDetail = [];
  public filterHLab: string = "";

  // TINDAKAN
  public dataTindakan = [];
  dataServiceTindakan: RemoteData; // utk Tindakan
  strTindakan: string = '';

  public can_praktek: boolean = false
  public info_praktek: boolean = false
  public offPrak: boolean = false
  public dokroy: boolean = false
  info: string = ''
  formJasaDok: FormGroup;
  ordersData = [];

  constructor(private service: AppService, public session: SessionService, private datePipe: DatePipe,
    private completerService: CompleterService, private httpclient: HttpClient, private fb: FormBuilder) {

    setInterval(() => this.reloadPage(), 60000);

    let useracces = this.session.getData();
    this.useraccessdata = JSON.parse(useracces);
    // console.log(this.useraccessdata);

    // Resep Non Racik
    this.dataService = completerService.remote(
      null,
      "ListSearch",
      "ListSearch");
    this.dataService.urlFormater(term => {
      return `api/AppDokterRJResepNonRacik/ObatResep/` + this.useraccessdata.kd_dokter + term;
    });
    this.dataService;


    // Resep Racik
    this.dataServiceRacik = completerService.remote(
      null,
      "ListSearch",
      "ListSearch");
    this.dataServiceRacik.urlFormater(term => {
      return `api/AppDokterRJResepNonRacik/ObatResep/` + this.useraccessdata.kd_dokter + term;
    });
    this.dataServiceRacik;

    // List Resep Racik Simpan
    this.dataServListRacikSave = completerService.remote(
      null,
      "ListSearch",
      "ListSearch");
    this.dataServListRacikSave.urlFormater(term => {
      return `api/AppDokterRJResepNonRacik/ObatResep/` + this.useraccessdata.kd_dokter + term;
    });
    this.dataServListRacikSave;

    // List Resep Racik Edit
    this.dataServListRacikEdit = completerService.remote(
      null,
      "ListSearch",
      "ListSearch");
    this.dataServListRacikEdit.urlFormater(term => {
      return `api/AppDokterRJResepNonRacik/ObatResep/` + this.useraccessdata.kd_dokter + term;
    });
    this.dataServListRacikEdit;


    // Insert Diagnosa Pasien

    this.dataDiagnosaUtama = completerService.remote(
      null,
      "ListSearch",
      "ListSearch");
    this.dataDiagnosaUtama.urlFormater(term => {
      return `api/AppDokterRJDiag/GetDiagnosaPasien/` + term;
    });
    this.dataDiagnosaUtama;

    this.dataDiagnosaTambahan = completerService.remote(
      null,
      "ListSearch",
      "ListSearch");
    this.dataDiagnosaTambahan.urlFormater(term => {
      return `api/AppDokterRJDiag/GetDiagnosaPasien/` + term;
    });
    this.dataDiagnosaTambahan;


    // Edit Diagnosa Pasien

    this.dataDiagnosaUtamaDetail = completerService.remote(
      null,
      "ListSearch",
      "ListSearch");
    this.dataDiagnosaUtamaDetail.urlFormater(term => {
      return `api/AppDokterRJDiag/GetDiagnosaPasien/` + term;
    });
    this.dataDiagnosaUtamaDetail;

    this.dataDiagnosaTambahanDetail = completerService.remote(
      null,
      "ListSearch",
      "ListSearch");
    this.dataDiagnosaTambahanDetail.urlFormater(term => {
      return `api/AppDokterRJDiag/GetDiagnosaPasien/` + term;
    });
    this.dataDiagnosaTambahanDetail;

    // Alkes Obat
    this.dataServiceAlkesObat = completerService.remote(
      null,
      "Nama_Barang",
      "Nama_Barang");
    this.dataServiceAlkesObat.urlFormater(term => {
      return `api/Users/GetAlkesObat/` + term;
    });
    this.dataServiceAlkesObat;

    // Tindakan
    this.dataServiceTindakan = completerService.remote(
      null,
      "Nama_Tindakan",
      "Nama_Tindakan");
    this.dataServiceTindakan.urlFormater(term => {
      return `api/Tindakan/GetTindakan/` + this.useraccessdata.kd_dokter + '/' + this.useraccessdata.kode_lyn + '/' + term;
    });
    this.dataServiceTindakan;
  }

  reloadPage() {
    this.getPasienRJ();
    this.func_statusAwalPrakDokter();
  }

  ngOnInit() {
    this.reloadPage();
    // this.checkingPraktek();
    setTimeout(() => {
      if (this.status_praktek != '') {
        if (this.status_praktek == '0') {
          this.can_praktek = false;
        }
        if (this.status_praktek == '1') {
          this.can_praktek = true;
        }
      }
    }, 1000)
  }

  // checkingPraktek() {
  //   var countdown = 5 * 60 * 1000;
  //   const timerid = setInterval(() => {
  //     countdown -= 1000;
  //     var min = Math.floor(countdown / (60 * 1000));
  //     var sec = Math.floor((countdown - (min * 60 * 1000)) / 1000);  //correct

  //     if (countdown <= 0) {
  //       console.log("5 min!");
  //       // clearInterval(timerid);
  //       // const date = new Date()
  //       // const cdate = date.toLocaleDateString('id-id', { weekday: 'long' })
  //       // const param = {
  //       //   kdlyn: this.useraccessdata.kode_lyn,
  //       //   kddokter: this.useraccessdata.kd_dokter,
  //       //   jam: this.useraccessdata.jam_praktek,
  //       //   hari: cdate,
  //       //   kdpoli: this.useraccessdata.kd_poli
  //       // }
  //       // this.service.post("api/AppDokterRJInfo/CheckJadwal", param).subscribe(r => {
  //       //   const res1 = JSON.parse(r)
  //       //   console.log(res1)
  //       //   // this.service.post("api/AppDokterRJInfo/UpdateStatPraktek", param).subscribe(r => {
  //       //   //   const res3 = JSON.parse(r)
  //       //   //   console.log(res3)
  //       //   // }, er => {
  //       //   //   console.log(er)
  //       //   // })
  //       // }, er => {
  //       //   console.log(er)
  //       // })
  //     } else {
  //       // console.log(min + " : " + sec);
  //     }

  //   }, 1000); //1000ms. = 1sec.

  // }

  // get pasien rj berdasarkan dokter yang memeriksa dan
  getPasienRJ() {
    this.loading = true;
    var param1 = this.useraccessdata.tgl_hari_ini;
    var param2 = this.useraccessdata.kode_lyn;
    var param3 = this.useraccessdata.kd_dokter;
    var param4 = this.useraccessdata.jam_praktek;

    this.service.post("api/AppDokterRawatJalan/ListPasienRJDokter", {
      tglMasuk: param1,
      // tglMasuk: '2019-08-06',
      // tglMasuk: '2020-01-30',
      // tglMasuk: '2020-03-09',
      kdLyn: param2,
      kdDokter: param3,
      jamPraktek: param4
    }).subscribe(result => {
      if (result == "") {
        this.service.notfound();
        this.dataPasienRJ = '';
        this.loading = false;
      } else {
        this.dataPasienRJ = JSON.parse(result);
        this.loading = false;
      }
    },
      err => {
        window.alert("Mohon maaf telah terjadi kesalahan pada server");
        this.loading = false;
      });
  }

  // okok
  kdrm: string = ''
  getKdRM(kdrm) {
    this.kdrm = kdrm;
  }

  noreg: string;
  getNoReg(noreg) {
    this.noreg = noreg;
  }

  nama_pasien = '';
  getNamaPasien(nama_pasien) {
    this.nama_pasien = nama_pasien;
  }

  getPasienByRM() {
    var data: any;
    this.service.get("api/AppDokterRawatJalan/GetPasienByRM/" + this.noreg + "/" + this.kdrm, data)
      .subscribe(result => {
        this.dataPasien = JSON.parse(result);
        this.buildForm();
      },
        err => {
          this.loading = false;
        });
  }

  private addCheckboxes() {
    (this.ordersData.length > 0)
    this.ordersData.forEach((o, i) => {
      const control = new FormControl; // if first item set to true, else false
      (this.formJasaDok.controls.orders as FormArray).push(control);
    });
  }

  minSelectedCheckboxes(min = 1) {
    const validator: ValidatorFn = (formArray: FormArray) => {
      const totalSelected = formArray.controls
        .map(control => control.value)
        .reduce((prev, next) => next ? prev + next : prev, 0);

      return totalSelected == min ? null : { required: true };
    };

    return validator;
  }

  submitJasa() {
    const selectedOrderIds = this.formJasaDok.value.orders
      .map((v, i) => v ? this.ordersData[i].Jasa_Dokter : null)
      .filter(v => v !== null);
    console.log(selectedOrderIds);
    this.checkJasa(selectedOrderIds[0]);
  }

  getJasaDokter() {
    if (this.useraccessdata.kd_dokter == '20192642') {
      this.formJasaDok = this.fb.group({
        orders: new FormArray([], this.minSelectedCheckboxes(1))
      });
      this.ordersData = []
      this.dokroy = true
      const param = {
        kd_dokter: this.useraccessdata.kd_dokter
      }
      this.service.post("api/AppDokterRawatJalan/ListJasaDokter", param)
        .subscribe(result => {
          const res = JSON.parse(result)
          res.map(x => x.check = 0)
          this.service.post("api/AppDokterRawatJalan/CheckJasaDokter", { noreg: this.rjNoreg })
            .subscribe(result => {
              const r = JSON.parse(result)
              const obj = res
              for (let i = 0; i < obj.length; i++) {
                if (parseFloat(obj[i].Jasa_Dokter) == parseFloat(r.Harga)) {
                  obj[i].check = 1
                } else {
                  obj[i].check = 0
                }
              }
              this.ordersData = obj;
              this.addCheckboxes();
            }, err => {
              console.log(err)
            });
        }, err => {
          console.log(err)
        });
    } else {
      this.dokroy = false
    }
  }

  tglLahir_pasien: string;
  nilaiUmur: string;
  buildForm(): void {
    this.tglLahir_pasien = this.datePipe.transform(this.dataPasien.Tgl_Lahir, "dd-MM-yyyy");

    let {
      AgeFromDateString,
      AgeFromDate
    } = require('age-calculator');
    let ageFromString = new AgeFromDateString(this.datePipe.transform(this.dataPasien.Tgl_Lahir, "yyyy-MM-dd")).age;
    this.nilaiUmur = ageFromString;
  }

  // Alergi
  alergi: string = '';
  getAlergi(item) {
    this.alergi = item;
  }

  statusFOC: string = '';
  checkStatusFOC(event: any) {
    console.log('checkStatusFOC :', event)
    this.statusFOC = event;
  }

  checkJasa(hrg) {
    this.loading = true
    const param = {
      noreg: this.rjNoreg,
      kdlyn: this.useraccessdata.kode_lyn,
      kddokter: this.useraccessdata.kd_dokter,
      qty: 1.0,
      cito: 0,
      hrg: parseFloat(hrg),
      disc: 0,
      total: parseFloat(hrg) * 1,
      hrgnormal: parseFloat(hrg),
      hrgcito: 0,
      hrgkary: 0
    }
    this.service.post("api/AppDokterRawatJalan/InstJasaDokter", param)
      .subscribe(result => {
        this.loading = false
        // this.getJasaDokter();
      }, err => {
        this.loading = false
        // this.getJasaDokter();
      });
  }

  onTabChange($event: NgbTabChangeEvent) {
    if ($event.nextId === 'tab-resep-nonracik') {
      this.paketObatNORacik = true;
      this.paketObatRacik = false;
      this.paketAlkes = false;
      this.paketPM = false;
      this.paketTind = false;
      this.riwayatLab = false;
    } else if ($event.nextId === 'tab-resep-racik') {
      this.paketObatNORacik = false;
      this.paketObatRacik = true;
      this.paketAlkes = false;
      this.paketPM = false;
      this.paketTind = false;
      this.riwayatLab = false;
    } else if ($event.nextId === 'tab-alkes') {
      this.paketObatNORacik = false;
      this.paketObatRacik = false;
      this.paketAlkes = true;
      this.paketPM = false;
      this.paketTind = false;
      this.riwayatLab = false;
    } else if ($event.nextId === 'tab-pm') {
      this.paketObatNORacik = false;
      this.paketObatRacik = false;
      this.paketAlkes = false;
      this.paketPM = true;
      this.paketTind = false;
      this.riwayatLab = false;
    } else if ($event.nextId === 'tab-tind') {
      this.paketObatNORacik = false;
      this.paketObatRacik = false;
      this.paketAlkes = false;
      this.paketPM = false;
      this.paketTind = true;
      this.riwayatLab = false;
    } else if ($event.nextId === 'tab-lab') {
      this.paketObatNORacik = false;
      this.paketObatRacik = false;
      this.paketAlkes = false;
      this.paketPM = false;
      this.paketTind = false;
      this.riwayatLab = true;
    }
  }

  onEditTabChange($event: NgbTabChangeEvent) {
    if ($event.nextId === 'tab-edit-resep-nonracik') {
      this.editPaketObatNORacik = true;
      this.editPaketObatRacik = false;
      this.editPaketAlkes = false;
      this.editPaketPM = false;
    } else if ($event.nextId === 'tab-edit-resep-racik') {
      this.editPaketObatNORacik = false;
      this.editPaketObatRacik = true;
      this.editPaketAlkes = false;
      this.editPaketPM = false;
    } else if ($event.nextId === 'tab-edit-alkes') {
      this.editPaketObatNORacik = false;
      this.editPaketObatRacik = false;
      this.editPaketAlkes = true;
      this.editPaketPM = false;
    } else if ($event.nextId === 'tab-edit-pm') {
      this.editPaketObatNORacik = false;
      this.editPaketObatRacik = false;
      this.editPaketAlkes = false;
      this.editPaketPM = true;
    }
  }

  // konvert jumlah di apsonracik dan  apsoracikh
  convertToRoman(num) {

    var roman = "";
    var values = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
    var literals = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"];

    for (var i = 0; i < values.length; i++) {
      if (num >= values[i]) {
        if (5 <= num && num <= 8) num -= 5;
        else if (1 <= num && num <= 3) num -= 1;
        else num -= values[i];
        roman += literals[i];
        i--;
      }
    }
    return roman;
  }

  listSatuan: any;
  getSatuanObat() {
    this.service.httpClientGet('api/AppDokterRawatJalan/GetSatuan', '')
      .subscribe(result => {
        this.listSatuan = result;
      });
  }

  /** Resep non racik */
  deleteFieldObatPaketNR(index) {
    this.dataObatNr.splice(index, 1);
  }
  deleteFieldObatNR(index) {
    this.dataNonRacik.splice(index, 1);
  }

  onSelectObatNR(value) {
    let data = value["originalObject"];
    this.dataNonRacik.push(data);
    this.obatStr = '';

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'MENAMBAHKAN OBAT NON RACIK ( ' + data.Nm_Brg + ' )',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  getPaketObatNRacik() {
    var data: any;
    var kddok = this.useraccessdata.kd_dokter;
    this.service.httpClientGet('api/AppDokterRJResepNonRacik/GetPaketObatByDok/' + kddok, data)
      .subscribe(result => {
        data = result;
        this.dataListPaketObatNonRacik = data;
      },
        err => {
          this.loading = false;
        });
  };

  SelectPaketNonRacik(item: any) {
    this.loading = true;
    var kddok = this.useraccessdata.kd_dokter;
    var kdtemp = item;

    this.service.httpClientGet('api/AppDokterRJResepNonRacik/GetObatByPaketObat/' + kddok + '/' + kdtemp, '')
      .subscribe(result => {
        if (result == "") {
          this.listObatPaketNR = '';
          this.loading = false;
        } else {
          this.listObatPaketNR = result;
          this.dataObatNr = this.listObatPaketNR;
          this.loading = false;
        }
      },
        err => {
          this.loading = false;
        });

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'MENGGUNAKAN PAKET RESEP OBAT NON RACIK',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  editAturanPOPaketNonRacik(value, kd) {
    var index = this.dataObatNr.findIndex(x => x.Kd_Brg == kd);
    if (index !== -1) {
      this.dataObatNr[index].Aturan_P = value;
    }
  }

  editAturanPONonRacik(value, kd) {
    var index = this.dataNonRacik.findIndex(x => x.Kd_Brg == kd);
    if (index !== -1) {
      this.dataNonRacik[index].Aturan_P = value;
    }
  }

  editJmlONonRacik(value, kd) {
    var index = this.dataNonRacik.findIndex(x => x.Kd_Brg == kd);
    if (index !== -1) {
      this.dataNonRacik[index].Jml_SO = value;
      this.dataNonRacik[index].Jml_RMW = this.convertToRoman(value);
    }
  }
  /** Resep non racik */

  /** Paket Obat Racik */
  getPaketObatRacik() {
    var data: any;
    var kddok = this.useraccessdata.kd_dokter;
    this.service.httpClientGet('api/AppDokterRJResepRacik/GetPaketObatRacikByDok/' + kddok, data)
      .subscribe(result => {
        data = result;
        this.dataListPaketObatRacik = data;
      },
        err => {
          this.loading = false;
        });
  };

  SelectPaketRacik(item: any) {
    this.loading = true;
    var kddok = this.useraccessdata.kd_dokter;
    var kdtemp = item;

    this.service.httpClientGet('api/AppDokterRJResepRacik/GetObatByPaketObatRacik/' + kddok + '/' + kdtemp, '')
      .subscribe(result => {
        if (result == "") {
          this.listObatPaketR = '';
          this.loading = false;
        } else {
          this.listObatPaketR = result;
          this.dataObatR = this.listObatPaketR;
          this.loading = false;
        }
      },
        err => {
          this.loading = false;
        });

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'MENGGUNAKAN PAKET RESEP OBAT RACIK',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  onSelectObatR(value) {
    let data = value["originalObject"];
    this.dataRacik.push(data);
    this.obatStrRacik = '';
    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'MENAMBAHKAN OBAT RACIK ( ' + data.Nm_Brg + ' )',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  // obat racik saja
  editJmlORacik(value, kd) {
    var index = this.dataRacik.findIndex(x => x.Kd_Brg == kd);
    if (index !== -1) {
      this.dataRacik[index].Jml_SO = value;
    }
  }

  selSatuanRacikObat(value, kd) {
    var index = this.dataRacik.findIndex(x => x.Kd_Brg == kd);
    if (index !== -1) {
      this.dataRacik[index].Sat_SO = value;
    }
  }

  deleteFieldObatR(index) {
    this.dataRacik.splice(index, 1);
  }

  editJmlOPaketRacik(value, kd) {
    var index = this.dataObatR.findIndex(x => x.Kd_Brg == kd);
    if (index !== -1) {
      this.dataObatR[index].Jml_SO = value;
    }
  }

  selSatuanRacikObatPaket(value, kd) {
    var index = this.dataObatR.findIndex(x => x.Kd_Brg == kd);
    if (index !== -1) {
      this.dataObatR[index].Sat_SO = value;
    }
  }

  // paket obat racik
  deleteFieldObatPaketR(index) {
    this.dataObatR.splice(index, 1);
  }
  /** Paket Obat Racik */


  /** Edit Resep Pasien Non Racik */
  dataEditObatNRacik: any;
  kd_noso: string = '';
  no_reg: string = '';
  getResepObatNonRacik() {
    this.loading = true;
    this.service.httpClientGet('api/AppDokterRJResepNonRacik/GetResepObatNonRacik/' + this.noreg, '')
      .subscribe(result => {
        if (result == []) {
          // this.servie.notfound();
          this.dataEditObatNRacik = '';
          this.loading = false;
        } else {
          this.dataEditObatNRacik = result;
          this.loading = false;
        }
      },
        err => {
          this.loading = false;
        });
  }
  /** Edit Resep Pasien Non Racik */

  /** Edit Resep Pasien Racik */
  // menampilkan na
  dataEditNamaRacik: any;
  getResepNamaRacik() {
    this.service.httpClientGet('api/AppDokterRJResepRacik/GetNamaRacik/' + this.no_so.replace(/\//g, "@"), '')
      .subscribe(result => {
        if (result == "") {
          // this.service.notfound();
          this.dataEditNamaRacik = '';
        } else {
          this.dataEditNamaRacik = result;
        }
      },
        err => {
          this.loading = false;
        });
  }
  /** Edit Resep Pasien Racik */
  /** menampilkan data racik ketika tampilan update update */
  dataResepRacik: any;
  func_getResepRacik() {
    this.loading = true;
    this.service.httpClientGet('api/AppDokterRJResepRacik/GetResepObatRacik/' + this.noreg, '')
      .subscribe(result => {
        this.dataResepRacik = result;
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  onClearRacik() {
    this.dataResepRacik = [];
  }

  // paramter masukan
  no_so = '';
  no_so_baru = '';
  kd_poli = '';
  getKdSO() {
    this.kd_poli = this.useraccessdata.kd_poli;
    var input = {
      kd_poli: this.kd_poli
    }
    this.service.post('api/AppDokterRJResepNonRacik/GetKDSO', JSON.stringify(input))
      .subscribe(result => {
        this.No_SO = JSON.parse(result);
        this.no_so = this.No_SO.No_SO;
      });
  }

  inputBnykNR(value, kd) {
    var index = this.dataObatNr.findIndex(x => x.Kd_Brg == kd);
    if (index !== -1) {
      this.dataObatNr[index].Jml_SO = value;
      this.dataObatNr[index].Jml_RMW = this.convertToRoman(value);
    }
  }

  noso_update: string = '';
  dataCekNoSO: any;
  func_cekNoSOByNoReg(no_reg: any) {
    var data: any;
    this.service.get("api/AppDokterRJResepNonRacik/CekNoSOByNoReg/" + no_reg, data)
      .subscribe(result => {
        this.dataCekNoSO = JSON.parse(result).Cek;
        this.noso_update = JSON.parse(result).No_SO;
      });
  }

  rj_noreg = '';
  onSubmit() {
    this.loading = true;

    // parameter masukan utk mendapatkan no so
    this.rj_noreg = this.dataPasien.RJ_NoReg;

    this.getStatusPanggil(this.ticket_id, this.ticket_posisi);
    var ticket_posisiInt;

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'INPUT DATA RESEP OBAT',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });

    // kondisi jika resep non racik dan racik koosng, tiket status diisi berdasarkan cara bayar
    if (this.dataNonRacik.length == 0 && this.dataObatNr.length == 0 && this.dataRacik.length == 0 && this.dataObatR.length == 0) {

      // console.log("kondisi tanpa racik");
      if (this.ticket_posisi == '1. Tunai' || this.ticket_posisi == '2. Kartu Kredit' || this.ticket_posisi == '3. Kartu Debet' ||
        this.ticket_posisi == '5. Keringanan' || this.ticket_posisi == '6. FOC') {
        ticket_posisiInt = '3';
        var paramUpdSttsPanggil = {
          ticket_id: this.ticket_id,
          ticket_posisi: ticket_posisiInt
        }
        this.service.post('api/AppDokterRJInfo/UpdStsPanggil', JSON.stringify(paramUpdSttsPanggil))
          .subscribe(result => { });
      } else {
        ticket_posisiInt = '4';
        var paramUpdSttsPanggil = {
          ticket_id: this.ticket_id,
          ticket_posisi: ticket_posisiInt
        }
        this.service.post('api/AppDokterRJInfo/UpdStsPanggil', JSON.stringify(paramUpdSttsPanggil))
          .subscribe(result => { });
      }

      // Update alergi di table dbPasienRS jika tidak ada resep
      var paramAlergi = {
        alergi: this.alergi
      }
      // console.log('api/AppDokterRJDiag/UpdateALergidbPasienRS/' + this.kdrm, JSON.stringify(paramAlergi))
      this.service.put('api/AppDokterRJDiag/UpdateALergidbPasienRS/' + this.kdrm, JSON.stringify(paramAlergi))
        .subscribe(result => { });

    } else {
      // looping data obat non racik
      for (var i = 0; i < this.dataObatNr.length; i++) {
        this.dataNonRacik.push(this.dataObatNr[i]);
      }
      this.dataPostNonRacik = this.dataNonRacik;
      // parameter masukan utk insert ke tabel ApSONRacik di db SIRS
      var paramNonRacik = {
        No_SO: this.no_so,
        data: this.dataNonRacik
      }

      // cek noso
      // console.log("api/AppDokterRJResepNonRacik/CekNoSO/" + this.no_so.replace(/\//g, "@"), '');
      this.service.get("api/AppDokterRJResepNonRacik/CekNoSO/" + this.no_so.replace(/\//g, "@"), '')
        .subscribe(result => {
          this.cek_noso = JSON.parse(result).CekNoSO;
          // console.log("Kondisi No SO: ", this.cek_noso);

          if (this.cek_noso == "0") { // kondisi noso blm ada
            // kondisi non racik terisi
            ticket_posisiInt = '5';
            var paramUpdSttsPanggil = {
              ticket_id: this.ticket_id,
              ticket_posisi: ticket_posisiInt
            }
            // console.log('api/AppDokterRJInfo/UpdStsPanggil', JSON.stringify(paramUpdSttsPanggil));
            this.service.post('api/AppDokterRJInfo/UpdStsPanggil', JSON.stringify(paramUpdSttsPanggil))
              .subscribe(result => { });

            // Post ApSOHeader
            var input1 = {
              No_SO: this.no_so,
              Kd_Poli: this.kd_poli,
              No_Reg: this.rj_noreg
            }
            this.service.post('api/AppDokterRJResepNonRacik/PostApSOHeader', JSON.stringify(input1))
              .subscribe(result => {
                this.dataPostApSOHeader = JSON.parse(result);
                // Update Sts_SO,  Cetak
                this.service.put('api/AppDokterRJResepNonRacik/UpdateApSOHeader/' + this.no_so.replace(/\//g, "@"), '')
                  .subscribe(result => { });

                if (this.dataPostNonRacik.length != 0) {
                  this.service.post('api/AppDokterRJResepNonRacik/PostApSONRacik', JSON.stringify(paramNonRacik))
                    .subscribe(result => {
                      this.dataPostApSONRacik = JSON.parse(result);
                    },
                      err => {
                        this.loading = false;
                      });
                } else {
                  this.loading = false;
                }

                // param alergi
                var paramAlergi = {
                  alergi: this.alergi
                }
                // console.log('api/AppDokterRJDiag/UpdateALergiApSOHeader/' + this.noreg, JSON.stringify(paramAlergi));
                this.service.put('api/AppDokterRJDiag/UpdateALergiApSOHeader/' + this.noreg, JSON.stringify(paramAlergi))
                  .subscribe(result => { });

                // console.log('api/AppDokterRJDiag/UpdateALergidbPasienRS/' + this.kdrm, JSON.stringify(paramAlergi));
                this.service.put('api/AppDokterRJDiag/UpdateALergidbPasienRS/' + this.kdrm, JSON.stringify(paramAlergi))
                  .subscribe(result => { });
              },
                err => {
                  this.loading = false;
                });

          } else if (this.cek_noso == "1") { // kondisi noso sudah ada
            // pengecekan noso berdasarkan noreg 
            this.service.get("api/AppDokterRJResepNonRacik/CekNoSOByNoReg/" + this.noreg, '')
              .subscribe(result => {
                this.cek_noso = JSON.parse(result).Cek;
                this.no_so = JSON.parse(result).No_SO;

                // jika sudah ada berdasarkan noreg
                if (this.cek_noso == "1") {
                  this.service.post('api/AppDokterRJResepNonRacik/PostApSONRacik', JSON.stringify(paramNonRacik))
                    .subscribe(result => {
                      this.dataPostApSONRacik = JSON.parse(result);
                    },
                      err => {
                        this.loading = false;
                      });
                }
                // jika tidak ada noso dicek berdasarkan noreg makan dibuat noso baru
                else if (this.cek_noso == "0") {
                  // generate api baru
                  this.kd_poli = this.useraccessdata.kd_poli;
                  var input = {
                    kd_poli: this.kd_poli
                  }
                  this.service.post('api/AppDokterRJResepNonRacik/GetKDSO', JSON.stringify(input))
                    .subscribe(result => {
                      this.No_SO = JSON.parse(result);
                      this.no_so_baru = this.No_SO.No_SO;

                      // kondisi non racik terisi
                      ticket_posisiInt = '5';
                      var paramUpdSttsPanggil = {
                        ticket_id: this.ticket_id,
                        ticket_posisi: ticket_posisiInt
                      }
                      // console.log('api/AppDokterRJInfo/UpdStsPanggil', JSON.stringify(paramUpdSttsPanggil))
                      this.service.post('api/AppDokterRJInfo/UpdStsPanggil', JSON.stringify(paramUpdSttsPanggil))
                        .subscribe(result => { });

                      // Post ApSOHeader
                      var paramApSOH = {
                        No_SO: this.no_so_baru,
                        Kd_Poli: this.kd_poli,
                        No_Reg: this.rj_noreg
                      }

                      // parameter masukan utk insert ke tabel ApSONRacik di db SIRS
                      var paramSoHNew = {
                        No_SO: this.no_so_baru,
                        data: this.dataPostNonRacik
                      }
                      this.service.post('api/AppDokterRJResepNonRacik/PostApSOHeader', JSON.stringify(paramApSOH))
                        .subscribe(result => {
                          this.dataPostApSOHeader = JSON.parse(result);
                          // Update Sts_SO,  Cetak
                          this.service.put('api/AppDokterRJResepNonRacik/UpdateApSOHeader/' + this.no_so_baru.replace(/\//g, "@"), '')
                            .subscribe(result => { });

                          // console.log('api/AppDokterRJResepNonRacik/PostApSONRacik', JSON.stringify(paramSoHNew));
                          this.service.post('api/AppDokterRJResepNonRacik/PostApSONRacik', JSON.stringify(paramSoHNew))
                            .subscribe(result => {
                              this.dataPostApSONRacik = JSON.parse(result);
                            },
                              err => {
                                this.loading = false;
                              });

                          // param alergi
                          var paramAlergi = {
                            alergi: this.alergi
                          }
                          // console.log('api/AppDokterRJDiag/UpdateALergiApSOHeader/' + this.noreg, JSON.stringify(paramAlergi))
                          this.service.put('api/AppDokterRJDiag/UpdateALergiApSOHeader/' + this.noreg, JSON.stringify(paramAlergi))
                            .subscribe(result => { });

                          // console.log('api/AppDokterRJDiag/UpdateALergidbPasienRS/' + this.kdrm, JSON.stringify(paramAlergi))
                          this.service.put('api/AppDokterRJDiag/UpdateALergidbPasienRS/' + this.kdrm, JSON.stringify(paramAlergi))
                            .subscribe(result => { });

                          // cek noso by noreg, digunkan utk mengambil noso, yg kemudian di kondisi diatas dicek kembali utk menambah obat
                          this.service.get("api/AppDokterRJResepNonRacik/CekNoSOByNoReg/" + this.noreg, '')
                            .subscribe(result => {
                              this.no_so = JSON.parse(result).No_SO;
                              // console.log("Noso baru: ", this.no_so);
                            });
                        },
                          err => {
                            this.loading = false;
                          });
                    });
                }
              });
          }
        });
    }

    setTimeout(() => {
      // redirect
      this.dataNonRacik = [];
      this.dataObatNr = [];
      this.dataRacik = [];
      this.dataObatR = [];
      this.dataEditNamaRacik = [];
      this.getResepObatNonRacik(); // reload data obat non racik
      this.func_getResepRacik(); //reload data obat racik
      this.loading = false;
      this.getPasienRJ();
    }, 2000)
  }

  onUpdateApSOHeader() {
    this.loading = true;

    for (var i = 0; i < this.dataObatNr.length; i++) {
      this.dataNonRacik.push(this.dataObatNr[i]);
    }
    this.dataPostNonRacik = this.dataNonRacik;

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'UPDATE RESEP OBAT NON RACIK',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });

    this.service.get("api/AppDokterRJResepNonRacik/CekNoSOByNoReg/" + this.noreg, '')
      .subscribe(result => {
        this.cek_noso = JSON.parse(result).Cek;
        this.noso_update = JSON.parse(result).No_SO;

        // parameter masukan utk insert ke tabel ApSONRacik di db SIRS
        var paramNonRacik = {
          No_SO: this.noso_update,
          data: this.dataPostNonRacik
        }
        if (this.dataPostNonRacik.length != 0) {
          this.service.post('api/AppDokterRJResepNonRacik/PostApSONRacik', JSON.stringify(paramNonRacik))
            .subscribe(result => {
              this.dataPostApSONRacik = JSON.parse(result);

              // redirect
              this.dataNonRacik = [];
              this.dataObatNr = [];
              this.dataRacik = [];
              this.dataObatR = [];
              this.dataEditNamaRacik = [];
              this.getResepObatNonRacik(); // reload data obat non racik
              this.func_getResepRacik(); //reload data obat racik
              this.loading = false;
            },
              err => {
                this.loading = false;
              });
        } else {
          this.loading = false;
          // redirect
          this.dataNonRacik = [];
          this.dataObatNr = [];
          this.dataRacik = [];
          this.dataObatR = [];
          this.dataEditNamaRacik = [];
          this.getResepObatNonRacik(); // reload data obat non racik
          this.func_getResepRacik(); //reload data obat racik
        }

        // param alergi
        var paramAlergi = {
          alergi: this.alergi
        }
        this.service.put('api/AppDokterRJDiag/UpdateALergiApSOHeader/' + this.noreg, JSON.stringify(paramAlergi))
          .subscribe(result => { });

        this.service.put('api/AppDokterRJDiag/UpdateALergidbPasienRS/' + this.kdrm, JSON.stringify(paramAlergi))
          .subscribe(result => { });
      });

    setTimeout(() => {
      this.getPasienRJ();
      this.loading = false;
    }, 1000);
  }


  // Racik
  dataGenerateRacik: any;
  kdRacik: string = '';
  noUrutRacik: string = '';
  namaRacik: string = '';
  getGeneRacik() {
    // Resep Racik
    // Generate Racik Header
    var paramGene = {
      no_so: this.no_so
    }
    this.service.post('api/AppDokterRJResepRacik/GenerateRacikHeader', JSON.stringify(paramGene))
      .subscribe(result => {
        this.dataGenerateRacik = JSON.parse(result);
        // console.log("Gene Racik: ", this.dataGenerateRacik);
        this.kdRacik = this.dataGenerateRacik.kd_Racik;
        this.noUrutRacik = this.dataGenerateRacik.NoUrut;
        this.namaRacik = this.dataGenerateRacik.Nama_Racikan;
      });
  }

  dataEditGenerate: any;
  edit_kdRacik: string = '';
  edit_noUrutRacik: string = '';
  edit_namaRacik: string = '';
  getGeneRacikEdit() {
    // Resep Racik
    // Generate Racik Header
    var paramGene = {
      no_so: this.noso_update
    }
    // console.log('api/AppDokterRJResepRacik/GenerateRacikHeader', JSON.stringify(paramGene))
    this.service.post('api/AppDokterRJResepRacik/GenerateRacikHeader', JSON.stringify(paramGene))
      .subscribe(result => {
        this.dataEditGenerate = JSON.parse(result);
        // console.log("dataEditGenerate",this.dataEditGenerate);

        this.edit_kdRacik = this.dataEditGenerate.kd_Racik;
        this.edit_noUrutRacik = this.dataEditGenerate.NoUrut;
        this.edit_namaRacik = this.dataEditGenerate.Nama_Racikan;
      });
  }

  // perdosis
  valDoSel = '';
  dosis_seluruhChange(value) {
    this.valDoSel = value;
    // console.log(this.valDoSel)
    if (this.valDoSel == 'Perdosis') {
      this.catatanRacik = 'Racik dibuat perdosis';
    } else if (this.valDoSel == 'Keseluruhan') {
      this.catatanRacik = 'Racik dibuat keseluruhan';
    }
  }

  dataApSoRacikH: any;
  dataApSORacikD: any;
  jmlNs: string = '';
  dosisRacik: string = '';
  satuanRacik: string = '';
  infoObatRacik: string = '';
  catatanRacik: string = '';
  ketRacik: string = '';
  cek_noso: string = '';
  onSubmitRacikan() {

    this.loading = true;
    var ticket_posisiInt;

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'INPUT DATA RESEP OBAT RACIK',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });

    // api pengecekan noso yg sudah diambil apakah sudah ada ato blm
    this.service.get("api/AppDokterRJResepNonRacik/CekNoSO/" + this.no_so.replace(/\//g, "@"), '')
      .subscribe(result => {
        this.cek_noso = JSON.parse(result).CekNoSO;
        // console.log("Kondisi No SO: ", this.cek_noso);

        if (this.cek_noso == "0") { // kondisi noso blm ada
          ticket_posisiInt = '5';
          var paramUpdSttsPanggil = {
            ticket_id: this.ticket_id,
            ticket_posisi: ticket_posisiInt
          }
          this.service.post('api/AppDokterRJInfo/UpdStsPanggil', JSON.stringify(paramUpdSttsPanggil))
            .subscribe(result => { });

          // looping obat racik
          for (var i = 0; i < this.dataObatR.length; i++) {
            this.dataRacik.push(this.dataObatR[i]);
          }

          if (this.dataRacik.length == 0) {
            this.loading = false;
          } else if (this.dataRacik.length != 0) {
            // Post ApSOHeader
            var input1 = {
              No_SO: this.no_so,
              Kd_Poli: this.kd_poli,
              No_Reg: this.noreg
            }
            // api post so header
            this.service.post('api/AppDokterRJResepNonRacik/PostApSOHeader', JSON.stringify(input1))
              .subscribe(result => {
                this.dataPostApSOHeader = JSON.parse(result);

                // api generate kode racik berdasarkan no so
                var paramGene = {
                  no_so: this.no_so
                }
                // console.log('api/AppDokterRJResepRacik/GenerateRacikHeader', JSON.stringify(paramGene));
                this.service.post('api/AppDokterRJResepRacik/GenerateRacikHeader', JSON.stringify(paramGene))
                  .subscribe(result => {
                    this.dataGenerateRacik = JSON.parse(result);
                    this.kdRacik = this.dataGenerateRacik.kd_Racik;
                    this.noUrutRacik = this.dataGenerateRacik.NoUrut;
                    this.namaRacik = this.dataGenerateRacik.Nama_Racikan;

                    // Kondisi obat racik kosong atau tidak
                    var paramApSoRacikH = {
                      no_so: this.no_so,
                      kd_racik: this.kdRacik,
                      no_urut: this.noUrutRacik,
                      nama_racikan: this.namaRacik,
                      jml_ns: this.jmlNs,
                      dosis_racik: this.dosisRacik,
                      satuan_racik: this.satuanRacik,
                      info_obat_racik: this.infoObatRacik,
                      catatan_racik: this.catatanRacik,
                      ket_racik: this.ketRacik,
                      jml_rmw: this.convertToRoman(this.jmlNs)
                    }
                    this.service.post('api/AppDokterRJResepRacik/PostApSoRacikH', JSON.stringify(paramApSoRacikH))
                      .subscribe(result => {
                        this.dataApSoRacikH = JSON.parse(result);

                        var paramApSORacikD = {
                          no_so: this.no_so,
                          kd_racik: this.kdRacik,
                          no_urut: this.noUrutRacik,
                          dataRacik: this.dataRacik
                        }
                        // console.log('api/AppDokterRJResepRacik/PostApSORacikD', JSON.stringify(paramApSORacikD));
                        this.service.post('api/AppDokterRJResepRacik/PostApSORacikD', JSON.stringify(paramApSORacikD))
                          .subscribe(result => {
                            this.dataApSORacikD = JSON.parse(result);

                            this.dataRacik = [];
                            this.dataObatR = [];
                            this.catatanRacik = '';
                            this.ketRacik = '';
                            this.jmlNs = '';
                            this.satuanRacik = '';
                            this.dosisRacik = '';
                            this.infoObatRacik = '';
                            this.obatStr = '';
                            this.getResepNamaRacik(); // menjalankan api utk menampilkan data racik yg sudah di masukan
                          })
                      },
                        err => {
                          this.loading = false;
                        });
                  });
              });
          }
        } else if (this.cek_noso == "1") { // kondisi noso sudah ada
          // jika kondisi noso sudah ada dicek dengan noreg
          this.service.get("api/AppDokterRJResepNonRacik/CekNoSOByNoReg/" + this.noreg, '')
            .subscribe(result => {
              // console.log("Cek noso: ", result)
              this.dataCekNoSO = JSON.parse(result).Cek;
              this.noso_update = JSON.parse(result).No_SO;

              if (this.dataCekNoSO == "1") { // kondisi noso ada berdasarkan noreg, jika ada simpan data racik tanpa header karna noso diheader sudh ada 
                // looping obat racik
                for (var i = 0; i < this.dataObatR.length; i++) {
                  this.dataRacik.push(this.dataObatR[i]);
                }

                if (this.dataRacik.length == 0) { // kondisi racik kosong
                  // console.log("Racik kosong");
                  this.loading = false;
                } else if (this.dataRacik.length != 0) { // kondisi racik tidak kosong
                  var paramGene = {
                    no_so: this.noso_update
                  }
                  // console.log('api/AppDokterRJResepRacik/GenerateRacikHeader', JSON.stringify(paramGene));
                  this.service.post('api/AppDokterRJResepRacik/GenerateRacikHeader', JSON.stringify(paramGene))
                    .subscribe(result => {
                      this.dataGenerateRacik = JSON.parse(result);
                      this.kdRacik = this.dataGenerateRacik.kd_Racik;
                      this.noUrutRacik = this.dataGenerateRacik.NoUrut;
                      this.namaRacik = this.dataGenerateRacik.Nama_Racikan;

                      // Kondisi obat racik kosong atau tidak
                      var paramApSoRacikH = {
                        no_so: this.noso_update,
                        kd_racik: this.kdRacik,
                        no_urut: this.noUrutRacik,
                        nama_racikan: this.namaRacik,
                        jml_ns: this.jmlNs,
                        dosis_racik: this.dosisRacik,
                        satuan_racik: this.satuanRacik,
                        info_obat_racik: this.infoObatRacik,
                        catatan_racik: this.catatanRacik,
                        ket_racik: this.ketRacik,
                        jml_rmw: this.convertToRoman(this.jmlNs)
                      }
                      this.service.post('api/AppDokterRJResepRacik/PostApSoRacikH', JSON.stringify(paramApSoRacikH))
                        .subscribe(result => {
                          this.dataApSoRacikH = JSON.parse(result);

                          var paramApSORacikD = {
                            no_so: this.noso_update,
                            kd_racik: this.kdRacik,
                            no_urut: this.noUrutRacik,
                            dataRacik: this.dataRacik
                          }
                          // console.log('api/AppDokterRJResepRacik/PostApSORacikD', JSON.stringify(paramApSORacikD));
                          this.service.post('api/AppDokterRJResepRacik/PostApSORacikD', JSON.stringify(paramApSORacikD))
                            .subscribe(result => {
                              this.dataApSORacikD = JSON.parse(result);

                              this.dataRacik = [];
                              this.dataObatR = [];
                              this.catatanRacik = '';
                              this.ketRacik = '';
                              this.jmlNs = '';
                              this.satuanRacik = '';
                              this.dosisRacik = '';
                              this.infoObatRacik = '';
                              this.dataEditNamaRacik = [];
                              this.obatStr = '';
                              this.getResepNamaRacik(); // menjalankan api utk menampilkan data racik yg sudah di masukan
                            });

                          setTimeout(() => {
                            this.service.httpClientGet('api/AppDokterRJResepRacik/GetNamaRacik/' + this.noso_update.replace(/\//g, "@"), '')
                              .subscribe(result => {
                                this.dataEditNamaRacik = result;
                              });
                          }, 1000);
                        },
                          err => {
                            this.loading = false;
                          });
                    });
                }
              } else if (this.dataCekNoSO == "0") { // kondisi noso tidak ada berdasarkan noreg, jika tidak ada maka dibuat noso batu
                // generate api baru
                this.kd_poli = this.useraccessdata.kd_poli;
                var input = {
                  kd_poli: this.kd_poli
                }
                this.service.post('api/AppDokterRJResepNonRacik/GetKDSO', JSON.stringify(input))
                  .subscribe(result => {
                    this.No_SO = JSON.parse(result);
                    this.no_so_baru = this.No_SO.No_SO;
                    // console.log("NO SO baru: ", this.no_so);

                    ticket_posisiInt = '5';
                    var paramUpdSttsPanggil = {
                      ticket_id: this.ticket_id,
                      ticket_posisi: ticket_posisiInt
                    }
                    this.service.post('api/AppDokterRJInfo/UpdStsPanggil', JSON.stringify(paramUpdSttsPanggil))
                      .subscribe(result => { });

                    // looping obat racik
                    for (var i = 0; i < this.dataObatR.length; i++) {
                      this.dataRacik.push(this.dataObatR[i]);
                    }

                    if (this.dataRacik.length == 0) {
                      // console.log("Racik kosong");
                      this.loading = false;
                    } else if (this.dataRacik.length != 0) {
                      // Post ApSOHeader
                      var input1 = {
                        No_SO: this.no_so_baru,
                        Kd_Poli: this.kd_poli,
                        No_Reg: this.noreg
                      }
                      // api post so header
                      this.service.post('api/AppDokterRJResepNonRacik/PostApSOHeader', JSON.stringify(input1))
                        .subscribe(result => {
                          this.dataPostApSOHeader = JSON.parse(result);

                          // api generate kode racik berdasarkan no so
                          var paramGene = {
                            no_so: this.no_so_baru
                          }
                          // console.log('api/AppDokterRJResepRacik/GenerateRacikHeader', JSON.stringify(paramGene));
                          this.service.post('api/AppDokterRJResepRacik/GenerateRacikHeader', JSON.stringify(paramGene))
                            .subscribe(result => {
                              this.dataGenerateRacik = JSON.parse(result);
                              this.kdRacik = this.dataGenerateRacik.kd_Racik;
                              this.noUrutRacik = this.dataGenerateRacik.NoUrut;
                              this.namaRacik = this.dataGenerateRacik.Nama_Racikan;

                              // Kondisi obat racik kosong atau tidak
                              var paramApSoRacikH = {
                                no_so: this.no_so_baru,
                                kd_racik: this.kdRacik,
                                no_urut: this.noUrutRacik,
                                nama_racikan: this.namaRacik,
                                jml_ns: this.jmlNs,
                                dosis_racik: this.dosisRacik,
                                satuan_racik: this.satuanRacik,
                                info_obat_racik: this.infoObatRacik,
                                catatan_racik: this.catatanRacik,
                                ket_racik: this.ketRacik,
                                jml_rmw: this.convertToRoman(this.jmlNs)
                              }
                              this.service.post('api/AppDokterRJResepRacik/PostApSoRacikH', JSON.stringify(paramApSoRacikH))
                                .subscribe(result => {
                                  this.dataApSoRacikH = JSON.parse(result);

                                  var paramApSORacikD = {
                                    no_so: this.no_so_baru,
                                    kd_racik: this.kdRacik,
                                    no_urut: this.noUrutRacik,
                                    dataRacik: this.dataRacik
                                  }
                                  // console.log('api/AppDokterRJResepRacik/PostApSORacikD', JSON.stringify(paramApSORacikD));
                                  this.service.post('api/AppDokterRJResepRacik/PostApSORacikD', JSON.stringify(paramApSORacikD))
                                    .subscribe(result => {
                                      this.dataApSORacikD = JSON.parse(result);

                                      this.dataRacik = [];
                                      this.dataObatR = [];
                                      this.catatanRacik = '';
                                      this.ketRacik = '';
                                      this.jmlNs = '';
                                      this.satuanRacik = '';
                                      this.dosisRacik = '';
                                      this.infoObatRacik = '';
                                      this.obatStr = '';
                                      this.func_getResepRacik(); // menjalankan api utk menampilkan data racik yg sudah di masukan
                                    })
                                },
                                  err => {
                                    this.loading = false;
                                  });
                            });
                        });
                    }
                  });
              }
            });

        }

        setTimeout(() => {
          this.loading = false;
        }, 500);
      });
  }

  onCancel() {
    this.dataEditNamaRacik = [];
    this.dataNonRacik = [];
    this.dataObatNr = [];
    this.dataRacik = [];
    this.dataObatR = [];
    this.dataAlkesObat = [];
    this.dataAlkesObat_tmp = [];
    this.cara_bayar = '';
    this.Harga = 0;
    this.jenis_plyn = '';
    this.arr_dataPemeriksaan = [];
    this.arr_editdataPemeriksaan = [];
    this.kd_pmlyn = '';
    this.dataHLabDetail = [];
    // reset data yg diget sebelumnya
    this.dataEditObatNRacik = [];
    this.dataResepRacik = [];
    this.dokroy = false;

    setTimeout(() => {
      this.getPasienRJ();
    }, 1000);
  }

  dataEditApSoRacikH: any;
  dataEditApSORacikD: any;
  onSubmitEditRacikan() {
    this.loading = true;

    this.service.get("api/AppDokterRJResepNonRacik/CekNoSOByNoReg/" + this.noreg, '')
      .subscribe(result => {
        // console.log("Cek noso: ", result)
        this.dataCekNoSO = JSON.parse(result).Cek;
        this.noso_update = JSON.parse(result).No_SO;

        if (this.dataCekNoSO == "1") { // kondisi noso ada berdasarkan noreg, jika ada simpan data racik tanpa header karna noso diheader sudh ada
          // looping obat racik
          for (var i = 0; i < this.dataObatR.length; i++) {
            this.dataRacik.push(this.dataObatR[i]);
          }

          if (this.dataRacik.length == 0) { // kondisi racik kosong
            // console.log("Racik kosong");
            this.loading = false;
          } else if (this.dataRacik.length != 0) {
            const paramLog = {
              NoReg: this.noreg,
              Kd_Dokter: this.useraccessdata.kd_dokter,
              Aksi: 'UPDATE DATA RESEP OBAT RACIK',
              Jenis: '1'
            }

            this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
              .subscribe(result => {
                this.loading = false;
              },
                err => {
                  this.loading = false;
                });

            var paramGene = {
              no_so: this.noso_update
            }
            // console.log('api/AppDokterRJResepRacik/GenerateRacikHeader', JSON.stringify(paramGene));
            this.service.post('api/AppDokterRJResepRacik/GenerateRacikHeader', JSON.stringify(paramGene))
              .subscribe(result => {
                this.dataGenerateRacik = JSON.parse(result);
                // console.log(this.dataGenerateRacik);
                this.edit_kdRacik = this.dataGenerateRacik.kd_Racik;
                this.edit_noUrutRacik = this.dataGenerateRacik.NoUrut;
                this.edit_namaRacik = this.dataGenerateRacik.Nama_Racikan;

                var paramApSoRacikH = {
                  no_so: this.noso_update,
                  kd_racik: this.edit_kdRacik,
                  no_urut: this.edit_noUrutRacik,
                  nama_racikan: this.edit_namaRacik,
                  jml_ns: this.jmlNs,
                  dosis_racik: this.dosisRacik,
                  satuan_racik: this.satuanRacik,
                  info_obat_racik: this.infoObatRacik,
                  catatan_racik: this.catatanRacik,
                  ket_racik: this.ketRacik,
                  jml_rmw: this.convertToRoman(this.jmlNs)
                }
                // console.log('api/AppDokterRJResepRacik/PostApSoRacikH', JSON.stringify(paramApSoRacikH));
                this.service.post('api/AppDokterRJResepRacik/PostApSoRacikH', JSON.stringify(paramApSoRacikH))
                  .subscribe(result => {
                    this.dataEditApSoRacikH = JSON.parse(result);

                    var paramApSORacikD = {
                      no_so: this.noso_update,
                      kd_racik: this.edit_kdRacik,
                      no_urut: this.edit_noUrutRacik,
                      dataRacik: this.dataRacik
                    }
                    // console.log('api/AppDokterRJResepRacik/PostApSORacikD', JSON.stringify(paramApSORacikD));
                    this.service.post('api/AppDokterRJResepRacik/PostApSORacikD', JSON.stringify(paramApSORacikD))
                      .subscribe(result => {
                        this.dataEditApSORacikD = JSON.parse(result);

                        // clear inputan
                        this.dataRacik = [];
                        this.dataObatR = [];
                        this.catatanRacik = '';
                        this.ketRacik = '';
                        this.jmlNs = '';
                        this.satuanRacik = '';
                        this.dosisRacik = '';
                        this.infoObatRacik = '';
                        this.func_getResepRacik();
                        this.dataEditNamaRacik = [];
                        this.obatStr = '';
                      });
                  },
                    err => {
                      this.loading = false;
                    });
              });
          }
        }
      });
  }

  dataAddObatListRacikan: any;
  onSubmitObatListRacikan() {
    this.loading = true;
    for (var i = 0; i < this.dataRacik.length; i++) {
      this.dataRacik[i];
      // console.log(this.dataRacik[i]);
    }

    if (this.dataRacik.length != 0) {
      var paramApSORacikD = {
        no_so: this.no_so,
        kd_racik: this.Listkd_racik,
        no_urut: this.Listno_urut,
        dataRacik: this.dataRacik
      }
      // console.log('api/AppDokterRJResepRacik/PostApSORacikD', JSON.stringify(paramApSORacikD));
      this.service.post('api/AppDokterRJResepRacik/PostApSORacikD', JSON.stringify(paramApSORacikD))
        .subscribe(result => {
          this.dataAddObatListRacikan = JSON.parse(result);
          this.dataRacik = [];
          this.obatStrRacik = '';

          // reload data racik
          this.getResepObatRacikDetail(this.no_so, this.Listkd_racik, this.Listnm_racik, this.Listno_urut,
            this.Listjml_bnyk, this.Listsatuan, this.Listdosis, this.Listinfo_obat, this.Listcatatan_racik,
            this.Listket_racik);
          this.loading = false;
        },
          err => {
            this.loading = false;
          });

      const paramLog = {
        NoReg: this.noreg,
        Kd_Dokter: this.useraccessdata.kd_dokter,
        Aksi: 'UPDATE RESEP OBAT RACIK',
        Jenis: '1'
      }

      this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
        .subscribe(result => {
          this.loading = false;
        },
          err => {
            this.loading = false;
          });
    } else {
      this.loading = false;
    }
  }

  /** start detail Resep Obat Pasien Racik */
  dataListObatRacik: any;
  Listkd_racik: any;
  Listnm_racik: any;
  Listno_urut: any;
  Listjml_bnyk: any;
  Listsatuan: any;
  Listdosis: any;
  Listinfo_obat: any;
  Listcatatan_racik: any;
  Listket_racik: any;
  getResepObatRacikDetail(noso, kd_racik, nm_racik, no_urut, jml_bnyk, satuan, dosis, info_obat, catatan_racik, ket_racik) {
    this.loading = true;
    this.no_so = noso;
    this.Listkd_racik = kd_racik;
    this.Listnm_racik = nm_racik;
    this.Listno_urut = no_urut;
    this.Listjml_bnyk = jml_bnyk;
    this.Listsatuan = satuan;
    this.Listdosis = dosis;
    this.Listinfo_obat = info_obat;
    this.Listcatatan_racik = catatan_racik;
    this.Listket_racik = ket_racik;
    this.service.httpClientGet('api/AppDokterRJResepRacik/GetResepObatRacikDetail/' + this.no_so.replace(/\//g, "@") + '/' + this.Listkd_racik, '')
      .subscribe(result => {
        if (result == "") {
          this.dataListObatRacik = '';
          this.loading = false;
        } else {
          this.dataListObatRacik = result;
          this.loading = false;
        }
      },
        err => {
          this.loading = false;
        });

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'MELIHAT DETAIL OBAT RACIK',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }
  /**  end detail Resep Obat Pasien Racik */

  // Pemberian Nama paket non racik baru
  dataPaketObatNonRacikNew: any;
  dataPostPaketNRNew: any;
  namaPaket: string;
  infoObat: string = null;
  onSubmitPaketObatNonRacik() {

    this.loading = true;
    var inputParamInsertPaketNRacik = {
      kd_dokter: this.useraccessdata.kd_dokter,
      nm_template: this.namaPaket,
      kd_poli: this.useraccessdata.kd_poli
    }

    // resep obat non racik
    for (var i = 0; i < this.dataObatNr.length; i++) {
      this.dataNonRacik.push(this.dataObatNr[i]);
    }

    if (this.dataNonRacik.length == 0) {
      this.loading = false;
    } else if (this.dataNonRacik.length != 0) {

      const paramLog = {
        NoReg: this.noreg,
        Kd_Dokter: this.useraccessdata.kd_dokter,
        Aksi: 'MEMBUAT PAKET RESEP OBAT NON RACIK',
        Jenis: '1'
      }

      this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
        .subscribe(result => {
          this.loading = false;
        },
          err => {
            this.loading = false;
          });

      this.service.post('api/AppDokterRJResepNonRacik/PostApSOTemplate_Resep', JSON.stringify(inputParamInsertPaketNRacik))
        .subscribe(result => {
          this.dataPaketObatNonRacikNew = JSON.parse(result);
          this.namaPaket = '';
          this.getPaketObatNRacik();
          this.loading = false;

          var inputParamInsertPaketObatNRacik = {
            kd_dokter: this.useraccessdata.kd_dokter,
            data: this.dataNonRacik,
            info_obat: this.infoObat,
            kd_poli: this.useraccessdata.kd_poli
          }
          this.service.post('api/AppDokterRJResepNonRacik/PostApSOTemplate_Resep_dt', JSON.stringify(inputParamInsertPaketObatNRacik))
            .subscribe(result => {
              this.dataPostPaketNRNew = JSON.parse(result);
              this.dataObatNr = [];
              this.dataNonRacik = [];
            },
              err => {
                this.loading = false;
              });
        },
          error => {
            this.namaPaket = '';
            this.loading = false;
          });
    }
  }
  onBatalPaketObat() {
    this.namaPaket = '';
  }

  /** Paket Penunjang Medis */
  getPaketPM() {
    var data: any;
    const kddok = this.useraccessdata.kd_dokter;
    const kdlyn = this.kd_pmlyn;
    this.service.httpClientGet('api/AppDokterRawatJalan/GetPaketPMByDok/' + kddok + '/' + kdlyn, data)
      .subscribe(result => {
        data = result;
        this.dataListPaketPM = data;
      },
        err => {
          this.dataListPaketPM = [];
          this.loading = false;
        });
  };

  // Pemberian Nama paket pelayanan penunjang medis
  dataPaketPM: any;
  dataPostPM: any;
  namaPaketPM: string;
  infoPM: string = null;
  arrCustome = [];
  onSubmitPaketPM() {
    this.loading = true;
    const obj = {
      kd_dokter: this.useraccessdata.kd_dokter,
      nm_template: this.namaPaketPM,
      kd_poli: this.useraccessdata.kd_poli,
      kd_lyn: this.kd_pmlyn
    }
    if (this.arr_dataPemeriksaan.length > 0) {
      for (var i = 0; i < this.arr_dataPemeriksaan.length; i++) {
        this.arrCustome.push(this.arr_dataPemeriksaan[i]);
      }
    }
    if (this.arr_editdataPemeriksaan.length > 0) {
      for (var i = 0; i < this.arr_editdataPemeriksaan.length; i++) {
        this.arrCustome.push(this.arr_editdataPemeriksaan[i]);
      }
    }
    const arrFil = this.arrCustome;
    this.service.post('api/AppDokterRawatJalan/InstPaketPM', JSON.stringify(obj))
      .subscribe(result => {
        const obj2 = {
          kd_dokter: this.useraccessdata.kd_dokter,
          data: arrFil,
          kd_poli: this.useraccessdata.kd_poli
        }
        this.service.post('api/AppDokterRawatJalan/InstPaketPMDT', JSON.stringify(obj2))
          .subscribe(result => {
            this.getPaketPM();
            this.namaPaketPM = '';
            this.loading = false;
            const paramLog = {
              NoReg: this.noreg,
              Kd_Dokter: this.useraccessdata.kd_dokter,
              Aksi: 'MEMBUAT PAKET PENUNJANG MEDIS',
              Jenis: '1'
            }

            this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
              .subscribe(result => {
                this.loading = false;
              },
                err => {
                  this.loading = false;
                });
          }, error => {
            this.arrCustome = [];
            this.namaPaketPM = '';
            this.loading = false;
          });
      }, error => {
        this.arrCustome = [];
        this.namaPaketPM = '';
        this.getPaketPM();
        this.loading = false;
      });
  }

  onBatalPaketPM() {
    this.namaPaketPM = '';
  }

  arrCust: any = []
  SelectPaketPM(item: any) {
    this.loading = true;
    var kddok = this.useraccessdata.kd_dokter;
    var kdtemp = item;
    this.service.httpClientGet('api/AppDokterRawatJalan/GetPemeriksaanByPaketPM/' + kddok + '/' + kdtemp + '/' + this.kd_pmlyn, '')
      .subscribe(result => {
        this.arrCust = result;
        for (const o of this.arrCust) {
          this.onSelectPemeriksaan(o.Kd_Tindakan, o.Jenis_Pelayanan);
        }
        this.loading = false;
      },
        err => {
          this.loading = false;
        });

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'MENGGUNAKAN PAKET PENUNJANG MEDIS',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  DelPaketPM(kdtemp) {
    this.loading = true;
    const obj = {
      kd_dokter: this.useraccessdata.kd_dokter,
      kd_template: kdtemp,
      kd_lyn: this.kd_pmlyn
    }
    this.service.post('api/AppDokterRawatJalan/DelPaketPM', JSON.stringify(obj))
      .subscribe(result => {
        this.getPaketPM();
        this.loading = false;
      }, error => {
        this.loading = false;
      });

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'DELETE PAKET PENUNJANG MEDIS',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  // Pemberian Nama paket racik baru
  dataPaketObatRacikNew: any;
  dataObatRacikNew: any;
  namaPaketRacik: string;
  satuan: string;
  aturanPakai: string;
  kdTempalate: string = '';
  onSubmitPaketObatRacik() {
    this.loading = true;
    var inputParamInsertPaketRacik = {
      kd_dokter: this.useraccessdata.kd_dokter,
      nm_template: this.namaPaketRacik,
      kd_poli: this.useraccessdata.kd_poli,
      satuan: this.satuan,
      aturan_pakai: this.aturanPakai
    }

    for (var i = 0; i < this.dataObatR.length; i++) {
      this.dataRacik.push(this.dataObatR[i]);
    }

    if (this.dataRacik.length == 0) {
      this.loading = false;
    } else if (this.dataRacik.length != 0) {

      const paramLog = {
        NoReg: this.noreg,
        Kd_Dokter: this.useraccessdata.kd_dokter,
        Aksi: 'MEMBUAT PAKET OBAT RACIK',
        Jenis: '1'
      }

      this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
        .subscribe(result => {
          this.loading = false;
        },
          err => {
            this.loading = false;
          });

      this.service.post('api/AppDokterRJResepRacik/PostApSOTemplate_Resep_Racik', JSON.stringify(inputParamInsertPaketRacik))
        .subscribe(result => {
          this.dataPaketObatRacikNew = JSON.parse(result);
          this.kdTempalate = this.dataPaketObatRacikNew.Kd_Template;

          this.namaPaketRacik = '';
          this.satuan = '';
          this.aturanPakai = '';
          this.getPaketObatRacik();
          this.loading = false;

          var inputParamInsertPaketObatRacik = {
            kd_dokter: this.useraccessdata.kd_dokter,
            kd_template: this.kdTempalate,
            kd_poli: this.useraccessdata.kd_poli,
            data: this.dataRacik
          }
          this.service.post('api/AppDokterRJResepRacik/PostApSOTemplate_Resep_Racik_dt', JSON.stringify(inputParamInsertPaketObatRacik))
            .subscribe(result => {
              this.dataObatRacikNew = JSON.parse(result);
              this.dataObatR = [];
              this.dataRacik = [];
            },
              err => {
                this.loading = false;
              });
        },
          error => {
            this.namaPaketRacik = '';
            this.satuan = '';
            this.aturanPakai = '';
            this.loading = false;
          });

    }
  }

  onBatalPaketObatRacik() {
    this.namaPaketRacik = '';
  }


  /** ANTRIAN */
  dataPanggil: any;
  ticketid: string = '';
  ticketstatus: string = '';

  value_panggil: string = '1';
  value_lewat: string = '2';
  // value_selesai: string = '3';
  onSubmitTicketPanggil(id, noreg) {
    this.loading = true;
    // this.getTicketPasien();
    this.ticketid = id;
    var nopoli = this.useraccessdata.no_poli;
    this.ticketstatus = this.value_panggil;

    var paramPanggil = {
      ticket_id: this.ticketid,
      counter_no: nopoli,
      ticket_status: this.ticketstatus
    }
    // console.log("Panggil")
    this.service.post('api/AppDokterRJInfo/AntrianTiket', JSON.stringify(paramPanggil))
      .subscribe(result => {
        // console.log("Panggil: ", result)
        this.getPasienRJ();
        this.loading = false;
      },
        err => {
          this.loading = false;
        });

    const paramLog = {
      NoReg: noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'MEMANGGIL PASIEN',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  no_karcis = 0;
  data_broadcast: any;
  noreg_broadcast = '';
  data_cekTicketSts: any;
  value_TicketSts = '';
  broadcast(no_karcis) {
    this.no_karcis = +no_karcis + 2;

    var paramBroadcast = {
      no_karcis: this.no_karcis,
      tanggal_masuk: this.useraccessdata.tgl_hari_ini,
      kode_dokter: this.useraccessdata.kd_dokter,
      jam_praktek: this.useraccessdata.jam_praktek
    }
    this.service.post('api/AppDokterRawatJalan/Broadcast', JSON.stringify(paramBroadcast))
      .subscribe(result => {
        this.data_broadcast = JSON.parse(result);
        if (Object.keys(this.data_broadcast).length === 0 && this.data_broadcast.constructor === Object) { } else {
          this.noreg_broadcast = this.data_broadcast.No_Reg;

          // cek status ticket 
          // jika ticket sts 1 jgn di broadcast
          // jika ticket sts 0 di broadcat
          // console.log('api/AppDokterRawatJalan/CekTicketStstus/' + this.noreg_broadcast, '');
          this.service.httpClientGet('api/AppDokterRawatJalan/CekTicketStstus/' + this.noreg_broadcast, '')
            .subscribe(result => {
              this.data_cekTicketSts = result;
              this.value_TicketSts = this.data_cekTicketSts.Ticket_Status;
              if (this.value_TicketSts == '1') { } else if (this.value_TicketSts == '0') {
                // send broadcast
                var paramSendBC = {
                  no_reg: this.noreg_broadcast
                }
                this.service.post('api/AppDokterRawatJalan/SendBroadcast', JSON.stringify(paramSendBC))
                  .subscribe(result => { });
              }
            });
        }
      })
  }

  broadcastWA(noreg) {
    var paramData = {
      Kd_Lyn: this.useraccessdata.kode_lyn,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Jam_Praktek: this.useraccessdata.jam_praktek,
      rj_noreg: noreg
    }
    // console.log(paramData)
    this.service.post('api/AppDokterRJInfo/BroadcastWA', JSON.stringify(paramData))
      .subscribe(result => {
        // console.log("Panggil: ", result)
        this.getPasienRJ();
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  onSubmitTicketLewat(id, noreg) {
    this.loading = true;
    this.ticketid = id;
    this.ticketstatus = this.value_lewat;
    var nopoli = this.useraccessdata.no_poli;
    var paramLewat = {
      ticket_id: this.ticketid,
      counter_no: nopoli,
      ticket_status: this.ticketstatus
    }
    this.service.post('api/AppDokterRJInfo/AntrianTiket', JSON.stringify(paramLewat))
      .subscribe(result => {
        this.getPasienRJ();
        this.loading = false;
      },
        err => {
          this.loading = false;
        });

    const paramLog = {
      NoReg: noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'MENEKAN TOMBOL LEWATI PASIEN',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }
  /** ANTRIAN */

  deleteAllResep() {
    this.loading = true;
    // console.log("Delete All Resep");
    var paramDeleteAllResep = {
      no_so: this.noso_update
    }
    this.service.post('api/AppDokterRJResepNonRacik/DeleteAllResep', JSON.stringify(paramDeleteAllResep))
      .subscribe(result => {
        // console.log(result)
      },
        err => {
          this.loading = false;
        });

    this.getStatusPanggil(this.ticket_id, this.ticket_posisi);
    var ticket_posisiInt;
    if (this.ticket_posisi == '1. Tunai' || this.ticket_posisi == '2. Kartu Kredit' || this.ticket_posisi == '3. Kartu Debet' || this.ticket_posisi == '5. Keringanan' || this.ticket_posisi == '6. FOC') {
      ticket_posisiInt = '3';
      var paramUpdSttsPanggil = {
        ticket_id: this.ticket_id,
        ticket_posisi: ticket_posisiInt
      }
      this.service.post('api/AppDokterRJInfo/UpdStsPanggil', JSON.stringify(paramUpdSttsPanggil))
        .subscribe(result => {

        });
    } else {
      ticket_posisiInt = '4';
      var paramUpdSttsPanggil = {
        ticket_id: this.ticket_id,
        ticket_posisi: ticket_posisiInt
      }
      this.service.post('api/AppDokterRJInfo/UpdStsPanggil', JSON.stringify(paramUpdSttsPanggil))
        .subscribe(result => {

        });
    }

    setTimeout(() => {
      this.getPasienRJ();
      this.loading = false;
    }, 1000)
  }

  deleteNonRacik(noso, no_urut) {
    this.loading = true;
    var paramDeleteApNonRacik = {
      no_so: noso,
      no_urut: no_urut
    }
    this.service.post('api/AppDokterRJResepNonRacik/DeleteApNonRacik', JSON.stringify(paramDeleteApNonRacik))
      .subscribe(result => {
        this.dataEditNamaRacik = [];
        this.getResepObatNonRacik(); // reload data obat non racik
        this.func_getResepRacik(); //reload data obat racik
        this.loading = false;
      },
        err => {
          this.loading = false;
        });

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'DELETE OBAT NON RACIK',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  // get parameter delete racik ApSORacikH
  deleteRacikApSORacikH(noso, kdracik) {
    this.loading = true;
    var paramDeleteApSORacikH = {
      no_so: noso,
      kd_racik: kdracik
    }
    this.service.post('api/AppDokterRJResepRacik/DeleteApSORacikH', JSON.stringify(paramDeleteApSORacikH))
      .subscribe(result => {
        this.dataEditNamaRacik = [];
        this.getResepObatNonRacik(); // reload data obat non racik
        this.func_getResepRacik(); //reload data obat racik
        this.loading = false;
      },
        err => {
          this.loading = false;
        });

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'DELETE RESEP OBAT RACIK',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  // get parameter delete list obat racik ApSORacikD
  deleteResepApSORacikD(noso, kdracik, kdbrg) {
    this.loading = true;
    var paramDeleteApSORacikD = {
      no_so: noso,
      kd_racik: kdracik,
      kd_brg: kdbrg
    }
    this.service.post('api/AppDokterRJResepRacik/DeleteApSORacikD', JSON.stringify(paramDeleteApSORacikD))
      .subscribe(result => {
        this.getResepObatRacikDetail(this.no_so, this.Listkd_racik, this.Listnm_racik, this.Listno_urut, this.Listjml_bnyk,
          this.Listsatuan, this.Listdosis, this.Listinfo_obat, this.Listcatatan_racik, this.Listket_racik);

        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  // Update Status Panggil
  ticket_id: string = '';
  ticket_posisi: string = '';
  getStatusPanggil(ticketid, ticketposisi) {
    this.ticket_id = ticketid;
    this.ticket_posisi = ticketposisi;
  }

  rjNoreg: string = '';
  namaPasien: string = '';
  getNoRegForDiag(rjNoReg, namaPasien, ticketid) {
    this.rjNoreg = rjNoReg;
    this.namaPasien = namaPasien;
    this.ticketid = ticketid;
  }

  // ------------------- START DIAGNOSA -------------------- //

  // new tgl 05-09-2019
  arr_dataDiagnosa = [];
  arr_dataDiagnosaD = [];
  // select diagnosa utama 19092019
  kd_icd = '';
  ket_icd = '';
  diagUtamaPilih = '';
  diagUtamaDetailPilih = '';
  onSelectDiagUtama(value) {
    let dataDiagUtama = value["originalObject"];
    dataDiagUtama.TERJEMAHAN.replace("\'", "");
    let dataDiagUtama2 = {
      ...dataDiagUtama,
      TERJEMAHAN: dataDiagUtama.TERJEMAHAN.replace("\'", "")
    }
    this.kd_icd = dataDiagUtama2.ICD_CODE;
    this.ket_icd = dataDiagUtama2.TERJEMAHAN;
    this.diagUtamaPilih = dataDiagUtama2.ListSearch;
    this.strDiagUtama = '';

    console.log(dataDiagUtama2.ListSearch);

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'MENGINPUT DIAGNOSA UTAMA ' + dataDiagUtama2.ListSearch,
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }
  onSaveDiagUtama() {
    if (this.strDiagUtama != '') {
      this.ket_icd = this.strDiagUtama;
      this.diagUtamaPilih = this.strDiagUtama;
    }
    // console.log(this.ket_icd);
  }
  onSelectDiagnosaNew(value) {
    let data = value["originalObject"];
    data.DESCRIPTION.replace("\'", "");
    let data2 = {
      ...data,
      DESCRIPTION: data.DESCRIPTION.replace("\'", "")
    }
    this.arr_dataDiagnosa.push(data2);
    this.strDiagTambahan = '';

    console.log(data.DESCRIPTION.replace("\'", ""))

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'MENGINPUT DIAGNOSA TAMBAHAN ' + data.DESCRIPTION.replace("\'", ""),
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }
  onSaveTmpDiagnosa() {
    let dt = new Date()
    let value = this.datePipe.transform(dt, 'HH:mm:s');
    if (this.strDiagTambahan != '') {
      var diagNoDB = {
        ICD_CODE: value,
        DESCRIPTION: this.strDiagTambahan
      }
      this.arr_dataDiagnosa.push(diagNoDB);
    }
  }
  deleteFieldDiagnosa(index) {
    this.arr_dataDiagnosa.splice(index, 1);
  }
  // new tgl 05-09-2019

  // for detail diagnosa

  onSelectDiagDetailUtama(value) {
    let dataDiagDetailUtama = value["originalObject"];
    dataDiagDetailUtama.TERJEMAHAN.replace("\'", "");
    let dataDiagDetailUtama2 = {
      ...dataDiagDetailUtama,
      TERJEMAHAN: dataDiagDetailUtama.TERJEMAHAN.replace("\'", "")
    }
    this.kd_icd = dataDiagDetailUtama2.ICD_CODE;
    this.ket_icd = dataDiagDetailUtama2.TERJEMAHAN;
    this.diagUtamaDetailPilih = dataDiagDetailUtama2.ListSearch;
    this.strDiagUtamaDetail = '';
    // console.log(dataDiagDetailUtama2);

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'MENAMBAHKAN DIAGNOSA UTAMA ( ' + dataDiagDetailUtama2.ListSearch + ' )',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }
  onSaveDiagDetailUtama() {
    if (this.strDiagUtamaDetail != '') {
      this.ket_icd = this.strDiagUtamaDetail;
      this.diagUtamaDetailPilih = this.strDiagUtamaDetail;
    }
    // console.log(this.diagUtamaDetailPilih);
  }

  // new tgl 03-09-2019 for diagnosa detail
  onSelectDiagnosaNewDetail(value) {
    let data = value["originalObject"];
    data.DESCRIPTION.replace("\'", "");
    let data2 = {
      ...data,
      DESCRIPTION: data.DESCRIPTION.replace("\'", "")
    }
    this.arr_dataDiagnosaD.push(data2);
    this.strDiagTambahanDetail = '';

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'MENAMBAHKAN DIAGNOSA TAMBAHAN ( ' + data.DESCRIPTION.replace("\'", "") + ' )',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }
  onSaveTmpDiagnosaDetail() {
    let dt = new Date()
    let value = this.datePipe.transform(dt, 'HH:mm:s');
    if (this.strDiagTambahanDetail != '') {
      var diagNoDB = {
        ICD_CODE: value,
        DESCRIPTION: this.strDiagTambahanDetail
      }
      this.arr_dataDiagnosaD.push(diagNoDB);
    }
  }
  deleteFieldDiagnosaDetail(index) {
    this.arr_dataDiagnosaD.splice(index, 1);
  }
  // for detail diagnosa

  // Pemeriksaan
  strPemeriksaanFisik: string = '';
  strPemeriksaanFisikD: string = '';
  strDiagUtamaD: string = '';

  // Planning in Dignosa
  strPlan: string = '';
  strPlanD: string = '';

  dataInsertDiagnosaDetail: any;
  user_id: string = '';

  onSubmitAllDiagnosa() {
    this.loading = true;
    this.user_id = this.useraccessdata.kd_dokter;
    this.kd_plyn = this.useraccessdata.kode_lyn;

    // skrinning / keluhan utama
    var paramUpdSkrinning = {
      KeluhanUtama: this.keluhanUtama,
      NoReg: this.rjNoreg
    }
    this.service.put('api/AppDokterRJDiag/UpdateSkrinningPasien', JSON.stringify(paramUpdSkrinning))
      .subscribe(result => {
        this.keluhanUtama = '';
      },
        err => {
          this.loading = false;
        });

    var paramInsDiagnosa = {
      RJ_NoReg: this.rjNoreg,
      Kd_Lyn: this.kd_plyn,
      Kd_Dokter: this.user_id
    }

    var paramInsPeriksa = {
      PemeriksaanFisik: this.strPemeriksaanFisik,
      Planning: this.strPlan,
      RJ_NoReg: this.rjNoreg
    }

    // update diagnosa utama 19-09-2019
    var paraDiagUtama = {
      RJ_NoReg: this.rjNoreg,
      Kd_ICD: this.kd_icd,
      Ket_ICD: this.ket_icd
    }

    var paramDiagnosaDetail = {
      RJ_NoReg: this.rjNoreg,
      UserID: this.user_id,
      data: this.arr_dataDiagnosa
    }

    // api insert RJ Diagnosa
    this.service.post('api/AppDokterRJDiag/InsertRJDiagnosa', JSON.stringify(paramInsDiagnosa))
      .subscribe(result => {

        // api update Pemeriksaan Fisik
        this.service.put('api/AppDokterRJDiag/UpdatePemeriksaanFisik', JSON.stringify(paramInsPeriksa))
          .subscribe(result => {
            this.strPemeriksaanFisik = '';
            this.strPlan = '';
          });

        // api udate diagnosa utama 19-09-19
        // console.log('api/AppDokterRJDiag/UpdateRJDiagnosa', JSON.stringify(paraDiagUtama));
        this.service.put('api/AppDokterRJDiag/UpdateRJDiagnosa', JSON.stringify(paraDiagUtama))
          .subscribe(result => {
            this.diagUtamaDetailPilih = '';
          },
            error => {
              this.loading = false;
            });

        // api insert diagnosa detail
        if (this.arr_dataDiagnosa.length != 0) {
          // baru
          this.service.post('api/AppDokterRJDiag/InsertDiagnosaDetail', JSON.stringify(paramDiagnosaDetail))
            .subscribe(result => {
              this.dataInsertDiagnosaDetail = JSON.parse(result);
              this.arr_dataDiagnosa = [];
              this.loading = false;
            });
          // baru
        } else {
          this.loading = false;
        }

        const paramLog = {
          NoReg: this.noreg,
          Kd_Dokter: this.useraccessdata.kd_dokter,
          Aksi: 'INPUT DATA DIAGNOSA',
          Jenis: '1'
        }

        this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });

        this.getJasaDokter();
        // reset detail riwayat diagnosa
        this.subjDetailView = '';
        this.ObjDetailView = '';
        this.diagUtamaView = '';
        // this.diagDetailView = '';
        this.planDetailView = '';
        this.dataDetailResepNRacik = [];
        this.dataDetailResepRacik = [];

      },
        err => {
          this.loading = false;
        });
  }

  // okok
  dataDiagnosaByNoReg: any;
  dataDiagnosaDetail: any;
  dataDiagnosaTTV: any;
  dataDiagnosaHTTV: any;
  bb: string = '';

  // t t v
  getDiagnosaTTV(noreg) {
    this.dataDiagnosaTTV = [];
    this.bb = '';
    this.service.get('api/AppDokterRJDiag/GetTTVByNoReg/' + noreg, '')
      .subscribe(result => {
        this.dataDiagnosaTTV = JSON.parse(result);
        for (let i = 0; i < this.dataDiagnosaTTV.length; i++) {
          this.bb = this.dataDiagnosaTTV[i].BB
        }
        this.loading = false;
      },
        error => {
          this.dataDiagnosaTTV = [];
          this.loading = false;
        });
  }
  getDiagnosaHTTV(noreg) {
    this.dataDiagnosaHTTV = [];
    this.service.get('api/AppDokterRJDiag/GetTTVByNoReg/' + noreg, '')
      .subscribe(result => {
        this.dataDiagnosaHTTV = JSON.parse(result);
        this.loading = false;
      },
        error => {
          this.dataDiagnosaHTTV = [];
          this.loading = false;
        });
  }
  ttvClear() {
    this.dataDiagnosaHTTV = [];
  }
  // t t v

  getDetailDiagnosa() {
    this.loading = true;
    this.strDiagUtamaDetail = '';
    // skrinning
    this.service.get("api/AppDokterRJDiag/GetSkrinningPasien/" + this.noreg, '')
      .subscribe(result => {
        this.keluhanUtamaD = JSON.parse(result).KeluhanUtama;

        // pemeriksaan fisik & planning
        this.service.get('api/AppDokterRJDiag/GetDiagnosaPasienByNoReg/' + this.noreg, '')
          .subscribe(result => {
            this.dataDiagnosaByNoReg = JSON.parse(result);
            this.strPemeriksaanFisikD = this.dataDiagnosaByNoReg.PemeriksaanFisik;
            this.strDiagUtamaD = this.dataDiagnosaByNoReg.Ket_ICD;
            this.strPlanD = this.dataDiagnosaByNoReg.Planning;

            // diagnosa detail
            this.service.get('api/AppDokterRJDiag/GetDiagnosaDetailByNoReg/' + this.noreg, '')
              .subscribe(result => {
                this.dataDiagnosaDetail = JSON.parse(result);
                this.loading = false;

                const paramLog = {
                  NoReg: this.noreg,
                  Kd_Dokter: this.useraccessdata.kd_dokter,
                  Aksi: 'MELIHAT DETAIL DIAGNOSA PASIEN YANG TELAH DI INPUT',
                  Jenis: '1'
                }

                this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
                  .subscribe(result => {
                    this.loading = false;
                  },
                    err => {
                      this.loading = false;
                    });
              },
                error => {
                  this.loading = false;
                }); // diagnosa detail
          },
            err => {
              this.loading = false;
            }); // pemeriksaan fisik & planning
      },
        err => {
          this.loading = false;
        }); // skrinning
  }

  // okok
  onSubmitAllDiagnosaDetail() {
    this.loading = true;
    this.user_id = this.useraccessdata.kd_dokter;

    // update skrinning
    var paramUpdSkrinningD = {
      KeluhanUtama: this.keluhanUtamaD,
      NoReg: this.noreg
    }
    this.service.put('api/AppDokterRJDiag/UpdateSkrinningPasien', JSON.stringify(paramUpdSkrinningD))
      .subscribe(result => {
        this.keluhanUtamaD = '';
      },
        error => {
          this.loading = false;
        }); // update skrinning

    // update diagnosa utama 18-09-2019
    var paraDiagDetailUtama = {
      RJ_NoReg: this.noreg,
      Kd_ICD: this.kd_icd,
      Ket_ICD: this.ket_icd
    }
    // api update Pemeriksaan Fisik
    this.service.put('api/AppDokterRJDiag/UpdateRJDiagnosa', JSON.stringify(paraDiagDetailUtama))
      .subscribe(result => {
        this.diagUtamaDetailPilih = '';
      },
        error => {
          this.loading = false;
        });

    // baru 05-09-2019
    var paramDiagnosaDetail = {
      RJ_NoReg: this.noreg,
      UserID: this.user_id,
      data: this.arr_dataDiagnosaD
    }
    // update Diagnosa detail
    if (this.arr_dataDiagnosaD.length != 0) {
      this.service.post('api/AppDokterRJDiag/InsertDiagnosaDetail', JSON.stringify(paramDiagnosaDetail))
        .subscribe(result => {
          this.dataInsertDiagnosaDetail = JSON.parse(result);
          this.arr_dataDiagnosaD = [];
          this.loading = false;
        },
          error => {
            this.loading = false;
          });
    } else {
      this.loading = false;
    } // update Diagnosa detail

    var paramInsPeriksa = {
      PemeriksaanFisik: this.strPemeriksaanFisikD,
      Planning: this.strPlanD,
      RJ_NoReg: this.noreg
    }
    // Update pemeriksaan dan plan
    if (this.strPemeriksaanFisikD != '' || this.strPlanD != '') {
      this.service.put('api/AppDokterRJDiag/UpdatePemeriksaanFisik', JSON.stringify(paramInsPeriksa))
        .subscribe(result => {
          this.strPemeriksaanFisikD = '';
          this.strPlanD = '';
        },
          error => {
            this.loading = false;
          });
    } else {
      this.loading = false;
    } // Update pemeriksaan dan plan

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'UPDATE DATA DIAGNOSA PASIEN',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });

    setTimeout(() => {
      this.getPasienRJ();
    }, 1000);
  }

  cancelDetailDiagnosaPasien() {
    this.strDiagTambahan = '';
    this.diagUtamaDetailPilih = '';
    this.strDiagTambahanDetail = '';
  }

  // Reset Riwayat
  resetDetailRiwayat() {
    this.diagUtamaDetailPilih = '';
    this.subjDetailView = '';
    this.ObjDetailView = '';
    this.diagUtamaView = '';
    this.planDetailView = '';
    this.dataDetailResepNRacik = [];
    this.dataDetailResepRacik = [];
    this.dataAllDiagnosa = [];

    this.strPemeriksaanFisik = '';
    this.strPlan = '';

    this.arr_dataDiagnosa = [];
    this.arr_dataDiagnosaD = [];

    this.dataDiagnosaDetail = [];
    this.dataDetailDiagnosaRiwayat = [];
  }
  /** Diagnosa Pasien */

  // Riwayat Diagnosa
  dataAllDiagnosa: any;
  getAllRiwayatDiagnosaPasien(kdrm) {
    this.loading = true;
    this.service.get("api/AppDokterRJDiag/GetAllDiagnosaPasien/" + kdrm, '')
      .subscribe(result => {
        this.dataAllDiagnosa = JSON.parse(result);
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  dataDetailDiagnosa: any;
  dataDetailDiagnosaRiwayat: any;
  dataDetailResepNRacik: any;
  dataDetailResepRacik: any;
  subjDetailView: string = '';
  diagUtamaView: string = '';
  ObjDetailView: string = '';
  // diagDetailView: string = '';
  planDetailView: string = '';
  getDetailRiwayatDiagnosaPasien(kdrm, noreg) {
    this.loading = true;
    this.service.get("api/AppDokterRJDiag/GetDetailDiagnosaPasien/" + kdrm + "/" + noreg, '')
      .subscribe(result => {
        this.dataDetailDiagnosa = JSON.parse(result);
        this.ObjDetailView = this.dataDetailDiagnosa.PemeriksaanFisik;
        this.diagUtamaView = this.dataDetailDiagnosa.Ket_ICD;
        this.planDetailView = this.dataDetailDiagnosa.Planning;
        // console.log(this.dataDetailDiagnosa);

        // diagnosa detail
        this.service.get('api/AppDokterRJDiag/GetDiagnosaDetailByNoReg/' + noreg, '')
          .subscribe(result => {
            this.dataDetailDiagnosaRiwayat = JSON.parse(result);
            this.loading = false;
          },
            error => {
              this.loading = false;
            }); // diagnosa detail

        // skrinning
        this.service.get("api/AppDokterRJDiag/GetSkrinningPasien/" + noreg, '')
          .subscribe(result => {
            this.subjDetailView = JSON.parse(result).KeluhanUtama;
          },
            error => {
              this.loading = false;
            }); // skrinning

        this.service.get("api/AppDokterRJDiag/GetAllRiwayatResepNRacik/" + noreg, '')
          .subscribe(result => {
            this.dataDetailResepNRacik = JSON.parse(result);
          },
            error => {
              this.loading = false;
            });

        this.service.get("api/AppDokterRJDiag/GetAllRiwayatResepRacik/" + noreg, '')
          .subscribe(result => {
            this.dataDetailResepRacik = JSON.parse(result);
          },
            error => {
              this.loading = false;
            });

        const paramLog = {
          NoReg: this.noreg,
          Kd_Dokter: this.useraccessdata.kd_dokter,
          Aksi: 'MELIHAT DETAIL RIWAYAT DIAGNOSA PASIEN',
          Jenis: '1'
        }

        this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });
      },
        error => {
          this.loading = false;
        }); // pemeriksaan fisik
  }
  // ------------------- END DIAGNOSA -------------------- //


  /** ------------------------- Start ALKES ----------------------- */
  getAlkesObatByNoReg() {
    var data: any;
    this.service.httpClientGet('api/Users/GetAlkesObatByNoReg/' + this.noreg, data)
      .subscribe(result => {
        data = result;
        this.dataAlkesObatByNoReg = data;
      });
  }
  deleteListAlkesObat(noreg, kdbrg) {
    this.loading = true;
    var paramDeleteLisAlkesObat = {
      noreg: noreg,
      kdbrg: kdbrg
    }
    this.service.post('api/Users/DeleteListAlkesObat', JSON.stringify(paramDeleteLisAlkesObat))
      .subscribe(result => {
        this.getAlkesObatByNoReg()
        this.loading = false;
      });

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'DELETE DATA ALKES',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  cara_bayar: string = '';
  getCaraBayarPasien(cb) {
    this.cara_bayar = cb;
    // console.log("Cara Bayar Pasien", this.cara_bayar);
  }

  onSelectAlkesObat(value) {
    let data = value["originalObject"];
    if (this.cara_bayar == '6. FOC') {
      this.dataAlkesObat.push(data);
      this.obatStrAlkesObat = '';
    } else {
      this.dataAlkesObat.push(data);
      this.obatStrAlkesObat = '';
    }

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'MENAMBAHKAN ALKES ( ' + data.Nama_Barang + ' )',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
    // console.log(this.dataAlkesObat);
  }

  editQty(value, kd) {
    var index = this.dataAlkesObat.findIndex(x => x.Kode == kd);
    if (index !== -1) {
      if (this.cara_bayar == '6. FOC') {
        this.dataAlkesObat[index].Qty = value;
        this.dataAlkesObat[index].Total = this.dataAlkesObat[index].Qty * this.dataAlkesObat[index].Hrg_Kary;
      } else {
        this.dataAlkesObat[index].Qty = value;
        this.dataAlkesObat[index].Total = this.dataAlkesObat[index].Qty * this.dataAlkesObat[index].Hrg_Jual;
      }
      // console.log(this.dataAlkesObat);
    }
  }

  deleteFieldAlkesObat(index) {
    this.dataAlkesObat.splice(index, 1);
    this.dataAlkesObat = [];
    this.dataAlkesObat_tmp = [];
  }

  dataPostAlkesObat: any;
  dataAlkesObat_tmp = [];
  onSubmitAlkesObat() {
    this.loading = true;

    if (this.cara_bayar == '6. FOC') {
      this.dataAlkesObat.map(item => {
        return {
          Kode: item.Kode,
          QTY: item.Qty,
          HargaSatuan: item.Hrg_Kary,
          Total: item.Total
        }
      }).forEach(item => this.dataAlkesObat_tmp.push(item));
    } else {
      this.dataAlkesObat.map(item => {
        return {
          Kode: item.Kode,
          QTY: item.Qty,
          HargaSatuan: item.Hrg_Jual,
          Total: item.Total
        }
      }).forEach(item => this.dataAlkesObat_tmp.push(item));
    }

    var paramInsertAlkesObat = {
      no_reg: this.noreg,
      data: this.dataAlkesObat_tmp
    }

    if (this.dataAlkesObat_tmp.length == 0) {
      this.loading = false;
    } else if (this.dataAlkesObat_tmp.length != 0) {

      const paramLog = {
        NoReg: this.noreg,
        Kd_Dokter: this.useraccessdata.kd_dokter,
        Aksi: 'INPUT DATA ALKES',
        Jenis: '1'
      }

      this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
        .subscribe(result => {
          this.loading = false;
        },
          err => {
            this.loading = false;
          });

      // console.log('api/Users/PostAlkesObat_RJ', JSON.stringify(paramInsertAlkesObat))
      this.service.post('api/Users/PostAlkesObat_RJ', JSON.stringify(paramInsertAlkesObat))
        .subscribe(result => {
          this.dataPostAlkesObat = JSON.parse(result);
          // console.log(this.dataPostAlkesObat);
          this.dataAlkesObat = [];
          this.dataAlkesObat_tmp = [];
          this.getAlkesObatByNoReg();
          this.loading = false;
        },
          error => {
            this.loading = false;
            this.dataAlkesObat = [];
            this.dataAlkesObat_tmp = [];
          });
    }

  }

  /** ------------------------- END ALKES ----------------------- */

  // ---------------------------- START PENGKAJIAN ------------------------------//
  // Riwayat pegnkajian
  dataRiwayatPengkajian: any;
  getAllRiwayatPengkajian(kd_rm, noreg) {
    // console.log('getAllRiwayatPengkajian : ', kd_rm)
    this.loading = true;
    this.service.get("api/Pengkajian/GetRiwayatPengkajian/" + kd_rm, '')
      .subscribe(result => {
        this.dataRiwayatPengkajian = JSON.parse(result);
        this.loading = false;
      },
        error => {
          this.loading = false;
        });

    const paramLog = {
      NoReg: noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'MELIHAT RIWAYAT PENGKAJIAN PASIEN',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }
  FormPengkajianAnak: boolean = false;
  FormPengkajianDewasa: boolean = false;
  FormPengkajianBedah: boolean = false;
  FormPengkajianBidan: boolean = false;

  // form anak
  AnakForm: FormGroup;
  pengkajian_didapat_dari: string = '';
  pengkajian_didapat_dari_hubungan: string = '';
  tanda_vital_alasan: string = '';
  hubungan_pasien_dengan_keluarga: string = '';
  status_psikologis: string = '';
  status_psikologis_kecenderungan_bunuh_diri: string = '';
  status_psikologis_lainnya: string = '';
  pekerjaan: string = '';
  agama: string = '';
  agama_lainnya: string = '';
  nilai_kepercayaan: string = '';
  nilai_kepercayaan_ya: string = '';
  nutrisi_keluhan: string = '';
  nutrisi_keluhan_ya: string = '';
  nutrisi_perubahan_bb: string = '';
  nutrisi_perubahan_bb_turun: string = '';
  nutrisi_perubahan_bb_naik: string = '';
  stamp_diagnosa_gizi: string = '';
  stamp_asupan_makan: string = '';
  stamp_gizi_pasien: string = '';
  nyeri_diprovokasi_oleh: string = '';
  lokasi_nyeri: string = '';
  penjalaran_nyeri: string = '';
  sifat_nyeri: string = '';
  nyeri: string = '';
  nyeri_hilang_bila: string = '';
  nyeri_hilang_bila_lain_lain: string = '';
  sempoyongan: string = '';
  penopang: string = '';
  hasilrisiko: string = '';
  pengobatan_saat_ini: string = '';
  pengobatan_saat_ini_ya: string = '';
  operasi_yang_dialami: string = '';
  riwayat_alergi: string = '';
  riwayat_alergi_ya: string = '';
  lama_kehamilan: string = '';
  komplikasi_kehamilan: string = '';
  komplikasi_kehamilan_ya: string = '';
  riwayat_persalinan: string = '';
  penyulit_kehamilan: string = '';
  penyulit_kehamilan_ya: string = '';
  rtk_neonatus: string = '';
  rtk_neonatus_ya: string = '';
  rtk_kongenital: string = '';
  rtk_kongenital_jelaskan: string = '';
  rtk_keluh_tumbuh_kembang: string = '';
  riwayat_imunisasi_bcg: string = '';
  riwayat_imunisasi_dpt_1: string = '';
  riwayat_imunisasi_dpt_2: string = '';
  riwayat_imunisasi_dpt_3: string = '';
  riwayat_imunisasi_dpt_ulangan_1: string = '';
  riwayat_imunisasi_dpt_ulangan_2: string = '';
  riwayat_imunisasi_polio_1: string = '';
  riwayat_imunisasi_polio_2: string = '';
  riwayat_imunisasi_polio_3: string = '';
  riwayat_imunisasi_polio_4: string = '';
  riwayat_imunisasi_hib_1: string = '';
  riwayat_imunisasi_hib_2: string = '';
  riwayat_imunisasi_hib_3: string = '';
  riwayat_imunisasi_hepatitis_1: string = '';
  riwayat_imunisasi_hepatitis_2: string = '';
  riwayat_imunisasi_hepatitis_3: string = '';
  riwayat_imunisasi_campak: string = '';
  riwayat_imunisasi_lain_lain: string = '';
  riwayat_imunisasi_terakhir_diimunisasi: string = '';
  riwayat_imunisasi_dimana: string = '';
  gangguan_bicara: string = '';
  gangguan_bicara_kapan: string = '';
  bahasa_sehari_hari: string = '';
  bahasa_sehari_hari_daerah: string = '';
  bahasa_sehari_hari_lainnya: string = '';
  perlu_penterjemah: string = '';
  perlu_penterjemah_bahasa: string = '';
  bahasa_isyarat: string = '';
  hambatan_belajar: string = '';
  cara_belajar_yang_disukai: string = '';
  tingkatan_pendidikan: string = '';
  potensi_kebutuhan_pembelajaran: string = '';
  ketersediaan_menerima_informasi: string = '';

  // form dewasa
  DewasaForm: FormGroup;
  cacat_tubuh: string = '';
  alat_bantu: string = '';
  prothesa: string = '';
  adl: string = '';
  hubungan_keluarga: string = '';
  kecenderungan_bunuh_diri: string = '';
  lainnya: string = '';
  perkerjaan: string = '';
  penuruan_bb: string = '';
  kesulitan_menerima_makanan: string = '';
  diagnosa_berhubungan_dengan_gizi: string = '';
  skormaa: number = 0;
  skormab: number = 0;
  skormac: number = 0;
  skorma: number = 0;
  risikoma: string = '';
  skriningrisiko: string = '';
  bicara: string = '';
  bicara_sejak: string = '';
  bahasa_daerah: string = '';
  bahasa_lainnya: string = '';
  cara_belajar: string = '';
  tingkat_pendidikan: string = '';
  pasien_bersedia_menerima_edukasi: string = '';
  nyeri_hilang_bila_lainnya: string = '';

  // form bedah
  BedahForm: FormGroup;
  MST1: number = 0;
  MST2: number = 0;
  MSTSkor: number = 0;
  MSTKeterangan: string = '';
  pasienanak: boolean = false;
  pasiendewasa: boolean = false;
  pasienlansia: boolean = false;
  DewasaRiwayatJatuh: number = 0;
  DewasaTerpasangAlat: number = 0;
  DewasaDiagnosaSekunder: number = 0;
  DewasaAlatBantu: number = 0;
  DewasaGayaBerjalan: number = 0;
  DewasaTotal: number = 0;
  DewasaKeterangan: string = '';
  AnakUmur: number = 0;
  AnakJenisKelamin: number = 0;
  AnakDiagnosa: number = 0;
  AnakGangguanKognirif: number = 0;
  AnakFaktorLingkungan: number = 0;
  AnakResponTerhadap: number = 0;
  AnakPenggunaanObat: number = 0;
  AnakTotal: number = 0;
  AnakKeterangan: string = '';
  LansiaRiwayatJatuh: number = 0;
  LansiaStatusMental: number = 0;
  LansiaPenglihatan: number = 0;
  LansiaKebiasaanBerkemih: number = 0;
  LansiaTransfer: number = 0;
  LansiaMobilitas: number = 0;
  LansiaTranferMobilitas: number = 0;
  LansiaTotalSkor: number = 0;
  LansiaKeterangan: string = '';
  penurunan_bb: string = '';
  penurunan_nafsu_makan: string = '';
  dewasa_riwayat_jatuh: string = '';
  dewasa_diagnosa_sekunder: string = '';
  dewasa_alat_bantu: string = '';
  dewasa_terpasang_infus: string = '';
  dewasa_gaya_berjalan: string = '';
  anak_umur: string = '';
  anak_jenis_kelamin: string = '';
  anak_diagnosa: string = '';
  anak_gangguan_kognitif: string = '';
  anak_faktor_lingkungan: string = '';
  anak_respon_terhadap_operasi: string = '';
  anak_penggunaan_obat: string = '';
  lansia_riwayat_jatuh_karena_jatuh: string = '';
  lansia_riwayat_jatuh_dalam_dua_bulan: string = '';
  lansia_status_mental_delirium: string = '';
  lansia_status_mental_disorientasi: string = '';
  lansia_status_mental_agitasi: string = '';
  lansia_pengelihatan_kacamata: string = '';
  lansia_pengelihatan_buram: string = '';
  lansia_pengelihatan_katarak: string = '';
  lansia_kebiasaan_berkemih: string = '';
  lansia_transfer: string = '';
  lansia_mobilitas: string = '';
  nilai_keyakinan: string = '';
  nilai_keyakinan_jabarkan: string = '';

  // form bidan
  BidanForm: FormGroup;
  MST3: number = 0;
  DewasaStatusMental: number = 0;
  keluhan_saat_masuk: string = '';
  fungsional_alat_bantu: string = '';
  fungsional_prothesa: string = '';
  fungsional_cacat_tubuh: string = '';
  fungsional_adl: string = '';
  psikososial_hubungan_pasien_keluarga: string = '';
  psikososial_status_psikologis: string = '';
  psikososial_support_keluarga: string = '';
  psikososial_pengetahuan_tentang_kehamilan: string = '';
  psikososial_kecenderungan_bunuh_diri: string = '';
  psikososial_lainnya: string = '';
  spiritual_agama: string = '';
  spiritual_agama_lainnya: string = '';
  dewasa_status_mental: string = '';
  riwayat_kesehatan_penyakit_yang_diderita: string = '';
  riwayat_kesehatan_pengobatan_di_rumah: string = '';
  operasi_yang_pernah_dialami: string = '';
  faktor_keturunan_gemeli: string = '';
  riwayat_penyakit_herediter: string = '';
  riwayat_penyakit_herediter_jelaskan: string = '';
  riwayat_penyakit_keluarga: string = '';
  ketergantungan_terhadap: string = '';
  ketergantungan_terhadap_lainnya: string = '';
  riwayat_perkerjaan_berhubungan_dengan_zat: string = '';
  riwayat_perkerjaan_berhubungan_dengan_zat_ya: string = '';
  riwayat_obstetri_menarche: string = '';
  riwayat_obstetri_menstruasi: string = '';
  riwayat_obstetri_menstruasi_teratur: string = '';
  riwayat_obstetri_menstruasi_tidak_teratur: string = '';
  riwayat_obstetri_sakit_menstruasi: string = '';
  riwayat_obstetri_menikah_yang_ke: string = '';
  riwayat_obstetri_menikah_lama: string = '';
  riwayat_obstetri_kontrasepsi_yang_pernah_digunakan: string = '';
  riwayat_obstetri_kontrasepsi_lamanya: string = '';
  data_kehamilan_sekarang_g: string = '';
  data_kehamilan_sekarang_p: string = '';
  data_kehamilan_sekarang_a: string = '';
  data_kehamilan_sekarang_hpht: string = '';
  data_kehamilan_sekarang_hpl: string = '';
  data_kehamilan_sekarang_keluhan: string = '';
  bahasa_bahasa: string = '';
  perlu_penterjemah_isyarat: string = '';
  habatan_belajar: string = '';

  getdatarj(rj: string, pengkajian) {
    this.loading = true;
    if (pengkajian == 'ANAK') this.formAnak();
    if (pengkajian == 'DEWASA') this.formDewasa();
    if (pengkajian == 'BEDAH') this.formBedah();
    if (pengkajian == 'BIDAN') this.formBidan();
    let params = new HttpParams()
    params = params.append('auto_duplicate', '0')
    this.httpclient.get(`${this.apiAskep}rj/pasien/${rj}`, { params: params, observe: 'response' }).subscribe(
      res => {
        this.loading = false;
        // console.log(res.body['data'])
        if (pengkajian == 'ANAK') {
          this.FormPengkajianAnak = true;
          this.AnakForm.patchValue({
            ...res.body['data']
          });
          this.pengkajian_didapat_dari = res.body['data'].pengkajian_didapat_dari;
          this.pengkajian_didapat_dari_hubungan = res.body['data'].pengkajian_didapat_dari_hubungan;
          this.tanda_vital_alasan = res.body['data'].tanda_vital_alasan;
          this.hubungan_pasien_dengan_keluarga = res.body['data'].hubungan_pasien_dengan_keluarga;
          this.status_psikologis = res.body['data'].status_psikologis;
          this.status_psikologis_kecenderungan_bunuh_diri = res.body['data'].status_psikologis_kecenderungan_bunuh_diri;
          this.status_psikologis_lainnya = res.body['data'].status_psikologis_lainnya;
          this.pekerjaan = res.body['data'].pekerjaan;
          this.agama = res.body['data'].agama;
          this.agama_lainnya = res.body['data'].agama_lainnya;
          this.nilai_kepercayaan = res.body['data'].nilai_kepercayaan;
          this.nilai_kepercayaan_ya = res.body['data'].nilai_kepercayaan_ya;
          this.nutrisi_keluhan = res.body['data'].nutrisi_keluhan;
          this.nutrisi_keluhan_ya = res.body['data'].nutrisi_keluhan_ya;
          this.nutrisi_perubahan_bb = res.body['data'].nutrisi_perubahan_bb;
          if (res.body['data'].nutrisi_perubahan_bb_turun == '1') {
            this.nutrisi_perubahan_bb_turun = 'Penurunan berat badan';
          } else {
            this.nutrisi_perubahan_bb_turun = 'Kenaikan berat badan';
          }
          this.nutrisi_perubahan_bb_naik = res.body['data'].nutrisi_perubahan_bb_naik;
          this.stamp_diagnosa_gizi = res.body['data'].stamp_diagnosa_gizi;
          this.stamp_asupan_makan = res.body['data'].stamp_asupan_makan;
          this.stamp_gizi_pasien = res.body['data'].stamp_gizi_pasien;
          this.nyeri_diprovokasi_oleh = res.body['data'].nyeri_diprovokasi_oleh;
          if (res.body['data'].skala_nyeri <= 3) {
            this.sifat_nyeri = 'Ringan';
          } else if (res.body['data'].skala_nyeri <= 6) {
            this.sifat_nyeri = 'Sedang';
          } else {
            this.sifat_nyeri = 'Berat';
          }
          this.lokasi_nyeri = res.body['data'].lokasi_nyeri;
          this.penjalaran_nyeri = res.body['data'].penjalaran_nyeri;
          if (res.body['data'].nyeri == '1') {
            this.nyeri = 'Nyeri akut';
          } else {
            this.nyeri = 'Nyeri kronis';
          }
          this.nyeri_hilang_bila = res.body['data'].nyeri_hilang_bila;
          this.nyeri_hilang_bila_lain_lain = res.body['data'].nyeri_hilang_bila_lain_lain;
          this.sempoyongan = res.body['data'].sempoyongan;
          this.penopang = res.body['data'].penopang;
          if (res.body['data'].sempoyongan == '1' && res.body['data'].penopang == '1') {
            this.hasilrisiko = "Risiko tinggi";
          } else if (res.body['data'].sempoyongan == '2' && res.body['data'].penopang == '2') {
            this.hasilrisiko = "Tidak berisiko";
          } else {
            this.hasilrisiko = "Risiko Rendah";
          }
          this.pengobatan_saat_ini = res.body['data'].pengobatan_saat_ini;
          this.pengobatan_saat_ini_ya = res.body['data'].pengobatan_saat_ini_ya;
          this.operasi_yang_dialami = res.body['data'].operasi_yang_dialami;
          this.riwayat_alergi = res.body['data'].riwayat_alergi;
          this.riwayat_alergi_ya = res.body['data'].riwayat_alergi_ya;
          this.lama_kehamilan = res.body['data'].lama_kehamilan;
          this.komplikasi_kehamilan = res.body['data'].komplikasi_kehamilan;
          this.komplikasi_kehamilan_ya = res.body['data'].komplikasi_kehamilan_ya;
          this.riwayat_persalinan = res.body['data'].riwayat_persalinan;
          this.penyulit_kehamilan = res.body['data'].penyulit_kehamilan;
          this.penyulit_kehamilan_ya = res.body['data'].penyulit_kehamilan_ya;
          this.rtk_neonatus = res.body['data'].rtk_neonatus;
          this.rtk_neonatus_ya = res.body['data'].rtk_neonatus_ya;
          this.rtk_kongenital = res.body['data'].rtk_kongenital;
          this.rtk_kongenital_jelaskan = res.body['data'].rtk_kongenital_jelaskan;
          this.rtk_keluh_tumbuh_kembang = res.body['data'].rtk_keluh_tumbuh_kembang;
          this.riwayat_imunisasi_bcg = res.body['data'].riwayat_imunisasi_bcg;
          this.riwayat_imunisasi_dpt_1 = res.body['data'].riwayat_imunisasi_dpt_1;
          this.riwayat_imunisasi_dpt_2 = res.body['data'].riwayat_imunisasi_dpt_2;
          this.riwayat_imunisasi_dpt_3 = res.body['data'].riwayat_imunisasi_dpt_3;
          this.riwayat_imunisasi_dpt_ulangan_1 = res.body['data'].riwayat_imunisasi_dpt_ulangan_1;
          this.riwayat_imunisasi_dpt_ulangan_2 = res.body['data'].riwayat_imunisasi_dpt_ulangan_2;
          this.riwayat_imunisasi_polio_1 = res.body['data'].riwayat_imunisasi_polio_1;
          this.riwayat_imunisasi_polio_2 = res.body['data'].riwayat_imunisasi_polio_2;
          this.riwayat_imunisasi_polio_3 = res.body['data'].riwayat_imunisasi_polio_3;
          this.riwayat_imunisasi_polio_4 = res.body['data'].riwayat_imunisasi_polio_4;
          this.riwayat_imunisasi_hib_1 = res.body['data'].riwayat_imunisasi_hib_1;
          this.riwayat_imunisasi_hib_2 = res.body['data'].riwayat_imunisasi_hib_2;
          this.riwayat_imunisasi_hib_3 = res.body['data'].riwayat_imunisasi_hib_3;
          this.riwayat_imunisasi_hepatitis_1 = res.body['data'].riwayat_imunisasi_hepatitis_1;
          this.riwayat_imunisasi_hepatitis_2 = res.body['data'].riwayat_imunisasi_hepatitis_2;
          this.riwayat_imunisasi_hepatitis_3 = res.body['data'].riwayat_imunisasi_hepatitis_3;
          this.riwayat_imunisasi_campak = res.body['data'].riwayat_imunisasi_campak;
          this.riwayat_imunisasi_lain_lain = res.body['data'].riwayat_imunisasi_lain_lain;
          this.riwayat_imunisasi_terakhir_diimunisasi = res.body['data'].riwayat_imunisasi_terakhir_diimunisasi;
          this.riwayat_imunisasi_dimana = res.body['data'].riwayat_imunisasi_dimana;
          this.gangguan_bicara = res.body['data'].gangguan_bicara;
          this.gangguan_bicara_kapan = res.body['data'].gangguan_bicara_kapan;
          this.bahasa_sehari_hari = res.body['data'].bahasa_sehari_hari;
          this.bahasa_sehari_hari_daerah = res.body['data'].bahasa_sehari_hari_daerah;
          this.bahasa_sehari_hari_lainnya = res.body['data'].bahasa_sehari_hari_lainnya;
          this.perlu_penterjemah = res.body['data'].perlu_penterjemah;
          this.perlu_penterjemah_bahasa = res.body['data'].perlu_penterjemah_bahasa;
          this.bahasa_isyarat = res.body['data'].bahasa_isyarat;
          this.hambatan_belajar = res.body['data'].hambatan_belajar;
          this.cara_belajar_yang_disukai = res.body['data'].cara_belajar_yang_disukai;
          this.tingkatan_pendidikan = res.body['data'].tingkatan_pendidikan;
          this.potensi_kebutuhan_pembelajaran = res.body['data'].potensi_kebutuhan_pembelajaran;
          this.ketersediaan_menerima_informasi = res.body['data'].ketersediaan_menerima_informasi;
        }
        if (pengkajian == 'DEWASA') {
          this.FormPengkajianDewasa = true;
          this.DewasaForm.patchValue({
            ...res.body['data']
          });
          this.cacat_tubuh = res.body['data'].cacat_tubuh;
          this.alat_bantu = res.body['data'].alat_bantu;
          this.prothesa = res.body['data'].prothesa;
          this.adl = res.body['data'].adl;
          this.hubungan_keluarga = res.body['data'].hubungan_keluarga;
          this.status_psikologis = res.body['data'].status_psikologis;
          this.kecenderungan_bunuh_diri = res.body['data'].kecenderungan_bunuh_diri;
          this.lainnya = res.body['data'].lainnya;
          this.perkerjaan = res.body['data'].perkerjaan;
          this.penuruan_bb = res.body['data'].penuruan_bb;
          this.kesulitan_menerima_makanan = res.body['data'].kesulitan_menerima_makanan;
          this.diagnosa_berhubungan_dengan_gizi = res.body['data'].diagnosa_berhubungan_dengan_gizi;
          if (res.body['data'].penuruan_bb == '1') this.skormaa = 0
          if (res.body['data'].penuruan_bb == '2') this.skormaa = 2
          if (res.body['data'].penuruan_bb == '3') this.skormaa = 1
          if (res.body['data'].penuruan_bb == '4') this.skormaa = 2
          if (res.body['data'].penuruan_bb == '5') this.skormaa = 3
          if (res.body['data'].penuruan_bb == '6') this.skormaa = 4
          if (res.body['data'].penuruan_bb == '7') this.skormaa = 2
          if (res.body['data'].kesulitan_menerima_makanan == '1') this.skormab = 0
          if (res.body['data'].kesulitan_menerima_makanan == '2') this.skormab = 1
          if (res.body['data'].diagnosa_berhubungan_dengan_gizi == '1') this.skormac = 0
          if (res.body['data'].diagnosa_berhubungan_dengan_gizi == '2') this.skormac = 1
          this.skorma = this.skormaa + this.skormab + this.skormac;
          if (this.skorma > 2) {
            this.risikoma = "Berisiko";
          } else {
            this.risikoma = "Tidak Berisiko";
          }
          this.nyeri_diprovokasi_oleh = res.body['data'].nyeri_diprovokasi_oleh;
          if (res.body['data'].skala_nyeri <= 3) {
            this.sifat_nyeri = 'Ringan';
          } else if (res.body['data'].skala_nyeri <= 6) {
            this.sifat_nyeri = 'Sedang';
          } else {
            this.sifat_nyeri = 'Berat';
          }
          this.lokasi_nyeri = res.body['data'].lokasi_nyeri;
          this.penjalaran_nyeri = res.body['data'].penjalaran_nyeri;
          if (res.body['data'].nyeri == '1') {
            this.nyeri = 'Nyeri akut';
          } else {
            this.nyeri = 'Nyeri kronis';
          }
          this.nyeri_hilang_bila = res.body['data'].nyeri_hilang_bila;
          this.nyeri_hilang_bila_lainnya = res.body['data'].nyeri_hilang_bila_lainnya;
          this.sempoyongan = res.body['data'].sempoyongan;
          this.penopang = res.body['data'].penopang;
          if (res.body['data'].sempoyongan == '1' && res.body['data'].penopang == '1') {
            this.skriningrisiko = "Risiko tinggi";
          } else if (res.body['data'].sempoyongan == '2' && res.body['data'].penopang == '2') {
            this.skriningrisiko = "Tidak berisiko";
          } else {
            this.skriningrisiko = "Risiko Rendah";
          }
          this.agama = res.body['data'].agama;
          this.agama_lainnya = res.body['data'].agama_lainnya;
          this.bicara = res.body['data'].bicara;
          this.bicara_sejak = res.body['data'].bicara_sejak;
          this.bahasa_sehari_hari = res.body['data'].bahasa_sehari_hari;
          this.bahasa_daerah = res.body['data'].bahasa_daerah;
          this.bahasa_lainnya = res.body['data'].bahasa_lainnya;
          this.hambatan_belajar = res.body['data'].hambatan_belajar;
          this.perlu_penterjemah = res.body['data'].perlu_penterjemah;
          this.perlu_penterjemah_bahasa = res.body['data'].perlu_penterjemah_bahasa;
          this.bahasa_isyarat = res.body['data'].bahasa_isyarat;
          this.cara_belajar = res.body['data'].cara_belajar;
          this.tingkat_pendidikan = res.body['data'].tingkat_pendidikan;
          this.potensi_kebutuhan_pembelajaran = res.body['data'].potensi_kebutuhan_pembelajaran;
          this.pasien_bersedia_menerima_edukasi = res.body['data'].pasien_bersedia_menerima_edukasi;
        }
        if (pengkajian == 'BEDAH') {
          this.FormPengkajianBedah = true;
          this.BedahForm.patchValue({
            ...res.body['data']
          });
          this.cacat_tubuh = res.body['data'].cacat_tubuh;
          this.alat_bantu = res.body['data'].alat_bantu;
          this.prothesa = res.body['data'].prothesa;
          this.adl = res.body['data'].adl;
          this.hubungan_keluarga = res.body['data'].hubungan_keluarga;
          this.status_psikologis = res.body['data'].status_psikologis;
          this.status_psikologis_kecenderungan_bunuh_diri = res.body['data'].status_psikologis_kecenderungan_bunuh_diri;
          this.status_psikologis_lainnya = res.body['data'].status_psikologis_lainnya;
          this.perkerjaan = res.body['data'].perkerjaan;
          this.penurunan_bb = res.body['data'].penurunan_bb;
          this.penurunan_nafsu_makan = res.body['data'].penurunan_nafsu_makan;
          if (res.body['data'].penurunan_bb == '1') {
            this.MST1 = 0;
          } if (res.body['data'].penurunan_bb == '2') {
            this.MST1 = 2;
          } if (res.body['data'].penurunan_bb == '3') {
            this.MST1 = 1;
          } if (res.body['data'].penurunan_bb == '4') {
            this.MST1 = 2;
          } if (res.body['data'].penurunan_bb == '5') {
            this.MST1 = 3;
          } if (res.body['data'].penurunan_bb == '6') {
            this.MST1 = 4;
          }
          if (res.body['data'].penurunan_nafsu_makan == '1') {
            this.MST2 = 0;
          } if (res.body['data'].penurunan_nafsu_makan == '2') {
            this.MST2 = 1;
          }
          this.MSTSkor = this.MST1 + this.MST2;
          if (this.MSTSkor < 2) {
            this.MSTKeterangan = 'Tidak Berisiko'
          } else if (this.MSTSkor < 4) {
            this.MSTKeterangan = 'Berisiko'
          } else {
            this.MSTKeterangan = 'Malnutrisi'
          }
          this.nyeri_diprovokasi_oleh = res.body['data'].nyeri_diprovokasi_oleh;
          if (res.body['data'].nyeri == '1') {
            this.nyeri = 'Nyeri akut';
          } else {
            this.nyeri = 'Nyeri kronis';
          }
          if (res.body['data'].skala_nyeri <= 3) {
            this.sifat_nyeri = 'Ringan';
          } else if (res.body['data'].skala_nyeri <= 6) {
            this.sifat_nyeri = 'Sedang';
          } else {
            this.sifat_nyeri = 'Berat';
          }
          this.lokasi_nyeri = res.body['data'].lokasi_nyeri;
          this.penjalaran_nyeri = res.body['data'].penjalaran_nyeri;
          this.nyeri_hilang_bila = res.body['data'].nyeri_hilang_bila;
          this.nyeri_hilang_bila_lain_lain = res.body['data'].nyeri_hilang_bila_lain_lain;
          if (res.body['data'].kategori_umur === 1) {
            this.pasienanak = true;
            if (res.body['data'].anak_umur == 1) {
              this.anak_umur = 'Di bawah 3 tahun';
              this.AnakUmur = 4;
            } else if (res.body['data'].anak_umur == 2) {
              this.anak_umur = '3 - 7 tahun';
              this.AnakUmur = 3;
            } else if (res.body['data'].anak_umur == 3) {
              this.anak_umur = '7 - 13 tahun';
              this.AnakUmur = 2;
            } else {
              this.anak_umur = '> 13 tahun';
              this.AnakUmur = 1;
            }
            if (res.body['data'].anak_jenis_kelamin == 1) {
              this.anak_jenis_kelamin = 'Ya';
              this.AnakJenisKelamin = 2;
            } else {
              this.anak_jenis_kelamin = 'Tidak';
              this.AnakJenisKelamin = 1;
            }
            if (res.body['data'].anak_diagnosa == 1) {
              this.anak_diagnosa = 'Kelainan neurologi';
              this.AnakDiagnosa = 4;
            } else if (res.body['data'].anak_diagnosa == 2) {
              this.anak_diagnosa = 'Perubahan dalam oksigenasi (masalah saluran nafas, dehidrasi, anemiam anoreksia, sinkop / sakit kepala, dll)';
              this.AnakDiagnosa = 3;
            } else if (res.body['data'].anak_diagnosa == 3) {
              this.anak_diagnosa = 'Kelainan psikis / perilaku';
              this.AnakDiagnosa = 2;
            } else {
              this.anak_diagnosa = 'Diagnosis lain';
              this.AnakDiagnosa = 1;
            }
            if (res.body['data'].anak_gangguan_kognitif == 1) {
              this.anak_gangguan_kognitif = 'Tidak sadar terhadap keterbatasan';
              this.AnakGangguanKognirif = 3;
            } else if (res.body['data'].anak_gangguan_kognitif == 2) {
              this.anak_gangguan_kognitif = 'Lupa keterbatasan';
              this.AnakGangguanKognirif = 2;
            } else {
              this.anak_gangguan_kognitif = 'Mengetahui kemampuan diri';
              this.AnakGangguanKognirif = 1;
            }
            if (res.body['data'].anak_faktor_lingkungan == 1) {
              this.anak_faktor_lingkungan = 'Riwayat jatuh dari tempat tidur saat bayi - anak';
              this.AnakFaktorLingkungan = 4;
            } else if (res.body['data'].anak_faktor_lingkungan == 2) {
              this.anak_faktor_lingkungan = 'Pasien menggunakan alat bantu atau box atau mebel';
              this.AnakFaktorLingkungan = 3;
            } else if (res.body['data'].anak_faktor_lingkungan == 3) {
              this.anak_faktor_lingkungan = 'Pasien berada di tempat tidur';
              this.AnakFaktorLingkungan = 2;
            } else {
              this.anak_faktor_lingkungan = 'Di luar area pasien';
              this.AnakFaktorLingkungan = 1;
            }
            if (res.body['data'].anak_respon_terhadap_operasi == 1) {
              this.anak_respon_terhadap_operasi = 'Dalam 24 jam';
              this.AnakResponTerhadap = 3;
            } else if (res.body['data'].anak_respon_terhadap_operasi == 2) {
              this.anak_respon_terhadap_operasi = 'Dalam 48 jam';
              this.AnakResponTerhadap = 2;
            } else {
              this.anak_respon_terhadap_operasi = '> 48 jam';
              this.AnakResponTerhadap = 1;
            }
            if (res.body['data'].anak_penggunaan_obat == 1) {
              this.anak_penggunaan_obat = 'Bermacam-macam obat yang digunakan obat sedatif (kecuali pasien ICU yang menggunakan sedasi dan paralisis), hipnotik, barbiturat, fenotiazin, antidepresan, laksans/diuretika, narkotik';
              this.AnakPenggunaanObat = 3;
            } else if (res.body['data'].anak_penggunaan_obat == 2) {
              this.anak_penggunaan_obat = 'Salah satu dari pengobatan diatas';
              this.AnakPenggunaanObat = 2;
            } else {
              this.anak_penggunaan_obat = 'Pengobatan lain';
              this.AnakPenggunaanObat = 1;
            }
            this.AnakTotal = this.AnakUmur + this.AnakJenisKelamin + this.AnakDiagnosa + this.AnakGangguanKognirif + this.AnakFaktorLingkungan + this.AnakResponTerhadap + this.AnakPenggunaanObat;
            if (this.AnakTotal <= 6) {
              this.AnakKeterangan = 'Tidak Berisiko'
            } else if (this.AnakTotal <= 11) {
              this.AnakKeterangan = 'Risiko rendah'
            } else if (this.AnakTotal >= 12) {
              this.AnakKeterangan = 'Risiko Tinggi'
            }
          } else if (res.body['data'].kategori_umur === 2) {
            this.pasiendewasa = true;
            if (res.body['data'].dewasa_riwayat_jatuh == 1) {
              this.dewasa_riwayat_jatuh = 'Ya';
              this.DewasaRiwayatJatuh = 25;
            } else {
              this.dewasa_riwayat_jatuh = 'Tidak';
              this.DewasaRiwayatJatuh = 0;
            }
            if (res.body['data'].dewasa_diagnosa_sekunder == 1) {
              this.dewasa_diagnosa_sekunder = 'Ya';
              this.DewasaDiagnosaSekunder = 15;
            } else {
              this.dewasa_diagnosa_sekunder = 'Tidak';
              this.DewasaDiagnosaSekunder = 0;
            }
            if (res.body['data'].dewasa_alat_bantu == 1) {
              this.dewasa_alat_bantu = 'Berpegangan pada perabot';
              this.DewasaAlatBantu = 30;
            } else if (res.body['data'].dewasa_alat_bantu == 2) {
              this.dewasa_alat_bantu = 'Tongkat / alat penopang';
              this.DewasaAlatBantu = 15;
            } else {
              this.dewasa_alat_bantu = 'Tidak ada / kursi roda / perawat / tirah baring';
              this.DewasaAlatBantu = 0;
            }
            if (res.body['data'].dewasa_terpasang_infus == 1) {
              this.dewasa_terpasang_infus = 'Ya';
              this.DewasaTerpasangAlat = 20;
            } else {
              this.dewasa_terpasang_infus = 'Tidak';
              this.DewasaTerpasangAlat = 0;
            }
            if (res.body['data'].dewasa_gaya_berjalan == 1) {
              this.dewasa_gaya_berjalan = 'Terganggu';
              this.DewasaGayaBerjalan = 20;
            } else if (res.body['data'].dewasa_gaya_berjalan == 2) {
              this.dewasa_gaya_berjalan = 'Lemah';
              this.DewasaGayaBerjalan = 10;
            } else {
              this.dewasa_gaya_berjalan = 'Normal / tirah baring / imobilisasi';
              this.DewasaGayaBerjalan = 0;
            }
            this.DewasaTotal = this.DewasaRiwayatJatuh + this.DewasaDiagnosaSekunder + this.DewasaAlatBantu + this.DewasaTerpasangAlat + this.DewasaGayaBerjalan;
            if (this.DewasaTotal <= 24) {
              this.DewasaKeterangan = 'Risiko rendah'
            } else if (this.DewasaTotal <= 44) {
              this.DewasaKeterangan = 'Risiko sedang'
            } else if (this.DewasaTotal > 44) {
              this.DewasaKeterangan = 'Risiko tinggi'
            }
          } else if (res.body['data'].kategori_umur === 3) {
            this.pasienlansia = true;
            if (res.body['data'].lansia_riwayat_jatuh_karena_jatuh == 1) {
              this.lansia_riwayat_jatuh_karena_jatuh = 'Ya';
            } else {
              this.lansia_riwayat_jatuh_karena_jatuh = 'Tidak';
            }
            if (res.body['data'].lansia_riwayat_jatuh_dalam_dua_bulan == 1) {
              this.lansia_riwayat_jatuh_dalam_dua_bulan = 'Ya';
            } else {
              this.lansia_riwayat_jatuh_dalam_dua_bulan = 'Tidak';
            }
            if (res.body['data'].lansia_riwayat_jatuh_karena_jatuh == 1 || res.body['data'].lansia_riwayat_jatuh_dalam_dua_bulan == 1) {
              this.LansiaRiwayatJatuh = 6;
            } else {
              this.LansiaRiwayatJatuh = 0;
            }
            if (res.body['data'].lansia_status_mental_delirium == 1) {
              this.lansia_status_mental_delirium = 'Ya';
            } else {
              this.lansia_status_mental_delirium = 'Tidak';
            }
            if (res.body['data'].lansia_status_mental_disorientasi == 1) {
              this.lansia_status_mental_disorientasi = 'Ya';
            } else {
              this.lansia_status_mental_disorientasi = 'Tidak';
            }
            if (res.body['data'].lansia_status_mental_agitasi == 1) {
              this.lansia_status_mental_disorientasi = 'Ya';
            } else {
              this.lansia_status_mental_disorientasi = 'Tidak';
            }
            if (res.body['data'].lansia_status_mental_agitasi == 1 || res.body['data'].lansia_status_mental_delirium == 1 || res.body['data'].lansia_status_mental_disorientasi == 1) {
              this.LansiaStatusMental = 14;
            } else {
              this.LansiaStatusMental = 0;
            }
            if (res.body['data'].lansia_pengelihatan_kacamata == 1) {
              this.lansia_pengelihatan_kacamata = 'Ya';
            } else {
              this.lansia_pengelihatan_kacamata = 'Tidak';
            }
            if (res.body['data'].lansia_pengelihatan_buram == 1) {
              this.lansia_pengelihatan_buram = 'Ya';
            } else {
              this.lansia_pengelihatan_buram = 'Tidak';
            }
            if (res.body['data'].lansia_pengelihatan_katarak == 1) {
              this.lansia_pengelihatan_katarak = 'Ya';
            } else {
              this.lansia_pengelihatan_katarak = 'Tidak';
            }
            if (res.body['data'].lansia_pengelihatan_kacamata == 1 || res.body['data'].lansia_pengelihatan_buram == 1 || res.body['data'].lansia_pengelihatan_katarak == 1) {
              this.LansiaPenglihatan = 1;
            } else {
              this.LansiaPenglihatan = 0;
            }
            if (res.body['data'].lansia_kebiasaan_berkemih == 1) {
              this.lansia_kebiasaan_berkemih = 'Ya';
              this.LansiaKebiasaanBerkemih = 2
            } else {
              this.lansia_kebiasaan_berkemih = 'Tidak';
              this.LansiaKebiasaanBerkemih = 0;
            }
            if (res.body['data'].lansia_transfer == 1) {
              this.lansia_transfer = 'Mandiri (boleh memakai alat bantu jalan)';
              this.LansiaTransfer = 0;
            } else if (res.body['data'].lansia_transfer == 2) {
              this.lansia_transfer = 'Memerlukan sedikit bantuan (1 orang) / dalam pengawasan';
              this.LansiaTransfer = 1;
            } else if (res.body['data'].lansia_transfer == 3) {
              this.lansia_transfer = 'Memerlukan bantuan yang nyata (2 orang)';
              this.LansiaTransfer = 2;
            } else {
              this.lansia_transfer = 'Tidak dapat duduk dengan seimbang, perlu bantuan total';
              this.LansiaTransfer = 3;
            }
            if (this.LansiaTransfer + this.LansiaMobilitas <= 3) {
              this.LansiaTranferMobilitas = 0;
            } else if (this.LansiaTransfer + this.LansiaMobilitas > 3) {
              this.LansiaTranferMobilitas = 7;
            }
            if (res.body['data'].lansia_mobilitas == 1) {
              this.lansia_mobilitas = 'Mandiri (boleh memakai alat bantu jalan)';
              this.LansiaTransfer = 0;
            } else if (res.body['data'].lansia_mobilitas == 2) {
              this.lansia_mobilitas = 'Berjalan dengan bantuan 1 orang (verbal/fisik)';
              this.LansiaTransfer = 1;
            } else if (res.body['data'].lansia_mobilitas == 3) {
              this.lansia_mobilitas = 'Menggunakan kursi roda';
              this.LansiaTransfer = 2;
            } else {
              this.lansia_mobilitas = 'Imobilisasi';
              this.LansiaTransfer = 3;
            }
            this.LansiaTotalSkor = this.LansiaRiwayatJatuh + this.LansiaStatusMental + this.LansiaPenglihatan + this.LansiaKebiasaanBerkemih + this.LansiaTranferMobilitas;
            if (this.LansiaTotalSkor <= 5) {
              this.LansiaKeterangan = 'Risiko rendah';
            } else if (this.LansiaTotalSkor <= 16) {
              this.LansiaKeterangan = 'Risiko sedang';
            } else if (this.LansiaTotalSkor > 16) {
              this.LansiaKeterangan = 'Risiko tinggi';
            }
          }
          this.agama = res.body['data'].agama;
          this.agama_lainnya = res.body['data'].agama_lainnya;
          this.nilai_keyakinan = res.body['data'].nilai_keyakinan;
          this.nilai_keyakinan_jabarkan = res.body['data'].nilai_keyakinan_jabarkan;
          this.bicara = res.body['data'].bicara;
          this.bicara_sejak = res.body['data'].bicara_sejak;
          this.bahasa_sehari_hari = res.body['data'].bahasa_sehari_hari;
          this.bahasa_sehari_hari_daerah = res.body['data'].bahasa_sehari_hari_daerah;
          this.bahasa_sehari_hari_lainnya = res.body['data'].bahasa_sehari_hari_lainnya;
          this.perlu_penterjemah = res.body['data'].perlu_penterjemah;
          this.perlu_penterjemah_bahasa = res.body['data'].perlu_penterjemah_bahasa;
          this.bahasa_isyarat = res.body['data'].bahasa_isyarat;
          this.hambatan_belajar = res.body['data'].hambatan_belajar;
          this.cara_belajar_yang_disukai = res.body['data'].cara_belajar_yang_disukai;
          this.tingkatan_pendidikan = res.body['data'].tingkatan_pendidikan;
          this.potensi_kebutuhan_pembelajaran = res.body['data'].potensi_kebutuhan_pembelajaran;
          this.ketersediaan_menerima_informasi = res.body['data'].ketersediaan_menerima_informasi;
        }
        if (pengkajian == 'BIDAN') {
          this.FormPengkajianBidan = true;
          this.BidanForm.patchValue({
            ...res.body['data']
          });
          this.keluhan_saat_masuk = res.body['data'].keluhan_saat_masuk;
          this.fungsional_alat_bantu = res.body['data'].fungsional_alat_bantu;
          this.fungsional_prothesa = res.body['data'].fungsional_prothesa;
          this.fungsional_cacat_tubuh = res.body['data'].fungsional_cacat_tubuh;
          this.fungsional_adl = res.body['data'].fungsional_adl;
          this.psikososial_hubungan_pasien_keluarga = res.body['data'].psikososial_hubungan_pasien_keluarga;
          this.psikososial_status_psikologis = res.body['data'].psikososial_status_psikologis;
          this.psikososial_support_keluarga = res.body['data'].psikososial_support_keluarga;
          this.psikososial_pengetahuan_tentang_kehamilan = res.body['data'].psikososial_pengetahuan_tentang_kehamilan;
          this.psikososial_kecenderungan_bunuh_diri = res.body['data'].psikososial_kecenderungan_bunuh_diri;
          this.psikososial_lainnya = res.body['data'].psikososial_lainnya;
          this.perkerjaan = res.body['data'].perkerjaan;
          this.spiritual_agama = res.body['data'].spiritual_agama;
          this.spiritual_agama_lainnya = res.body['data'].spiritual_agama_lainnya;
          this.nilai_kepercayaan = res.body['data'].nilai_kepercayaan;
          this.nilai_kepercayaan_ya = res.body['data'].nilai_kepercayaan_ya;
          this.penurunan_bb = res.body['data'].penurunan_bb;
          this.penurunan_nafsu_makan = res.body['data'].penurunan_nafsu_makan;
          this.diagnosa_berhubungan_dengan_gizi = res.body['data'].diagnosa_berhubungan_dengan_gizi;
          if (res.body['data'].penurunan_bb == '1') {
            this.MST1 = 0;
          } if (res.body['data'].penurunan_bb == '2') {
            this.MST1 = 2;
          } if (res.body['data'].penurunan_bb == '3') {
            this.MST1 = 1;
          } if (res.body['data'].penurunan_bb == '4') {
            this.MST1 = 2;
          } if (res.body['data'].penurunan_bb == '5') {
            this.MST1 = 3;
          } if (res.body['data'].penurunan_bb == '6') {
            this.MST1 = 4;
          }
          if (res.body['data'].penurunan_nafsu_makan == '1') {
            this.MST2 = 0;
          } if (res.body['data'].penurunan_nafsu_makan == '2') {
            this.MST2 = 1;
          }
          if (res.body['data'].diagnosa_berhubungan_dengan_gizi == '1') {
            this.MST3 = 0;
          } if (res.body['data'].diagnosa_berhubungan_dengan_gizi == '2') {
            this.MST3 = 1;
          }
          this.MSTSkor = this.MST1 + this.MST2 + this.MST3;
          if (this.MSTSkor < 2) {
            this.MSTKeterangan = 'Tidak Berisiko'
          } else if (this.MSTSkor < 4) {
            this.MSTKeterangan = 'Berisiko'
          }
          this.nyeri_diprovokasi_oleh = res.body['data'].nyeri_diprovokasi_oleh;
          if (res.body['data'].skala_nyeri <= 3) {
            this.sifat_nyeri = 'Ringan';
          } else if (res.body['data'].skala_nyeri <= 6) {
            this.sifat_nyeri = 'Sedang';
          } else {
            this.sifat_nyeri = 'Berat';
          }
          this.lokasi_nyeri = res.body['data'].lokasi_nyeri;
          this.penjalaran_nyeri = res.body['data'].penjalaran_nyeri;
          this.nyeri_hilang_bila = res.body['data'].nyeri_hilang_bila;
          this.nyeri_hilang_bila_lainnya = res.body['data'].nyeri_hilang_bila_lainnya;
          if (res.body['data'].nyeri == '1') {
            this.nyeri = 'Nyeri akut';
          } else {
            this.nyeri = 'Nyeri kronis';
          }
          if (res.body['data'].kategori_umur === 2) {
            this.pasiendewasa = true;
            if (res.body['data'].dewasa_riwayat_jatuh == 1) {
              this.dewasa_riwayat_jatuh = 'Ya';
              this.DewasaRiwayatJatuh = 25;
            } else {
              this.dewasa_riwayat_jatuh = 'Tidak';
              this.DewasaRiwayatJatuh = 0;
            }
            if (res.body['data'].dewasa_diagnosa_sekunder == 1) {
              this.dewasa_diagnosa_sekunder = 'Ya';
              this.DewasaDiagnosaSekunder = 15;
            } else {
              this.dewasa_diagnosa_sekunder = 'Tidak';
              this.DewasaDiagnosaSekunder = 0;
            }
            if (res.body['data'].dewasa_alat_bantu == 1) {
              this.dewasa_alat_bantu = 'Berpegangan pada perabot';
              this.DewasaAlatBantu = 30;
            } else if (res.body['data'].dewasa_alat_bantu == 2) {
              this.dewasa_alat_bantu = 'Tongkat / alat penopang';
              this.DewasaAlatBantu = 15;
            } else {
              this.dewasa_alat_bantu = 'Tidak ada / kursi roda / perawat / tirah baring';
              this.DewasaAlatBantu = 0;
            }
            if (res.body['data'].dewasa_terpasang_infus == 1) {
              this.dewasa_terpasang_infus = 'Ya';
              this.DewasaTerpasangAlat = 20;
            } else {
              this.dewasa_terpasang_infus = 'Tidak';
              this.DewasaTerpasangAlat = 0;
            }
            if (res.body['data'].dewasa_gaya_berjalan == 1) {
              this.dewasa_gaya_berjalan = 'Terganggu';
              this.DewasaGayaBerjalan = 20;
            } else if (res.body['data'].dewasa_gaya_berjalan == 2) {
              this.dewasa_gaya_berjalan = 'Lemah';
              this.DewasaGayaBerjalan = 10;
            } else {
              this.dewasa_gaya_berjalan = 'Normal / tirah baring / imobilisasi';
              this.DewasaGayaBerjalan = 0;
            }
            if (res.body['data'].dewasa_status_mental == 1) {
              this.dewasa_status_mental = 'Sering Lupa akan keterbatasan yang dimiliki';
              this.DewasaStatusMental = 20;
            } else {
              this.dewasa_status_mental = 'Sadar akan kemampuan diri sendiri';
              this.DewasaStatusMental = 0;
            }
            this.DewasaTotal = this.DewasaRiwayatJatuh + this.DewasaDiagnosaSekunder + this.DewasaAlatBantu + this.DewasaTerpasangAlat + this.DewasaGayaBerjalan + this.DewasaStatusMental;
            if (this.DewasaTotal <= 24) {
              this.DewasaKeterangan = 'Risiko rendah'
            } else if (this.DewasaTotal <= 44) {
              this.DewasaKeterangan = 'Risiko sedang'
            } else if (this.DewasaTotal > 44) {
              this.DewasaKeterangan = 'Risiko tinggi'
            }
          } else if (res.body['data'].kategori_umur === 3) {
            this.pasienlansia = true;
            if (res.body['data'].lansia_riwayat_jatuh_karena_jatuh == 1) {
              this.lansia_riwayat_jatuh_karena_jatuh = 'Ya';
            } else {
              this.lansia_riwayat_jatuh_karena_jatuh = 'Tidak';
            }
            if (res.body['data'].lansia_riwayat_jatuh_dalam_dua_bulan == 1) {
              this.lansia_riwayat_jatuh_dalam_dua_bulan = 'Ya';
            } else {
              this.lansia_riwayat_jatuh_dalam_dua_bulan = 'Tidak';
            }
            if (res.body['data'].lansia_riwayat_jatuh_karena_jatuh == 1 || res.body['data'].lansia_riwayat_jatuh_dalam_dua_bulan == 1) {
              this.LansiaRiwayatJatuh = 6;
            } else {
              this.LansiaRiwayatJatuh = 0;
            }
            if (res.body['data'].lansia_status_mental_delirium == 1) {
              this.lansia_status_mental_delirium = 'Ya';
            } else {
              this.lansia_status_mental_delirium = 'Tidak';
            }
            if (res.body['data'].lansia_status_mental_disorientasi == 1) {
              this.lansia_status_mental_disorientasi = 'Ya';
            } else {
              this.lansia_status_mental_disorientasi = 'Tidak';
            }
            if (res.body['data'].lansia_status_mental_agitasi == 1) {
              this.lansia_status_mental_disorientasi = 'Ya';
            } else {
              this.lansia_status_mental_disorientasi = 'Tidak';
            }
            if (res.body['data'].lansia_status_mental_agitasi == 1 || res.body['data'].lansia_status_mental_delirium == 1 || res.body['data'].lansia_status_mental_disorientasi == 1) {
              this.LansiaStatusMental = 14;
            } else {
              this.LansiaStatusMental = 0;
            }
            if (res.body['data'].lansia_pengelihatan_kacamata == 1) {
              this.lansia_pengelihatan_kacamata = 'Ya';
            } else {
              this.lansia_pengelihatan_kacamata = 'Tidak';
            }
            if (res.body['data'].lansia_pengelihatan_buram == 1) {
              this.lansia_pengelihatan_buram = 'Ya';
            } else {
              this.lansia_pengelihatan_buram = 'Tidak';
            }
            if (res.body['data'].lansia_pengelihatan_katarak == 1) {
              this.lansia_pengelihatan_katarak = 'Ya';
            } else {
              this.lansia_pengelihatan_katarak = 'Tidak';
            }
            if (res.body['data'].lansia_pengelihatan_kacamata == 1 || res.body['data'].lansia_pengelihatan_buram == 1 || res.body['data'].lansia_pengelihatan_katarak == 1) {
              this.LansiaPenglihatan = 1;
            } else {
              this.LansiaPenglihatan = 0;
            }
            if (res.body['data'].lansia_kebiasaan_berkemih == 1) {
              this.lansia_kebiasaan_berkemih = 'Ya';
              this.LansiaKebiasaanBerkemih = 2
            } else {
              this.lansia_kebiasaan_berkemih = 'Tidak';
              this.LansiaKebiasaanBerkemih = 0;
            }
            if (res.body['data'].lansia_transfer == 1) {
              this.lansia_transfer = 'Mandiri (boleh memakai alat bantu jalan)';
              this.LansiaTransfer = 0;
            } else if (res.body['data'].lansia_transfer == 2) {
              this.lansia_transfer = 'Memerlukan sedikit bantuan (1 orang) / dalam pengawasan';
              this.LansiaTransfer = 1;
            } else if (res.body['data'].lansia_transfer == 3) {
              this.lansia_transfer = 'Memerlukan bantuan yang nyata (2 orang)';
              this.LansiaTransfer = 2;
            } else {
              this.lansia_transfer = 'Tidak dapat duduk dengan seimbang, perlu bantuan total';
              this.LansiaTransfer = 3;
            }
            if (this.LansiaTransfer + this.LansiaMobilitas <= 3) {
              this.LansiaTranferMobilitas = 0;
            } else if (this.LansiaTransfer + this.LansiaMobilitas > 3) {
              this.LansiaTranferMobilitas = 7;
            }
            if (res.body['data'].lansia_mobilitas == 1) {
              this.lansia_mobilitas = 'Mandiri (boleh memakai alat bantu jalan)';
              this.LansiaTransfer = 0;
            } else if (res.body['data'].lansia_mobilitas == 2) {
              this.lansia_mobilitas = 'Berjalan dengan bantuan 1 orang (verbal/fisik)';
              this.LansiaTransfer = 1;
            } else if (res.body['data'].lansia_mobilitas == 3) {
              this.lansia_mobilitas = 'Menggunakan kursi roda';
              this.LansiaTransfer = 2;
            } else {
              this.lansia_mobilitas = 'Imobilisasi';
              this.LansiaTransfer = 3;
            }
            this.LansiaTotalSkor = this.LansiaRiwayatJatuh + this.LansiaStatusMental + this.LansiaPenglihatan + this.LansiaKebiasaanBerkemih + this.LansiaTranferMobilitas;
            if (this.LansiaTotalSkor <= 5) {
              this.LansiaKeterangan = 'Risiko rendah';
            } else if (this.LansiaTotalSkor <= 16) {
              this.LansiaKeterangan = 'Risiko sedang';
            } else if (this.LansiaTotalSkor > 16) {
              this.LansiaKeterangan = 'Risiko tinggi';
            }
          }
          this.riwayat_kesehatan_penyakit_yang_diderita = res.body['data'].riwayat_kesehatan_penyakit_yang_diderita;
          this.riwayat_kesehatan_pengobatan_di_rumah = res.body['data'].riwayat_kesehatan_pengobatan_di_rumah;

          let riwayatobat = <FormArray>this.BidanForm.controls.obatBidan;
          for (const obat of res.body['data'].obatBidan) {
            riwayatobat.push(
              this.fb.group({
                Id_PemberianInfusObat: [{ value: obat.Id_PemberianInfusObat, disabled: true }],
                Waktu_Pemberian_InfusObat: [{ value: this.datePipe.transform(obat.Waktu_Pemberian_InfusObat, 'MM/dd/yyyy'), disabled: true }],
                nama: [{ value: obat.nama, disabled: true }],
                dosis: [{ value: obat.dosis, disabled: true }],
                cara_pemberian: [{ value: obat.cara_pemberian, disabled: true }],
                frekuensi: [{ value: obat.frekuensi, disabled: true }]
              })
            )
          }
          this.operasi_yang_pernah_dialami = res.body['data'].operasi_yang_pernah_dialami;
          this.faktor_keturunan_gemeli = res.body['data'].faktor_keturunan_gemeli;
          this.riwayat_penyakit_herediter = res.body['data'].riwayat_penyakit_herediter;
          this.riwayat_penyakit_herediter_jelaskan = res.body['data'].riwayat_penyakit_herediter_jelaskan;
          this.riwayat_penyakit_keluarga = res.body['data'].riwayat_penyakit_keluarga;
          this.ketergantungan_terhadap = res.body['data'].ketergantungan_terhadap;
          this.ketergantungan_terhadap_lainnya = res.body['data'].ketergantungan_terhadap_lainnya;
          this.riwayat_perkerjaan_berhubungan_dengan_zat = res.body['data'].riwayat_perkerjaan_berhubungan_dengan_zat;
          this.riwayat_perkerjaan_berhubungan_dengan_zat_ya = res.body['data'].riwayat_perkerjaan_berhubungan_dengan_zat_ya;

          let riwayatkehamilan = <FormArray>this.BidanForm.controls.riwayatKehamilan
          for (const kehamilan of res.body['data'].riwayatKehamilan) {
            var jenkel = '';
            if (kehamilan.jenis_kelamin == 1) {
              jenkel = 'L';
            } else {
              jenkel = 'P';
            }
            riwayatkehamilan.push(
              this.fb.group({
                idRiwayatKehamilan: [{ value: kehamilan.idRiwayatKehamilan, disabled: true }],
                jenis_kelamin: [{ value: jenkel, disabled: true }],
                umur_anak: [{ value: kehamilan.umur_anak, disabled: true }],
                ku_anak: [{ value: kehamilan.ku_anak, disabled: true }],
                bbl: [{ value: kehamilan.bbl, disabled: true }],
                pb: [{ value: kehamilan.pb, disabled: true }],
                riwayat_persalinan: [{ value: kehamilan.riwayat_persalinan, disabled: true }],
                ditolong_oleh_tempat: [{ value: kehamilan.ditolong_oleh_tempat, disabled: true }],
              })
            )
          }
          this.riwayat_obstetri_menarche = res.body['data'].riwayat_obstetri_menarche;
          this.riwayat_obstetri_menstruasi = res.body['data'].riwayat_obstetri_menstruasi;
          this.riwayat_obstetri_menstruasi_teratur = res.body['data'].riwayat_obstetri_menstruasi_teratur;
          this.riwayat_obstetri_menstruasi_tidak_teratur = res.body['data'].riwayat_obstetri_menstruasi_tidak_teratur;
          this.riwayat_obstetri_sakit_menstruasi = res.body['data'].riwayat_obstetri_sakit_menstruasi;
          this.riwayat_obstetri_menikah_yang_ke = res.body['data'].riwayat_obstetri_menikah_yang_ke;
          this.riwayat_obstetri_menikah_lama = res.body['data'].riwayat_obstetri_menikah_lama;
          this.riwayat_obstetri_kontrasepsi_yang_pernah_digunakan = res.body['data'].riwayat_obstetri_kontrasepsi_yang_pernah_digunakan;
          this.riwayat_obstetri_kontrasepsi_lamanya = res.body['data'].riwayat_obstetri_kontrasepsi_lamanya;
          this.data_kehamilan_sekarang_g = res.body['data'].data_kehamilan_sekarang_g;
          this.data_kehamilan_sekarang_p = res.body['data'].data_kehamilan_sekarang_p;
          this.data_kehamilan_sekarang_a = res.body['data'].data_kehamilan_sekarang_a;
          this.data_kehamilan_sekarang_hpht = res.body['data'].data_kehamilan_sekarang_hpht;
          this.data_kehamilan_sekarang_hpl = res.body['data'].data_kehamilan_sekarang_hpl;
          this.data_kehamilan_sekarang_keluhan = res.body['data'].data_kehamilan_sekarang_keluhan;
          this.bicara = res.body['data'].bicara;
          this.bicara_sejak = res.body['data'].bicara_sejak;
          this.bahasa_sehari_hari = res.body['data'].bahasa_sehari_hari;
          this.bahasa_daerah = res.body['data'].bahasa_daerah;
          this.bahasa_bahasa = res.body['data'].bahasa_bahasa;
          this.perlu_penterjemah = res.body['data'].perlu_penterjemah;
          this.perlu_penterjemah_bahasa = res.body['data'].perlu_penterjemah_bahasa;
          this.perlu_penterjemah_isyarat = res.body['data'].perlu_penterjemah_isyarat;
          this.habatan_belajar = res.body['data'].habatan_belajar;
          this.cara_belajar_yang_disukai = res.body['data'].cara_belajar_yang_disukai;
          this.tingkatan_pendidikan = res.body['data'].tingkatan_pendidikan;
          this.potensi_kebutuhan_pembelajaran = res.body['data'].potensi_kebutuhan_pembelajaran;
          this.pasien_bersedia_menerima_edukasi = res.body['data'].pasien_bersedia_menerima_edukasi;
        }
      },
      err => {
        // console.log(err)
        this.loading = false;
      }
    )
  }

  formAnak(): void {
    this.AnakForm = this.fb.group({
      tanda_vital_td: [{ value: '', disabled: true }],
      tanda_vital_nadi: [{ value: '', disabled: true }],
      tanda_vital_rr: [{ value: '', disabled: true }],
      tanda_vital_suhu: [{ value: '', disabled: true }],
      tanda_vital_bb: [{ value: '', disabled: true }],
      tanda_vital_tb: [{ value: '', disabled: true }],
      nutrisi_diit: [{ value: '', disabled: true }],
      nutrisi_makan: [{ value: '', disabled: true }],
      nutrisi_minum: [{ value: '', disabled: true }],
      skala_nyeri: [{ value: '', disabled: true }],
      durasi_nyeri: [{ value: '', disabled: true }],
      frekuensi_nyeri: [{ value: '', disabled: true }],
      rtk_lingkar_kepala: [{ value: '', disabled: true }],
      rtk_bb: [{ value: '', disabled: true }],
      rtk_tb: [{ value: '', disabled: true }],
      rtk_asi: [{ value: '', disabled: true }],
      rtk_susu_formula: [{ value: '', disabled: true }],
      rtk_tengkurap: [{ value: '', disabled: true }],
      rtk_duduk: [{ value: '', disabled: true }],
      rtk_merangkak: [{ value: '', disabled: true }],
      rtk_berdiri: [{ value: '', disabled: true }],
      rtk_berjalan: [{ value: '', disabled: true }],
      rtk_makanan: [{ value: '', disabled: true }]
    })
  }

  formDewasa() {
    this.DewasaForm = this.fb.group({
      tekanan_darah: [{ value: '', disabled: true }],
      frek_nadi: [{ value: '', disabled: true }],
      frek_nafas: [{ value: '', disabled: true }],
      suhu: [{ value: '', disabled: true }],
      skala_nyeri: [{ value: '', disabled: true }],
      durasi_nyeri: [{ value: '', disabled: true }],
      frekuensi: [{ value: '', disabled: true }],
      skrining_nutrisi_bb: [{ value: '', disabled: true }],
      skrining_nutrisi_tb: [{ value: '', disabled: true }],
      skrining_nutrisi_imt: [{ value: '', disabled: true }]
    })
  }

  formBedah() {
    this.BedahForm = this.fb.group({
      tekanan_darah: [{ value: '', disabled: true }],
      frek_nadi: [{ value: '', disabled: true }],
      nafas: [{ value: '', disabled: true }],
      suhu: [{ value: '', disabled: true }],
      skrining_nutrisi_bb: [{ value: '', disabled: true }],
      skrining_nutrisi_tb: [{ value: '', disabled: true }],
      skrining_nutrisi_imt: [{ value: '', disabled: true }],
      skrining_nutrisi_lingkar_kepala: [{ value: '', disabled: true }],
      skala_nyeri: [{ value: '', disabled: true }],
      durasi_nyeri: [{ value: '', disabled: true }],
      frekuensi: [{ value: '', disabled: true }]
    })
  }

  formBidan() {
    this.BidanForm = this.fb.group({
      tanda_vital_td: [{ value: '', disabled: true }],
      tanda_vital_n: [{ value: '', disabled: true }],
      tanda_vital_rr: [{ value: '', disabled: true }],
      tanda_vital_s: [{ value: '', disabled: true }],
      skrining_nutrisi_bb: [{ value: '', disabled: true }],
      skrining_nutrisi_tb: [{ value: '', disabled: true }],
      skrining_nutrisi_imt: [{ value: '', disabled: true }],
      skala_nyeri: [{ value: '', disabled: true }],
      durasi_nyeri: [{ value: '', disabled: true }],
      frekuensi: [{ value: '', disabled: true }],
      obatBidan: this.fb.array([]),
      riwayatKehamilan: this.fb.array([]),
    })
  }

  noreg_kaji = '';
  kdrm_kaji = '';
  nama_kaji = '';
  jk_kaji = '';
  tglLahir_kaji = '';
  umur_kaji = '';

  viewDetailPengkajianPasien(no_reg, kd_rm, nama, jk, tgl_lahir, umur) {
    this.noreg_kaji = no_reg;
    this.kdrm_kaji = kd_rm;
    this.nama_kaji = nama;
    this.jk_kaji = jk;
    this.tglLahir_kaji = tgl_lahir;
    this.umur_kaji = umur;
  }

  /** PM */
  getJenisPelayanan() {
    var data: any;
    this.service.httpClientGet('api/Users/GetJenisPelayananPM', data)
      .subscribe(result => {
        data = result;
        this.dataJenisPlyn = data;
      });
  }

  clearDetailHLab() {
    this.dataHLabDetail = [];
  }

  /** HLab */
  getHasilLab(rm) {
    var param = {
      Kd_RM: rm
    }
    this.service.post('api/Users/GetTrxPasienRM', JSON.stringify(param))
      .subscribe(result => {
        // console.log(result)
        this.dataHLab = JSON.parse(result);
        // this.dataHLab = result
      });
  }

  getHasilLabByTrx(trx) {
    var param = {
      No_Trx: trx
    }
    this.service.post('api/Users/GetHasilLabByTrx', JSON.stringify(param))
      .subscribe(result => {
        this.dataHLabDetail = JSON.parse(result);
        const paramLog = {
          NoReg: this.noreg,
          Kd_Dokter: this.useraccessdata.kd_dokter,
          Aksi: 'MELIHAT DETAIL HASIL LAB',
          Jenis: '1'
        }
    
        this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });
      });
  }

  Kd_PM: any;
  kd_pm: string = '';
  geneKdPM() {
    this.service.post('api/Users/GeneKDPM', '')
      .subscribe(result => {
        this.Kd_PM = JSON.parse(result);
        this.kd_pm = this.Kd_PM.RL_NoReg;
      });
  }

  valBtnSaveLynD: boolean = true;
  kd_plyn: string = '';
  kd_pmlyn: string = '';
  jenis_plyn: string = '';
  selectIsiPenunjangMedis(kdPlyn, jenisPlyn) {
    this.editDataPemeriksaan = false;
    this.kd_pmlyn = kdPlyn;
    this.jenis_plyn = jenisPlyn;
    var paramPeriksa = {
      Kd_Lyn: this.kd_pmlyn,
      Kd_Tarif: "05",
      Kd_Dokter: "99999999"
    }
    this.service.post('api/Users/GetPemeriksaanByKdLyn', JSON.stringify(paramPeriksa))
      .subscribe(result => {
        this.dataPemeriksaanByKdLyn = JSON.parse(result);
        this.getPaketPM();
        const paramLog = {
          NoReg: this.noreg,
          Kd_Dokter: this.useraccessdata.kd_dokter,
          Aksi: 'MEMILIH JENIS PELAYANAN PENUNJANG MEDIS',
          Jenis: '1'
        }
    
        this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });
      });
    setTimeout(() => {
      this.loading = false;
    }, 500)
  }

  dataPemeriksaanByKdLyn: any;

  arr_dataPemeriksaan = [];
  kd_tindakan: string = '';
  nama_tindakan: string = '';
  dataObjPemeriksaan: any;

  getPelPenunjangMedis(noreg) {
    const paramObj = {
      No_Reg: noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter
    }
    this.service.post('api/AppDokterRawatJalan/GetPenunMedisByNoreg', JSON.stringify(paramObj))
      .subscribe(result => {
        var data: any;
        data = JSON.parse(result);
        // const data = JSON.parse(result);
        this.dataListPenunjangMedis = data;
      },
        err => {
          this.loading = false;
        });
  }

  onSelectPemeriksaan(kd_tind, nama) {
    this.kd_tindakan = kd_tind;
    this.nama_tindakan = nama;

    var index = this.dataPemeriksaanByKdLyn.findIndex(x => x.Kd_Tindakan == kd_tind);
    if (index !== -1) {
      this.dataPemeriksaanByKdLyn.splice(index, 1);
    }
    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'MENAMBAHKAN PEMERIKSAAN PENUNJANG MEDIS ( ' + nama + ' )',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
    var paramObjPeriksa = {
      Kd_Lyn: this.kd_pmlyn,
      Kd_Tarif: "05",
      Kd_Dokter: "99999999",
      Kd_Tindakan: kd_tind
    }
    this.service.post('api/Users/GetObjPemeriksaanByKdTndk', JSON.stringify(paramObjPeriksa))
      .subscribe(result => {
        const arrDt = JSON.parse(result);
        let price = 0;
        if (this.cara_bayar == '6. FOC') {
          price = 1 * arrDt.Karyawan;
        } else if (this.cara_bayar == '9. BPJS') {
          price = 1 * arrDt.BPJS;
        } else {
          price = 1 * arrDt.Standard;
        }

        const obj = {
          No_Trx: '',
          Kd_Lyn: this.kd_pmlyn,
          No_Reg: this.dataPasien.RJ_NoReg,
          No_Urut: '',
          Tgl_Periksa: this.useraccessdata.tgl_hari_ini,
          Kd_Dokter_Kirim: this.useraccessdata.kd_dokter,
          Dokter_Kirim: this.useraccessdata.nama,
          Kd_DrLab: '99999999',
          Kd_PenataLab: '',
          Jumlah: price,
          Discount: '0',
          StatKaryawan: '0',
          FOC: '0',
          UserID: this.useraccessdata.kd_dokter,
          KD_Kelas: this.kdkls,
          Kunjungan: '',
          Total: price,
          Kd_Tindakan: kd_tind,
          JmlTrx: '1',
          StatCITO: '0',
          TotalH: '0',
          Harga: price
        }
        this.dataObjPemeriksaan = { ...arrDt, ...obj };
        this.arr_dataPemeriksaan.push(this.dataObjPemeriksaan);
      });
  }

  editQtyPenunMedis(qty, kd) {
    const value = qty ? qty : 1;
    var index = this.arr_dataPemeriksaan.findIndex(x => x.Kd_Tindakan == kd);
    if (index !== -1) {
      if (this.cara_bayar == '6. FOC') {
        this.arr_dataPemeriksaan[index].JmlTrx = value;
        this.arr_dataPemeriksaan[index].Jumlah = this.arr_dataPemeriksaan[index].Karyawan;
        this.arr_dataPemeriksaan[index].StatKaryawan = '1';
        this.arr_dataPemeriksaan[index].FOC = '1';
        this.arr_dataPemeriksaan[index].Harga = this.arr_dataPemeriksaan[index].Karyawan;
        this.arr_dataPemeriksaan[index].Total = value * this.arr_dataPemeriksaan[index].Karyawan;
      } else if (this.cara_bayar == '9. BPJS') {
        this.arr_dataPemeriksaan[index].JmlTrx = value;
        this.arr_dataPemeriksaan[index].Jumlah = this.arr_dataPemeriksaan[index].BPJS;
        this.arr_dataPemeriksaan[index].Harga = this.arr_dataPemeriksaan[index].BPJS;
        this.arr_dataPemeriksaan[index].Total = value * this.arr_dataPemeriksaan[index].BPJS;
      } else {
        this.arr_dataPemeriksaan[index].JmlTrx = value;
        this.arr_dataPemeriksaan[index].Jumlah = this.arr_dataPemeriksaan[index].Standard;
        this.arr_dataPemeriksaan[index].Harga = this.arr_dataPemeriksaan[index].Standard;
        this.arr_dataPemeriksaan[index].Total = value * this.arr_dataPemeriksaan[index].Standard;
      }
    }
  }

  deleteFieldPemeriksaan(index, kd_tind) {
    this.dataPemeriksaanByKdLyn.push(this.arr_dataPemeriksaan[index]);
    this.arr_dataPemeriksaan.splice(index, 1);
  }

  submitLynPenunjangMedis() {
    // this.loading = true;
    const obj = this.arr_dataPemeriksaan.filter(x => x['JmlTrx'] !== '0');
    const today = new Date();
    let currentIndex = 1;
    for (let i = 0; i < obj.length; i++) {
      let unique = '' + currentIndex;
      const m = ('0' + (today.getMonth() + 1)).slice(-2);
      while (unique.length < 4) {
        unique = '0' + unique;
      }
      obj[i].No_Trx = `${this.kd_pmlyn}${today.getFullYear()}${m}${unique}`;
      // currentIndex++
    }
    let objtotal = 0;
    obj.map(x => objtotal += x.Total)
    obj.map(x => x.TotalH = objtotal)

    setTimeout(() => {
      this.service.post('api/AppDokterRawatJalan/InstPenunMedisH', JSON.stringify(obj))
        .subscribe(result => {
          const r = JSON.parse(result)
          if (r.Trx) {
            obj.map(x => x.No_Trx = `${r.Trx}`)
          }
          this.service.post('api/AppDokterRawatJalan/InstPenunMedisD', JSON.stringify(obj))
            .subscribe(result => {
              this.strTindakan = '';
              this.arrTindJprs = [];
              this.dataTindakan = [];
              this.arr_dataPemeriksaan = [];
              this.getdataTindPasien(this.noreg, this.kdlyn);
              this.getPelPenunjangMedis(this.noreg);
              const paramLog = {
                NoReg: this.noreg,
                Kd_Dokter: this.useraccessdata.kd_dokter,
                Aksi: 'INPUT DATA PENUNJANG MEDIS',
                Jenis: '1'
              }

              this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
                .subscribe(result => {
                  this.loading = false;
                },
                  err => {
                    this.loading = false;
                  });
            }, error => {
              this.strTindakan = '';
              this.arrTindJprs = [];
              this.dataTindakan = [];
              this.arr_dataPemeriksaan = [];
              this.getdataTindPasien(this.noreg, this.kdlyn);
              this.getPelPenunjangMedis(this.noreg);
            });
        }, error => {
          this.strTindakan = '';
          this.arrTindJprs = [];
          this.dataTindakan = [];
          this.arr_dataPemeriksaan = [];
          this.getdataTindPasien(this.noreg, this.kdlyn);
          this.getPelPenunjangMedis(this.noreg);
        });
    }, 1000);
  }


  submitUpdtLynPenunjangMedis() {
    this.loading = true;
    (this.trxPM != '' && this.arr_dataPemeriksaan.length > 0)
    const obj = this.arr_dataPemeriksaan.filter(x => x['JmlTrx'] !== '0');
    for (let i = 0; i < obj.length; i++) {
      obj[i].No_Trx = this.trxPM;
    }
    let objtotal = 0;
    obj.map(x => objtotal += x.Total)
    obj.map(x => x.TotalH = objtotal)

    this.service.post('api/AppDokterRawatJalan/UpdtPenunMedisH', JSON.stringify(obj))
      .subscribe(result => {
        this.service.post('api/AppDokterRawatJalan/UpdtPenunMedisD', JSON.stringify(obj))
          .subscribe(result => {
            this.strTindakan = '';
            this.arrTindJprs = [];
            this.dataTindakan = [];
            this.arr_dataPemeriksaan = [];
            this.getdataTindPasien(this.noreg, this.kdlyn);
            this.getPelPenunjangMedis(this.noreg);
            this.loading = false;
            const paramLog = {
              NoReg: this.noreg,
              Kd_Dokter: this.useraccessdata.kd_dokter,
              Aksi: 'UPDATE DATA PENUNJANG MEDIS',
              Jenis: '1'
            }

            this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
              .subscribe(result => {
                this.loading = false;
              },
                err => {
                  this.loading = false;
                });
          }, error => {
            this.strTindakan = '';
            this.arrTindJprs = [];
            this.dataTindakan = [];
            this.arr_dataPemeriksaan = [];
            this.getdataTindPasien(this.noreg, this.kdlyn);
            this.getPelPenunjangMedis(this.noreg);
            this.loading = false;
          });
      }, error => {
        this.strTindakan = '';
        this.arrTindJprs = [];
        this.dataTindakan = [];
        this.arr_dataPemeriksaan = [];
        this.getdataTindPasien(this.noreg, this.kdlyn);
        this.getPelPenunjangMedis(this.noreg);
        this.loading = false;
      });

  }

  deletePelPenunjangMedis(notrx) {
    this.loading = true;
    const obj = {
      No_Trx: notrx,
      No_Reg: this.noreg
    }
    this.service.post('api/AppDokterRawatJalan/DelPenunMedis', JSON.stringify(obj))
      .subscribe(result => {
        this.strTindakan = '';
        this.arrTindJprs = [];
        this.dataTindakan = [];
        this.arr_dataPemeriksaan = [];
        this.getdataTindPasien(this.noreg, this.kdlyn);
        this.getPelPenunjangMedis(this.noreg);
        this.getHasilLab(this.kdrm);
        this.loading = false;
      }, error => {
        this.strTindakan = '';
        this.arrTindJprs = [];
        this.dataTindakan = [];
        this.arr_dataPemeriksaan = [];
        this.getdataTindPasien(this.noreg, this.kdlyn);
        this.getPelPenunjangMedis(this.noreg);
        this.getHasilLab(this.kdrm);
        this.loading = false;
      });

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'DELETE DATA PENUNJANG MEDIS',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  deleteTindakanPemeriksaan(kd_tindakan, trx, Kd_Lyn, Jenis_Pelayanan) {
    this.loading = true;
    const obj = {
      No_Trx: trx,
      Kd_Tindakan: kd_tindakan
    }
    this.service.post('api/AppDokterRawatJalan/DelTindakanPenunMedis', JSON.stringify(obj))
      .subscribe(result => {
        this.strTindakan = '';
        this.arrTindJprs = [];
        this.dataTindakan = [];
        this.arr_dataPemeriksaan = [];
        this.editPenunMedis(Kd_Lyn, Jenis_Pelayanan, trx);
        this.getPaketPM();
        this.getdataTindPasien(this.noreg, this.kdlyn);
        this.getPelPenunjangMedis(this.noreg);
        this.loading = false;
        const paramLog = {
          NoReg: this.noreg,
          Kd_Dokter: this.useraccessdata.kd_dokter,
          Aksi: 'DELETE PEMERIKSAAN PENUNJANG MEDIS',
          Jenis: '1'
        }

        this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });
      }, error => {
        this.strTindakan = '';
        this.arrTindJprs = [];
        this.dataTindakan = [];
        this.arr_dataPemeriksaan = [];
        this.editPenunMedis(Kd_Lyn, Jenis_Pelayanan, trx);
        this.getPaketPM();
        this.getdataTindPasien(this.noreg, this.kdlyn);
        this.getPelPenunjangMedis(this.noreg);
        this.loading = false;
      });
  }

  arr_editdataPemeriksaan = [];
  editDataPemeriksaan = false;
  trxPM = '';
  editPenunMedis(Kd_Lyn, Jenis_Pelayanan, trx) {
    this.trxPM = trx;
    this.selectIsiPenunjangMedis(Kd_Lyn, Jenis_Pelayanan);
    const obj = {
      No_Trx: trx,
      Kd_Dokter: this.useraccessdata.kd_dokter
    }
    this.service.post('api/AppDokterRawatJalan/GetEditTindMedis', JSON.stringify(obj))
      .subscribe(result => {
        const data = JSON.parse(result);
        this.editDataPemeriksaan = true;
        this.arr_editdataPemeriksaan = data;
        this.getPaketPM();
        this.loading = false;
        const paramLog = {
          NoReg: this.noreg,
          Kd_Dokter: this.useraccessdata.kd_dokter,
          Aksi: 'MELIHAT DATA PENUNJANG MEDIS YANG TELAH DI INPUT',
          Jenis: '1'
        }

        this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });
      }, error => {
        this.getPaketPM();
        this.editDataPemeriksaan = false;
        this.loading = false;
      });
  }

  onResetPM() {
    this.trxPM = '';
    this.kd_pmlyn = '';
    this.arrCust = [];
    this.arrCustome = [];
    this.arr_dataPemeriksaan = [];
    this.arr_editdataPemeriksaan = [];
  }

  filterPemeriksaanLyn: string = '';
  clearCariPM() {
    this.filterPemeriksaanLyn = '';
  }
  /** PM */

  /** ----------------------- start Tindakan ------------------------------ */

  arrTindJprs = [];
  cito_tind: number = 0;
  disc_tind: number = 0;
  public dataJprs: any;
  getDataJprs() {
    var kddokter = this.useraccessdata.kd_dokter;
    var kdlyn = this.useraccessdata.kode_lyn;
    this.service.get("api/Tindakan/GetTindakanJPRS/" + kddokter + "/" + kdlyn, '')
      .subscribe(result => {
        this.dataJprs = JSON.parse(result);
        this.arrTindJprs = this.dataJprs;
        this.arrTindJprs.map(x => {
          x.Qty = '0'
          x.Total = '0'
          x.Harga = '0',
            x.Cito = '0',
            x.Disc = '0'
        })
        // console.log('getDataJprs :', this.arrTindJprs);
      });
  }

  editQtyTindJprs(value, kd) {
    var index = this.arrTindJprs.findIndex(x => x.Kd_Tindakan == kd);
    if (index !== -1) {
      if (this.cara_bayar == '6. FOC') {
        this.arrTindJprs[index].Cito = this.cito_tind;
        this.arrTindJprs[index].Disc = this.disc_tind;
        this.arrTindJprs[index].Qty = value;
        this.arrTindJprs[index].Harga = this.arrTindJprs[index].Hrg_Kary;
        this.arrTindJprs[index].Total = this.arrTindJprs[index].Qty * this.arrTindJprs[index].Hrg_Kary;

      } else if (this.cara_bayar == '9. BPJS') {
        this.arrTindJprs[index].Cito = this.cito_tind;
        this.arrTindJprs[index].Disc = this.disc_tind;
        this.arrTindJprs[index].Qty = value;
        this.arrTindJprs[index].Harga = this.arrTindJprs[index].Hrg_BPJS;
        this.arrTindJprs[index].Total = this.arrTindJprs[index].Qty * this.arrTindJprs[index].Hrg_BPJS;
      } else {
        this.arrTindJprs[index].Cito = this.cito_tind;
        this.arrTindJprs[index].Disc = this.disc_tind;
        this.arrTindJprs[index].Qty = value;
        this.arrTindJprs[index].Harga = this.arrTindJprs[index].Hrg_Std;
        this.arrTindJprs[index].Total = this.arrTindJprs[index].Qty * this.arrTindJprs[index].Hrg_Std;
      }
    }
  }

  deleteTindakanJprs(index) {
    this.arrTindJprs.splice(index, 1);
  }

  deleteTindakanJprsNew(index, noreg, kdlyn, kdtind) {
    this.loading = true;
    this.arrTindJprs.splice(index, 1);
    var paramDeleteListTindJprs = {
      noreg: noreg,
      kdlyn: kdlyn,
      kdtind: kdtind
    }
    // console.log('api/Tindakan/DeleteListTindakan', JSON.stringify(paramDeleteListTind));
    this.service.post('api/Tindakan/DeleteListTindakan', JSON.stringify(paramDeleteListTindJprs))
      .subscribe(result => {
        // console.log(result)
        this.getdataTindPasien(this.noreg, this.kdlyn);
        this.loading = false;
        const paramLog = {
          NoReg: this.noreg,
          Kd_Dokter: this.useraccessdata.kd_dokter,
          Aksi: 'DELETE DATA TINDAKAN',
          Jenis: '1'
        }
    
        this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
          .subscribe(result => {
            this.loading = false;
          },
            err => {
              this.loading = false;
            });
      });
  }

  onSelectTindakan(value) {
    let data = value["originalObject"];
    // console.log('onSelectTindakan data :', data)
    this.dataTindakan.push({ ...data, Qty: '0', Cito: '0', Disc: '0', Harga: '0', Total: '0' });
    this.strTindakan = '';

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'MENAMBAHKAN TINDAKAN ( ' + data.Nama_Tindakan + ' )',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  Harga: number = 0;
  editQtyTind(value, kd) {
    var index = this.dataTindakan.findIndex(x => x.Kd_Tindakan == kd);
    if (index !== -1) {
      if (this.cara_bayar == '6. FOC') {
        this.dataTindakan[index].Cito = this.cito_tind;
        this.dataTindakan[index].Disc = this.disc_tind;
        this.dataTindakan[index].Qty = value;
        this.dataTindakan[index].Harga = this.dataTindakan[index].Hrg_Kary;
        this.dataTindakan[index].Total = this.dataTindakan[index].Qty * this.dataTindakan[index].Hrg_Kary;

      } else if (this.cara_bayar == '9. BPJS') {
        this.dataTindakan[index].Cito = this.cito_tind;
        this.dataTindakan[index].Disc = this.disc_tind;
        this.dataTindakan[index].Qty = value;
        this.dataTindakan[index].Harga = this.dataTindakan[index].Hrg_BPJS;
        this.dataTindakan[index].Total = this.dataTindakan[index].Qty * this.dataTindakan[index].Hrg_BPJS;
      } else {
        this.dataTindakan[index].Cito = this.cito_tind;
        this.dataTindakan[index].Disc = this.disc_tind;
        this.dataTindakan[index].Qty = value;
        this.dataTindakan[index].Harga = this.dataTindakan[index].Hrg_Std;
        this.dataTindakan[index].Total = this.dataTindakan[index].Qty * this.dataTindakan[index].Hrg_Std;
      }
    }
  }

  deleteFieldTindakan(index) {
    this.dataTindakan.splice(index, 1);
  }

  dataPostTindakan: any;
  onSubmitTindakan() {
    this.loading = true;
    const allTindakan = this.arrTindJprs.concat(this.dataTindakan)
    const allTindakanFilt = allTindakan.filter(x => x['Qty'] !== '0')
    const paramInsertTindakan = {
      NoReg: this.noreg,
      Kd_Lyn: this.useraccessdata.kode_lyn,
      data: allTindakanFilt
    }
    this.service.post('api/Tindakan/PostTindakan', JSON.stringify(paramInsertTindakan))
      .subscribe(result => {
        this.dataPostTindakan = JSON.parse(result);
        this.strTindakan = '';
        this.arrTindJprs = [];
        this.dataTindakan = [];
        this.getdataTindPasien(this.noreg, this.kdlyn);
        this.loading = false;
      }, error => {
        this.strTindakan = '';
        this.arrTindJprs = [];
        this.dataTindakan = [];
        this.getdataTindPasien(this.noreg, this.kdlyn);
        this.loading = false;
      });

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'INPUT DATA TINDAKAN',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  deleteListTindakan(noreg, kdlyn, kdtind) {
    this.loading = true;
    var paramDeleteListTind = {
      noreg: noreg,
      kdlyn: kdlyn,
      kdtind: kdtind
    }
    // console.log('api/Tindakan/DeleteListTindakan', JSON.stringify(paramDeleteListTind));
    this.service.post('api/Tindakan/DeleteListTindakan', JSON.stringify(paramDeleteListTind))
      .subscribe(result => {
        // console.log(result)
        this.getdataTindPasien(this.noreg, this.kdlyn);
        this.loading = false;
      });

    const paramLog = {
      NoReg: this.noreg,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'DELETE DATA TINDAKAN',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  dataTindakanPasien: any;
  kdlyn = '';
  kdkls = '';
  getdataTindPasien(noreg, kdlyn) {
    this.noreg = noreg;
    this.kdlyn = kdlyn;
    // console.log("api/Tindakan/GetDataTindakanPasien/" + noreg + "/" + kdlyn, '');
    this.service.get("api/Tindakan/GetDataTindakanPasien/" + noreg + "/" + kdlyn, '')
      .subscribe(result => {
        this.dataTindakanPasien = JSON.parse(result);
        // console.log(this.dataTindakanPasien);
      });

    this.service.get('api/Users/GetObjKdKelas/' + noreg, '')
      .subscribe(r => {
        const res = JSON.parse(r);
        this.kdkls = res[0]['KD. KLS'];
      });
  }
  /** ------------------------ End Tindakan ----------------------- */

  /** Get Update Skrinning pasien utk diagnosa */
  dataSkrinningPasien: any;
  keluhanUtama: string = '';
  keluhanUtamaD: string = '';
  getSkrinningPasien(rjNoreg) {
    this.diagUtamaPilih = '';
    var data: any;
    this.service.get("api/AppDokterRJDiag/GetSkrinningPasien/" + rjNoreg, data)
      .subscribe(result => {
        this.keluhanUtama = JSON.parse(result).KeluhanUtama;
      });
  }

  onUpdateSkrinningPasienD() {
    var paramUpdSkrinningD = {
      KeluhanUtama: this.keluhanUtamaD,
      NoReg: this.rjNoreg
    }
    // console.log('api/AppDokterRJDiag/UpdateSkrinningPasien', JSON.stringify(paramUpdSkrinningD))
    this.service.put('api/AppDokterRJDiag/UpdateSkrinningPasien', JSON.stringify(paramUpdSkrinningD))
      .subscribe(result => {
        this.keluhanUtamaD = '';
      });
  }
  /** Get Update Skrinning pasien utk diagnosa */

  /** Fungsi Mulai-Selesai Praktek */
  data_QService: any;
  status_praktek = '';
  func_statusAwalPrakDokter() {
    this.kd_poli = this.useraccessdata.kd_poli;
    var input = {
      Kd_Poli: this.kd_poli
    }

    this.service.post('api/AppDokterRJInfo/StatusAwalPraktekDokter', JSON.stringify(input))
      .subscribe(result => {
        this.data_QService = JSON.parse(result);
        this.status_praktek = this.data_QService.Status_Praktek;
      });
  }

  /** Status Praktek Dokter Mulai */
  func_mulai() {
    this.loading = true;
    this.can_praktek = true;
    var paramStatusMulai = {
      Kd_Lyn: this.useraccessdata.kode_lyn,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Jam_Praktek: this.useraccessdata.jam_praktek,
      Kd_Poli: this.useraccessdata.kd_poli
    }

    this.service.put('api/AppDokterRJInfo/UpdateStatusPraktekMulai', JSON.stringify(paramStatusMulai))
      .subscribe(result => {
        this.func_statusAwalPrakDokter();
        this.loading = false;
      },
        err => {
          // console.log(err);
          this.loading = false;
        });

    const paramLog = {
      NoReg: '',
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'MULAI PRAKTEK',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  /** Status Praktek Dokter Selesai */
  func_selesai() {
    this.loading = true;
    this.can_praktek = false;
    var paramStatusSelesai = {
      Kd_Poli: this.useraccessdata.kd_poli,
      Tgl_Masuk: this.useraccessdata.tgl_hari_ini,
      Kd_Lyn: this.useraccessdata.kode_lyn,
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Jam_Praktek: this.useraccessdata.jam_praktek
    }
    this.service.put('api/AppDokterRJInfo/UpdateStatusPraktekSelesai', JSON.stringify(paramStatusSelesai))
      .subscribe(result => {
        this.func_statusAwalPrakDokter();
        this.loading = false;
      },
        err => {
          this.loading = false;
        });

    const paramLog = {
      NoReg: '',
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'SELESAI PRAKTEK',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  func_info() {
    this.loading = true;
    const param = {
      kdpoli: this.useraccessdata.kd_poli,
      info: this.info
    }
    this.service.post('api/AppDokterRJInfo/UpdateInfoPraktek', JSON.stringify(param))
      .subscribe(result => {
        this.info_praktek = true;
        this.can_praktek = false;
        this.offPrak = true;
        this.loading = false;
      },
        err => {
          this.loading = false;
        });

    const paramLog = {
      NoReg: '',
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'MENGINFOKAN STATUS PRAKTEK',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  func_lanjut() {
    this.loading = true;
    const param = {
      kdpoli: this.useraccessdata.kd_poli,
      info: null
    }
    this.service.post('api/AppDokterRJInfo/UpdateInfoPraktek', JSON.stringify(param))
      .subscribe(result => {
        this.info = null;
        this.info_praktek = false;
        this.can_praktek = true;
        this.offPrak = false;
        this.loading = false;
      },
        err => {
          this.loading = false;
        });

    const paramLog = {
      NoReg: '',
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'LANJUT PRAKTEK',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  resetData() {
    // meng clear data sebelumnya
    this.dataEditObatNRacik = [];
    this.dataEditNamaRacik = [];
    this.dataResepRacik = [];
    this.dataAlkesObatByNoReg = [];
    this.dataTindakanPasien = [];
  }

}
