import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuUtamaComponent } from './menu-utama.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';

export const MenuUtamaRoutes: Routes = [
{
  path: '',
  component: MenuUtamaComponent,
  data: {
    breadcrumb: 'Dashboard RJ Dokter',
    icon: 'icofont-home bg-c-blue',
    status: false,
    title: 'MenuUtamaDokter'
  }
}
];

@NgModule({
  imports: [
  CommonModule,
  RouterModule.forChild(MenuUtamaRoutes),
  SharedModule,
  LoadingModule
  ],
  declarations: [MenuUtamaComponent],
  providers: [AppService]
})
export class MenuUtamaModule { }