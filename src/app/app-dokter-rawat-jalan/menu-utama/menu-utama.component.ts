import {
  Component,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import '../../../assets/echart/echarts-all.js';
import * as _ from "lodash";
import {
  AppService
} from "../../shared/service/app.service";
import swal from 'sweetalert2';
import {
  SessionService
} from '../../shared/service/session.service';
import {
  Pipe,
  PipeTransform
} from '@angular/core';
import {
  DatePipe
} from '@angular/common';

@Component({
  selector: 'app-dokter',
  templateUrl: './menu-utama.component.html',
  styleUrls: [
    './menu-utama.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class MenuUtamaComponent implements OnInit {

  public useraccessdata: any;
  public loading = false;

  public dataPasienRJ: any = {};

  constructor(private service: AppService, public session: SessionService) {

    setInterval(() => this.reloadPage(), 60000);

    let useracces = this.session.getData();
    this.useraccessdata = JSON.parse(useracces);
    // console.log(this.useraccessdata);
  }

  reloadPage() {
    this.ngOnInit();
  }

  ngOnInit() {
    this.getPasienRJ();
  }

  log() {
    const paramLog = {
      NoReg: '',
      Kd_Dokter: this.useraccessdata.kd_dokter,
      Aksi: 'MELIHAT JUMLAH PASIEN',
      Jenis: '1'
    }

    this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
      .subscribe(result => {
        this.loading = false;
      },
        err => {
          this.loading = false;
        });
  }

  getPasienRJ() {
    var param1 = this.useraccessdata.tgl_hari_ini;
    var param2 = this.useraccessdata.kode_lyn;
    var param3 = this.useraccessdata.kd_dokter;
    var param4 = this.useraccessdata.jam_praktek;

    // console.log("api/AppDokterRawatJalan/CountListPasienRJDokter", { tglMasuk: '2019-05-02', kdLyn: param2, kdDokter: param3, jamPraktek: param4 })
    this.service.post("api/AppDokterRawatJalan/CountListPasienRJDokter", {
      //tglMasuk: '2019-05-21',
      tglMasuk: param1,
      kdLyn: param2,
      kdDokter: param3,
      jamPraktek: param4
    })
      .subscribe(result => {
        if (result == '') {
          this.dataPasienRJ = '';
        } else {
          this.dataPasienRJ = JSON.parse(result);
        }
      },
        error => {
          window.alert("Mohon maaf terjadi kesalahan pada server");
        });
  }
}
