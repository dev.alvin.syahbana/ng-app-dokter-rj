import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { AppService } from "../../shared/service/app.service"; 
import { ActivatedRoute } from '@angular/router';
import { SessionService } from '../../shared/service/session.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html'
})
export class ResetComponent implements OnInit {

  
  public loading = false;
  private _serviceUrl = '/api/Auth/ChangePassword';
  public data: any;
  changepasswordform: FormGroup;
  public useraccesdata:any;
  public form: FormGroup;
  id:string;
  
 
  constructor(private service: AppService, private route: ActivatedRoute, private router: Router, private session: SessionService) {
    
    //get user level  
    let password = new FormControl('', Validators.required);  
    let rpassword = new FormControl('', [Validators.required, CustomValidators.equalTo(password)]);

    this.changepasswordform = new FormGroup({ 
      password: password,
      rpassword: rpassword 
    });

  }

  ngOnInit() {
    
    this.id = this.route.snapshot.params['code'];
    console.log(this.id);

    //get activity data detail
    let data = "";
    this.service.get("/api/Auth/ResetPassword/"+this.id,data)
      .subscribe(result => { 
        this.loading = false;  
        var resource = JSON.parse(result);
        console.log(resource);
        if(resource['code'] == '1') {
            //this.service.openSuccessSwal(resource['message']); 
        }
        else{ 
            swal(
              'Information!',
              resource['message'],
              'error'
            );
            this.router.navigate(['/forget-password']);
        }
        
    },
    error => { 
      this.loading = false;  
        this.service.errorserver();
    });
 
  }

  onSubmit() {
    this.loading = true; 
    this.changepasswordform.value.ResetPasswordCode = this.id;
    this.changepasswordform.value.Password = this.changepasswordform.value.password;

    let data = JSON.stringify(this.changepasswordform.value); 
     this.service.post(this._serviceUrl, data)
     .subscribe(result => {
       
        this.loading = false;  
        var resource = JSON.parse(result);
        if(resource['code'] == '1') {
            this.service.openSuccessSwal(resource['message']); 
        }
        else{ 
            swal(
              'Information!',
              resource['message'],
              'error'
            );
        }
     },
     error => { 
      this.loading = false;  
         this.service.errorserver();
     });
     
     this.router.navigate(['/login']);
  }
}
