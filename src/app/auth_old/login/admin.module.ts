import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';



export const AdminRoutes: Routes = [
  {
    path: '',
    component: AdminComponent,
    data: {
      breadcrumb: 'Admin ',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    LoadingModule,
    RouterModule.forChild(AdminRoutes),
    SharedModule
  ],
  declarations: [AdminComponent],
  providers: [AppService]
})
export class AdminModule { }
