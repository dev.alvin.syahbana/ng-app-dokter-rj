import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { Router } from '@angular/router';
import { Http, Headers, Response } from "@angular/http";
import { SessionService } from '../../shared/service/session.service';

import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';

import { state, style, transition, animate, trigger, AUTO_STYLE } from "@angular/animations";

import { TranslateService } from "@ngx-translate/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: [
    './admin.component.css'
  ],
  animations: [
    trigger('mobileMenuTop', [
      state('no-block, void',
        style({
          overflow: 'hidden',
          height: '0px',
        })
      ),
      state('yes-block',
        style({
          height: AUTO_STYLE,
        })
      ),
      transition('no-block <=> yes-block', [
        animate('400ms ease-in-out')
      ])
    ])
  ]
})
export class AdminComponent implements OnInit {

  deviceType = 'desktop';
  verticalNavType = 'expanded';
  verticalEffect = 'shrink';
  innerHeight: string;
  isCollapsedMobile = 'no-block';
  windowWidth: number;

  public loading = false;

  private _serviceUrl = '/api/auth/LoginUser';
  myForm: FormGroup;
  messageResult: string = '';
  messageError: string = '';
  public useraccesdata: any;

  listlanguage: any;
  listlanguage1 = [];
  public ActiveLang: string = "";

  constructor(public session: SessionService, private router: Router, private _http: Http, private translate: TranslateService, private http: HttpClient) {

    let Email = new FormControl('', [Validators.required, Validators.email]);
    let Password = new FormControl('', Validators.required);

    this.myForm = new FormGroup({
      Email: Email,
      Password: Password
    });

    let scrollHeight = window.screen.height - 150;
    this.innerHeight = scrollHeight + 'px';
    this.windowWidth = window.innerWidth;
    this.setMenuAttributs(this.windowWidth);

    function compare(a, b) {
      // Use toUpperCase() to ignore character casing
      const valueA = a.KeyValue.toUpperCase();
      const valueB = b.KeyValue.toUpperCase();

      let comparison = 0;
      if (valueA > valueB) {
        comparison = 1;
      } else if (valueA < valueB) {
        comparison = -1;
      }
      return comparison;
    }

    this.http
      .get("api/Dictionaries/where/{'Parent':'Languages','Status':'A'}")
      .subscribe(result => {
        translate.langs = [];
        var resTemp: any;
        resTemp = result;
        if (resTemp.length != 0) {
          for (let i = 0; i < resTemp.length; i++) {
            if (this.listlanguage1.length == 0) {
              this.listlanguage1.push(resTemp[i]);
            } else {
              for (let k = 0; k < this.listlanguage1.length; k++) {
                var exist = false;
                if (resTemp[i].Key == this.listlanguage1[k].Key) {
                  exist = true;
                }
              }
              if (exist == false) {
                this.listlanguage1.push(resTemp[i]);
              }
            }
          }
        }
        translate.addLangs(this.listlanguage1.sort(compare));
      });

    //Menetapkan bahasa pada browser sesuai dengan bahasa yang dipilih
    if (localStorage.getItem("language")) {
      translate.setDefaultLang(localStorage.getItem("language"));
      translate.use(localStorage.getItem("language"));
      this.ActiveLang = localStorage.getItem("language");
    } else {
      translate.setDefaultLang("Bahasa Indonesia");
      translate.use("Bahasa Indonesia");
      localStorage.setItem("language", "Bahasa Indonesia");
      this.ActiveLang = "Bahasa Indonesia";
    }


  }

  ngOnInit() {
  }

  setLanguage(value) {
    this.translate.use(value);
    localStorage.setItem("language", value);
    this.ActiveLang = value;
    location.reload();
  }

  onResize(event) {
    this.innerHeight = event.target.innerHeight + 'px';
    /* menu responsive */
    this.windowWidth = event.target.innerWidth;
    this.setMenuAttributs(this.windowWidth);
  }

  setMenuAttributs(windowWidth) {
    if (windowWidth >= 768 && windowWidth <= 1024) {
      this.deviceType = 'tablet';
      this.verticalNavType = 'collapsed';
      this.verticalEffect = 'push';
    } else if (windowWidth < 768) {
      this.deviceType = 'mobile';
      this.verticalNavType = 'offcanvas';
      this.verticalEffect = 'overlay';
    } else {
      this.deviceType = 'desktop';
      this.verticalNavType = 'expanded';
      this.verticalEffect = 'shrink';
    }
  }

  onMobileMenu() {
    this.isCollapsedMobile = this.isCollapsedMobile === 'yes-block' ? 'no-block' : 'yes-block';
  }

  //submin form login
  submitted: boolean;

  onSubmit() {
    this.submitted = true


    // //get value from form login eim (object)
    let login_val = this.myForm.value;

    // console.log("");
    // console.log("login eim form value (without convert(object)) :");
    // console.log("email -> "+login_val.email);
    // console.log("password -> "+login_val.password);
    // console.log("course code -> "+login_val.course);

    // //convert object to json
    let login_val_json = JSON.stringify(login_val);

    // console.log("");
    // console.log("login eim form value (convert object to json) :");
    // console.log("login value (json) -> "+login_val_json);

    this.loading = true;
    this.post(login_val_json)
      .subscribe(result => {
        this.loading = false;
        this.messageResult = result;
        // console.log(result);
        var resource = JSON.parse(result);
        //console.log(resource);
       // console.log(resource['code']);
         //console.log(resource['msg']);
        if (resource['code'] == '1') {
          /*
        swal(
          'Authentication!',
          resource['msg'],
          'success'
        );
          */
          this.session.logIn(resource["jwt"], JSON.stringify(resource['user']));

          let useracces = this.session.getData();
          this.useraccesdata = JSON.parse(useracces);

          var level2 = this.useraccesdata.UserLevelId.split(',');

          if (level2[0] == "ADMIN" || level2[0] == "ORGANIZATION" || level2[0] == "DISTRIBUTOR" || level2[0] == "TRAINER" || level2[0] == "STUDENT") {
            this.router.navigate(['/dashboard-' + level2[0].toLowerCase() + '']);
          }
          else {
            this.router.navigate(['/dashboard-igd']);
          }

        }
        else if (resource['code'] == 'firstlogin') {
          this.router.navigate(['/set-password'], { queryParams: { id: resource['id'] } });
        }
        else {
          swal(
            'Authentication!',
            resource['msg'],
            'error'
          );
          this.loading = false;
        }
      },
        error => {
          this.messageError = <any>error
          swal(
            'Authentication!',
            'Can\'t connect to server.',
            'error'
          );
          this.loading = false;
        });

  }

  post(data): Observable<string> {
    return this._http.post(
      this._serviceUrl,
      data,
      { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
    )
      .map((response: Response) => {
        return response.text();
      });
  }

}
