import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ForgotComponent} from './forgot.component';
import {SharedModule} from '../../shared/shared.module';
import { LoadingModule } from 'ngx-loading';
import {AppFormatDate} from "../../shared/format-date/app.format-date";

export const forgotRoutes: Routes = [
  {
    path: '',
    component: ForgotComponent,
    data: {
      breadcrumb: 'Forgot'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    LoadingModule,
    RouterModule.forChild(forgotRoutes),
    SharedModule
  ],
  declarations: [ForgotComponent],
  providers:[AppFormatDate]
})
export class ForgotModule { }
