import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TerminateAccountComponent } from './terminate-account.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {AppFormatDate} from "../../shared/format-date/app.format-date";
import {AppService} from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';
import { SessionService } from '../../shared/service/session.service';
 


export const TerminateAccountRoutes: Routes = [
  {
    path: '',
    component: TerminateAccountComponent,
    data: {
      breadcrumb: 'Student Register',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    LoadingModule,
    RouterModule.forChild(TerminateAccountRoutes),
    SharedModule
  ],
  declarations: [TerminateAccountComponent],
  providers:[AppService,AppFormatDate,SessionService]
})
export class TerminateAccountModule { }
