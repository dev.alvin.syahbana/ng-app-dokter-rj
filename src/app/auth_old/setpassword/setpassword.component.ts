import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { AppService } from "../../shared/service/app.service"; 
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import {PasswordValidators} from 'ngx-validators';

@Component({
  selector: 'app-setpassword',
  templateUrl: './setpassword.component.html'
})
export class SetPasswordComponent implements OnInit {
 
  private _serviceUrl = '/api/ContactAll';
  public data: any;
  changepasswordform: FormGroup;
  id:String="";
  password:String="";
  contactid:String="";
  siteid:String="";

  constructor(private service: AppService, private route: ActivatedRoute, private router: Router) {
    var sub: any;
    sub = this.route.queryParams.subscribe(params => {
        this.id = params['id'] || "";
        this.password = params['p'] || "";
        this.contactid = params['c'] || "";
        this.siteid = params['s'] || "";
    });

    let currentpassword = new FormControl(this.password, Validators.required);
    let password = new FormControl('', [Validators.required, Validators.minLength(6),Validators.compose([
      PasswordValidators.digitCharacterRule(1),
      PasswordValidators.specialCharacterRule(1)])]);
    let rpassword = new FormControl('', [Validators.required, CustomValidators.equalTo(password)]);

    this.changepasswordform = new FormGroup({
      password: password,
      rpassword: rpassword,
      currentpassword: currentpassword
    });

  }

  ngOnInit() {}

  onSubmit() {
    this.changepasswordform.controls['password'].markAsTouched();
    this.changepasswordform.controls['rpassword'].markAsTouched();
    this.changepasswordform.controls['currentpassword'].markAsTouched();

    if(this.changepasswordform.valid){
      let data = JSON.stringify(this.changepasswordform.value);
      if(this.contactid != "" && this.siteid !=""){
        this.service.httpCLientPutPassword(this._serviceUrl+'/ChangePassword',this.id, data, '/manage/contact/detail-contact,'+this.contactid+','+this.siteid, '/set-password');
      }else{
        this.service.httpCLientPutPassword(this._serviceUrl+'/ChangePassword',this.id, data, '/logout-contact', '/set-password');
      }
      this.changepasswordform.reset();
    }
  }
  
}
