import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SetPasswordComponent} from './setpassword.component';
import {SharedModule} from '../../shared/shared.module';
import { LoadingModule } from 'ngx-loading';

export const SetPasswordRoutes: Routes = [
  {
    path: '',
    component: SetPasswordComponent,
    data: {
      breadcrumb: 'Set Password'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    LoadingModule,
    RouterModule.forChild(SetPasswordRoutes),
    SharedModule
  ],
  declarations: [SetPasswordComponent]
})
export class SetPasswordModule { }
