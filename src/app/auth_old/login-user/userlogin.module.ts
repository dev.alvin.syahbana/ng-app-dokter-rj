import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserLoginComponent } from './userlogin.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';



export const UserLoginRoutes: Routes = [
  {
    path: '',
    component: UserLoginComponent,
    data: {
      breadcrumb: 'User ',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    LoadingModule,
    RouterModule.forChild(UserLoginRoutes),
    SharedModule
  ],
  declarations: [UserLoginComponent],
  providers: [AppService]
})
export class UserLoginModule { }
