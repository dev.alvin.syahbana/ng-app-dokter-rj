import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { Router } from '@angular/router';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import { PasswordValidators } from 'ngx-validators';
import { CompleterService, CompleterData, RemoteData } from "ng2-completer";
import { state, style, transition, animate, trigger, AUTO_STYLE } from "@angular/animations";
import { TranslateService } from "@ngx-translate/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { elementsFromPoint } from '@swimlane/ngx-datatable/release/utils';

@Component({
  selector: 'app-studentregister',
  templateUrl: './studentregister.component.html',
  animations: [
    trigger('mobileMenuTop', [
      state('no-block, void',
        style({
          overflow: 'hidden',
          height: '0px',
        })
      ),
      state('yes-block',
        style({
          height: AUTO_STYLE,
        })
      ),
      transition('no-block <=> yes-block', [
        animate('400ms ease-in-out')
      ])
    ])
  ],
  encapsulation: ViewEncapsulation.None
})

export class StudentRegisterComponent implements OnInit {

  deviceType = 'desktop';
  verticalNavType = 'expanded';
  verticalEffect = 'shrink';
  innerHeight: string;
  isCollapsedMobile = 'no-block';
  windowWidth: number;

  public loading = false;

  private _serviceUrl = '/api/Auth/Register';
  registerstudent: FormGroup;
  public data: any;
  public datadetail: any;
  public datacountry: any;
  messageResult: string = '';
  messageError: string = '';
  // modelPopup1: NgbDateStruct = {year: new Date().getFullYear() - 26, month: new Date().getMonth(), day: new Date().getDate()}; 
  public minDate: NgbDateStruct;
  public maxDate: NgbDateStruct;
  language: any;

  listlanguage: any;
  listlanguage1 = [];
  public ActiveLang: string = "";
  public ifStudent = false;
  // public isExist = true;
  // public isExpired = true;
  public isFinal = false;
  protected courseId: string = "";
  private selectedCourseId: string;
  protected dataService1: RemoteData;
  public disableVal = false;
  public showSelected = false;
  CourseSelected: string = "";
  obj: any;

  constructor(private router: Router, private _http: Http,
    private service: AppService, private formatdate: AppFormatDate, private translate: TranslateService, private http: HttpClient, private completerService: CompleterService) {

    this.minDate = { year: new Date().getFullYear() - 68, month: new Date().getMonth() + 1, day: new Date().getDate() };
    this.maxDate = { year: new Date().getFullYear() - 16, month: new Date().getMonth() + 1, day: new Date().getDate() - 1};

    /*
    let email = new FormControl('', [Validators.required,Validators.email]);
    let password = new FormControl('', Validators.required);
    let course = new FormControl('', Validators.required);

    this.myForm = new FormGroup({
      email:email,
      password: password,
      course: course
    });
    */

    this.dataService1 = completerService.remote(null, "Keyword", "Keyword");
    this.dataService1.urlFormater(term => {
      return "api/StudentsCourses/CheckCourse/" + term;
    });
    this.dataService1.dataField("results");

    //validation
    let FirstName = new FormControl('', Validators.required);
    let LastName = new FormControl('', Validators.required);
    let EmailAddress = new FormControl('', [Validators.required, Validators.email]);
    let EmailAddress2 = new FormControl('', [Validators.required, Validators.email, CustomValidators.notEqualTo(EmailAddress)]);
    let Country = new FormControl('', Validators.required);
    let CourseId = new FormControl('');
    let InstEmail1 = new FormControl('');
    let InstEmail2 = new FormControl('');
    // let ConfirmEmailAddress = new FormControl('', [Validators.required, CustomValidators.equalTo(EmailAddress)]); 
    let DateAdded = new FormControl('');
    let AddedBy = new FormControl('');
    let DateBirth = new FormControl('');
    let LanguageID = new FormControl('', Validators.required);

    // let password = new FormControl('', [Validators.required, Validators.minLength(6), Validators.compose([
    //   PasswordValidators.digitCharacterRule(1),
    //   PasswordValidators.specialCharacterRule(1)])]);
    // let rpassword = new FormControl('', [Validators.required, CustomValidators.equalTo(password)]);

    this.registerstudent = new FormGroup({
      FirstName: FirstName,
      LastName: LastName,
      EmailAddress: EmailAddress,
      EmailAddress2:EmailAddress2,
      // ConfirmEmailAddress:ConfirmEmailAddress,
      Country: Country,
      DateAdded: DateAdded,
      AddedBy: AddedBy,
      DateBirth: DateBirth,
      LanguageID: LanguageID,
      CourseId: CourseId,
      InstEmail1: InstEmail1,
      InstEmail2: InstEmail2
      // password: password,
      // rpassword: rpassword
    });

    // pemilihan bahasa
    let scrollHeight = window.screen.height - 150;
    this.innerHeight = scrollHeight + 'px';
    this.windowWidth = window.innerWidth;
    this.setMenuAttributs(this.windowWidth);

    function compare(a, b) {
      // Use toUpperCase() to ignore character casing
      const valueA = a.KeyValue.toUpperCase();
      const valueB = b.KeyValue.toUpperCase();

      let comparison = 0;
      if (valueA > valueB) {
        comparison = 1;
      } else if (valueA < valueB) {
        comparison = -1;
      }
      return comparison;
    }

    this.http
      .get("api/Dictionaries/where/{'Parent':'Languages','Status':'A'}")
      .subscribe(result => {
        translate.langs = [];
        var resTemp: any;
        resTemp = result;
        if (resTemp.length != 0) {
          for (let i = 0; i < resTemp.length; i++) {
            if (this.listlanguage1.length == 0) {
              this.listlanguage1.push(resTemp[i]);
            } else {
              for (let k = 0; k < this.listlanguage1.length; k++) {
                var exist = false;
                if (resTemp[i].Key == this.listlanguage1[k].Key) {
                  exist = true;
                }
              }
              if (exist == false) {
                this.listlanguage1.push(resTemp[i]);
              }
            }
          }
        }
        translate.addLangs(this.listlanguage1.sort(compare));
      });

    //Menetapkan bahasa pada browser sesuai dengan bahasa yang dipilih
    if (localStorage.getItem("language")) {
      translate.setDefaultLang(localStorage.getItem("language"));
      translate.use(localStorage.getItem("language"));
      this.ActiveLang = localStorage.getItem("language");
    } else {
      translate.setDefaultLang("English");
      translate.use("English");
      localStorage.setItem("language", "English");
      this.ActiveLang = "English";
    }
  }

  ngOnInit() {

    var data = '';
    this.service.httpClientGet('/api/Countries', data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.datacountry = '';
        }
        else {
          this.datacountry = result;
        }
      },
        error => {
          this.service.errorserver();
          this.datacountry = '';
        });

    var language = '';
    this.service.get("api/Dictionaries/where/{'Parent':'Languages','Status':'A'}", language)
      .subscribe(result => {
        this.language = JSON.parse(result);
      },
        error => {
          this.service.errorserver();
        });
  }

  // pemilihan bahasa
  setLanguage(value) {
    this.translate.use(value);
    localStorage.setItem("language", value);
    this.ActiveLang = value;
    location.reload();
  }

  onResize(event) {
    this.innerHeight = event.target.innerHeight + 'px';
    /* menu responsive */
    this.windowWidth = event.target.innerWidth;
    this.setMenuAttributs(this.windowWidth);
  }

  setMenuAttributs(windowWidth) {
    if (windowWidth >= 768 && windowWidth <= 1024) {
      this.deviceType = 'tablet';
      this.verticalNavType = 'collapsed';
      this.verticalEffect = 'push';
    } else if (windowWidth < 768) {
      this.deviceType = 'mobile';
      this.verticalNavType = 'offcanvas';
      this.verticalEffect = 'overlay';
    } else {
      this.deviceType = 'desktop';
      this.verticalNavType = 'expanded';
      this.verticalEffect = 'shrink';
    }
  }

  onMobileMenu() {
    this.isCollapsedMobile = this.isCollapsedMobile === 'yes-block' ? 'no-block' : 'yes-block';
  }
  // pemilihan bahasa

  validDateBirth: Boolean = true;
  onSubmit() {
    this.registerstudent.controls['FirstName'].markAsTouched();
    this.registerstudent.controls['LastName'].markAsTouched();
    this.registerstudent.controls['EmailAddress'].markAsTouched();
    this.registerstudent.controls['EmailAddress2'].markAsTouched();
    // this.registerstudent.controls['ConfirmEmailAddress'].markAsTouched();
    this.registerstudent.controls['Country'].markAsTouched();
    this.registerstudent.controls['DateBirth'].markAsTouched();
    // this.registerstudent.controls['password'].markAsTouched();

    if(this.registerstudent.value.DateBirth == undefined){
      this.registerstudent.value.DateBirth = (this.ifStudent) ? { year: new Date().getFullYear(), month: new Date().getMonth(), day: new Date().getDate()} : { year: new Date().getFullYear() - 16, month: new Date().getMonth() + 1, day: new Date().getDate() - 1 };
    }else{
      this.registerstudent.value.DateBirth = (this.ifStudent) ? this.registerstudent.value.DateBirth : { year: new Date().getFullYear() - 16, month: new Date().getMonth() + 1, day: new Date().getDate() - 1 };
    }
    
    var date_birth = this.formatdate.dateCalendarToYMD(this.registerstudent.value.DateBirth)

    var ageDifMs = Date.now() - new Date(date_birth).getTime();
    var ageDate = new Date(ageDifMs);
    var age = Math.abs(ageDate.getUTCFullYear() - 1970);

    if (age < 16) {
      this.validDateBirth = false;
    } else {
      this.validDateBirth = true;
    }

    if (this.registerstudent.valid && this.validemail && this.validDateBirth) {

      this.loading = true;

      this.registerstudent.value.UserLevelId = 'STUDENT';
      this.registerstudent.value.Email = this.registerstudent.value.EmailAddress;
      // this.registerstudent.value.Password = '5f4dcc3b5aa765d61d8327deb882cf99';
      // this.registerstudent.value.PasswordPlain = 'password';
      this.registerstudent.value.Country = this.registerstudent.value.Country;
      this.registerstudent.value.DateBirth = date_birth;

      if(this.obj == undefined || this.obj == ""){
        this.registerstudent.value.CourseId = "";
        this.registerstudent.value.InstEmail1 = "";
        this.registerstudent.value.InstEmail2 = "";
      }else{
        this.registerstudent.value.CourseId = this.obj.CourseId;
        this.registerstudent.value.InstEmail1 = this.obj.EmailAddress;
        this.registerstudent.value.InstEmail2 = this.obj.EmailAddress2;
      }
      
      //convert object to json
      let data = JSON.stringify(this.registerstudent.value);

      // console.log(data)

      this.post(data)
        .subscribe(result => {
          this.messageResult = result;
          var resource = JSON.parse(result);
          this.loading = false;
          if (resource['code'] == '1') {
            swal(
              'Information!',
              resource['message'],
              'success'
            );
            this.registerstudent.reset();
            this.router.navigate(['/login-student']);
          }
          else {
            swal(
              'Information!',
              resource['message'],
              'error'
            );
            this.registerstudent.reset();
          }
        },
          error => {
            this.loading = false;
            this.registerstudent.reset();
            console.log("failed");
          });

    }

  }

  onSelected(item) {
    if (item == null) {
      this.disableVal = false;
      this.showSelected = false;
      this.CourseSelected = "";
      this.isFinal = false;
    }

    if (item != null) {
      this.obj = item["originalObject"];
      var currentDate = new Date();
      var courseEndDate = new Date(this.obj.EndDate);
      if (courseEndDate.getTime() < currentDate.getTime()) {
        this.disableVal = false;
        this.showSelected = false;
        this.CourseSelected = "";
        this.isFinal = true;
      } else {
        this.disableVal = true;
        this.showSelected = true;
        this.CourseSelected = this.obj.SiteId + ", " + this.obj.ContactName + ", " + this.obj.StartDate + " - " + this.obj.CompletionDate;
      }
    }
  }

  onChange(isChecked: boolean) {
    this.ifStudent = (isChecked) ? true : false;
  }

  post(data): Observable<string> {
    return this._http.post(
      this._serviceUrl,
      data,
      { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
    )
      .map((response: Response) => {
        return response.text();
      });
  }

  //check email registered
  validemail: Boolean = true;
  public emaildata: any;
  checkEmailDuplicate(value) {
    if (value != '' && value != null) {
      this.service.httpClientGet("api/Student/" + value, '')
        .subscribe(result => {
          this.emaildata = result;
          if (this.emaildata.Email != null) {
            this.validemail = false;
          }
          else if (this.emaildata.Email == null) {
            this.validemail = true;
          }
        },
          error => {
            this.service.errorserver();
          });
    }
    else {
      this.validemail = true;
    }
  }

}
