import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentRegisterComponent } from './studentregister.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { AppService } from "../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';
import { Ng2CompleterModule } from "ng2-completer";



export const StudentRegisterRoutes: Routes = [
  {
    path: '',
    component: StudentRegisterComponent,
    data: {
      breadcrumb: 'Student Register',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    LoadingModule,
    RouterModule.forChild(StudentRegisterRoutes),
    SharedModule,
    Ng2CompleterModule
  ],
  declarations: [StudentRegisterComponent],
  providers: [AppService, AppFormatDate]
})
export class StudentRegisterModule { }
