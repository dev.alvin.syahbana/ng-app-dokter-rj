import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ResetComponent} from './reset.component';
import {SharedModule} from '../../shared/shared.module';
import { LoadingModule } from 'ngx-loading';

export const ResetRoutes: Routes = [
  {
    path: '',
    component: ResetComponent,
    data: {
      breadcrumb: 'reset'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    LoadingModule,
    RouterModule.forChild(ResetRoutes),
    SharedModule
  ],
  declarations: [ResetComponent]
})
export class ResetModule { }
