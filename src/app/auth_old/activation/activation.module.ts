import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ActivationComponent} from './activation.component';
import {SharedModule} from '../../shared/shared.module';
import { LoadingModule } from 'ngx-loading';

export const ActivationRoutes: Routes = [
  {
    path: '',
    component: ActivationComponent,
    data: {
      breadcrumb: 'Set Password'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    LoadingModule,
    RouterModule.forChild(ActivationRoutes),
    SharedModule
  ],
  declarations: [ActivationComponent]
})
export class ActivationModule { }
