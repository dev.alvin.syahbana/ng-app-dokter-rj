import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { AppService } from "../../shared/service/app.service"; 
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import {PasswordValidators} from 'ngx-validators';

@Component({
  selector: 'app-activation',
  templateUrl: './activation.component.html'
})
export class ActivationComponent implements OnInit {
 
  private _serviceUrl = '';
  public data: any;
  changepasswordform: FormGroup;
  link:string="";
  id:string="";
  curentpassword:string="";
  activationpage:boolean=false;
  
  public loading = false;

  constructor(private service: AppService, private route: ActivatedRoute, private router: Router) {

    let password = new FormControl('', [Validators.required, Validators.minLength(6),Validators.compose([
      PasswordValidators.digitCharacterRule(1),
      PasswordValidators.specialCharacterRule(1)])]);
    let rpassword = new FormControl('', [Validators.required, CustomValidators.equalTo(password)]);

    this.changepasswordform = new FormGroup({
      password: password,
      rpassword: rpassword
    });

  }

  linksplit:any;
  ngOnInit() {

    this.link = this.route.snapshot.params['id'];
    this.linksplit = this.link.split('-');

    if(this.linksplit[0] == "contact" || this.linksplit[0] == "student"){
      this.service.get("api/auth/ActivationCheck/"+this.linksplit[0]+"/"+this.linksplit[1], '')
      .subscribe(result => {
        var data = JSON.parse(result);
        if (result == "Not Found" || result == "{}") {
          this.service.delete("api/auth/ExpiredActivation/"+this.linksplit[0]+'/'+this.linksplit[1],'');
          this.router.navigate(["/forbidden"]); 
        }
        else {
          this.activationpage=true;
          if(this.linksplit[0] == "contact"){
            this._serviceUrl = '/api/ContactAll';
            this.id = data["ContactId"];
          }else{
            this._serviceUrl = '/api/Student';
            this.id = data["StudentID"];
          }
          this.curentpassword = data["password_plain"]
        }
      },
      error => {
        this.service.delete("api/auth/ExpiredActivation/"+this.linksplit[0]+'/'+this.linksplit[1],'');
        this.router.navigate(["/forbidden"]); 
      });
    }else{
      this.service.delete("api/auth/ExpiredActivation/"+this.linksplit[0]+'/'+this.linksplit[1],'');
      this.router.navigate(["/forbidden"]); 
    }

  }

  onSubmit() {
    this.changepasswordform.controls['password'].markAsTouched();
    this.changepasswordform.controls['rpassword'].markAsTouched();

    if(this.changepasswordform.valid){
      this.loading = true;

      this.changepasswordform.value.currentpassword = this.curentpassword;

      let data = JSON.stringify(this.changepasswordform.value);

      this.service.httpCLientPutPassword(this._serviceUrl+'/ChangePassword', this.id, data, '/logout-'+this.linksplit[0], '/set-password');

      this.changepasswordform.reset();

      setTimeout(() => {
        this.loading = false;
      }, 1000)

    }
  }
  
}
