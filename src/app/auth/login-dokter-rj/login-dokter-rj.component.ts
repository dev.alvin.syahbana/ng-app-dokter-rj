import {
  Component,
  OnInit,
  ViewEncapsulation,
  getPlatform
} from '@angular/core';
import '../../../assets/echart/echarts-all.js';
import {
  FormGroup,
  FormControl
} from "@angular/forms";
import {
  AppService
} from "../../shared/service/app.service";
import {
  SessionService
} from '../../shared/service/session.service';
import {
  Router
} from '@angular/router';
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";


@Component({
  selector: 'login-dokter-rj',
  templateUrl: './login-dokter-rj.component.html',
  styleUrls: [
    './login-dokter-rj.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class LoginDokterRJComponent implements OnInit {

  // loginForm: any;
  dropdownListPoli: any;
  selectedItemPoli = [];
  dropdownSettings = {};
  public listDokter: any;
  jamPraktekDok: any;
  public loading = false;

  public validDokter = true;
  public validPraktek = true;

  today: number = Date.now();

  constructor(private router: Router, public session: SessionService, private service: AppService) { }

  ngOnInit() {
    this.getPoli();
    this.dropdownSettings = {
      singleSelection: true,
      text: "--Praktek Dokter--",
      enableSearchFilter: true,
      enableCheckAll: false,
      classes: "myclass custom-class",
      disabled: false,
      maxHeight: 120,
      searchAutofocus: true
    };
  }

  getPoli() {
    var data: any;
    this.service.httpClientGet('api/AppDokterRawatJalan/GetPoli', data)
      .subscribe(result => {
        data = result;
        if (data == "") {
          this.selectedItemPoli = [];
        } else {
          this.dropdownListPoli = data.map((item) => {
            return {
              id: item.Kd_Poli,
              itemName: item.Desk_Poli,
              itemNo: item.Ruang
            }
          })
          // console.log("Poli", this.dropdownListPoli);
        }
      },
        error => {
          window.alert("Mohon maaf terjadi kesalahan pada server");
          // console.log("Error API");
        });
    this.selectedItemPoli = [];
  }

  kd_poli = '';
  no_poli = '';
  poliklinik = '';
  onItemPoliSelect(item: any) {
    this.kd_poli = item.id;
    this.poliklinik = item.itemName;
    this.no_poli = item.itemNo;
    this.service.httpClientGet('api/AppDokterRawatJalan/DokterPraktek/' + item.id, '')
      .subscribe(result => {
        if (result == "") {
          this.listDokter = '';
          // window.alert("'Tidak ada Dokter'");
        } else {
          this.listDokter = result;
        }
      },
        error => {
          // this.service.errorserver();
          window.alert("Mohon maaf terjadi kesalahan pada server");
          // console.log("Error API");
        });

    this.selectDokter(''); // clear select dokter apabila ada perubahan pilih poli
  }

  OnItemPoliDeSelect(item: any) {
    this.listDokter = [];
    this.selectDokter(''); // clear select dokter apabila ada perubahan pilih poli
  }

  kd_dokter = '';
  nm_dokter = '';
  kd_lyn = '';
  jenis_pelayanan = '';
  selectDokter(value) {
    // console.log(value);
    if (value != '') {
      this.validDokter = false;
      this.kd_dokter = value;
      var index = this.listDokter.findIndex(x => x.Kd_Dokter == value);
      if (index !== -1) {
        this.nm_dokter = this.listDokter[index].Nm_Dokter;
        this.kd_lyn = this.listDokter[index].Kd_Lyn;
        this.jenis_pelayanan = this.listDokter[index].Jenis_Pelayanan;
      }

      // api untuk menentukan jam praktek dokter berdasarkan hari praktek
      this.service.httpClientGet('api/AppDokterRawatJalan/JamPrakterByKdDok/' + this.kd_dokter, '')
        .subscribe(result => {
          if (result == '') {
            this.jamPraktekDok = '';
          } else {
            this.jamPraktekDok = result;
          }
        });
    } else {
      this.validDokter = true;
    }
  }

  jam_praktek: '';
  selectJamPraktek(value) {
    // console.log(value);
    if (value != '') {
      this.validPraktek = false;
      this.jam_praktek = value;
    }

  }

  onSubmit() {
    this.loading = true;
    // console.log(this.kd_dokter)
    // Pengambilan tanggal hari ini
    var d = new Date();
    var year = d.getFullYear();
    var month = '' + (d.getMonth() + 1);
    if (month.length == 1)
      month = "0" + month;
    var day = '' + d.getDate();
    if (day.length == 1)
      day = "0" + day;
    var dates = year + "-" + month + "-" + day;

    this.session.logIn('token', JSON.stringify({
      // tgl_hari_ini: '2019-06-17', // poli 8 achmad saleh
      tgl_hari_ini: dates,
      // tgl_hari_ini: '2019-07-18', // poli 3 dr daud
      // tgl_hari_ini: '2019-07-04', //poli 8 achmad saleh
      kd_poli: this.kd_poli,
      no_poli: this.no_poli,
      poli: this.poliklinik,
      kd_dokter: this.kd_dokter,
      nama: this.nm_dokter,
      kode_lyn: this.kd_lyn,
      jenis_pelayanan: this.jenis_pelayanan,
      jam_praktek: this.jam_praktek
    }));
    // console.log("Masuk")
    if (this.validDokter == false && this.validPraktek == false) {
      // this.router.navigate(['/app-dokter/list-pasien-rawat-jalan']);
      this.router.navigate(['menu-utama']);
      this.loading = false;
      const paramLog = {
        NoReg: '',
        Kd_Dokter: this.kd_dokter,
        Aksi: 'LOGIN',
        Jenis: '1'
      }
      this.service.post('api/AppDokterRJInfo/InsertLogSystem', JSON.stringify(paramLog))
        .subscribe(result => {
          this.loading = false;
        },
          err => {
            this.loading = false;
          });
    } else {
      window.alert("Data masih ada yang kosong");
      this.loading = false;
    }
  }
}
