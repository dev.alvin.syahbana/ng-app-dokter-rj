import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl } from "@angular/forms";
import { AppService } from "../../shared/service/app.service";
import { SessionService } from '../../shared/service/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'Login',
  templateUrl: './Login.component.html',
  styleUrls: [
    './Login.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class LoginComponent implements OnInit {

  loginForm: any;

  constructor(private router: Router, public session: SessionService, private service: AppService) {

    let Username = new FormControl('');
    let Password = new FormControl('');

    this.loginForm = new FormGroup({
      Username: Username,
      Password: Password
    });

  }

  ngOnInit() { }

  onSubmit() {
    var data = JSON.stringify(this.loginForm.value);
    console.log('api/Users/Login', data)
    this.service.post('api/Users/Login', data)
      .subscribe(result => {
        var data = JSON.parse(result);
        if (data.id != undefined) {
          this.session.logIn('token', JSON.stringify(data));

          if (data.level == "Dr") {
            this.router.navigate(['/app-dokter/dashboard-rj-dokter']);
          } else {
            this.router.navigate(['/admin/user']);
          }
        } else {
          window.alert("username atau password salah")
        }
      })
  }

}
