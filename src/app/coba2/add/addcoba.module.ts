import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddCobaComponent } from './addcoba.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const AddCobaRoutes: Routes = [
  {
    path: '',
    component: AddCobaComponent,
    data: {
      breadcrumb: 'Add Coba',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AddCobaRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [AddCobaComponent],
  providers:[AppService, AppFormatDate]
})
export class AddCobaModule { }