import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';

@Component({
  selector: 'app-addcoba',
  templateUrl: './addcoba.component.html',
  styleUrls: [
    './addcoba.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class AddCobaComponent implements OnInit {

	private _serviceUrl = 'api/Glossaries';
  	AddCobaForm: FormGroup;
  	public loading = false;
  	public useraccesdata: any;

	constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate, private session: SessionService) {

		//get user level
	    let useracces = this.session.getData();
	    this.useraccesdata = JSON.parse(useracces);

	    //validation
	    // let GlossaryId = new FormControl('', Validators.required);
	    let GlossaryTerm = new FormControl('', Validators.required);

	    this.AddCobaForm = new FormGroup({
	      // GlossaryId: GlossaryId,
	      GlossaryTerm: GlossaryTerm
	    });

	}

	ngOnInit() { }

	//submit form
  	onSubmit() {

	    // this.AddCobaForm.controls['GlossaryId'].markAsTouched();
	    this.AddCobaForm.controls['GlossaryTerm'].markAsTouched();

	    if (this.AddCobaForm.valid) {

	      this.loading = true;
	      this.AddCobaForm.value.Cdate = this.formatdate.dateJStoYMD(new Date());
	      this.AddCobaForm.value.Mdate = this.formatdate.dateJStoYMD(new Date());
	      this.AddCobaForm.value.Muid = "Admin";      
	      this.AddCobaForm.value.Cuid = "Admin";
	      this.AddCobaForm.value.Status = "A";      
	      this.AddCobaForm.value.Description = "Tetew";
	      // this.AddCobaForm.value.UserId = this.useraccesdata.UserId;

	      //convert object to json
	      let data = JSON.stringify(this.AddCobaForm.value);
	      console.log(data);

	      // post action
	      this.service.httpClientPost(this._serviceUrl, data);
	      setTimeout(() => {
	        //redirect
	        this.router.navigate(['/coba2-list']);
	        this.loading = false;
	      }, 1000)
	    }
  	}	
}