import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: [
    './add.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class GlosarryAddEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Glossaries';
  public data:any;
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "";
  public sortOrder: string = "asc";

  constructor(private router: Router, private _http: Http, private service: AppService) {}

  ngOnInit() { 
    // translate (get lang list from api)
    var data = '';
    this.service.httpClientGet(this._serviceUrl,data).subscribe(result => { 
      console.log(result);
      if(result==null){
        swal(
          'Infomation!',
          'List Language Not Found.',
          'error'
        );
        this.data = '';
      }
      else{
        this.data = result;
      } 
    },
    error => {
      this.service.errorserver();
      this.data = '';
    });
  }
}
