import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GlosarryAddEPDBComponent } from './add.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

import {AppService} from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const GlosarryAddEPDBRoutes: Routes = [
  {
    path: '',
    component: GlosarryAddEPDBComponent,
    data: {
      breadcrumb: 'Add List',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(GlosarryAddEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [GlosarryAddEPDBComponent],
  providers:[AppService, AppFormatDate]
})
export class GlosarryAddEPDBModule { }
