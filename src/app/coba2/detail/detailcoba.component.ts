import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbDateParserFormatter, NgbDateStruct, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import {Http, Headers, Response} from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import swal from 'sweetalert2';
import {ActivatedRoute} from '@angular/router';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import {AppService} from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";

@Component({
  selector: 'app-detailcoba',
  templateUrl: './detailcoba.component.html',
  styleUrls: [
    './detailcoba.component.css',
    '../../../../node_modules/c3/c3.min.css',
    ],
  encapsulation: ViewEncapsulation.None
})

export class DetailCobaComponent implements OnInit {
	private _serviceUrl = 'api/Glossaries';
  	public data:any;
  	public datadetail:any;

  	constructor(private service:AppService, private formatdate:AppFormatDate, private route:ActivatedRoute, private router: Router) {

  	}

  	id:string;
  	ngOnInit() {

	    this.id = this.route.snapshot.params['id'];
	    var data = '';
	    this.service.httpClientGet(this._serviceUrl+"/"+this.id, data)
	      .subscribe(result => {
	      	console.log(result)
	        if(result=="Not found"){
	            this.service.notfound();
	            this.datadetail = '';
	        }
	        else{
	            this.datadetail = result;
	        }
	      },
	      error => {
	        this.service.errorserver();
	        this.data = '';
	      });

  	}
}