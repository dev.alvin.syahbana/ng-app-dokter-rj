import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailCobaComponent } from './detailcoba.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

import {AppService} from "../../shared/service/app.service";
import {AppFormatDate} from "../../shared/format-date/app.format-date";

export const DetailCobaRoutes: Routes = [
  {
    path: '',
    component: DetailCobaComponent,
    data: {
      breadcrumb: 'Detail Coba',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DetailCobaRoutes),
    SharedModule
  ],
  declarations: [DetailCobaComponent],
  providers:[AppService, AppFormatDate]
})
export class DetailCobaModule { }