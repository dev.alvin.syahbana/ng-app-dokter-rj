import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataFilterActivitiesPipe, DataFilterStatusPipe, ListPBFDComponent, DataFilterMonPipe, ConvertDatePipe } from './listpemakaianbarangfarmasidetail.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';


export const ListPBFDRoutes: Routes = [
  {
    path: '',
    component: ListPBFDComponent,
    data: {
      breadcrumb: 'eim.profile.my_profile.my_profile',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ListPBFDRoutes),
    SharedModule,
    LoadingModule,
    AngularMultiSelectModule
  ],
  declarations: [ListPBFDComponent, DataFilterMonPipe, ConvertDatePipe, DataFilterActivitiesPipe, DataFilterStatusPipe],
  providers: [AppService, AppFormatDate]
})
export class ListPBFDModule { }
