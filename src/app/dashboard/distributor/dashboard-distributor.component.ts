import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { SessionService } from '../../shared/service/session.service';


import { AppService } from "../../shared/service/app.service";

@Component({
  selector: 'app-dashboard-distributor',
  templateUrl: './dashboard-distributor.component.html',
  styleUrls: [
    './dashboard-distributor.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class DashboardDistributorComponent implements OnInit {
  messageError: string = '';
  donutChartData: any
  doughChartPartnerGeo: any
  doughChartInstructorGeo: any
  doughChartCourseMonth: any
  doughChartPartnerParticipated: any
  doughChartEvaluationMonth: any

  dataCoursesByMonth = []
  dataEvaluationMonth = []
  dataActivePartner = []
  dataActiveInstructors = [];
  dataCoursesDeliveredByPartners = [];
  dataKeyValueCoursesDeliveredByPartners = [];

  dataCoursesDelivered: any;

  private _serviceUrlCoursesByMonth = '/api/DashboardCoursesDelivered';


  private _serviceUrlActiveInstructors = '/api/DashboardActiveInstructors';
  private _serviceUrlCoursesDeliveredByPartners = '/api/DashboardCoursesDeliveredByPartners';


  content = ` Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at,`;
  timeline = [
    { caption: '16 Jan', date: new Date('2014, 1, 16'), selected: true, title: 'Horizontal Timeline', content: this.content },
    { caption: '28 Feb', date: new Date('2014, 2, 28'), title: 'Event title here', content: this.content },
    { caption: '20 Mar', date: new Date('2014, 3, 20'), title: 'Event title here', content: this.content },
    { caption: '20 May', date: new Date('2014, 5, 20'), title: 'Event title here', content: this.content },
    { caption: '09 Jul', date: new Date('2014, 7, 9'), title: 'Event title here', content: this.content },
    { caption: '30 Aug', date: new Date('2014, 8, 30'), title: 'Event title here', content: this.content },
    { caption: '15 Sep', date: new Date('2014, 9, 15'), title: 'Event title here', content: this.content },
    { caption: '01 Nov', date: new Date('2014, 11, 1'), title: 'Event title here', content: this.content },
    { caption: '10 Dec', date: new Date('2014, 12, 10'), title: 'Event title here', content: this.content },
    { caption: '29 Jan', date: new Date('2015, 1, 19'), title: 'Event title here', content: this.content },
    { caption: '3 Mar', date: new Date('2015,  3,  3'), title: 'Event title here', content: this.content },
  ];

  /*Bar chart Start*/
  type_NewPartnerByMont = 'bar';
  data_NewPartnerByMont = {}
  options_NewPartnerByMont = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      position: 'bottom'
    }
  };


  //Active Partner
  type_ActivePartner = 'bar';
  data_ActivePartner = {};
  options_ActivePartner = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      position: 'bottom'
    }
  };


  //Active Instructor
  type_ActiveInstructor = 'bar';
  data_ActiveInstructor = {}
  options_ActiveInstructor = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      position: 'bottom'
    }
  };


  /*Bar chart End*/
  useraccesdata: any;
  constructor(private service: AppService, private session: SessionService) {
    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);
  }

  //Get Role Access
  checkrole(): boolean {
    let userlvlarr = this.useraccesdata.UserLevelId.split(',');
    for (var i = 0; i < userlvlarr.length; i++) {
      if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
        return false;
      } else {
        return true
      }
    }
  }
  urlGetOrgId(): string {
    var orgarr = this.useraccesdata.OrgId.split(',');
    var orgnew = [];
    for (var i = 0; i < orgarr.length; i++) {
      orgnew.push('"' + orgarr[i] + '"');
    }
    return orgnew.toString();
  }
  //Get Role Access


  ngOnInit() {
    this.getPartnerByMonth();
    this.getCoursesByMonth();
    this.getActivePartner();
    this.getActiveInstructors();
    this.getEvaluationByMonth()
    this.setTimeout();
  }

  getPartnerByMonth() {
    // start NEW PARTNERS BY MONTH
    var data: any;
    var roleid = ["60", "58", "1"];
    var teritorryarr = [];
    var dataset = [];

    var orgid = "";
    if (this.checkrole()) {
      orgid = this.urlGetOrgId();
      console.log("->"+orgid)
    }

    for (var k = 0; k < roleid.length; k++) {
      this.service.get("api/DashboardPartnerMonth/where/{'OrgId':'" + orgid + "','RoleId':'" + roleid[k] + "'}", '')
        .subscribe(result => {
          if (result == "Not found") {
            this.service.notfound();
          }
          else {
            data = JSON.parse(result);
            teritorryarr = [];
            var partners = [];
            for (var i = 0; i < data.length; i++) {
              partners.push(data[i].CountPartners);
              teritorryarr.push(data[i].Territory_Name);
            }
            dataset.push({
              label: data[0].RoleCode,
              data: partners,
              backgroundColor: data[0].BackgroundColor
            })
          }
        },
        error => {
          this.service.errorserver();
        });
    }
    setTimeout(() => {
      this.data_NewPartnerByMont = {
        labels: teritorryarr,
        datasets: dataset
      }
    }, 5000);
  }

  getCoursesByMonth() {
    //get data action
    var data: any;
    var roleid = ["60", "58", "1"];
    var teritorryarr = [];
    var dataset = [];
    var orgid = "";
    if (this.checkrole()) {
      orgid = this.urlGetOrgId();
    }
    this.service.get("api/DashboardCoursesByMonth/where/{'OrgId':'" + orgid + "'}", '')
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();

        }
        else {
          var CoursesDelivered = JSON.parse(result);
          this.dataCoursesDelivered = []
          for (var i in CoursesDelivered) {
            this.dataCoursesDelivered.push({
              'PartnerType': CoursesDelivered[i].SiteName,
              'Total': Number(CoursesDelivered[i].TotalCourseMonthByPartnerType),
            });
          }
          if (this.dataCoursesDelivered == "") {
            this.dataCoursesDelivered.push({
              'PartnerType': '-',
              'Total': 0
            })
          }
          console.log(this.dataCoursesDelivered)
          Morris.Bar({
            element: 'morris-bar-chart',
            data: this.dataCoursesDelivered,
            xkey: 'PartnerType',
            ykeys: ['Total'],
            labels: ['Total'],
            barColors: ['#5FBEAA', '#5D9CEC', '#cCcCcC'],
            hideHover: 'auto',
            gridLineColor: '#eef0f2',
            resize: true
          });
        }
      },
      error => {
        this.messageError = <any>error
        this.service.errorserver();
      });
  }

  getActivePartner() {
    // start NEW PARTNERS BY MONTH
    var data: any;
    var roleid = ["60", "58", "1"];
    var teritorryarr = [];
    var dataset = [];

    var orgid = "";
    if (this.checkrole()) {
      orgid = this.urlGetOrgId();
    }

    this.service.get("api/DashboardActivePartner/where/{'DistributorOrgId':'" + orgid + "'}", '')
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
        }
        else {
          data = JSON.parse(result);
          teritorryarr = [];
          var partnersATC = [];
          var partnersAAP = [];
          var partnersCTC = [];
          var countriesName = []
          var backgroundColorATC = []
          var backgroundColorAAP = []
          var backgroundColorMTP = []
        
          for (var i = 0; i < data.length; i++) {
            if (data[i].RoleCode == 'ATC') {
              partnersATC.push(data[i].TotalPartnerThisMonth);
              partnersAAP.push(0);
              partnersCTC.push(0)
            }

            else if (data[i].RoleCode == 'AAP') {
              partnersATC.push(0);
              partnersAAP.push(data[i].TotalPartnerThisMonth);
              partnersCTC.push(0)

            } else if (data[i].RoleCode == 'CTC') {
              partnersATC.push(0);
              partnersAAP.push(0);
              partnersCTC.push(data[i].TotalPartnerThisMonth)
            }

            countriesName.push(data[i].countries_name);
            backgroundColorATC.push("#5FBEAA")
            backgroundColorAAP.push("#1b66a2")
            backgroundColorMTP.push("#5D9CEC")
            this.data_ActivePartner = {
              labels: countriesName,
              datasets: [{
                label: 'ATC',
                data: partnersATC,
                backgroundColor: backgroundColorATC
              },
              {
                label: 'AAP',
                data: partnersAAP,
                backgroundColor: backgroundColorAAP
              },
              {
                label: 'MTP',
                data: partnersCTC,
                backgroundColor: backgroundColorMTP
              }]
            }
          }
        }
      },
      error => {
        this.service.errorserver();
      });
  }

  getActiveInstructors() {
     // start NEW PARTNERS BY MONTH
     var data: any;
     var roleid = ["60", "58", "1"];
     var teritorryarr = [];
     var dataset = [];
 
     var orgid = "";
     if (this.checkrole()) {
       orgid = this.urlGetOrgId();
     }
 
     this.service.get("api/DashboardActiveInstructors/where/{'OrgId':'" + orgid + "'}", '')
       .subscribe(result => {
         if (result == "Not found") {
           this.service.notfound();
         }
         else {
           data = JSON.parse(result);
           var partnersATC = [];
           var partnersAAP = [];
           var partnersCTC = [];
           var countriesName = []
           var backgroundColorATC = []
           var backgroundColorAAP = []
           var backgroundColorMTP = []
         
           for (var i = 0; i < data.length; i++) {
             if (data[i].RoleCode == 'ATC') {
               partnersATC.push(data[i].TotalActiveInstructor);
               partnersAAP.push(0);
               partnersCTC.push(0)
             }
 
             else if (data[i].RoleCode == 'AAP') {
               partnersATC.push(0);
               partnersAAP.push(data[i].TotalActiveInstructor);
               partnersCTC.push(0)
 
             } else if (data[i].RoleCode == 'CTC') {
               partnersATC.push(0);
               partnersAAP.push(0);
               partnersCTC.push(data[i].TotalActiveInstructor)
             }
 
             countriesName.push(data[i].geo_name);
             backgroundColorATC.push("#5FBEAA")
             backgroundColorAAP.push("#1b66a2")
             backgroundColorMTP.push("#5D9CEC")

            //  Set Into Chart
             this.data_ActiveInstructor = {
               labels: countriesName,
               datasets: [{
                 label: 'ATC',
                 data: partnersATC,
                 backgroundColor: backgroundColorATC
               },
               {
                 label: 'AAP',
                 data: partnersAAP,
                 backgroundColor: backgroundColorAAP
               },
               {
                 label: 'MTP',
                 data: partnersCTC,
                 backgroundColor: backgroundColorMTP
               }]
             }
           }
         }
       },
       error => {
         this.service.errorserver();
       });
  }

  getEvaluationByMonth() {
    // start NEW PARTNERS BY MONTH
    var data: any;
    var roleid = ["60", "58", "1"];
    var teritorryarr = [];
    var dataset = [];

    var orgid = "";
    if (this.checkrole()) {
      orgid = this.urlGetOrgId();
    }
    this.service.get("api/DashboardMonthEvaluationSurvey/where/{'OrgId':'" + orgid + "'}", '')
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();

        }
        else {
          var EvaByMonth = JSON.parse(result);
          this.dataEvaluationMonth = []
          for (var i in EvaByMonth) {
            this.dataEvaluationMonth.push({
              'PartnerType': EvaByMonth[i].SiteName,
              'Total': Number(EvaByMonth[i].TotalMOnthlySurveyTaken),
            });
          }
          


          Morris.Bar({
            element: 'id-evaluation-by-month',
            data: this.dataEvaluationMonth,
            xkey: 'PartnerType',
            ykeys: ['Total'],
            labels: ['Total'],
            barColors: ['#5FBEAA', '#5D9CEC', '#cCcCcC'],
            hideHover: 'auto',
            gridLineColor: '#eef0f2',
            resize: true
          });


      
        }
      },
      error => {
        this.messageError = <any>error
        this.service.errorserver();
      });
  }





















 

















  private _serviceUrlPartnerSurvey = "/api/DashboardPartnerSurvey";
  dataPartnerSurvey = [];
  countPartner: number = 0;
  countPartnerString: String = "";
  getPartnerSurvey() {
    //get data action
    var data = '';
    this.service.get(this._serviceUrlPartnerSurvey, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.dataPartnerSurvey = ['Task', 'Hours per Day'];
          this.service.notfound();
        }
        else {
          var PartnerSurvey = JSON.parse(result);
          this.dataPartnerSurvey.push(['Task', 'Hours per Day']);
          for (var i in PartnerSurvey) {
            this.dataPartnerSurvey.push([PartnerSurvey[i].RoleName, Number(PartnerSurvey[i].TotalPartnersOnSurvey)]);
            this.countPartner = +PartnerSurvey[i].TotalPartnersOnSurvey + this.countPartner;
          }
          this.countPartnerString = this.countPartner.toString();
        }
      },
      error => {
        this.dataPartnerSurvey = ['Task', 'Hours per Day'];
        this.service.errorserver();
      });
  }

  hoverPercent() {
    this.countPartnerString = this.countPartner.toString() + "%";
  }

  unhoverPercent() {
    this.countPartnerString = this.countPartner.toString();
  }

  getCoursesDeliveredByPartners() {
    //get data action
    var data = '';
    this.service.get(this._serviceUrlCoursesDeliveredByPartners, data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
        }
        else {
          var CoursesDeliveredByPartners = JSON.parse(result);
          for (var i in CoursesDeliveredByPartners) {
            this.dataCoursesDeliveredByPartners.push([CoursesDeliveredByPartners[i].KeyValue, Number(CoursesDeliveredByPartners[i].TotalCourseMonthByPartnerType)]);
            this.dataKeyValueCoursesDeliveredByPartners.push([CoursesDeliveredByPartners[i].KeyValue])
          }
        }
      },
      error => {
        this.messageError = <any>error
        this.service.errorserver();
      });
  }


  setTimeout() {
    setTimeout(() => {
      const chart = c3.generate({
        bindto: '#chart4',
        data: {
          columns: this.dataCoursesDeliveredByPartners,
          xkey: ['Jan 2017', 'Feb 2017', 'Mar 2017', 'Jan 2017', 'Feb 2017', 'Mar 2017', 'Jan 2017', 'Feb 2017', 'Mar 2017', 'Jan 2017', 'Feb 2017', 'Mar 2017'],
          type: 'bar',
          colors: {
            data1: '#03A9F3',
            data2: '#FEC107',
          },
          groups: this.dataKeyValueCoursesDeliveredByPartners
        }
      });
      $('.resource-barchart1').sparkline([5, 6, 9, 7, 8, 4, 6], {
        type: 'bar',
        barWidth: '6px',
        height: '32px',
        barColor: '#1abc9c',
        tooltipClassname: 'abc'
      });

      $('.resource-barchart2').sparkline([6, 4, 8, 7, 9, 6, 5], {
        type: 'bar',
        barWidth: '6px',
        height: '32px',
        barColor: '#1abc9c',
        tooltipClassname: 'abc'
      });
    }, 2000);

    setTimeout(() => {
      // this.doughChartPartnerGeo = {
      //   chartType: 'PieChart',
      //   dataTable: this.dataActivePartner,
      //   options: {
      //     height: 360,
      //     title: 'New Partners by Month',
      //     pieHole: 0.4,
      //     colors: [
      //       '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
      //     ]
      //   },
      // };
      this.doughChartInstructorGeo = {
        chartType: 'PieChart',
        dataTable: this.dataActiveInstructors,
        options: {
          height: 360,
          title: 'New Partners by Month',
          pieHole: 0.4,
          colors: [
            '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
          ]
        },
      };
      this.doughChartCourseMonth = {
        chartType: 'PieChart',
        dataTable: [
          ['Task', 'Hours per Day'],
          ['NULL', 1]

        ],
        options: {
          height: 360,
          title: 'New Partners by Month',
          pieHole: 0.4,
          colors: [
            '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
          ]
        },
      };
      this.doughChartPartnerParticipated = {
        chartType: 'PieChart',
        dataTable: this.dataPartnerSurvey,
        options: {
          height: 360,
          title: 'New Partners by Month',
          pieHole: 0.4,
          colors: [
            '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
          ]
        },
      };
      this.doughChartEvaluationMonth = {
        chartType: 'PieChart',
        dataTable: [
          ['Task', 'Hours per Day'],
          ['NULL', 1]

        ],
        options: {
          height: 360,
          title: 'New Partners by Month',
          pieHole: 0.4,
          colors: [
            '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
          ]
        },
      };

      this.donutChartData = {
        chartType: 'PieChart',
        dataTable: [
          ['Task', 'Hours per Day'],
          ['NULL', 1]

        ],
        options: {
          height: 320,
          title: 'My Daily Activities',
          pieHole: 0.4,
          colors: ['#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f']
        },
      };
    }, 1000)
  }

}
