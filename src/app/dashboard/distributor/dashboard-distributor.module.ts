import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardDistributorComponent } from './dashboard-distributor.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const DashboardDistributorRoutes: Routes = [
  {
    path: '',
    component: DashboardDistributorComponent,
    data: {
      breadcrumb: 'menu_dashboard.dashboard_distributor',
      icon: 'icofont-home bg-c-blue',
      status: false,
      title: 'Dashboard'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DashboardDistributorRoutes),
    SharedModule
  ],
  declarations: [DashboardDistributorComponent]
})
export class DashboardDistributorModule { }
