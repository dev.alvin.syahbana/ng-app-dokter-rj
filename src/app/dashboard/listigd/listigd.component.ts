import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
    constructor(private datepipe: DatePipe) { }
    transform(date: string): any {
        return this.datepipe.transform(new Date(date.substr(0, 10)), 'dd-MM-yyyy');
    }
}

@Pipe({ name: 'dataFilterActivities' })
export class DataFilterActivitiesPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.idRj0701.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.noRekMedis.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.namaPasien.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.jenisKelamin.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.umur.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.triase.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.tindakan.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.namaPerawat.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.namaDokter.toLowerCase().indexOf(query.toLowerCase()) > -1)
            );
        }
        return array;
    }
}

@Pipe({ name: 'dataFilterStatus' })
export class DataFilterStatusPipe {
    transform(array: any[], query: string): any {
        if (query == "Active") {
            return _.filter(array, row =>
                (row.SurveyTaken.indexOf("0") > -1) &&
                (row.activesurveylink.indexOf("Active") > -1));
        }
        else if (query == "Expired" || query == "Pending") {
            return _.filter(array, row =>
                (row.activesurveylink.indexOf(query) > -1));
        } else if (query == "Completed") {
            return _.filter(array, row =>
                (row.SurveyTaken.indexOf("1") > -1));
        }
        return array;
    }
}


@Component({
    selector: 'app-dashboard-listigd',
    templateUrl: './listigd.component.html',
    styleUrls: [
        './listigd.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})


export class DashboardIGDComponent implements OnInit {

    private _serviceUrl = '/api/IgdRj07PerawatPengkajianAwal';
    public studentcourse: any;
    public data: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "waktuPeriksa";
    public sortOrder: string = "desc";
    public useraccesdata: any;
    public filterStatus: string = "All";
    public title = 'dini';
    public loading = false;

    constructor(private service: AppService, private formatdate: AppFormatDate, private session: SessionService, private router: Router) {
        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    ngOnInit() {
        this.getDataRJ07();
    }

    getDataRJ07() {
        this.loading = true;
        var data = '';
        this.service.httpClientGet(this._serviceUrl, data).subscribe(result => {
            console.log(result);
            if (result == null) {
                swal(
                    'Informasi!',
                    'Data tidak ditemukan.',
                    'error'
                );
                this.data = '';
                this.loading = false;
            }
            else {
                this.data = result;
                this.loading = false;
            }
        },
            error => {
                this.service.errorserver();
                this.data = '';
                this.loading = false;
            });
    }

}
