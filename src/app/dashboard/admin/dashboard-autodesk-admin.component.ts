import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { AppService } from "../../shared/service/app.service";
import { SessionService } from '../../shared/service/session.service';


@Component({
    selector: 'app-dashboard-autodesk-admin',
    templateUrl: './dashboard-autodesk-admin.component.html',
    styleUrls: [
        './dashboard-autodesk-admin.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class DashboardAutodeskAdminComponent implements OnInit {

    lineChartOptions: any;
    lineChartData: any;
    discreteBarOptions: any;
    doughChartPartnerMonth: any;
    doughChartActivePartner: any;
    doughChartActiveInstructure: any;
    doughtChartCourseMonth: any;
    donutChartData: any
    doughChartCoreProductsAgainstAllCourses: any;
    doughCoreProductsAgainstAllCourses: any
    doughATCParticipatedEvaSurvey: any
    doughAAPParticipatedEvaSurvey: any
    MEETTHEIRANNUALREQUIREMENTS: any

    messageError: string = '';

    private _serviceUrlPartnerMonth = '/api/DashboardPartnerMonth';
    private _serviceUrlActivePartner = '/api/DashboardActivePartner';
    private _serviceUrlActiveInstructors = '/api/DashboardActiveInstructors';
    private _serviceUrlMonthEvaluationSurvey = '/api/DashboardMonthEvaluationSurvey';
    private _serviceUrlCoursesDeliveredByPartners = '/api/DashboardCoursesDeliveredByPartners';
    private _serviceUrlCoreProductsAgainstAllCourses = '/api/DashboardCoreProductsAgainstAllCourses';

    dataMEETTHEIRANNUALREQUIREMENTS = []
    dataPartnerMonth = [];
    dataActivePartner = [];
    dataActiveInstructors = [];
    dataCoursesDelivered: any
    dataCoreProductsAgainstAllCourses = [];
    dataKeyValueCoursesDeliveredByPartners = [];
    dataCoursesDeliveredByPartners = [];
    dataMonthEvaluationSurvey: any;

    dataATCParticipatedEvaSurvey = []
    dataAAPParticipatedEvaSurvey = []

    type1 = 'horizontalBar';
    data1 = {};

    optionsBar = {
        responsive: true,
        maintainAspectRatio: false,
    };

    useraccesdata: any;
    constructor(private service: AppService, private session: SessionService) {
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    result: string = "";
    hoverPercent() {
        this.result = "%";
    }

    unhoverPercent() {
        this.result = "";
    }

    checkrole(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                return false;
            } else {
                return true
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }

    ngOnInit() {
        setTimeout(() => {
            this.getNewPartnerByMonth();
            this.getActiveInstructors();
            this.getCoursesByMonth();
            this.getATCParticipatedEvaSurvey();
            this.getAAPParticipatedEvaSurvey();
           
        }, 10000);

    }

    getNewPartnerByMonth() {
        var data: any;
        var roleid = ["60", "58", "1"];
        var teritorryarr = [];
        var dataset = [];
        var orgid = "";
        if (this.checkrole()) {
            orgid = this.urlGetOrgId();
        }

        for (var k = 0; k < roleid.length; k++) {
            this.service.get("api/DashboardPartnerMonth/where/{'OrgId':'" + orgid + "','RoleId':'" + roleid[k] + "'}", '')
                .subscribe(result => {
                    if (result == "Not found") {
                        this.service.notfound();
                    }
                    else {
                        data = JSON.parse(result);
                        teritorryarr = [];
                        var partners = [];
                        
                        for (var i = 0; i < data.length; i++) {
                            partners.push(data[i].CountPartners);
                            teritorryarr.push(data[i].Territory_Name);
                        }
                        
                        dataset.push({
                            label: data[0].RoleCode,
                            data: partners,
                            backgroundColor: data[0].BackgroundColor
                        })
                        
                       
                            this.data1 = {
                                labels: teritorryarr,
                                datasets: dataset
                            }
                       

                    }
                },
                error => {
                    this.service.errorserver();
                });
        }
    }

    getATCParticipatedEvaSurvey() {
        var data: any;
        var roleid = ["60", "58", "1"];
        var teritorryarr = [];
        var dataset = [];

        var orgid = "";
        if (this.checkrole()) {
            orgid = this.urlGetOrgId();
        }
        this.service.get("api/DashboardCoreProductsAgainstAllCourses/ATC/where/{'OrgId':'" + orgid + "'}", '') //api nya masih wip
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                }
                else {
                    var dataResult = result.replace(/\t/g, " ");
                    dataResult = dataResult.replace(/\r/g, " ");
                    dataResult = dataResult.replace(/\n/g, " ");

                    var dataParse = JSON.parse(dataResult);
                    this.dataATCParticipatedEvaSurvey.push(['Product Name', 'Total']);

                    for (var i in dataParse) {
                        this.dataATCParticipatedEvaSurvey.push([dataParse[i].Territory_Name, Number(dataParse[i].TotalCourseMonthByCoreProduct)]);
                    }
                    
                    if (dataParse == "") {
                        this.dataATCParticipatedEvaSurvey.push([' ', 1])
                    }

                   
                        this.doughATCParticipatedEvaSurvey = {
                            chartType: 'PieChart',
                            dataTable: this.dataATCParticipatedEvaSurvey,
                            options: {
                                height: 360,
                                title: 'ATC',
                                pieHole: 0.4,
                                colors: [
                                    '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
                                ]
                            },
                        };
                   

                }
            },
            error => {
                this.messageError = <any>error
                this.service.errorserver();
            });

    }

    getAAPParticipatedEvaSurvey() {
        var data: any;
        var roleid = ["60", "58", "1"];
        var teritorryarr = [];
        var dataset = [];

        var orgid = "";
        if (this.checkrole()) {
            orgid = this.urlGetOrgId();
        }
        this.service.get("api/DashboardCoreProductsAgainstAllCourses/AAP/where/{'OrgId':'" + orgid + "'}", '')
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                }
                else {
                    var dataResult = result.replace(/\t/g, " ");
                    dataResult = dataResult.replace(/\r/g, " ");
                    dataResult = dataResult.replace(/\n/g, " ");

                    var dataParse = JSON.parse(dataResult);
                    this.dataAAPParticipatedEvaSurvey.push(['Product Name', 'Total']);
                    
                    for (var i in dataParse) {
                        this.dataAAPParticipatedEvaSurvey.push([dataParse[i].Territory_Name, Number(dataParse[i].TotalCourseMonthByCoreProduct)]);
                    }

                    if (dataParse == "") {
                        this.dataAAPParticipatedEvaSurvey.push([' ', 1])
                    }

                  
                        this.doughAAPParticipatedEvaSurvey = {
                            chartType: 'PieChart',
                            dataTable: this.dataAAPParticipatedEvaSurvey,
                            options: {
                                height: 360,
                                title: 'AAP',
                                pieHole: 0.4,
                                colors: [
                                    '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
                                ]
                            },
                        };
                  

                }
            },
            error => {
                this.messageError = <any>error
                this.service.errorserver();
            });
    }

    getActiveInstructors() {
        var data: any;
        var roleid = ["60", "58", "1"];
        var teritorryarr = [];
        var dataset = [];

        var orgid = "";
        if (this.checkrole()) {
            orgid = this.urlGetOrgId();
        }
        this.service.get(this._serviceUrlActiveInstructors, data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                }
                else {
                    var ActiveInstructors = JSON.parse(result);
                    this.dataActiveInstructors.push(['Intructure', 'Total']);
                    
                    for (var i in ActiveInstructors) {
                        this.dataActiveInstructors.push([ActiveInstructors[i].Territory_Name, Number(ActiveInstructors[i].TotalActiveInstructor)]);
                    }

               
                        this.doughChartActiveInstructure = {
                            chartType: 'BarChart',
                            dataTable: this.dataActiveInstructors,
                            options: {
                                legend: { position: 'none' },
                                height: 300,
                                title: 'Total Active Intructors',
                                chartArea: { width: '50%' },
                                isStacked: false,
                                hAxis: {
                                    title: 'Value',
                                    minValue: 0,
                                },
                                vAxis: {
                                    title: 'Partner Type'
                                },
                                colors: ['#2ecc71', '#5faee3']
                            },
                        };
                  

                }
            },
            error => {
                this.messageError = <any>error
                this.service.errorserver();
            });
    }

    getCoursesByMonth() {
        var data: any;
        var roleid = ["60", "58", "1"];
        var teritorryarr = [];
        var dataset = [];
        var orgid = "";
        if (this.checkrole()) {
          orgid = this.urlGetOrgId();
        }
        this.service.get("api/DashboardCoursesByMonth/where/{'OrgId':'" + orgid + "'}", '')
          .subscribe(result => {
            if (result == "Not found") {
              this.service.notfound();
    
            }
            else {
              var CoursesDelivered = JSON.parse(result);
              this.dataCoursesDelivered = [];

              for (var i in CoursesDelivered) {
                this.dataCoursesDelivered.push({
                  'PartnerType': CoursesDelivered[i].SiteName,
                  'Total': Number(CoursesDelivered[i].TotalCourseMonthByPartnerType),
                });
              }

              if (this.dataCoursesDelivered == "") {
                this.dataCoursesDelivered.push({
                  'PartnerType': '-',
                  'Total': 0
                })
              }

             
                Morris.Bar({
                    element: 'morris-bar-chart',
                    data: this.dataCoursesDelivered,
                    xkey: 'PartnerType',
                    ykeys: ['Total'],
                    labels: ['Total'],
                    barColors: ['#5FBEAA', '#5D9CEC', '#cCcCcC'],
                    hideHover: 'auto',
                    gridLineColor: '#eef0f2',
                    resize: true
                });
            
            }
          },
          error => {
            this.messageError = <any>error
            this.service.errorserver();
          });
    }

    getSiteMeetTheirAnnualReq(){
        console.log("test")
    }


        // Line chart
        // this.dataMEETTHEIRANNUALREQUIREMENTS = [
        //     ['City', 'Meet', 'Not Meet'],
        //     ['Turkey, Israel & Africa', 8175000, 8008000],
        //     ['Taiwan', 3792000, 3694000],
        //     ['Rusia', 2695000, 2896000],
        //     ['N America', 2099000, 1953000],
        //     ['ME & N.Africa', 1526000, 1517000]
        // ]

        // setTimeout(() => {
        //     this.MEETTHEIRANNUALREQUIREMENTS = {
        //         chartType: 'BarChart',
        //         dataTable: this.dataMEETTHEIRANNUALREQUIREMENTS,
        //         options: {
        //             height: 300,
        //             title: 'OF SITES MEET THEIR ANNUAL REQUIREMENTS',
        //             chartArea: { width: '50%' },
        //             isStacked: true,
        //             hAxis: {
        //                 title: ' ',
        //                 minValue: 0,
        //             },
        //             vAxis: {
        //                 title: ' '
        //             },
        //             colors: ['#2ecc71', '#5faee3']
        //         },
        //     };




       

}
