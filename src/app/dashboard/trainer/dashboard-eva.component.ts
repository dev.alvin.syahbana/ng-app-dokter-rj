import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import {AppService} from "../../shared/service/app.service";

@Component({
  selector: 'app-dashboard-eva',
  templateUrl: './dashboard-eva.component.html',
  styleUrls: [
    './dashboard-eva.component.css',
    '../../../../node_modules/c3/c3.min.css', 
    ], 
  encapsulation: ViewEncapsulation.None
})
 
export class DashboardEVAComponent implements OnInit {

    doughChartCoreProductsAgainstAllCourses:any;
    doughChartTotalEvaluationSurveyByMonth:any;
    doughChartPerformanceSite:any;
    tableChartDataTopInstructor:any;
 
    private _serviceUrlPerformanceSite = '/api/DashboardPerformanceSite';
    private _serviceUrlCoursesDeliveredBySite = '/api/DashboardCoursesDeliveredBySite';
    private _serviceUrlMonthEvaluationSurveyByGeo = '/api/DashboardMonthEvaluationSurveyByGeo';
    private _serviceUrlEvaluationSurveyByCoreProducts = '/api/DashboardEvaluationByCoreProducts';
    private _serviceUrlTop10PerformanceInstructor = '/api/DashboardTop10PerformanceInstructor';

    dataCoursesDelivered = [];
    dataCoreProductsAgainstAllCourses = [];
    dataKeyValueCoursesDeliveredByPartners = [];
    dataTotalEvaluationSurveyByMonthByGeo=[];
    DataPerformanceSite=[];
    DataTop10PerformanceIntructor=[];
   
    constructor(private service:AppService) { }

    ngOnInit() {
        this.getMonthPerformanceSite();
        this.getTop10PerformanceInstructor();
        this.getCoursesDelivered();
        this.getMonthEvaluationSurvey();
        this.getCoreProductsAgainstAllCourses();
    }

    // baru site performance buat api nya
    getMonthPerformanceSite(){
        var data = '';
        this.service.get(this._serviceUrlPerformanceSite,data)
        .subscribe(result => {
            if(result=="Not found"){
                this.service.notfound();
            }
            else{
                var PerformanceSite = JSON.parse(result);
                this.DataPerformanceSite.push( ['Task', 'Hours per Day']);
                for(var i in PerformanceSite) {
                    this.DataPerformanceSite.push([PerformanceSite[i].geo_name, Number(PerformanceSite[i].TotalPerformanceSite)]);
                }
                this.doughChartPerformanceSite =  {
                    chartType: 'PieChart',
                    dataTable: this.DataPerformanceSite,
                    options: {
                    height: 360,
                    title: 'Evaluation Survey Taken By Month',
                    pieHole: 0.4,
                    colors: [
                        '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
                        ]
                    },
                };
            }
        },
        error => {
            this.service.errorserver();
        });
    }

    getTop10PerformanceInstructor(){
        var data = '';
        this.service.get(this._serviceUrlTop10PerformanceInstructor,data)
        .subscribe(result => {
            if(result=="Not found"){
                this.service.notfound();
            }
            else{
                var Top10PerformanceIntructor = JSON.parse(result);
                this.DataTop10PerformanceIntructor.push( ['Contact Id', 'Contact Name', 'Course Taken']);
                for(var i in Top10PerformanceIntructor) {
                    this.DataTop10PerformanceIntructor.push([Top10PerformanceIntructor[i].ContactId, Top10PerformanceIntructor[i].ContactName, Number(Top10PerformanceIntructor[i].TotalCourseDeliveredByIntructor)]);
                }
                console.log(this.DataTop10PerformanceIntructor)
                this.tableChartDataTopInstructor =  {
                    chartType: 'Table',
                    dataTable: this.DataTop10PerformanceIntructor,
                    options: {
                      height: 340,
                      title: 'Countries',
                      allowHtml: true
                    }
                };
            }
        },
        error => {
            this.service.errorserver();
        });
    }

    getCoursesDelivered(){
        var data = '';
        this.service.get(this._serviceUrlCoursesDeliveredBySite,data)
        .subscribe(result => {
            if(result=="Not found"){
                this.service.notfound();
            }
            else{
                var CoursesDelivered = JSON.parse(result);
                this.dataCoursesDelivered.push( ['Task', 'Hours per Day']);
                for(var i in CoursesDelivered) {
                    this.dataCoursesDelivered.push([CoursesDelivered[i].SiteName, Number(CoursesDelivered[i].TotalCourseDeliveredByPartners)]);
                }
                const chart = c3.generate({
                    bindto: '#chart4',
                    scaleShowLabels: false,
                    data: {
                    columns: this.dataCoursesDelivered,
                    xkey: ['Jan 2017', 'Feb 2017', 'Mar 2017','Jan 2017', 'Feb 2017', 'Mar 2017','Jan 2017', 'Feb 2017', 'Mar 2017','Jan 2017', 'Feb 2017', 'Mar 2017'],
                    type: 'bar',
                    colors: {
                        data1: '#03A9F3',
                        data2: '#FEC107', 
                    }, 
                    groups: this.dataKeyValueCoursesDeliveredByPartners
                    }
                });
            }
        },
        error => {
            this.service.errorserver();
        });
    }

    getMonthEvaluationSurvey(){
        var data = '';
        this.service.get(this._serviceUrlMonthEvaluationSurveyByGeo,data)
        .subscribe(result => {
            if(result=="Not found"){
                this.service.notfound();
            }
            else{
                var TotalEvaluationSurveyByMonth = JSON.parse(result);
                this.dataTotalEvaluationSurveyByMonthByGeo.push( ['Task', 'Hours per Day']);
                for(var i in TotalEvaluationSurveyByMonth) {
                    this.dataTotalEvaluationSurveyByMonthByGeo.push([TotalEvaluationSurveyByMonth[i].geo_name, Number(TotalEvaluationSurveyByMonth[i].TotalMOnthlySurveyTaken)]);
                }
                this.doughChartTotalEvaluationSurveyByMonth =  {
                    chartType: 'PieChart',
                    dataTable: this.dataTotalEvaluationSurveyByMonthByGeo,
                    options: {
                    height: 360,
                    title: 'Evaluation Survey Taken By Month',
                    pieHole: 0.4,
                    colors: [
                        '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
                        ]
                    },
                };
            }
        },
        error => {
            this.service.errorserver();
        });
    }

    getCoreProductsAgainstAllCourses(){
       var data = '';
       this.service.get(this._serviceUrlEvaluationSurveyByCoreProducts,data)
       .subscribe(result => {
            if(result=="Not found"){
                this.service.notfound();
            }
            else{
                var CoreProductsAgainstAllCourses = JSON.parse(result);
                this.dataCoreProductsAgainstAllCourses.push( ['Task', 'Hours per Day']);
                for(var i in CoreProductsAgainstAllCourses) {
                    this.dataCoreProductsAgainstAllCourses.push([CoreProductsAgainstAllCourses[i].productName, Number(CoreProductsAgainstAllCourses[i].TotalSurveyTaken)]);
                }
                this.doughChartCoreProductsAgainstAllCourses =  {
                    chartType: 'PieChart',
                    dataTable: this.dataCoreProductsAgainstAllCourses,
                    options: {
                    height: 360,
                    title: 'Evaluation Survey Taken By Core Products',
                    pieHole: 0.4,
                    colors: [
                        '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
                        ]
                    },
                };
            }
       },
       error => {
           this.service.errorserver();
       });
    }
}