import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl, Validators, NgControl } from "@angular/forms";
import * as XLSX from 'xlsx';

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
    constructor(private datepipe: DatePipe) { }
    transform(date: string): any {
        return this.datepipe.transform(new Date(date.substr(0, 10)), 'dd-MM-yyyy');
    }
}

@Pipe({ name: 'dataFilterMon' })
export class DataFilterMonPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.IdMonitoringAim.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.KodeAlat.toLowerCase().indexOf(query.toLowerCase()) > -1)
            );
        }
        return array;
    }
}

const now = new Date();

@Component({
    selector: 'app-tambahmonitoringAIM',
    templateUrl: './tambahmonitoringAIM.component.html',
    styleUrls: [
        './tambahmonitoringAIM.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})


export class TambahMonitoringAIMComponent implements OnInit {

    private _serviceUrl = '/api/MonitoringAlatInstrumenMedis';
    public data: any;
    modelPopup0: NgbDateStruct;

    formMonAIM: FormGroup;

    dropdownListAIM = [];
    selectedItemsAIM = [];
    dropdownSettingsAIM = {};

    public loading = false;

    constructor(private service: AppService, private formatdate: AppFormatDate, private session: SessionService, private router: Router, private datePipe: DatePipe) {

        this.modelPopup0 = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };

        let TanggalMonitoring = new FormControl('', Validators.required);
        let KodeAlatInstrumenMedis = new FormControl('', Validators.required);
        let NamaAlatInstrumenMedis = new FormControl('');
        let Pagi = new FormControl(false);
        let Siang = new FormControl(false);
        let Malam = new FormControl(false);

        this.formMonAIM = new FormGroup({
            TanggalMonitoring: TanggalMonitoring,
            KodeAlatInstrumenMedis: KodeAlatInstrumenMedis,
            NamaAlatInstrumenMedis: NamaAlatInstrumenMedis,
            Pagi: Pagi,
            Siang: Siang,
            Malam: Malam
        });
    }

    ngOnInit() {
        // this.getDataMonAIM();
        this.getDataInstrumenMedis();
        this.dropdownSettingsAIM = {
            singleSelection: true,
            text: "-- Pilih --",
            enableSearchFilter: true,
            enableCheckAll: false,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            searchAutofocus: true
        };

    }

    getDataInstrumenMedis() {
        var data: any;
        this.service.get('api/AlatInstrumenMedis', data)
            .subscribe(result => {
                // console.log(result);
                data = JSON.parse(result);
                if (data == "Not found") {
                    this.service.notfound();
                    this.selectedItemsAIM = [];
                }
                else {
                    this.dropdownListAIM = data.map((item) => {
                        return {
                            id: item.kodeAlatInstrumenMedis,
                            itemName: item.namaAlatInstrumenMedis
                        }
                    })
                }
            });
        this.selectedItemsAIM = [];
    }

    onItemSelect(item: any) { }

    OnItemDeSelect(item: any) { }

    tgl = "";
    onSelectTanggal(date: NgbDateStruct) {
        if (date != null) {
            this.formMonAIM.value.TanggalMonitoring = date;
            this.tgl = this.formatdate.dateCalendarToYMD(this.formMonAIM.value.TanggalMonitoring);
            // console.log(this.tgl);

            var data: any;
            this.service.get('api/AlatInstrumenMedis/GetDataNotInByDate/' + this.tgl, data)
                .subscribe(result => {
                    data = JSON.parse(result);
                    // console.log(data);
                    if (data == "Not found") {
                        this.service.notfound();
                        this.selectedItemsAIM = [];
                    }
                    else {
                        this.dropdownListAIM = data.map((item) => {
                            return {
                                id: item.kodeAlatInstrumenMedis,
                                itemName: item.namaAlatInstrumenMedis
                            }
                        })
                    }
                });
            this.selectedItemsAIM = [];
        }
    }

    onSubmit() {

        this.formMonAIM.controls['KodeAlatInstrumenMedis'].markAsTouched();
        this.formMonAIM.controls['TanggalMonitoring'].markAsTouched();

        if (this.formMonAIM.valid) {
            this.loading = true;
            // this.formMonAIM.value.TanggalMonitoring = this.formatdate.dateJStoYMD(new Date());
            this.formMonAIM.value.TanggalMonitoring = this.formatdate.dateCalendarToYMD(this.formMonAIM.value.TanggalMonitoring);
            this.formMonAIM.value.KodeAlatInstrumenMedis = this.selectedItemsAIM[0].id;
            this.formMonAIM.value.NamaAlatInstrumenMedis = this.selectedItemsAIM[0].itemName;

            if (this.formMonAIM.value.Pagi == true) {
                this.formMonAIM.value.Pagi = "Ya";
            }
            else {
                this.formMonAIM.value.Pagi = "Tidak";
            }

            if (this.formMonAIM.value.Siang == true) {
                this.formMonAIM.value.Siang = "Ya";
            }
            else {
                this.formMonAIM.value.Siang = "Tidak";
            }

            if (this.formMonAIM.value.Malam == true) {
                this.formMonAIM.value.Malam = "Ya";
            }
            else {
                this.formMonAIM.value.Malam = "Tidak";
            }

            //convert object to json
            let data = JSON.stringify(this.formMonAIM.value);
            // console.log(data);

            // post action
            this.service.httpClientPost(this._serviceUrl, data);
            setTimeout(() => {
                swal(
                    'Sukses!',
                    'Data Berhasil Disimpan',
                    'success'
                )
                this.router.navigate(['/listmonitoringAIM']);
                this.loading = false;
            }, 5000);
        }
    }

}
