import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';
import { SessionService } from '../../shared/service/session.service';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import * as XLSX from 'xlsx';

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
    constructor(private datepipe: DatePipe) { }
    transform(date: string): any {
        return this.datepipe.transform(new Date(date.substr(0, 10)), 'dd-MM-yyyy');
    }
}

@Pipe({ name: 'dataFilterMonAlkes' })
export class DataFilterMonAlkesPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.IdMonitoringAlkes.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.kodeAlatKesehatan.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Sisa.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Terpakai.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Kurang.toLowerCase().indexOf(query.toLowerCase()) > -1)
            );
        }
        return array;
    }
}

const now = new Date();

@Component({
    selector: 'app-listmonitoringalkes',
    templateUrl: './listmonitoringalkes.component.html',
    styleUrls: [
        './listmonitoringalkes.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})


export class ListMonitoringAlkesComponent implements OnInit {

    public _serviceUrl = '/api/MonitoringAlatKesehatans';
    public studentcourse: any;
    public dataAlkes: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "idMonitoringAlkes";
    public sortOrder: string = "desc";
    public filterStatus: string = "All";
    public title = 'dini';
    modelPopup1: NgbDateStruct;
    modelPopup2: NgbDateStruct;
    formFilter: FormGroup;
    public pilihanSort;
    public data: any;

    public loading = false;

    constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate, private session: SessionService, private datePipe: DatePipe) {

        // Form Filter By Tanggal
        let dariTanggal = new FormControl('', Validators.required);
        let sampaiTanggal = new FormControl('', Validators.required);
        // this.modelPopup1 = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };

        this.formFilter = new FormGroup({
            dariTanggal: dariTanggal,
            sampaiTanggal: sampaiTanggal
        });
    }

    ngOnInit() {
        this.getDataMonAlkes();
    }

    getDataMonAlkes() {
        this.loading = true;
        var data: any;
        this.service.httpClientGet('api/MonitoringAlatKesehatans/GetMonitoringkesAlkes', data)
            .subscribe(result => {
                // console.log(result);
                if (result == null) {
                    swal(
                        'Informasi!',
                        'Data tidak ditemukan.',
                        'error'
                    );
                    this.data = '';
                    this.loading = false;
                }
                else {
                    this.data = result;
                    this.loading = false;
                }
            },
                error => {
                    this.service.errorserver();
                    this.data = '';
                    this.loading = false;
                });
    }

    ExportExcel() {
        // var date = this.service.formatDate();
        var fileName = "MonitoringAlKes.xls";
        var ws = XLSX.utils.json_to_sheet(this.data);
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, fileName);
        XLSX.writeFile(wb, fileName);
    }

    dariTgl = "";
    onSelectDariTanggal(date: NgbDateStruct) {
        if (date != null) {
            this.formFilter.value.dariTanggal = date;
            this.dariTgl = this.formatdate.dateCalendarToYMD(this.formFilter.value.dariTanggal);
            // console.log(this.dariTgl);
        }
    }

    sampaiTgl = "";
    onSelectSampaiTanggal(date: NgbDateStruct) {
        if (date != null) {
            this.formFilter.value.sampaiTanggal = date;
            this.sampaiTgl = this.formatdate.dateCalendarToYMD(this.formFilter.value.sampaiTanggal);
            // console.log(this.sampaiTgl);
        }
    }

    // on Submit untuk Filter by Tanggal
    onSubmit() {

        this.formFilter.controls['dariTanggal'].markAsTouched();
        this.formFilter.controls['sampaiTanggal'].markAsTouched();

        if (this.formFilter.valid) {

            this.loading = true;
            var data: any;
            this.service.httpClientGet("api/MonitoringAlatKesehatans/FilterByTanggal/" + this.dariTgl + "/" + this.sampaiTgl + "", data)
                .subscribe(result => {
                    // console.log(result);
                    if (result == null) {
                        swal(
                            'Informasi!',
                            'Data tidak ditemukan.',
                            'error'
                        );
                        this.data = '';
                        this.loading = false;
                    }
                    else {
                        this.data = result;
                        this.loading = false;
                    }
                });
            error => {
                this.service.errorserver();
                this.data = '';
                this.loading = false;
            }

        }
    }

}
