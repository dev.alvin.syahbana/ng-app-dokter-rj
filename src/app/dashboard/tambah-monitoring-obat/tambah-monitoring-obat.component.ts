import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl, Validators, NgControl } from "@angular/forms";
import * as XLSX from 'xlsx';

const now = new Date();

@Component({
    selector: 'app-tambah-monitoring-obat',
    templateUrl: './tambah-monitoring-obat.component.html',
    styleUrls: [
        './tambah-monitoring-obat.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class TambahMonitoringObatComponent implements OnInit {

    public _serviceUrl = '/api/MonitoringObats';
    public data: any;
    modelPopup0: NgbDateStruct;
    formMonObat: FormGroup;

    dropdownListObat = [];
    selectedItemsObat = [];
    dropdownSettingsObat = {};

    public loading = false;

    constructor(private service: AppService, private formatdate: AppFormatDate, private session: SessionService, private router: Router, private datePipe: DatePipe) {

        this.modelPopup0 = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };

        let TanggalMonitoring = new FormControl('', Validators.required);
        let KodeObat = new FormControl('', Validators.required);
        let NamaObat = new FormControl('');
        let Sisa = new FormControl('', Validators.required);
        let Terpakai = new FormControl('', Validators.required);
        let Kurang = new FormControl('', Validators.required);

        this.formMonObat = new FormGroup({
            TanggalMonitoring: TanggalMonitoring,
            KodeObat: KodeObat,
            NamaObat: NamaObat,
            Sisa: Sisa,
            Terpakai: Terpakai,
            Kurang: Kurang
        });

    }

    ngOnInit() {
        this.getDataObat();
        this.dropdownSettingsObat = {
            singleSelection: true,
            text: "-- Pilih --",
            enableSearchFilter: true,
            enableCheckAll: false,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            searchAutofocus: true
        };
    }

    getDataObat() {
        var data: any;
        this.service.get('api/DaftarObats', data)
            .subscribe(result => {
                // console.log(result);
                data = JSON.parse(result);
                if (data == "Not found") {
                    this.service.notfound();
                    this.selectedItemsObat = [];
                }
                else {
                    this.dropdownListObat = data.map((item) => {
                        return {
                            id: item.kodeObat,
                            itemName: item.namaObat
                        }
                    })
                }
            });
        this.selectedItemsObat = [];
    }

    tgl = "";
    onSelectTanggal(date: NgbDateStruct) {
        if (date != null) {
            this.formMonObat.value.TanggalMonitoring = date;
            this.tgl = this.formatdate.dateCalendarToYMD(this.formMonObat.value.TanggalMonitoring);
            // console.log(this.tgl);

            var data: any;
            this.service.get('api/DaftarObats/GetDataNotInByDate/' + this.tgl, data)
                .subscribe(result => {
                    data = JSON.parse(result);
                    // console.log(data);
                    if (data == "Not found") {
                        this.service.notfound();
                        this.selectedItemsObat = [];
                    }
                    else {
                        this.dropdownListObat = data.map((item) => {
                            return {
                                id: item.kodeObat,
                                itemName: item.namaObat
                            }
                        })
                    }
                });
            this.selectedItemsObat = [];
        }
    }

    onItemSelect(item: any) { }

    OnItemDeSelect(item: any) { }

    onSubmit() {
        this.formMonObat.controls['KodeObat'].markAsTouched();
        this.formMonObat.controls['TanggalMonitoring'].markAsTouched();
        this.formMonObat.controls['Sisa'].markAsTouched();
        this.formMonObat.controls['Terpakai'].markAsTouched();
        this.formMonObat.controls['Kurang'].markAsTouched();

        if (this.formMonObat.valid) {
            this.loading = true;
            // this.formMonObat.value.TanggalMonitoring = this.formatdate.dateJStoYMD(new Date());
            this.formMonObat.value.TanggalMonitoring = this.formatdate.dateCalendarToYMD(this.formMonObat.value.TanggalMonitoring);
            this.formMonObat.value.KodeObat = this.selectedItemsObat[0].id;
            this.formMonObat.value.NamaObat = this.selectedItemsObat[0].itemName;

            //convert object to json
            let data = JSON.stringify(this.formMonObat.value);
            // console.log(data);

            // post action
            this.service.httpClientPost(this._serviceUrl, data);
            setTimeout(() => {
                swal(
                    'Sukses!',
                    'Data Berhasil Disimpan',
                    'success'
                )
                this.router.navigate(['/listmonitoringobat']);
                this.loading = false;
            }, 2500);
        }
    }
}