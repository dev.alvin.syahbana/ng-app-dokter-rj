import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TambahMonitoringObatComponent } from './tambah-monitoring-obat.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

export const TambahMonitoringObatRoutes: Routes = [
    {
        path: '',
        component: TambahMonitoringObatComponent,
        data: {
            breadcrumb: 'Tambah Monitoring Obat',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(TambahMonitoringObatRoutes),
      SharedModule,
      LoadingModule,
      AngularMultiSelectModule
    ],
    declarations: [TambahMonitoringObatComponent],
    providers: [AppService, AppFormatDate]
  })
  export class TambahMonitoringObatModule { }