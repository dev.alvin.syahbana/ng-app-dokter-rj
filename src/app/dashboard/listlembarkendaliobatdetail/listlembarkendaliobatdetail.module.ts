import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { ListLKODComponent } from './listlembarkendaliobatdetail.component';
import { DataFilterActivitiesPipe, DataFilterStatusPipe, ConvertDatePipe, DataFilterMonPipe } from './listlembarkendaliobatdetail.component';

export const ListLKODRoutes: Routes = [
  {
    path: '',
    component: ListLKODComponent,
    data: {
      breadcrumb: 'eim.profile.my_profile.my_profile',
      icon: 'icofont-home bg-c-blue',
      status: false,
      title: 'Dashboard'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ListLKODRoutes),
    SharedModule,
    LoadingModule,
    AngularMultiSelectModule
  ],
  declarations: [ListLKODComponent, DataFilterMonPipe, ConvertDatePipe, DataFilterActivitiesPipe, DataFilterStatusPipe],
  providers: [AppService, AppFormatDate]
  
})
export class ListLKODModule { }
