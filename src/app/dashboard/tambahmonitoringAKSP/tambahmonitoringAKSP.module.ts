import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TambahMonitoringAKSPComponent, DataFilterMonPipe, ConvertDatePipe } from './tambahmonitoringAKSP.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';


export const TambahMonitoringAKSPRoutes: Routes = [
  {
    path: '',
    component: TambahMonitoringAKSPComponent,
    data: {
      breadcrumb: 'eim.profile.my_profile.my_profile',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TambahMonitoringAKSPRoutes),
    SharedModule,
    LoadingModule,
    AngularMultiSelectModule
  ],
  declarations: [TambahMonitoringAKSPComponent, DataFilterMonPipe, ConvertDatePipe],
  providers: [AppService, AppFormatDate]
})
export class TambahMonitoringAKSPModule { }
