import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl, Validators, NgControl } from "@angular/forms";
import * as XLSX from 'xlsx';

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
    constructor(private datepipe: DatePipe) { }
    transform(date: string): any {
        return this.datepipe.transform(new Date(date.substr(0, 10)), 'dd-MM-yyyy');
    }
}

@Pipe({ name: 'dataFilterMon' })
export class DataFilterMonPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.idMonitoringAmsp.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.KodeAlat.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Nama_Alat.toLowerCase().indexOf(query.toLowerCase()) > -1)
            );
        }
        return array;
    }
}

const now = new Date();

@Component({
    selector: 'app-tambahmonitoringAKSP',
    templateUrl: './tambahmonitoringAKSP.component.html',
    styleUrls: [
        './tambahmonitoringAKSP.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})


export class TambahMonitoringAKSPComponent implements OnInit {

    private _serviceUrl = '/api/MonitoringAlatMedisSiapPakais';
    public data: any;
    modelPopup0: NgbDateStruct;

    formMonAKSP: FormGroup;

    dropdownListAKSP = [];
    selectedItemsAKSP = [];
    dropdownSettingsAKSP = {};

    public loading = false;

    constructor(private service: AppService, private formatdate: AppFormatDate, private session: SessionService, private router: Router) {

        this.modelPopup0 = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };

        let TanggalMonitoring = new FormControl('', Validators.required);
        let KodeAlatSp = new FormControl('', Validators.required);
        let NamaAlatSp = new FormControl('');
        let StatusTerpakai = new FormControl(false);

        this.formMonAKSP = new FormGroup({
            TanggalMonitoring: TanggalMonitoring,
            KodeAlatSp: KodeAlatSp,
            NamaAlatSp: NamaAlatSp,
            StatusTerpakai: StatusTerpakai,
        });
    }

    ngOnInit() {
        // this.getDataMonAKSP();
        this.getDataAlkesSiapPakai();
        this.dropdownSettingsAKSP = {
            singleSelection: true,
            text: "-- Pilih --",
            enableSearchFilter: true,
            enableCheckAll: false,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            searchAutofocus: true
        };
    }

    getDataAlkesSiapPakai() {
        var data: any;
        this.service.get('api/AlatKesehatanSiapPakais', data)
            .subscribe(result => {
                // console.log(result);
                data = JSON.parse(result);
                if (data == "Not found") {
                    this.service.notfound();
                    this.selectedItemsAKSP = [];
                }
                else {
                    this.dropdownListAKSP = data.map((item) => {
                        return {
                            id: item.kodeAlatSp,
                            itemName: item.namaAlatSp
                        }
                    })
                }
            });
        this.selectedItemsAKSP = [];
    }

    onItemSelect(item: any) { }

    OnItemDeSelect(item: any) { }

    tgl = "";
    onSelectTanggal(date: NgbDateStruct) {
        if (date != null) {
            this.formMonAKSP.value.TanggalMonitoring = date;
            this.tgl = this.formatdate.dateCalendarToYMD(this.formMonAKSP.value.TanggalMonitoring);

            var data: any;
            this.service.get('api/AlatKesehatanSiapPakais/GetDataNotInByDate/' + this.tgl, data)
                .subscribe(result => {
                    data = JSON.parse(result);
                    // console.log(data);
                    if (data == "Not found") {
                        this.service.notfound();
                        this.selectedItemsAKSP = [];
                    }
                    else {
                        this.dropdownListAKSP = data.map((item) => {
                            return {
                                id: item.kodeAlatSp,
                                itemName: item.namaAlatSp
                            }
                        })
                    }
                });
            this.selectedItemsAKSP = [];
        }
    }

    onSubmit() {

        this.formMonAKSP.controls['KodeAlatSp'].markAsTouched();
        this.formMonAKSP.controls['TanggalMonitoring'].markAsTouched();

        if (this.formMonAKSP.valid) {
            this.loading = true;
            // this.formMonAKSP.value.TanggalMonitoring = this.formatdate.dateJStoYMD(new Date());
            this.formMonAKSP.value.TanggalMonitoring = this.formatdate.dateCalendarToYMD(this.formMonAKSP.value.TanggalMonitoring);
            this.formMonAKSP.value.KodeAlatSp = this.selectedItemsAKSP[0].id;
            this.formMonAKSP.value.NamaAlatSp = this.selectedItemsAKSP[0].itemName;

            if (this.formMonAKSP.value.StatusTerpakai == true) {
                this.formMonAKSP.value.StatusTerpakai = "Ya";
            }
            else {
                this.formMonAKSP.value.StatusTerpakai = "Tidak";
            }

            //convert object to json
            let data = JSON.stringify(this.formMonAKSP.value);
            // console.log(data);

            // post action
            this.service.httpClientPost(this._serviceUrl, data);
            setTimeout(() => {
                swal(
                    'Sukses!',
                    'Data Berhasil Disimpan',
                    'success'
                )
                this.router.navigate(['/listmonitoringAKSP']);
                this.loading = false;
            }, 5000);
        }
    }

}
