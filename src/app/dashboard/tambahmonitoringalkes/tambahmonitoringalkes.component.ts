import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';
import { SessionService } from '../../shared/service/session.service';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import * as XLSX from 'xlsx';

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
    constructor(private datepipe: DatePipe) { }
    transform(date: string): any {
        return this.datepipe.transform(new Date(date.substr(0, 10)), 'dd-MM-yyyy');
    }
}

@Pipe({ name: 'dataFilterMonAlkes' })
export class DataFilterMonAlkesPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.IdMonitoringAlkes.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.KodeAlatKesehatan.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Sisa.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Terpakai.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.Kurang.toLowerCase().indexOf(query.toLowerCase()) > -1)
            );
        }
        return array;
    }
}

const now = new Date();

@Component({
    selector: 'app-tambahmonitoringalkes',
    templateUrl: './tambahmonitoringalkes.component.html',
    styleUrls: [
        './tambahmonitoringalkes.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})


export class TambahMonitoringAlkesComponent implements OnInit {

    public _serviceUrl = '/api/MonitoringAlatKesehatans';
    modelPopup0: NgbDateStruct;
    public data: any;

    formMonAlkes: FormGroup;

    dropdownListAlkes = [];
    selectedItemsAlkes = [];
    dropdownSettingsAlkes = {};

    public loading = false;

    constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate, private session: SessionService, private datePipe: DatePipe) {

        this.modelPopup0 = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };

        // Simpan Data ke tabel Monitoring Alat Kesehatan
        let TanggalMonitoring = new FormControl('', Validators.required);
        let KodeAlatKesehatan = new FormControl('', Validators.required);
        let NamaAlatKesehatan = new FormControl('');
        let Sisa = new FormControl('', Validators.required);
        let Terpakai = new FormControl('', Validators.required);
        let Kurang = new FormControl('', Validators.required);

        this.formMonAlkes = new FormGroup({
            TanggalMonitoring: TanggalMonitoring,
            KodeAlatKesehatan: KodeAlatKesehatan,
            NamaAlatKesehatan: NamaAlatKesehatan,
            Sisa: Sisa,
            Terpakai: Terpakai,
            Kurang: Kurang
        });
    }

    ngOnInit() {
        this.getDataAlkes();
        // this.getDataMonAlkes();
        this.dropdownSettingsAlkes = {
            singleSelection: true,
            text: "-- Pilih --",
            enableSearchFilter: true,
            enableCheckAll: false,
            classes: "myclass custom-class",
            disabled: false,
            maxHeight: 120,
            searchAutofocus: true
        };
    }

    getDataAlkes() {
        var data: any;
        this.service.get('api/AlatKesehatans', data)
            .subscribe(result => {
                data = JSON.parse(result);
                if (data == "Not found") {
                    this.service.notfound();
                    this.selectedItemsAlkes = [];
                }
                else {
                    this.dropdownListAlkes = data.map((item) => {
                        return {
                            id: item.kodeAlatKesehatan,
                            itemName: item.namaAlatKesehatan
                        }
                    })
                }
            });
        this.selectedItemsAlkes = [];
    }

    onItemSelect(item: any) { }

    OnItemDeSelect(item: any) { }

    tgl = "";
    onSelectTanggal(date: NgbDateStruct) {
        if (date != null) {
            this.formMonAlkes.value.TanggalMonitoring = date;
            this.tgl = this.formatdate.dateCalendarToYMD(this.formMonAlkes.value.TanggalMonitoring);
            // console.log(this.tgl);

            var data: any;
            this.service.get('api/AlatKesehatans/GetDataNotInByDate/' + this.tgl, data)
                .subscribe(result => {
                    data = JSON.parse(result);
                    // console.log(data);
                    if (data == "Not found") {
                        this.service.notfound();
                        this.selectedItemsAlkes = [];
                    }
                    else {
                        this.dropdownListAlkes = data.map((item) => {
                            return {
                                id: item.kodeAlatKesehatan,
                                itemName: item.namaAlatKesehatan
                            }
                        })
                    }
                });
            this.selectedItemsAlkes = [];
        }
    }

    onSubmit() {
        this.formMonAlkes.controls['TanggalMonitoring'].markAsTouched();
        this.formMonAlkes.controls['KodeAlatKesehatan'].markAsTouched();
        this.formMonAlkes.controls['Sisa'].markAsTouched();
        this.formMonAlkes.controls['Terpakai'].markAsTouched();
        this.formMonAlkes.controls['Kurang'].markAsTouched();

        if (this.formMonAlkes.valid) {
            this.loading = true;
            // this.formMonAlkes.value.TanggalMonitoring = this.formatdate.dateJStoYMD(new Date());
            this.formMonAlkes.value.TanggalMonitoring = this.formatdate.dateCalendarToYMD(this.formMonAlkes.value.TanggalMonitoring);
            this.formMonAlkes.value.KeteranganMonitoring = "Sukses";
            this.formMonAlkes.value.KodeAlatKesehatan = this.selectedItemsAlkes[0].id;
            this.formMonAlkes.value.NamaAlatKesehatan = this.selectedItemsAlkes[0].itemName;

            //convert object to json
            let data = JSON.stringify(this.formMonAlkes.value);
            // console.log(data);

            // post action
            this.service.httpClientPost(this._serviceUrl, data);
            setTimeout(() => {
                swal(
                    'Sukses!',
                    'Data Berhasil Disimpan',
                    'success'
                )
                this.router.navigate(['/listmonitoringalkes']);
                // this.getDataMonAlkes();
                this.loading = false;
            }, 5000);

            // this.formMonAlkes.reset();
            // this.selectedItemsAlkes = [];
        }
    }

}
