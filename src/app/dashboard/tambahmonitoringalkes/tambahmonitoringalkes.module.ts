import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TambahMonitoringAlkesComponent, DataFilterMonAlkesPipe, ConvertDatePipe } from './tambahmonitoringalkes.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';


export const TambahMonitoringAlkesRoutes: Routes = [
  {
    path: '',
    component: TambahMonitoringAlkesComponent,
    data: {
      breadcrumb: 'List Monitoring Alat Kesehatan',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TambahMonitoringAlkesRoutes),
    SharedModule,
    LoadingModule,
    AngularMultiSelectModule
  ],
  declarations: [TambahMonitoringAlkesComponent, DataFilterMonAlkesPipe, ConvertDatePipe],
  providers: [AppService, AppFormatDate]
})
export class TambahMonitoringAlkesModule { }
