import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataFilterActivitiesPipe, DataFilterStatusPipe, ListPBFComponent, DataFilterMonPipe, ConvertDatePipe } from './listpemakaianbarangfarmasi.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';


export const ListPBFRoutes: Routes = [
  {
    path: '',
    component: ListPBFComponent,
    data: {
      breadcrumb: 'eim.profile.my_profile.my_profile',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ListPBFRoutes),
    SharedModule,
    LoadingModule,
    AngularMultiSelectModule
  ],
  declarations: [ListPBFComponent, DataFilterMonPipe, ConvertDatePipe, DataFilterActivitiesPipe, DataFilterStatusPipe],
  providers: [AppService, AppFormatDate]
})
export class ListPBFModule { }
