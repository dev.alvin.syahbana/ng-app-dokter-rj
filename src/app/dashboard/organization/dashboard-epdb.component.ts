import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
import { Http } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { AppService } from "../../shared/service/app.service";
import { SessionService } from '../../shared/service/session.service';

@Component({
    selector: 'app-dashboard-epdb',
    templateUrl: './dashboard-epdb.component.html',
    styleUrls: [
        './dashboard-epdb.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class DashboardEPDBComponent implements OnInit {

    doughChartCoreProductsAgainstAllCourses: any;
    doughChartTotalEvaluationSurveyByMonth: any;
    doughChartPerformanceSite: any;
    tableChartDataTopInstructor: any;
    messageError: string = '';
    private _serviceUrlPerformanceSite = '/api/DashboardPerformanceSite';
    private _serviceUrlCoursesDeliveredBySite = '/api/DashboardCoursesDeliveredBySite';
    private _serviceUrlMonthEvaluationSurveyByGeo = '/api/DashboardMonthEvaluationSurveyByGeo';
    private _serviceUrlEvaluationSurveyByCoreProducts = '/api/DashboardEvaluationByCoreProducts';
    private _serviceUrlTop10PerformanceInstructor = '/api/DashboardTop10PerformanceInstructor';

    dataCoursesDelivered = [];
    dataCoreProductsAgainstAllCourses = [];
    dataKeyValueCoursesDeliveredByPartners = [];
    dataTotalEvaluationSurveyByMonthByGeo = [];
    DataPerformanceSite = [];
    DataTop10PerformanceIntructor = [];

    /*Bar chart*/
    //Month Courses
    type_MonthCoursesByPartner = 'bar';
    data_MonthCoursesByPartner = {}
    options_MonthCoursesByPartner = {
        responsive: true,
        maintainAspectRatio: false,
        legend:{
            position: 'bottom'
          }
    };

    //Month Eva
    type_MonthEvaluationSurver = 'bar';
    data_MonthEvaluationSurver = {}
    options_MonthEvaluationSurver = {
        responsive: true,
        maintainAspectRatio: false,
        legend:{
            position: 'bottom'
          }
    };


    //Total Eva
    type_TotalEvalutionTaken = 'bar';
    data_TotalEvalutionTaken = {}
    options_TotalEvalutionTaken = {
        responsive: true,
        maintainAspectRatio: false,
        legend:{
            position: 'bottom'
          }
    };


    /*Bar chart End*/


    useraccesdata: any;
    constructor(private service: AppService, private session: SessionService) { 
        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    checkrole(): boolean {
        let userlvlarr = this.useraccesdata.UserLevelId.split(',');
        for (var i = 0; i < userlvlarr.length; i++) {
            if (userlvlarr[i] == "ADMIN" || userlvlarr[i] == "SUPERADMIN") {
                return false;
            } else {
                return true
            }
        }
    }

    urlGetOrgId(): string {
        var orgarr = this.useraccesdata.OrgId.split(',');
        var orgnew = [];
        for (var i = 0; i < orgarr.length; i++) {
            orgnew.push('"' + orgarr[i] + '"');
        }
        return orgnew.toString();
    }


    ngOnInit() {
        this.getCoursesByMonth();
        this.getEvaluationByMonth()
        this.getTotalEvaluation()

        this.getMonthPerformanceSite();

        
        // this.getTop10PerformanceInstructor();
        
        // this.getMonthEvaluationSurvey();
       
        // this.getCoreProductsAgainstAllCourses();
    }

    getCoursesByMonth() {
        //get data action
        var data: any;
        var roleid = ["60", "58", "1"];
        var teritorryarr = [];
        var dataset = [];
        var orgid = "";
        if (this.checkrole()) {
            orgid = this.urlGetOrgId();
        }
        this.service.get("api/DashboardCoursesByMonth/where/{'OrgId':'" + orgid + "'}", '')
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                }
                else {
                    data = JSON.parse(result);
                    teritorryarr = [];
                    var partnersATC = [];
                    var partnersAAP = [];
                    var partnersCTC = [];
                    var countriesName = []
                    var backgroundColorATC = []
                    var backgroundColorAAP = []
                    var backgroundColorMTP = []

                    for (var i = 0; i < data.length; i++) {
                        if (data[i].RoleCode == 'ATC') {
                            partnersATC.push(data[i].TotalCourseMonthByPartnerType);
                            partnersAAP.push(0);
                            partnersCTC.push(0)
                        }

                        else if (data[i].RoleCode == 'AAP') {
                            partnersATC.push(0);
                            partnersAAP.push(data[i].TotalCourseMonthByPartnerType);
                            partnersCTC.push(0)

                        } else if (data[i].RoleCode == 'CTC') {
                            partnersATC.push(0);
                            partnersAAP.push(0);
                            partnersCTC.push(data[i].TotalCourseMonthByPartnerType)
                        }

                        countriesName.push(data[i].SiteName);
                        backgroundColorATC.push("#5FBEAA")
                        backgroundColorAAP.push("#1b66a2")
                        backgroundColorMTP.push("#5D9CEC")
                        this.data_MonthCoursesByPartner = {
                            labels: countriesName,
                            datasets: [{
                                label: 'ATC',
                                data: partnersATC,
                                backgroundColor: backgroundColorATC
                            },
                            {
                                label: 'AAP',
                                data: partnersAAP,
                                backgroundColor: backgroundColorAAP
                            },
                            {
                                label: 'MTP',
                                data: partnersCTC,
                                backgroundColor: backgroundColorMTP
                            }]
                        }
                    }
                }
            },
            error => {
                this.messageError = <any>error
                this.service.errorserver();
            });
    }

    getEvaluationByMonth() {
        // start NEW PARTNERS BY MONTH
        var data: any;
        var roleid = ["60", "58", "1"];
        var teritorryarr = [];
        var dataset = [];
    
        var orgid = "";
        if (this.checkrole()) {
          orgid = this.urlGetOrgId();
        }
        this.service.get("api/DashboardMonthEvaluationSurvey/where/{'OrgId':'" + orgid + "'}", '')
          .subscribe(result => {
            if (result == "Not found") {
              this.service.notfound();
    
            }
            else {
                data = JSON.parse(result);
                teritorryarr = [];
                var partnersATC = [];
                var partnersAAP = [];
                var partnersCTC = [];
                var countriesName = []
                var backgroundColorATC = []
                var backgroundColorAAP = []
                var backgroundColorMTP = []

                for (var i = 0; i < data.length; i++) {
                    if (data[i].RoleCode == 'ATC') {
                        partnersATC.push(data[i].TotalMOnthlySurveyTaken);
                        partnersAAP.push(0);
                        partnersCTC.push(0)
                    }

                    else if (data[i].RoleCode == 'AAP') {
                        partnersATC.push(0);
                        partnersAAP.push(data[i].TotalMOnthlySurveyTaken);
                        partnersCTC.push(0)

                    } else if (data[i].RoleCode == 'CTC') {
                        partnersATC.push(0);
                        partnersAAP.push(0);
                        partnersCTC.push(data[i].TotalMOnthlySurveyTaken)
                    }

                    countriesName.push(data[i].SiteName);
                    backgroundColorATC.push("#5FBEAA")
                    backgroundColorAAP.push("#1b66a2")
                    backgroundColorMTP.push("#5D9CEC")
                    this.data_MonthEvaluationSurver = {
                        labels: countriesName,
                        datasets: [{
                            label: 'ATC',
                            data: partnersATC,
                            backgroundColor: backgroundColorATC
                        },
                        {
                            label: 'AAP',
                            data: partnersAAP,
                            backgroundColor: backgroundColorAAP
                        },
                        {
                            label: 'MTP',
                            data: partnersCTC,
                            backgroundColor: backgroundColorMTP
                        }]
                    }
                }
            }
          },
          error => {
            this.messageError = <any>error
            this.service.errorserver();
          });
      }


      getTotalEvaluation() {
        // start NEW PARTNERS BY MONTH
        var data: any;
        var roleid = ["60", "58", "1"];
        var teritorryarr = [];
        var dataset = [];
    
        var orgid = "";
        if (this.checkrole()) {
          orgid = this.urlGetOrgId();
        }
        this.service.get("api/DashboardMonthEvaluationSurvey/total/where/{'OrgId':'" + orgid + "'}", '')
          .subscribe(result => {
            if (result == "Not found") {
              this.service.notfound();
    
            }
            else {
                data = JSON.parse(result);
                teritorryarr = [];
                var partnersATC = [];
                var partnersAAP = [];
                var partnersCTC = [];
                var countriesName = []
                var backgroundColorATC = []
                var backgroundColorAAP = []
                var backgroundColorMTP = []

                for (var i = 0; i < data.length; i++) {
                    if (data[i].RoleCode == 'ATC') {
                        partnersATC.push(data[i].TotalSurveyTaken);
                        partnersAAP.push(0);
                        partnersCTC.push(0)
                    }

                    else if (data[i].RoleCode == 'AAP') {
                        partnersATC.push(0);
                        partnersAAP.push(data[i].TotalSurveyTaken);
                        partnersCTC.push(0)

                    } else if (data[i].RoleCode == 'CTC') {
                        partnersATC.push(0);
                        partnersAAP.push(0);
                        partnersCTC.push(data[i].TotalSurveyTaken)
                    }

                    countriesName.push(data[i].SiteName);
                    backgroundColorATC.push("#5FBEAA")
                    backgroundColorAAP.push("#1b66a2")
                    backgroundColorMTP.push("#5D9CEC")
                    this.data_TotalEvalutionTaken = {
                        labels: countriesName,
                        datasets: [{
                            label: 'ATC',
                            data: partnersATC,
                            backgroundColor: backgroundColorATC
                        },
                        {
                            label: 'AAP',
                            data: partnersAAP,
                            backgroundColor: backgroundColorAAP
                        },
                        {
                            label: 'MTP',
                            data: partnersCTC,
                            backgroundColor: backgroundColorMTP
                        }]
                    }
                }
            }
          },
          error => {
            this.messageError = <any>error
            this.service.errorserver();
          });
      }





































    // baru site performance buat api nya
    getMonthPerformanceSite() {
        var data = '';
        this.service.get(this._serviceUrlPerformanceSite, data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                }
                else {
                    var PerformanceSite = JSON.parse(result);
                    this.DataPerformanceSite = []
                    for (var i in PerformanceSite) {
                        // this.DataPerformanceSite.push([PerformanceSite[i].geo_name, Number(PerformanceSite[i].TotalPerformanceSite)]);
                        this.DataPerformanceSite.push({
                            'ATC': PerformanceSite[i].geo_name,
                            'Total': Number(PerformanceSite[i].TotalPerformanceSite),
                        });
                    }

                    // this.doughChartPerformanceSite =  {
                    //     chartType: 'PieChart',
                    //     dataTable: this.DataPerformanceSite,
                    //     options: {
                    //     height: 360,
                    //     title: 'Evaluation Survey Taken By Month',
                    //     pieHole: 0.4,
                    //     colors: [
                    //         '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
                    //         ]
                    //     },
                    // };

                    Morris.Bar({
                        element: 'id-month-site-performance',
                        data: this.DataPerformanceSite,
                        xkey: 'ATC',
                        ykeys: ['Total'],
                        labels: ['Total'],
                        barColors: ['#5FBEAA', '#5D9CEC', '#cCcCcC'],
                        hideHover: 'auto',
                        gridLineColor: '#eef0f2',
                        resize: true
                    });
                }
            },
            error => {
                this.service.errorserver();
            });
    }

    getTop10PerformanceInstructor() {
        var data = '';
        this.service.get(this._serviceUrlTop10PerformanceInstructor, data)
            .subscribe(result => {

                if (result == "Not found") {
                    this.service.notfound();
                }
                else {
                    var Top10PerformanceIntructor = JSON.parse(result);
                    this.DataTop10PerformanceIntructor.push(['Contact Id', 'Contact Name', 'Course Taken']);
                    for (var i in Top10PerformanceIntructor) {
                        this.DataTop10PerformanceIntructor.push([Top10PerformanceIntructor[i].ContactId, Top10PerformanceIntructor[i].ContactName, Number(Top10PerformanceIntructor[i].TotalCourseDeliveredByIntructor)]);
                    }

                    this.tableChartDataTopInstructor = {
                        chartType: 'Table',
                        dataTable: this.DataTop10PerformanceIntructor,
                        options: {
                            height: 340,
                            title: 'Countries',
                            allowHtml: true
                        }
                    };
                }
            },
            error => {
                this.service.errorserver();
            });
    }

    getCoursesDelivered() {
        var data = '';
        this.service.get(this._serviceUrlCoursesDeliveredBySite, data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                }
                else {
                    var CoursesDelivered = JSON.parse(result);
                    this.dataCoursesDelivered.push(['Task', 'Hours per Day']);
                    for (var i in CoursesDelivered) {
                        this.dataCoursesDelivered.push([CoursesDelivered[i].SiteName, Number(CoursesDelivered[i].TotalCourseDeliveredByPartners)]);
                    }
                    const chart = c3.generate({
                        bindto: '#chart4',
                        scaleShowLabels: false,
                        data: {
                            columns: this.dataCoursesDelivered,
                            xkey: ['Jan 2017', 'Feb 2017', 'Mar 2017', 'Jan 2017', 'Feb 2017', 'Mar 2017', 'Jan 2017', 'Feb 2017', 'Mar 2017', 'Jan 2017', 'Feb 2017', 'Mar 2017'],
                            type: 'bar',
                            colors: {
                                data1: '#03A9F3',
                                data2: '#FEC107',
                            },
                            groups: this.dataKeyValueCoursesDeliveredByPartners
                        }
                    });
                }
            },
            error => {
                this.service.errorserver();
            });
    }

    getMonthEvaluationSurvey() {
        var data = '';
        this.service.get(this._serviceUrlMonthEvaluationSurveyByGeo, data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                }
                else {
                    var TotalEvaluationSurveyByMonth = JSON.parse(result);
                    this.dataTotalEvaluationSurveyByMonthByGeo.push(['Task', 'Hours per Day']);
                    for (var i in TotalEvaluationSurveyByMonth) {
                        this.dataTotalEvaluationSurveyByMonthByGeo.push([TotalEvaluationSurveyByMonth[i].geo_name, Number(TotalEvaluationSurveyByMonth[i].TotalMOnthlySurveyTaken)]);
                    }
                    this.doughChartTotalEvaluationSurveyByMonth = {
                        chartType: 'PieChart',
                        dataTable: this.dataTotalEvaluationSurveyByMonthByGeo,
                        options: {
                            height: 360,
                            title: 'Evaluation Survey Taken By Month',
                            pieHole: 0.4,
                            colors: [
                                '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
                            ]
                        },
                    };
                }
            },
            error => {
                this.service.errorserver();
            });
    }

    getCoreProductsAgainstAllCourses() {
        var data = '';
        this.service.get(this._serviceUrlEvaluationSurveyByCoreProducts, data)
            .subscribe(result => {
                if (result == "Not found") {
                    this.service.notfound();
                }
                else {
                    var CoreProductsAgainstAllCourses = JSON.parse(result);
                    this.dataCoreProductsAgainstAllCourses.push(['Task', 'Hours per Day']);
                    for (var i in CoreProductsAgainstAllCourses) {
                        this.dataCoreProductsAgainstAllCourses.push([CoreProductsAgainstAllCourses[i].productName, Number(CoreProductsAgainstAllCourses[i].TotalSurveyTaken)]);
                    }
                    this.doughChartCoreProductsAgainstAllCourses = {
                        chartType: 'PieChart',
                        dataTable: this.dataCoreProductsAgainstAllCourses,
                        options: {
                            height: 360,
                            title: 'Evaluation Survey Taken By Core Products',
                            pieHole: 0.4,
                            colors: [
                                '#2ecc71', '#01C0C8', '#FB9678', '#5faee3', '#f4d03f', '#e90b0b', '#e9580b', '#1f1f1f', '#8ce90b', '#0be919', '#0be9c4', '#0ba5e9', '#0b20e9', '#850be9', '#da0be9', '#e90b56', '#665959'
                            ]
                        },
                    };
                }
            },
            error => {
                this.service.errorserver();
            });
    }

}
