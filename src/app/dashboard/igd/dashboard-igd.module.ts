import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardIGDComponent } from './dashboard-igd.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { DataFilterActivitiesPipe,DataFilterStatusPipe,ConvertDatePipe } from './dashboard-igd.component';
import { AppFormatDate } from "../../shared/format-date/app.format-date";

export const DashboardIGDRoutes: Routes = [
  {
    path: '',
    component: DashboardIGDComponent,
    data: {
      breadcrumb: 'menu_dashboard.dashboard_student',
      icon: 'icofont-home bg-c-blue',
      status: false,
      title: 'Dashboard'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DashboardIGDRoutes),
    SharedModule
  ],
  declarations: [DashboardIGDComponent, DataFilterActivitiesPipe, DataFilterStatusPipe, ConvertDatePipe],
  providers: [AppService, AppFormatDate]
})
export class DashboardIGDModule { }
