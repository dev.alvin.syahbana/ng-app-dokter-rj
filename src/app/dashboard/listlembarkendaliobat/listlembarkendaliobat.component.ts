import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';

import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { SessionService } from '../../shared/service/session.service';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl, Validators, NgControl } from "@angular/forms";
import * as XLSX from 'xlsx';

// Convert Date
@Pipe({ name: 'convertDate' })
export class ConvertDatePipe {
    constructor(private datepipe: DatePipe) { }
    transform(date: string): any {
        return this.datepipe.transform(new Date(date.substr(0, 10)), 'dd-MM-yyyy');
    }
}

@Pipe({ name: 'dataFilterActivities' })
export class DataFilterActivitiesPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.idRj0701.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.noRekMedis.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.namaPasien.toLowerCase().indexOf(query.toLowerCase()) > -1) 
            );
        }
        return array;
    }
}

@Pipe({ name: 'dataFilterStatus' })
export class DataFilterStatusPipe {
    transform(array: any[], query: string): any {
        if (query == "Active") {
            return _.filter(array, row =>
                (row.SurveyTaken.indexOf("0") > -1) &&
                (row.activesurveylink.indexOf("Active") > -1));
        }
        else if (query == "Expired" || query == "Pending") {
            return _.filter(array, row =>
                (row.activesurveylink.indexOf(query) > -1));
        } else if (query == "Completed") {
            return _.filter(array, row =>
                (row.SurveyTaken.indexOf("1") > -1));
        }
        return array;
    }
}

@Pipe({ name: 'dataFilterMon' })
export class DataFilterMonPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.idMonitoringAmsp.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.kodeAlatSp.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.namaAlatSp.toLowerCase().indexOf(query.toLowerCase()) > -1)
            );
        }
        return array;
    }
}

const now = new Date();

@Component({
    selector: 'app-listlembarkendaliobat',
    templateUrl: './listlembarkendaliobat.component.html',
    styleUrls: [
        './listlembarkendaliobat.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})


export class ListLKOComponent implements OnInit {

    private _serviceUrl = 'api/IgdRj07PerawatPengkajianAwal';
    private _serviceUrl2 = 'api/LembarKendaliObats';
    public studentcourse: any;
    public data: any;
    public data2: any;
    public datadetail: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "idRj0701";
    public sortOrder: string = "desc";
    public useraccesdata: any;
    public filterStatus: string = "All";
    public title = 'test';
    // modelPopup0: NgbDateStruct;
    modelPopup1: NgbDateStruct;
    modelPopup2: NgbDateStruct;
    formFilter: FormGroup;
    public pilihanSort;

   

    // formMonAKSP: FormGroup;

    // dropdownListAKSP = [];
    // selectedItemsAKSP = [];
    // dropdownSettingsAKSP = {};

    public loading = false;

    constructor(private service: AppService, private formatdate: AppFormatDate, private session: SessionService, private router: Router) {

        // Form Filter By Tanggal
        let dariTanggal = new FormControl('', Validators.required);
        let sampaiTanggal = new FormControl('', Validators.required);
        // this.modelPopup1 = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };

        this.formFilter = new FormGroup({
            dariTanggal: dariTanggal,
            sampaiTanggal: sampaiTanggal
        });
    }

    ngOnInit() {
        this.getDataLKO();
        this.getDataPasien();
    }

    ExportExcel() {
        // var date = this.service.formatDate();
        var fileName = "LembarKendaliObat.xls";
        var ws = XLSX.utils.json_to_sheet(this.data);
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, fileName);
        XLSX.writeFile(wb, fileName);
    }

    getDataLKO() {
        this.loading = true;
        var data2 = '';
        this.service.httpClientGet(this._serviceUrl, data2).subscribe(result => {
            console.log(result);
            if (result == null) {
                swal(
                    'Informasi!',
                    'Data tidak ditemukan.',
                    'error'
                );
                this.data2 = '';
                this.loading = false;
            }
            else {
                this.data2 = result;
                this.loading = false;
            }
        },
            error => {
                this.service.errorserver();
                this.data2 = '';
                this.loading = false;
            });
    }

    getDataPasien() {
        this.loading = true;
        var data = '';
        this.service.httpClientGet(this._serviceUrl, data).subscribe(result => {
            console.log(result);
            if (result == null) {
                swal(
                    'Informasi!',
                    'Data tidak ditemukan.',
                    'error'
                );
                this.data = '';
                this.loading = false;
            }
            else {
                this.data = result;
                this.loading = false;
            }
        },
            error => {
                this.service.errorserver();
                this.data = '';
                this.loading = false;
            });
            
    }

    dariTgl = "";
    onSelectDariTanggal(date: NgbDateStruct) {
        if (date != null) {
            this.formFilter.value.dariTanggal = date;
            this.dariTgl = this.formatdate.dateCalendarToYMD(this.formFilter.value.dariTanggal);
            // console.log(this.dariTgl);
        }
    }

    sampaiTgl = "";
    onSelectSampaiTanggal(date: NgbDateStruct) {
        if (date != null) {
            this.formFilter.value.sampaiTanggal = date;
            this.sampaiTgl = this.formatdate.dateCalendarToYMD(this.formFilter.value.sampaiTanggal);
            // console.log(this.sampaiTgl);
        }
    }

    // on Submit untuk Filter by Tanggal
    onSubmit() {

        this.formFilter.controls['dariTanggal'].markAsTouched();
        this.formFilter.controls['sampaiTanggal'].markAsTouched();

        if (this.formFilter.valid) {

            this.loading = true;
            var data: any;
            this.service.httpClientGet("api/LembarKendaliObats/FilterByTanggal/" + this.dariTgl + "/" + this.sampaiTgl + "", data)
                .subscribe(result => {
                    // console.log(result);
                    if (result == null) {
                        swal(
                            'Informasi!',
                            'Data tidak ditemukan.',
                            'error'
                        );
                        this.data = '';
                        this.loading = false;
                    }
                    else {
                        this.data = result;
                        this.loading = false;
                    }
                });
            error => {
                this.service.errorserver();
                this.data = '';
                this.loading = false;
            }

        }
    }

}
