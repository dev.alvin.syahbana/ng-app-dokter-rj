import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { AppService } from "../../shared/service/app.service";
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { ListLKOComponent } from './listlembarkendaliobat.component';
import { DataFilterActivitiesPipe, DataFilterStatusPipe, ConvertDatePipe, DataFilterMonPipe } from './listlembarkendaliobat.component';

export const ListLKORoutes: Routes = [
  {
    path: '',
    component: ListLKOComponent,
    data: {
      breadcrumb: 'eim.profile.my_profile.my_profile',
      icon: 'icofont-home bg-c-blue',
      status: false,
      title: 'Dashboard'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ListLKORoutes),
    SharedModule,
    LoadingModule,
    AngularMultiSelectModule
  ],
  declarations: [ListLKOComponent, DataFilterMonPipe, ConvertDatePipe, DataFilterActivitiesPipe, DataFilterStatusPipe],
  providers: [AppService, AppFormatDate]
  
})
export class ListLKOModule { }
