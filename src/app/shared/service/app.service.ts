import { Injectable } from '@angular/core';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as Rollbar from 'rollbar';

@Injectable()

export class AppService {

    constructor(private _http: Http, private http: HttpClient, private router: Router, private rollbar: Rollbar) { }

    //api get
    get(url, data): Observable<string> {
        return this._http.get(
            url,
            { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
        )
            .map((response: Response) => {
                return response.text();
            })
            .catch((err) => {
                return Observable.throw(err);
            });
    }

    //post data
    post(url, data): Observable<string> {
        return this._http.post(
            url,
            data,
            { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
        )
            .map((response: Response) => {
                return response.text();
            })
            .catch((err) => {
                this.rollbar.log("error", err);
                return Observable.throw(err);
            });;
    }

    //update data
    put(url, data): Observable<string> {
        return this._http.put(
            url,
            data,
            { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
        )
            .map((response: Response) => {
                return response.text();
            })
            .catch((err) => {
                this.rollbar.log("error", err);
                return Observable.throw(err);
            });;
    }

    //delete data
    delete(url, data): Observable<string> {
        return this._http.delete(
            url,
            { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': '' }) }
        )
            .map((response: Response) => {
                return response.text();
            })
            .catch((err) => {
                this.rollbar.log("error", err);
                return Observable.throw(err);
            });;
    }

    //error message action
    errorserver() {
        swal(
            'Infomation!',
            'Can\'t connect to server.',
            'error'
        );
    }

    //not found
    notfound() {
        swal(
            'Infomation!',
            'Data not found.',
            'error'
        );
    }

    //success notification
    openSuccessSwal(message) {
        swal({
            title: 'Information!',
            text: message,
            type: 'success'
        }).catch(swal.noop);
    }

    successSwal() {
        swal({
            type: 'success',
            title: 'Data Pengkajian Pasien Tersimpan',
            showConfirmButton: true
        })
    }


    //http client get
    httpClientGet(url, data) {
        return this.http.get(url);
    }

    //http client post
    httpClientPost(url, data) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        headers = headers.set('authorization', 'Bearer ');
        this.http.post(url, data, { headers }).subscribe(result => {
            if (result['code'] != '1') {
                // swal(
                //     'Information!',
                //     "Insert Data Failed",
                //     'error'
                // );
                this.rollbar.warning("Insert data failed at URL: " + url + "; Data: " + data);
            }
        },
            error => {
                this.rollbar.log("error", "Something went wrong " + error);
                this.errorserver();
            });
    }

    //http client update
    httpCLientPut(url, id, data) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        headers = headers.set('authorization', 'Bearer ');
        this.http.put(url + '/' + id, data, { headers }).subscribe(result => {
            if (result['code'] != '1') {
                swal(
                    'Information!',
                    "Update Data Failed",
                    'error'
                );
                this.rollbar.warning("Update data failed at URL: " + url + "; Data: " + data + "; ID: " + id);
            }
        },
            error => {
                this.rollbar.log("error", "Something went wrong " + error);
                this.errorserver();
            });
    }

    //http client delete with confirm
    httpClientDelete(url, data, id, index) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(result => {
            if (result == true) {
                this.http.delete(url + '/' + id)
                    .subscribe(result => {
                        var resource = result;
                        if (resource['code'] == '1') {
                            if (index !== -1) {
                                data.splice(index, 1);
                            }
                        }
                        else {
                            swal(
                                'Information!',
                                "Delete Data Failed",
                                'error'
                            );
                            this.rollbar.warning("Delete data failed at URL: " + url + "; Data: " + data + "; ID: " + id);
                        }
                    },
                        error => {
                            this.rollbar.log("error", "Something went wrong " + error);
                            this.errorserver();
                        });
            }
        }).catch(swal.noop);
    }

    //http client post
    httpClientPostArray(url, data) {
        var success = false;
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        headers = headers.set('authorization', 'Bearer ');
        for (var i = 0; i < data.length; i++) {
            this.http.post(url, data[i], { headers }).subscribe(result => {
                if (result['code'] != '1') {
                    swal(
                        'Information!',
                        "Insert Data Failed",
                        'error'
                    );
                    this.rollbar.warning("Insert data failed at URL: " + url + "; Data: " + data[i]);
                }
            },
                error => {
                    this.rollbar.log("error", "Something went wrong " + error);
                    this.errorserver();
                });
        }
    }

    //http client post
    httpClientPostModal(url, data, refreshdata, dataarray) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        headers = headers.set('authorization', 'Bearer ');
        this.http.post(url, data, { headers }).subscribe(result => {
            if (result['code'] == '1') {
                refreshdata.push(dataarray);
            }
            else {
                swal(
                    'Information!',
                    "Insert Data Failed",
                    'error'
                );
                this.rollbar.warning("Insert data failed at URL: " + url + "; Data: " + data);
            }
        },
            error => {
                this.rollbar.log("error", "Something went wrong " + error);
                this.errorserver();
            });
    }

    //http client update
    httpCLientPutModal(url, id, data, index, refreshdata, dataarray) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        headers = headers.set('authorization', 'Bearer ');
        this.http.put(url + '/' + id, data, { headers }).subscribe(result => {
            if (result['code'] == '1') {
                if (index !== -1) {
                    refreshdata[index] = dataarray;
                }
            }
            else {
                swal(
                    'Information!',
                    "Update Data Failed",
                    'error'
                );
                this.rollbar.warning("Update data failed at URL: " + url + "; Data: " + data + "; ID: " + id);
            }
        },
            error => {
                this.rollbar.log("error", "Something went wrong " + error);
                this.errorserver();
            });
    }

    getNativeWindow() {
        return window;
    }


    //http client update
    httpCLientPutPassword(url, id, data, routingtrue, routingfalse) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        headers = headers.set('authorization', 'Bearer ');
        // console.log(url + '/' + id);
        var routingtruearr = routingtrue.split(',');
        this.http.put(url + '/' + id, data, { headers }).subscribe(result => {
            if (result['code'] == '1') {
                if (routingtruearr.length > 1) {
                    this.router.navigate([routingtruearr[0], routingtruearr[1], routingtruearr[2]]);
                } else {
                    var routelogoutrole = routingtrue.split('-');
                    this.router.navigate([routelogoutrole[0]], { queryParams: { id: id, role: routelogoutrole[1] } });
                }
            }
            else {
                swal({
                    title: 'Information!',
                    text: result['message'],
                    type: 'error'
                }).catch(swal.noop);
                // this.router.navigate([routingfalse], { queryParams: { id: id } });
                this.rollbar.warning(result['message'] + " at URL: " + url + "; Data: " + data + "; ID: " + id);
            }
        },
            error => {
                this.rollbar.log("error", "Something went wrong " + error);
                this.errorserver();
            });
    }

    DateConversion(date) {
        var dateString = date.substr(0, 10).replace(/\b0/g, '');
        var dateConversion = dateString.split("/");
        var newConversion = dateConversion[0] + "-" + dateConversion[1] + "-" + dateConversion[2];
        return newConversion;
    }

    httpCLientPutNew(url, id, data, getdata) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        headers = headers.set('authorization', 'Bearer ');
        this.http.put(url + '/' + id, data, { headers }).subscribe(result => {
            if (result['code'] == '1') {
                this.http.get(getdata);
            }
            else {
                swal(
                    'Information!',
                    "Update Data Failed",
                    'error'
                );
                this.rollbar.warning("Update data failed at URL: " + url + "; Data: " + data + "; ID: " + id);
            }
        },
            error => {
                this.rollbar.log("error", "Something went wrong " + error);
                this.errorserver();
            });
    }

    formatDate() {
        var d = new Date(),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }
}
