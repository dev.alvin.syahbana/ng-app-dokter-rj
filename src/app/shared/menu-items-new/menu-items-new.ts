import { Injectable } from '@angular/core';
import { SessionService } from '../service/session.service';
import { Router } from '@angular/router';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

let MENUITEMS_2 = [];

const MENUITEMS = [
  {
    label: '',
    main: [  
      // New
      {
        state: 'menu-utama',
        name: 'Menu Utama',
        type: 'link',
        icon: 'ti-file',
        show: true,
      },
      // New
    ]
  }
];

@Injectable()
export class MenuItemsNew {
  constructor(public session: SessionService) { }

  viewRoles() {
    // if (this.session.isLoggedIn()) {

    //   var arr = MENUITEMS[0].main;
    //   var access = JSON.parse(this.session.getData()).Access;
    //   var link = "";

    //   //level 1
    //   for (var i = 0; i < arr.length; i++) {
    //     for (const key of Object.keys(arr[i])) {
    //       if (key == "type") {
    //         if (arr[i]["type"] == "sub") {
    //           link = arr[i].state;
    //           for (const key of Object.keys(access)) {
    //             if (link == key) {
    //               var accessPage = access[key];
    //               if (accessPage == "1") {
    //                 arr[i].show = true;
    //               }
    //               else {
    //                 arr[i].show = false;
    //               }
    //             }
    //           }

    //           //level 2
    //           for (var j = 0; j < arr[i]["children"].length; j++) {
    //             for (const key of Object.keys(arr[i]["children"][j])) {
    //               if (key == "type") {
    //                 if (arr[i]["children"][j]["type"] == "sub") {
    //                   link = arr[i].state + "/" + arr[i]["children"][j].state;
    //                   for (const key of Object.keys(access)) {
    //                     if (link == key) {
    //                       var accessPage = access[key];
    //                       if (accessPage == "1") {
    //                         arr[i]["children"][j].show = true;
    //                       }
    //                       else {
    //                         arr[i]["children"][j].show = false;
    //                       }
    //                     }

    //                     //level 3
    //                     if (arr[i]["children"][j]["children"] != undefined) {
    //                       for (var k = 0; k < arr[i]["children"][j]["children"].length; k++) {
    //                         for (const key of Object.keys(arr[i]["children"][j]["children"][k])) {
    //                           if (key == "type") {
    //                             if (arr[i]["children"][j]["children"][k]["type"] == "sub") {
    //                               link = arr[i].state + "/" + arr[i]["children"][j].state + "/" + arr[i]["children"][j]["children"][k].state;
    //                               for (const key of Object.keys(access)) {
    //                                 if (link == key) {
    //                                   var accessPage = access[key];
    //                                   if (accessPage == "1") {
    //                                     arr[i]["children"][j]["children"][k].show = true;
    //                                   } else {
    //                                     arr[i]["children"][j]["children"][k].show = false;
    //                                   }
    //                                 }
    //                               }
    //                             }
    //                             else {
    //                               link = arr[i].state + "/" + arr[i]["children"][j].state + "/" + arr[i]["children"][j]["children"][k].state;
    //                               for (const key of Object.keys(access)) {
    //                                 if (link == key) {
    //                                   var accessPage = access[key];
    //                                   if (accessPage == "1") {
    //                                     arr[i]["children"][j]["children"][k].show = true;
    //                                   } else {
    //                                     arr[i]["children"][j]["children"][k].show = false;
    //                                   }
    //                                 }
    //                               }
    //                             }
    //                           }
    //                         }
    //                       }
    //                     }
    //                   }
    //                 }
    //                 else {
    //                   link = arr[i].state + "/" + arr[i]["children"][j].state;
    //                   for (const key of Object.keys(access)) {
    //                     if (link == key) {
    //                       var accessPage = access[key];
    //                       if (accessPage == "1") {
    //                         arr[i]["children"][j].show = true;
    //                       } else {
    //                         arr[i]["children"][j].show = false;
    //                       }
    //                     }
    //                   }
    //                 }
    //               }
    //             }
    //           }
    //         }
    //         else {
    //           link = arr[i].state;
    //           for (const key of Object.keys(access)) {
    //             if (link == key) {
    //               var accessPage = access[key];
    //               if (accessPage == "1") {
    //                 arr[i].show = true;
    //               } else {
    //                 arr[i].show = false;
    //               }
    //             }
    //           }
    //         }
    //       }
    //     }
    //   }

    //   var arr2 = new Array();
    //   var obj = new Object();

    //   obj['label'] = "";
    //   obj['main'] = arr;

    //   arr2[0] = obj;

    //   MENUITEMS_2 = arr2;
    // }
    // else {
    //   MENUITEMS_2 = MENUITEMS;
    // }

  }

  getAll(): Menu[] {
    return MENUITEMS;
  }
}
