import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router, ActivatedRoute } from '@angular/router';
import { SessionService } from '../../shared/service/session.service';
import { AppService } from "../../shared/service/app.service";
import { TranslateService } from '@ngx-translate/core';
import { AppFormatDate } from "../../shared/format-date/app.format-date";
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from '@angular/common';



// @Pipe({ name: 'dataFilterLanguage' })
// export class DataFilterLanguagePipe {
//     transform(array: any[], query: string): any {
//         if (query) {
//             return _.filter(array, row =>
//                 // (row.countries_code.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
//                 (row.KeyValue.toLowerCase().indexOf(query.toLowerCase()) > -1));
//         }
//         return array;
//     }
// }

@Component({
    selector: 'app-add-edit-email-body',
    templateUrl: './add-edit-email-body.component.html',
    styleUrls: [
        './add-edit-email-body.component.css',
        '../../../../node_modules/c3/c3.min.css'
    ],
    encapsulation: ViewEncapsulation.None
})

export class AddEditEmailComponent implements OnInit {

    private _serviceUrl = "api/EmailBody";
    public data: any;
    addEmailBody: FormGroup;
    public content;
    public useraccesdata: any;
    private body_id;
    language;
    public loading = false;

    constructor(private router: Router, private _http: Http, private service: AppService,
        private session: SessionService, private formatdate: AppFormatDate, private datePipe: DatePipe, private route: ActivatedRoute) {
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);

        let body_type = new FormControl('', Validators.required);
        let body_email = new FormControl('', Validators.required);
        let subject = new FormControl('', [Validators.required, Validators.maxLength(100)]);
        let Key = new FormControl('', Validators.required);

        this.addEmailBody = new FormGroup({
            body_type: body_type,
            body_email: body_email,
            subject: subject,
            Key: Key
        });
    }

    // getLanguage() {
    //     var language = '';
    //     this.service.get("api/Dictionaries/where/{'Parent':'Languages','Status':'A'}", language)
    //         .subscribe(result => {
    //             this.language = JSON.parse(result);
    //         },
    //             error => {
    //                 this.service.errorserver();
    //             });
    // }

    body_typearr=[];
    ngOnInit() {

        this.body_typearr =
            [
                "Invoice Notif",
                "Student Terminate",
                "Survey Reminder",
                "Invoice 1st reminder (10 days before 31st January)",
                "Invoice 2nd reminder (Actual date – 31st January)",
                "Student termination reminder (Email sent once student choose to terminate account)",
                "Survey 1st reminder (Send by the course end date)",
                "Survey 2nd reminder (Send by 14th day after survey ends)",
                "Survey 3rd reminder (Send by 27th day after survey ends)",
                "Tag student to course",
            ];

        // this.getLanguage();

        var sub: any;
        sub = this.route.queryParams.subscribe(params => {
            this.body_id = params['body_id'] || '';
        });

        if (this.body_id != '') {
            var data: any;
            this.service.get(this._serviceUrl + "/" + this.body_id, data)
                .subscribe(res => {
                    data = res;
                    data = data.replace(/\r/g, "\\r");
                    data = data.replace(/\n/g, "\\n");
                    data = JSON.parse(data);
                    if (data != null) {
                        this.data = data;
                        this.buildForm();
                    }
                }, error => {
                    this.service.errorserver();
                });
        }
    }

    buildForm(): void {
        let body_type = new FormControl(this.data.body_type, Validators.required);
        this.onChangeType(this.data.body_type, this.data.Key);
        let body_email = new FormControl(this.data.body_email, Validators.required);
        let subject = new FormControl(this.data.subject, Validators.required);
        let Key = new FormControl(this.data.Key, Validators.required);

        this.addEmailBody = new FormGroup({
            body_type: body_type,
            body_email: body_email,
            subject: subject,
            Key: Key
        });
    }

    onSubmit() {
        this.addEmailBody.controls['body_type'].markAsTouched();
        this.addEmailBody.controls['body_email'].markAsTouched();
        this.addEmailBody.controls['subject'].markAsTouched();
        this.addEmailBody.controls['Key'].markAsTouched();

        if (this.addEmailBody.valid) {
            this.loading = true;
            let subjectTemp = this.addEmailBody.value.subject;
            let body = this.addEmailBody.value.body_email;

            //perlu 4x backslash karena kalau 3x ditolak sama mysql pas nyimpen, berhubung di serialize dulu
            subjectTemp = subjectTemp.replace(/\"/g, '\\\\"');
            body = body.replace(/\"/g, '\\\\"');
            if (this.body_id != '') {
                var dataUpdate = {
                    'body_type': this.addEmailBody.value.body_type,
                    'body_email': body,
                    'subject': subjectTemp,
                    'Key': this.addEmailBody.value.Key,
                    'LastAdminBy': this.useraccesdata.ContactName,
                    'DateLastAdmin': this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"),
                    'muid': this.useraccesdata.ContactName,
                    'cuid': this.useraccesdata.ContactName,
                    'UserId': this.useraccesdata.UserId
                };
                this.service.httpCLientPut(this._serviceUrl, this.body_id, dataUpdate);
            } else {
                var data = {
                    'body_type': this.addEmailBody.value.body_type,
                    'body_email': body,
                    'subject': subjectTemp,
                    'Key': this.addEmailBody.value.Key,
                    'AddedBy': this.useraccesdata.ContactName,
                    'DateAdded': this.datePipe.transform(new Date().toLocaleString(), "yyyy-MM-dd H:m:s"),
                    'muid': this.useraccesdata.ContactName,
                    'cuid': this.useraccesdata.ContactName,
                    'UserId': this.useraccesdata.UserId
                };
                this.service.httpClientPost(this._serviceUrl, JSON.stringify(data));
            }
            setTimeout(() => {
                this.router.navigate(['/admin/email/list-email-body']);
                this.loading = false;
            }, 1000);
        }
    }

    // resetFormAdd() {
    //     this.addEmailBody.reset();
    // }

    onChangeType(value, key){
        if(key != ""){
            this.service.get(this._serviceUrl+"/CheckLanguage/"+value+"/"+key, '')
                .subscribe(res => {
                    this.language = JSON.parse(res);
                })
        }else{
            this.service.get(this._serviceUrl+"/CheckLanguage/"+value+"/null", '')
                .subscribe(res => {
                    this.language = JSON.parse(res);
                })
        }
    }

}
