import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VariablesDetailEPDBComponent } from './variables-detail-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";

export const VariablesDetailEPDBRoutes: Routes = [
  {
    path: '',
    component: VariablesDetailEPDBComponent,
    data: {
      breadcrumb: 'detail Variables',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(VariablesDetailEPDBRoutes),
    SharedModule
  ],
  declarations: [VariablesDetailEPDBComponent],
  providers:[AppService]
})
export class VariablesDetailEPDBModule { }
