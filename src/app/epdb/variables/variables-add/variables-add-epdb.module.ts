import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VariablesAddEPDBComponent } from './variables-add-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";
import {AppFormatDate} from "../../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const VariablesAddEPDBRoutes: Routes = [
  {
    path: '',
    component: VariablesAddEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.variables.add_new_variable',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(VariablesAddEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [VariablesAddEPDBComponent],
  providers:[AppService, AppFormatDate]
})
export class VariablesAddEPDBModule { }
