import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VariablesUpdateEPDBComponent } from './variables-update-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";

import {AppService} from "../../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';

export const VariablesUpdateEPDBRoutes: Routes = [
  {
    path: '',
    component: VariablesUpdateEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.variables.edit_variable',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(VariablesUpdateEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [VariablesUpdateEPDBComponent],
  providers:[AppService]
})
export class VariablesUpdateEPDBModule { }
