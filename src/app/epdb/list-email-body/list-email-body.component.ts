import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';
import { AppService } from "../../shared/service/app.service";
import { TranslateService } from '@ngx-translate/core';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";
import { SessionService } from '../../shared/service/session.service';

@Pipe({ name: 'dataFilter' })
export class DataFilterPipe {
    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row =>
                (row.body_id.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.body_type.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.subject.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
                (row.KeyValue.toLowerCase().indexOf(query.toLowerCase()) > -1));
        }
        return array;
    }
}

@Component({
    selector: 'app-list-email-body',
    templateUrl: './list-email-body.component.html',
    styleUrls: [
        './list-email-body.component.css',
        '../../../../node_modules/c3/c3.min.css',
    ],
    encapsulation: ViewEncapsulation.None
})

export class ListEmailBodyComponent implements OnInit {

    private _serviceUrl = "api/EmailBody";
    public data: any;
    public rowsOnPage: number = 10;
    public filterQuery: string = "";
    public sortBy: string = "";
    public sortOrder: string = "asc";
    public useraccesdata: any;

    constructor(public session: SessionService, private router: Router, private _http: Http, private service: AppService) {
        
        //get user level
        let useracces = this.session.getData();
        this.useraccesdata = JSON.parse(useracces);
    }

    getEmailBody() {
        var data: any;
        this.service.get(this._serviceUrl, data)
            .subscribe(result => {
                if (result == 'Not found') {
                    swal(
                        'Infomation!',
                        'Data Not Found.',
                        'error'
                    );
                    this.data = '';
                }
                else {
                    data = result;
                    data = data.replace(/\r/g, "\\r");
                    data = data.replace(/\n/g, "\\n");
                    // console.log(JSON.parse(data));
                    data = JSON.parse(data);
                    data.length > 0 ? this.data = data : this.data = '';
                }
            },
                error => {
                    this.service.errorserver();
                    this.data = '';
                });
    }

    accesAddBtn: Boolean = true;
    accesUpdateBtn: Boolean = true;
    accesDeleteBtn: Boolean = true;
    ngOnInit() {
        this.getEmailBody();

        this.accesAddBtn = this.session.checkAccessButton("admin/email/add-edit-email");
        this.accesUpdateBtn = this.session.checkAccessButton("admin/email/add-edit-email");
        this.accesDeleteBtn = this.session.checkAccessButton("admin/email/delete-email");
    }

    openConfirmsSwal(id, index) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        })
            .then(result => {
                if (result == true) {
                    var cuid = this.useraccesdata.ContactName;
                    var UserId = this.useraccesdata.UserId;
                    var data = '';
                    this.service.delete(this._serviceUrl + "/" + id + '/' + cuid + '/' + UserId, data)
                        .subscribe(value => {
                            var resource = JSON.parse(value);
                            this.getEmailBody();
                        },
                            error => {
                                this.service.errorserver();
                            });
                }

            }).catch(swal.noop);
    }

    editEmailBody(id) {
        this.router.navigate(['/admin/email/add-edit-email'], { queryParams: { body_id: id } });
    }
}
