import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GlossaryEPDBComponent } from './glossary-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const GlossaryEPDBRoutes: Routes = [
    {
        path: '',
        component: GlossaryEPDBComponent,
        data: {
            breadcrumb: 'Help / Glossary',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }
];

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(GlossaryEPDBRoutes),
      SharedModule
    ],
    declarations: [GlossaryEPDBComponent]
  })
  export class GlossaryEPDBModule { }