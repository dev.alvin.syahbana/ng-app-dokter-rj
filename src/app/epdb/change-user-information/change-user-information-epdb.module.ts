import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangeUserInformationEPDBComponent } from './change-user-information-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const ChangeUserInformationEPDBRoutes: Routes = [
  {
    path: '',
    component: ChangeUserInformationEPDBComponent,
    data: {
      breadcrumb: 'Change User Information',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ChangeUserInformationEPDBRoutes),
    SharedModule
  ],
  declarations: [ChangeUserInformationEPDBComponent]
})
export class ChangeUserInformationEPDBModule { }
