import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
import { Http, Headers, Response } from "@angular/http";
declare const $: any;
declare var Morris: any;
import '../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import swal from 'sweetalert2';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import { Router } from '@angular/router';

import { AppService } from "../../shared/service/app.service";
import { TranslateService } from '@ngx-translate/core';

import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'dataFilterActivities' })
export class DataFilterActivitiesPipe {
  transform(array: any[], query: string): any {
    if (query) {
      return _.filter(array, row => (row.activity_name.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.activity_id.toLowerCase().indexOf(query.toLowerCase()) > -1) ||
        (row.unique.toLowerCase().indexOf(query.toLowerCase()) > -1));
    }
    return array;
  }
}

@Component({
  selector: 'app-setup-language-epdb',
  templateUrl: './setup-language-epdb.component.html',
  styleUrls: [
    './setup-language-epdb.component.css',
    '../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class SetupLanguageEPDBComponent implements OnInit {

  private _serviceUrl = 'api/Language';
  public rowsOnPage: number = 10;
  public filterQuery: string = "";
  public sortBy: string = "type";
  public sortOrder: string = "asc";
  public data: any;
  public datamodule: any;
  languageselect: string = '';
  moduleselect: boolean = false;
  submoduleselect: boolean = false;
  submoduleselectcountry: boolean = false;
  submoduleselectorganization: boolean = false;
  submoduleselectsite: boolean = false;
  submoduleselectcontact: boolean = false;
  submenuselect: boolean = false;
  menuselect: boolean = false;
  submenuselectcontent: boolean = false;
  modulearr = [];
  submodulearr = [];
  menuarr = [];
  submenuarr = [];
  contentarr = [];
  public changevalue: string = '';
  public itemvalue: string = '';

  constructor(private router: Router, private _http: Http, private service: AppService, private translate: TranslateService) {

    translate.addLangs(["English", "Bahasa", "France"]);
    translate.setDefaultLang('English');

    let browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/English|Bahasa/) ? browserLang : 'English');

  }

  ngOnInit() { }

  // module
  changetable(value) {
    this.languageselect = value;

    this.modulearr =
      [
        {
          "value": "eim",
          "name": "EIM"
        },
        {
          "value": "epdb",
          "name": "EPDB"
        },
        {
          "value": "eva",
          "name": "EVA"
        },
        {
          "value": "student",
          "name": "Student"
        },
        {
          "value": "general",
          "name": "General"
        }
      ]

    var data = '';
    this.service.httpClientGet(this._serviceUrl + '/' + value, data)
      .subscribe(result => {
        this.data = result;
      },
      error => {
        this.service.errorserver();
        this.data = '';
      });
  }

  // Sub module 1
  pickmodule(value) {
    this.moduleselect = true;
    if (value == "epdb") {
      this.submodulearr =
        [
          {
            "value": "epdb-dashboard",
            "name": "Dashboard"
          },
          {
            "value": "epdb-admin",
            "name": "Admin"
          },
          {
            "value": "epdb-user",
            "name": "User"
          },
          {
            "value": "epdb-manager",
            "name": "Manager"
          },
          {
            "value": "epdb-report",
            "name": "Report"
          }
        ]
    }
    else if (value == "eva") {
      this.submodulearr =
        [
          {
            "value": "eva-dashboard",
            "name": "Dashboard"
          },
          {
            "value": "eva-manage-evaluation",
            "name": "Manage Evaluation"
          },
          {
            "value": "eva-manage-course",
            "name": "Manage Course"
          },
          {
            "value": "eva-manage-student-info",
            "name": "Manage Student Info"
          },
          {
            "value": "eva-manage-certificate",
            "name": "Manage Certificate"
          }
        ]
    }
    else if (value == "eim") {
      this.submodulearr =
        [
          {
            "value": "eim-dashboard",
            "name": "Dashboard"
          },
          {
            "value": "eim-profile",
            "name": "My Profile"
          }
        ]
    }
    else if (value == "general") {
      this.moduleselect = false;
      this.menuselect = true;
      this.contentarr =
      [
        {
          "attribut": '"btn_submit":',
          "value": this.data.general.btn_submit,
          "name": "Label Button Submit"
        },
        {
          "attribut": '"btn_save":',
          "value": this.data.general.btn_save,
          "name": "Label Button Save"
        },
        {
          "attribut": '"btn_save_change":',
          "value": this.data.general.btn_save_change,
          "name": "Label Button Save Change"
        },
        {
          "attribut": '"btn_reset":',
          "value": this.data.general.btn_reset,
          "name": "Label Button Reset"
        },
        {
          "attribut": '"btn_cancel":',
          "value": this.data.general.btn_cancel,
          "name": "Label Button Cancel"
        },
        {
          "attribut": '"btn_run_query":',
          "value": this.data.general.btn_run_query,
          "name": "Label Button Run Query"
        },
        {
          "attribut": '"btn_stop_query":',
          "value": this.data.general.btn_stop_query,
          "name": "Label Button Stop Query"
        },
        {
          "attribut": '"btn_search":',
          "value": this.data.general.btn_search,
          "name": "Label Button Search"
        },
        {
          "attribut": '"btn_preview":',
          "value": this.data.general.btn_preview,
          "name": "Label Button Preview"
        },
        {
          "attribut": '"btn_close":',
          "value": this.data.general.btn_close,
          "name": "Label Button Close"
        },
        {
          "attribut": '"btn_help":',
          "value": this.data.general.btn_help,
          "name": "Label Button Help"
        },
        {
          "attribut": '"btn_add":',
          "value": this.data.general.btn_add,
          "name": "Label Button Add"
        },
        {
          "attribut": '"btn_update":',
          "value": this.data.general.btn_update,
          "name": "Label Button Update"
        },
        {
          "attribut": '"show":',
          "value": this.data.general.show,
          "name": "Label Show"
        },
        {
          "attribut": '"entries":',
          "value": this.data.general.entries,
          "name": "Label Entries"
        },
        {
          "attribut": '"search":',
          "value": this.data.general.search,
          "name": "Label Search"
        },
        {
          "attribut": '"action":',
          "value": this.data.general.action,
          "name": "Label Action"
        },
        {
          "attribut": '"status":',
          "value": this.data.general.status,
          "name": "Label Status"
        },
        {
          "attribut": '"description":',
          "value": this.data.general.description,
          "name": "Label Description"
        },
        {
          "attribut": '"select":',
          "value": this.data.general.select,
          "name": "Label Select"
        },
        {
          "attribut": '"tool_tip_view":',
          "value": this.data.general.tool_tip_view,
          "name": "Tooltip Viwe"
        },
        {
          "attribut": '"tool_tip_edit":',
          "value": this.data.general.tool_tip_edit,
          "name": "Tooltip Edit"
        },
        {
          "attribut": '"tool_tip_delete":',
          "value": this.data.general.tool_tip_delete,
          "name": "Tooltip Delete"
        },
        {
          "attribut": '"date":',
          "value": this.data.general.date,
          "name": "Label Date"
        },
        {
          "attribut": '"start_date":',
          "value": this.data.general.start_date,
          "name": "Label Start Date"
        },
        {
          "attribut": '"end_date":',
          "value": this.data.general.end_date,
          "name": "Label End Date"
        },
        {
          "attribut": '"date_added":',
          "value": this.data.general.date_added,
          "name": "Label Date Added"
        },
        {
          "attribut": '"last_admin":',
          "value": this.data.general.last_admin,
          "name": "Label Last Admin"
        },
        {
          "attribut": '"admin_added":',
          "value": this.data.general.admin_added,
          "name": "Label Admin Added"
        } 
      ]
    } 
  }

  // sub module 2
  picksubmodule(value) {
    this.submoduleselect = true;
    if (value == "epdb-dashboard") {
      this.menuarr =
        [
          {
            "value": "epdb-dashboard-chart",
            "name": "Chart"
          },
          {
            "value": "epdb-dashboard-head_table",
            "name": "Header On Table"
          }
        ]
    }
    else if (value == "epdb-admin") {
      this.menuarr =
        [
          {
            "value": "epdb-admin-activities",
            "name": "Activities"
          },
          {
            "value": "epdb-admin-product",
            "name": "Product"
          },
          {
            "value": "epdb-admin-sku",
            "name": "SKU"
          },
          {
            "value": "epdb-admin-glosarry",
            "name": "Glosarry"
          },
          {
            "value": "epdb-admin-country",
            "name": "Country"
          },
          {
            "value": "epdb-admin-market-type",
            "name": "Market Type"
          },
          {
            "value": "epdb-admin-site-service",
            "name": "Site Services"
          },
          {
            "value": "epdb-admin-variable",
            "name": "Variables"
          },
          {
            "value": "epdb-admin-sub-partner-type",
            "name": "Sub Partner Type"
          },
          {
            "value": "epdb-admin-partner-type",
            "name": "Partner Type"
          },
          {
            "value": "epdb-admin-currency-conversion",
            "name": "Currency Conversion"
          }
        ];
    }
    else if (value == "epdb-user") {
      this.menuarr =
        [
          {
            "value": "epdb-user-view",
            "name": "View User Information"
          },
          {
            "value": "epdb-user-change",
            "name": "Change User Information"
          }
        ]
    }
    else if (value == "epdb-manager") {
      this.menuarr =
        [
          {
            "value": "epdb-manager-organization",
            "name": "Organization"
          },
          {
            "value": "epdb-manager-site",
            "name": "Site"
          },
          {
            "value": "epdb-manager-contact",
            "name": "Contact"
          },
          {
            "value": "epdb-manager-distributor",
            "name": "Distributor"
          }
        ]
    }
    else if (value == "eim-profile") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
      [
        {
          "attribut": '"my_profile":',
          "value": this.data.eim.profile.my_profile,
          "name": "My Profile"
        },
        {
          "attribut": '"btn_change_password":',
          "value": this.data.eim.profile.btn_change_password,
          "name": "Change Password"
        },
        {
          "attribut": '"partner_id":',
          "value": this.data.eim.profile.partner_id,
          "name": "Partner ID"
        },
        {
          "attribut": '"first_name":',
          "value": this.data.eim.profile.first_name,
          "name": "First Name"
        },
        {
          "attribut": '"last_name":',
          "value": this.data.eim.profile.last_name,
          "name": "Last Name"
        },
        {
          "attribut": '"honorific":',
          "value": this.data.eim.profile.honorific,
          "name": "Honorific"
        },
        {
          "attribut": '"gender":',
          "value": this.data.eim.profile.gender,
          "name": "Gender"
        },
        {
          "attribut": '"primary_industry":',
          "value": this.data.eim.profile.primary_industry,
          "name": "Primary Industry"
        },
        {
          "attribut": '"primary_language":',
          "value": this.data.eim.profile.primary_language,
          "name": "Primary Language"
        },
        {
          "attribut": '"secondary_language":',
          "value": this.data.eim.profile.secondary_language,
          "name": "Secondary Language"
        },
        {
          "attribut": '"address":',
          "value": this.data.eim.profile.address,
          "name": "Address"
        },
        {
          "attribut": '"city":',
          "value": this.data.eim.profile.city,
          "name": "City"
        },
        {
          "attribut": '"state":',
          "value": this.data.eim.profile.state,
          "name": "State"
        },
        {
          "attribut": '"country":',
          "value": this.data.eim.profile.country,
          "name": "Country"
        },
        {
          "attribut": '"postal_code":',
          "value": this.data.eim.profile.postal_code,
          "name": "ZIP / Postal Code"
        },
        {
          "attribut": '"website":',
          "value": this.data.eim.profile.website,
          "name": "Website"
        },
        {
          "attribut": '"bio":',
          "value": this.data.eim.profile.bio,
          "name": "Bio"
        },
        {
          "attribut": '"allow_other_to_see":',
          "value": this.data.eim.profile.allow_other_to_see,
          "name": "Allow others to see my profile in search results"
        },
        // {
        //   "attribut": '"email_address":',
        //   "value": this.data.eim.profile.email_address,
        //   "name": "Email Address"
        // },
        {
          "attribut": '"phone_number":',
          "value": this.data.eim.profile.phone_number,
          "name": "Phone Number"
        },
        {
          "attribut": '"mobile_number":',
          "value": this.data.eim.profile.mobile_number,
          "name": "Mobile Number"
        },
        {
          "attribut": '"telephone":',
          "value": this.data.eim.profile.telephone,
          "name": "Telephone"
        },
        {
          "attribut": '"do_not_send_email":',
          "value": this.data.eim.profile.do_not_send_email,
          "name": "Do not send email"
        },
        {
          "attribut": '"share_my_email":',
          "value": this.data.eim.profile.share_my_email,
          "name": "Share my email address"
        },
        {
          "attribut": '"share_my_phone":',
          "value": this.data.eim.profile.share_my_phone,
          "name": "Share my phone number"
        },
        {
          "attribut": '"share_my_mobile_number":',
          "value": this.data.eim.profile.share_my_mobile_number,
          "name": "Share my Mobile Number"
        }
      ]
    }
    else if (value == "eva-manage-evaluation") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
      [
        {
          "attribut": '"list_survey_template":',
          "value": this.data.eva.manage_evaluation.list_survey_template,
          "name": "List Survey Template"
        },
        {
          "attribut": '"btn_add_template":',
          "value": this.data.eva.manage_evaluation.btn_add_template,
          "name": "Add Template"
        },
        {
          "attribut": '"evaluation_template_code":',
          "value": this.data.eva.manage_evaluation.evaluation_template_code,
          "name": "Evaluation Template Code"
        },
        {
          "attribut": '"evaluation_template_title":',
          "value": this.data.eva.manage_evaluation.evaluation_template_title,
          "name": "Evaluation Template Title"
        },
        {
          "attribut": '"year":',
          "value": this.data.eva.manage_evaluation.year,
          "name": "Year"
        },
        {
          "attribut": '"template_builder":',
          "value": this.data.eva.manage_evaluation.template_builder,
          "name": "Template Builder"
        },
        {
          "attribut": '"add_evaluation_template_data":',
          "value": this.data.eva.manage_evaluation.add_evaluation_template_data,
          "name": "Add Evaluation Template Data"
        },
        {
          "attribut": '"edit_evaluation_template_data":',
          "value": this.data.eva.manage_evaluation.edit_evaluation_template_data,
          "name": "Edit Evaluation Template Data"
        }
      ]
    }
    else if (value == "eva-manage-course") {
      this.submoduleselect = false;
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
      [
        {
          "attribut": '"list_course":',
          "value": this.data.eva.manage_course.list_course.list_course,
          "name": "List Course"
        },
        {
          "attribut": '"btn_add_course":',
          "value": this.data.eva.manage_course.list_course.btn_add_course,
          "name": "Label Button Add Course"
        },
        {
          "attribut": '"course_id":',
          "value": this.data.eva.manage_course.list_course.course_id,
          "name": "Course ID"
        },
        {
          "attribut": '"course_title":',
          "value": this.data.eva.manage_course.list_course.course_title,
          "name": "Course Title"
        },
        {
          "attribut": '"survey_partner_type":',
          "value": this.data.eva.manage_course.list_course.survey_partner_type,
          "name": "Survey Partner Type"
        },
        {
          "attribut": '"instructor":',
          "value": this.data.eva.manage_course.list_course.instructor,
          "name": "Instructor"
        },
        {
          "attribut": '"instructor_id":',
          "value": this.data.eva.manage_course.list_course.instructor_id,
          "name": "Instructor ID"
        },
        {
          "attribut": '"instructor_name":',
          "value": this.data.eva.manage_course.list_course.instructor_name,
          "name": "Instructor Name"
        },
        {
          "attribut": '"detail_course":',
          "value": this.data.eva.manage_course.detail.detail_course,
          "name": "Detail Course"
        },
        {
          "attribut": '"hour_training":',
          "value": this.data.eva.manage_course.detail.hour_training,
          "name": "Hours Training"
        },
        {
          "attribut": '"survey_indicator":',
          "value": this.data.eva.manage_course.detail.survey_indicator,
          "name": "Survey Indicator"
        },
        {
          "attribut": '"institution":',
          "value": this.data.eva.manage_course.detail.institution,
          "name": "Institution"
        },
        {
          "attribut": '"teaching_level":',
          "value": this.data.eva.manage_course.detail.teaching_level,
          "name": "Teaching Level"
        },
        {
          "attribut": '"version":',
          "value": this.data.eva.manage_course.detail.version,
          "name": "Version"
        },
        {
          "attribut": '"add_course":',
          "value": this.data.eva.manage_course.add.add_course,
          "name": "Title in Add Course"
        },
        {
          "attribut": '"course_facility":',
          "value": this.data.eva.manage_course.add.course_facility,
          "name": "Course Facility"
        },
        {
          "attribut": '"computer_equipment":',
          "value": this.data.eva.manage_course.add.computer_equipment,
          "name": "Computer Equipment"
        },
        {
          "attribut": '"survey":',
          "value": this.data.eva.manage_course.add.survey,
          "name": "Survey"
        }         
      ]
    }
  }

  // sub menu
  picksubmenu(value) {
    this.submenuselect = true;
    if (value == "epdb-admin-country") {
      this.submenuarr =
      [
        {
          "value": "epdb-admin-country-geo",
          "name": "Geo"
        },
        {
          "value": "epdb-admin-country-territory",
          "name": "Territory"
        },
        {
          "value": "epdb-admin-country-region",
          "name": "Region"
        },
        // {
        //   "value": "epdb-admin-country-sub-region",
        //   "name": "Sub Region"
        // },
        {
          "value": "epdb-admin-country-countries",
          "name": "Country"
        }
      ]
    }
    else if (value == "epdb-manager-organization") {
      this.submenuarr =
        [
          {
            "value": "epdb-manager-organization-add",
            "name": "Add Organization"
          },
          {
            "value": "epdb-manager-organization-search",
            "name": "Search Organization"
          },
          {
            "value": "epdb-manager-organization-detail",
            "name": "Detail Organization"
          },
          {
            "value": "epdb-manager-organization-edit",
            "name": "Edit Organization"
          },
          {
            "value": "epdb-manager-organization-journal-entries",
            "name": "Add Organization Journal Entries"
          },
          {
            "value": "epdb-manager-invoice",
            "name": "Add Invoice"
          }
        ]
    }
    else if (value == "epdb-manager-site") {
      this.submenuarr =
        [
          {
            "value": "epdb-manager-site-add",
            "name": "Add Site"
          },
          {
            "value": "epdb-manager-site-search",
            "name": "Search Site"
          },
          {
            "value": "epdb-manager-site-detail",
            "name": "Detail Site"
          },
          {
            "value": "epdb-manager-site-edit",
            "name": "Edit Site"
          },
          {
            "value": "epdb-manager-site-partner-type",
            "name": "Add Partner Type"
          }
        ]
    }
    else if (value == "epdb-manager-contact") {
      this.submenuarr =
        [
          {
            "value": "epdb-manager-contact-add",
            "name": "Add Contact"
          },
          {
            "value": "epdb-manager-contact-search",
            "name": "Search Contact"
          },
          {
            "value": "epdb-manager-contact-detail",
            "name": "Detail Contact"
          },
          {
            "value": "epdb-manager-contact-edit",
            "name": "Edit Contact"
          },
          {
            "value": "epdb-manager-contact-affiliated-site",
            "name": "Affiliated Sites"
          }
        ]
    }
  }


  pickmenu(value) {
    this.menuselect = true;
    if (value == "epdb-dashboard-chart") {
      this.contentarr =
        [
          {
            "attribut": '"week":',
            "value": this.data.epdb.dashboard.chart.week,
            "name": "Week"
          },
          {
            "attribut": '"month":',
            "value": this.data.epdb.dashboard.chart.month,
            "name": "Month"
          },
          {
            "attribut": '"year":',
            "value": this.data.epdb.dashboard.chart.year,
            "name": "Year"
          }
        ]
    }
    else if (value == "epdb-dashboard-head_table"){
      this.contentarr =
        [
          {
            "attribut": '"type":',
            "value": this.data.epdb.dashboard.head_table.type,
            "name": "Type"
          },
          {
            "attribut": '"name":',
            "value": this.data.epdb.dashboard.head_table.name,
            "name": "Name"
          },
          {
            "attribut": '"views":',
            "value": this.data.epdb.dashboard.head_table.views,
            "name": "Views"
          },
          {
            "attribut": '"favourites":',
            "value": this.data.epdb.dashboard.head_table.favourites,
            "name": "Favourites"
          },
          {
            "attribut": '"last_visit":',
            "value": this.data.epdb.dashboard.head_table.last_visit,
            "name": "Last Visit"
          },
          {
            "attribut": '"last_action":',
            "value": this.data.epdb.dashboard.head_table.last_action,
            "name": "Last Action"
          },
          {
            "attribut": '"last_date":',
            "value": this.data.epdb.dashboard.head_table.last_date,
            "name": "Last Date"
          }
        ]
    }

    else if (value == "epdb-admin-activities") {
      this.contentarr =
        [
          {
            "attribut": '"activities":',
            "value": this.data.epdb.admin.activities.activities,
            "name": "Activities"
          },
          {
            "attribut": '"activities_list":',
            "value": this.data.epdb.admin.activities.activities_list,
            "name": "Title in List Activities"
          },
          {
            "attribut": '"add_new_activities":',
            "value": this.data.epdb.admin.activities.add_new_activities,
            "name": " Title in Add New Activities"
          },
          {
            "attribut": '"detail_activities":',
            "value": this.data.epdb.admin.activities.detail_activities,
            "name": "Title in Detail Activities"
          },
          {
            "attribut": '"edit_activities":',
            "value": this.data.epdb.admin.activities.edit_activities,
            "name": "Title in Edit Activities"
          },
          {
            "attribut": '"btn_add_activities":',
            "value": this.data.epdb.admin.activities.btn_add_activities,
            "name": "Label Button Add Activity"
          },
          {
            "attribut": '"activity_id":',
            "value": this.data.epdb.admin.activities.activity_id,
            "name": "Activity ID"
          },
          {
            "attribut": '"activity_name":',
            "value": this.data.epdb.admin.activities.activity_name,
            "name": "Activity Name"
          },
          {
            "attribut": '"activity_type":',
            "value": this.data.epdb.admin.activities.activity_type,
            "name": "Activity Type"
          },
          {
            "attribut": '"unique":',
            "value": this.data.epdb.admin.activities.unique,
            "name": "Unique"
          },
          {
            "attribut": '"must_be_unique":',
            "value": this.data.epdb.admin.activities.must_be_unique,
            "name": "Must be Unique"
          },
          {
            "attribut": '"there_can_only":',
            "value": this.data.epdb.admin.activities.there_can_only,
            "name": "There can only be one attribute of this type per Organization, Site, or Instructor"
          }
        ]
    }
    else if (value == "epdb-admin-product") {
      this.contentarr =
        [
          {
            "attribut": '"product":',
            "value": this.data.epdb.admin.product.product,
            "name": "Product"
          },
          {
            "attribut": '"product_list":',
            "value": this.data.epdb.admin.product.product_list,
            "name": "Title in List Products"
          },
          {
            "attribut": '"add_new_product":',
            "value": this.data.epdb.admin.product.add_new_product,
            "name": "Title in Add New Products"
          },
          {
            "attribut": '"detail_product":',
            "value": this.data.epdb.admin.product.detail_product,
            "name": "Title in Detail Product"
          },
          {
            "attribut": '"edit_product":',
            "value": this.data.epdb.admin.product.edit_product,
            "name": "Title in Edit Product"
          },
          {
            "attribut": '"btn_add_product":',
            "value": this.data.epdb.admin.product.btn_add_product,
            "name": "Label Button Add Product"
          },
          {
            "attribut": '"product_id":',
            "value": this.data.epdb.admin.product.product_id,
            "name": "Product ID"
          },
          {
            "attribut": '"product_name":',
            "value": this.data.epdb.admin.product.product_name,
            "name": "Product Name"
          },
          {
            "attribut": '"year":',
            "value": this.data.epdb.admin.product.year,
            "name": "Year"
          },
          {
            "attribut": '"category_list":',
            "value": this.data.epdb.admin.product.category_list,
            "name": "Title in List Category"
          },
          {
            "attribut": '"add_new_category":',
            "value": this.data.epdb.admin.product.add_new_category,
            "name": "Title in Add New Category"
          },
          {
            "attribut": '"edit_category":',
            "value": this.data.epdb.admin.product.edit_category,
            "name": "Title in Edit Category"
          },
          {
            "attribut": '"product_category":',
            "value": this.data.epdb.admin.product.product_category,
            "name": "Products Category"
          },
          {
            "attribut": '"btn_add_category":',
            "value": this.data.epdb.admin.product.btn_add_category,
            "name": "Label Button Add Category"
          },
          {
            "attribut": '"category_id":',
            "value": this.data.epdb.admin.product.category_id,
            "name": "Category ID"
          },
          {
            "attribut": '"category_name":',
            "value": this.data.epdb.admin.product.category_name,
            "name": "Category Name"
          }
        ]
    }
    else if (value == "epdb-admin-sku") {
      this.contentarr =
        [
          {
            "attribut": '"list_sku":',
            "value": this.data.epdb.admin.sku.list_sku,
            "name": "List SKU"
          },
          {
            "attribut": '"btn_add_sku":',
            "value": this.data.epdb.admin.sku.btn_add_sku,
            "name": "Add SKU"
          },
          {
            "attribut": '"sku_id":',
            "value": this.data.epdb.admin.sku.sku_id,
            "name": "SKU ID"
          },
          {
            "attribut": '"price_license":',
            "value": this.data.epdb.admin.sku.price_license,
            "name": "Price / License"
          },
          {
            "attribut": '"currency":',
            "value": this.data.epdb.admin.sku.currency,
            "name": "Currency"
          },
          {
            "attribut": '"add_edit_sku":',
            "value": this.data.epdb.admin.sku.add_edit_sku,
            "name": "Add / Update SKU"
          },
          {
            "attribut": '"geo":',
            "value": this.data.epdb.admin.sku.geo,
            "name": "Geo"
          },
          {
            "attribut": '"region":',
            "value": this.data.epdb.admin.sku.region,
            "name": "Region"
          },
          {
            "attribut": '"sub_region":',
            "value": this.data.epdb.admin.sku.sub_region,
            "name": "Sub Region"
          },
          {
            "attribut": '"country":',
            "value": this.data.epdb.admin.sku.country,
            "name": "Country"
          },
          {
            "attribut": '"distributor":',
            "value": this.data.epdb.admin.sku.distributor,
            "name": "Distributor"
          }
        ]
    }
    else if (value == "epdb-admin-glosarry") {
      this.contentarr =
        [
          {
            "attribut": '"glosarry_list":',
            "value": this.data.epdb.admin.glosarry.glosarry_list,
            "name": "Title in List Glosarry"
          },
          {
            "attribut": '"add_new_term":',
            "value": this.data.epdb.admin.glosarry.add_new_term,
            "name": "Title in Add New Term"
          },
          {
            "attribut": '"detail_term":',
            "value": this.data.epdb.admin.glosarry.detail_term,
            "name": "Title in Detail Term"
          },
          {
            "attribut": '"edit_term":',
            "value": this.data.epdb.admin.glosarry.edit_term,
            "name": "Title in Edit Term"
          },
          {
            "attribut": '"btn_add_term":',
            "value": this.data.epdb.admin.glosarry.btn_add_term,
            "name": "Label Button Add Term"
          },
          {
            "attribut": '"term":',
            "value": this.data.epdb.admin.glosarry.term,
            "name": "Term"
          },
          {
            "attribut": '"term_id":',
            "value": this.data.epdb.admin.glosarry.term_id,
            "name": "Term ID"
          },
          {
            "attribut": '"term_name":',
            "value": this.data.epdb.admin.glosarry.term_name,
            "name": "Term Name"
          }
        ]
    }
    else if (value == "epdb-admin-market-type") {
      this.contentarr =
        [
          {
            "attribut": '"market_type_list":',
            "value": this.data.epdb.admin.market_type.market_type_list,
            "name": "Title in List Market Type"
          },
          {
            "attribut": '"market_type":',
            "value": this.data.epdb.admin.market_type.market_type,
            "name": "Market Type"
          },
          {
            "attribut": '"btn_add_market_type":',
            "value": this.data.epdb.admin.market_type.btn_add_market_type,
            "name": "Label ButtonAdd Market Type"
          },
          {
            "attribut": '"add_update_market_type":',
            "value": this.data.epdb.admin.market_type.add_update_market_type,
            "name": "Title in Add / Update Market Type"
          },
          {
            "attribut": '"market_name":',
            "value": this.data.epdb.admin.market_type.market_name,
            "name": "Market Name"
          }
        ]
    }
    else if (value == "epdb-admin-site-service") {
      this.contentarr =
        [
          {
            "attribut": '"site_services_list":',
            "value": this.data.epdb.admin.site_services.site_services_list,
            "name": "Title in List Site Services"
          },
          {
            "attribut": '"btn_add_site_services":',
            "value": this.data.epdb.admin.site_services.btn_add_site_services,
            "name": "Label Button Add Site Service"
          },
          {
            "attribut": '"code":',
            "value": this.data.epdb.admin.site_services.code,
            "name": "Code"
          },
          {
            "attribut": '"name":',
            "value": this.data.epdb.admin.site_services.name,
            "name": "Name"
          },
          {
            "attribut": '"add_new_site_services":',
            "value": this.data.epdb.admin.site_services.add_new_site_services,
            "name": "Title in  Add New Site Services"
          },
          {
            "attribut": '"detail_site_service":',
            "value": this.data.epdb.admin.site_services.detail_site_service,
            "name": "Title in  Detail Site Service"
          },
          {
            "attribut": '"site_service_id":',
            "value": this.data.epdb.admin.site_services.site_service_id,
            "name": "Site Service ID"
          },
          {
            "attribut": '"edit_site_services":',
            "value": this.data.epdb.admin.site_services.edit_site_services,
            "name": "Title in Edit Site Service"
          },
          {
            "attribut": '"site_service_code":',
            "value": this.data.epdb.admin.site_services.site_service_code,
            "name": "Site Service Code"
          },
          {
            "attribut": '"site_service_name":',
            "value": this.data.epdb.admin.site_services.site_service_name,
            "name": "Site Servive Name"
          }
        ]
    }
    else if (value == "epdb-admin-variable") {
      this.contentarr =
        [
          {
            "attribut": '"variable_list":',
            "value": this.data.epdb.admin.variables.variable_list,
            "name": "Title in List Variables"
          },
          {
            "attribut": '"add_new_variable":',
            "value": this.data.epdb.admin.variables.add_new_variable,
            "name": "Title in Add New Variable"
          },
          {
            "attribut": '"edit_variable":',
            "value": this.data.epdb.admin.variables.edit_variable,
            "name": "Title in Edit Variable"
          },
          {
            "attribut": '"detail_variable":',
            "value": this.data.epdb.admin.variables.detail_variable,
            "name": "Title in Detail Variable"
          },
          {
            "attribut": '"btn_add_variable":',
            "value": this.data.epdb.admin.variables.btn_add_variable,
            "name": "Label Button Add Variable"
          },
          {
            "attribut": '"id":',
            "value": this.data.epdb.admin.variables.id,
            "name": "ID"
          },
          {
            "attribut": '"value":',
            "value": this.data.epdb.admin.variables.value,
            "name": "Value"
          },
          {
            "attribut": '"key":',
            "value": this.data.epdb.admin.variables.key,
            "name": "Key"
          },
          {
            "attribut": '"variable_id":',
            "value": this.data.epdb.admin.variables.variable_id,
            "name": "Variable ID"
          },
          {
            "attribut": '"variabel_type":',
            "value": this.data.epdb.admin.variables.variabel_type,
            "name": "Variables Type"
          }
        ]
    }
    // sub partner type
    else if (value == "epdb-admin-sub-partner-type") {
      this.contentarr =
        [
          {
            "attribut": '"sub_partner_type":',
            "value": this.data.epdb.admin.sub_partner_type.sub_partner_type,
            "name": "Sub Partner Type"
          },
          {
            "attribut": '"list_sub_partner_type":',
            "value": this.data.epdb.admin.sub_partner_type.list_sub_partner_type,
            "name": "Title in List Sub Partner Type"
          },
          {
            "attribut": '"add_new_sub_partner_type":',
            "value": this.data.epdb.admin.sub_partner_type.add_new_sub_partner_type,
            "name": "Title in Add New Sub Partner Type"
          },
          {
            "attribut": '"detail_sub_partner_type":',
            "value": this.data.epdb.admin.sub_partner_type.detail_sub_partner_type,
            "name": "Title in Detail Sub Partner Type"
          },
          {
            "attribut": '"edit_sub_partner_type":',
            "value": this.data.epdb.admin.sub_partner_type.edit_sub_partner_type,
            "name": "Title in Edit Sub Partner Type"
          },
          {
            "attribut": '"btn_add_sub_partner_type":',
            "value": this.data.epdb.admin.sub_partner_type.btn_add_sub_partner_type,
            "name": "Label Button Add Sub Partner Type"
          },
          {
            "attribut": '"sub_partner_type_id":',
            "value": this.data.epdb.admin.sub_partner_type.sub_partner_type_id,
            "name": "Sub Partner Type ID"
          },
          {
            "attribut": '"sub_partner_type_name":',
            "value": this.data.epdb.admin.sub_partner_type.sub_partner_type_name,
            "name": "Sub Partner Type Name"
          },
          {
            "attribut": '"sub_partner_type":',
            "value": this.data.epdb.admin.sub_partner_type.sub_partner_type,
            "name": "Sub Partner Type"
          }
        ]
    }
    else if (value == "epdb-admin-partner-type") {
      this.contentarr =
        [
          {
            "attribut": '"list_partner_type":',
            "value": this.data.epdb.admin.partner_type.list_partner_type,
            "name": "Title in List Partner Type"
          },
          {
            "attribut": '"add_new_partner_type":',
            "value": this.data.epdb.admin.partner_type.add_new_partner_type,
            "name": "Title in Add New Partner Type"
          },
          {
            "attribut": '"detail_partner_type":',
            "value": this.data.epdb.admin.partner_type.detail_partner_type,
            "name": "Title in Detail Partner Type"
          },
          {
            "attribut": '"edit_partner_type":',
            "value": this.data.epdb.admin.partner_type.edit_partner_type,
            "name": "Title in Edit Partner Type"
          },
          {
            "attribut": '"btn_add_partner_type":',
            "value": this.data.epdb.admin.partner_type.btn_add_partner_type,
            "name": "Label Button Add Partner Type"
          },
          {
            "attribut": '"id":',
            "value": this.data.epdb.admin.partner_type.id,
            "name": "ID"
          },
          {
            "attribut": '"code":',
            "value": this.data.epdb.admin.partner_type.code,
            "name": "Code"
          },
          {
            "attribut": '"role_name":',
            "value": this.data.epdb.admin.partner_type.role_name,
            "name": "Role Name"
          },
          {
            "attribut": '"siebel_name":',
            "value": this.data.epdb.admin.partner_type.siebel_name,
            "name": "Siebel Name"
          },
          {
            "attribut": '"framework_name":',
            "value": this.data.epdb.admin.partner_type.framework_name,
            "name": "Framework Name"
          },
          {
            "attribut": '"crb_approved_name":',
            "value": this.data.epdb.admin.partner_type.crb_approved_name,
            "name": "CRB Approved Name"
          },
          {
            "attribut": '"role_type":',
            "value": this.data.epdb.admin.partner_type.role_type,
            "name": "Role Type"
          },
          {
            "attribut": '"role_sub_type":',
            "value": this.data.epdb.admin.partner_type.role_sub_type,
            "name": "Role Sub Type"
          },
          {
            "attribut": '"certificate_type":',
            "value": this.data.epdb.admin.partner_type.certificate_type,
            "name": "Certificate Type"
          }
        ]
    }
    // partner type
    else if (value == "epdb-admin-currency-conversion") {
      this.contentarr =
        [
          {
            "attribut": '"currency_conversion":',
            "value": this.data.epdb.admin.currency.currency_conversion,
            "name": "Title in Currency Conversion"
          },
          {
            "attribut": '"currency":',
            "value": this.data.epdb.admin.currency.currency,
            "name": "Currency"
          },
          {
            "attribut": '"list_currency":',
            "value": this.data.epdb.admin.currency.list_currency,
            "name": "List Currency"
          },
          {
            "attribut": '"year":',
            "value": this.data.epdb.admin.currency.year,
            "name": "Year"
          },
          {
            "attribut": '"value_1_usd":',
            "value": this.data.epdb.admin.currency.value_1_usd,
            "name": "Value 1 USD"
          }
        ]
    }
    // User 
    else if (value == "epdb-user-view") {
      this.contentarr =
      [
        {
          "attribut": '"view_user_information":',
          "value": this.data.epdb.user.view_user_information,
          "name": "View User Information"
        },
        {
          "attribut": '"user_id":',
          "value": this.data.epdb.user.user_id,
          "name": "User ID"
        },
        {
          "attribut": '"company_name":',
          "value": this.data.epdb.user.company_name,
          "name": "Company Name"
        },
        {
          "attribut": '"user_name":',
          "value": this.data.epdb.user.user_name,
          "name": "User Name"
        },
        {
          "attribut": '"email_address":',
          "value": this.data.epdb.user.email_address,
          "name": "Email Address"
        },
        {
          "attribut": '"last_changed":',
          "value": this.data.epdb.user.last_changed,
          "name": "Last Changed"
        },
        {
          "attribut": '"permissions":',
          "value": this.data.epdb.user.permissions,
          "name": "Permission"
        },
        {
          "attribut": '"current_session":',
          "value": this.data.epdb.user.current_session,
          "name": "Current Session"
        },
        {
          "attribut": '"ip_address":',
          "value": this.data.epdb.user.ip_address,
          "name": "IP Address"
        },
        {
          "attribut": '"current_date_time":',
          "value": this.data.epdb.user.current_date_time,
          "name": "Current Date Time"
        },
        {
          "attribut": '"history":',
          "value": this.data.epdb.user.history,
          "name": "History"
        }
      ]
    }
    else if (value == "epdb-user-change") {
      this.contentarr =
        [
          {
            "attribut": '"change_user_information":',
            "value": this.data.epdb.user.change_user_information,
            "name": "Change User Information"
          },
          {
            "attribut": '"user_id":',
            "value": this.data.epdb.user.user_id,
            "name": "User ID"
          },
          {
            "attribut": '"new_password":',
            "value": this.data.epdb.user.new_password,
            "name": "New Password"
          },
          {
            "attribut": '"company_name":',
            "value": this.data.epdb.user.company_name,
            "name": "Company Name"
          },
          {
            "attribut": '"user_name":',
            "value": this.data.epdb.user.user_name,
            "name": "User Name"
          },
          {
            "attribut": '"":',
            "value": this.data.epdb.user.email_address,
            "name": "Email Address"
          },
          {
            "attribut": '"show_duplicate":',
            "value": this.data.epdb.user.show_duplicate,
            "name": "Show Duplicate"
          },
          {
            "attribut": '"label_show_duplicate":',
            "value": this.data.epdb.user.label_show_duplicate,
            "name": "Show Duplicates on Summary pages"
          }
        ]
    }
  }

  
  picksubmenucontent(value) {
    this.submenuselectcontent = true;
    // sub country
    if (value == "epdb-admin-country-geo") { 
    this.contentarr =
      [
        {
          "attribut": '"geo":',
          "value": this.data.epdb.admin.country.geo.geo,
          "name": "Geo"
        },
        {
          "attribut": '"geo_list":',
          "value": this.data.epdb.admin.country.geo.geo_list,
          "name": "Title in List Geo"
        },
        {
          "attribut": '"add_new_geo":',
          "value": this.data.epdb.admin.country.geo.add_new_geo,
          "name": "Title in Add New Geo"
        },
        {
          "attribut": '"detail_geo":',
          "value": this.data.epdb.admin.country.geo.detail_geo,
          "name": "Title in Detail Geo"
        },
        {
          "attribut": '"edit_geo":',
          "value": this.data.epdb.admin.country.geo.edit_geo,
          "name": "Title in Edit Geo"
        },
        {
          "attribut": '"btn_add_geo":',
          "value": this.data.epdb.admin.country.geo.btn_add_geo,
          "name": "Label Button Add Geo"
        },
        {
          "attribut": '"geo_id":',
          "value": this.data.epdb.admin.country.geo.geo_id,
          "name": "Geo ID"
        },
        {
          "attribut": '"geo_code":',
          "value": this.data.epdb.admin.country.geo.geo_code,
          "name": "Geo Code"
        },
        {
          "attribut": '"geo_name":',
          "value": this.data.epdb.admin.country.geo.geo_name,
          "name": "Geo Name"
        }
      ]
    }
    else if (value == "epdb-admin-country-territory") {
      this.contentarr =
        [
          {
            "attribut": '"territory":',
            "value": this.data.epdb.admin.country.territory.territory,
            "name": "Territory"
          },
          {
            "attribut": '"territory_list":',
            "value": this.data.epdb.admin.country.territory.territory_list,
            "name": "Title in Territory List"
          },
          {
            "attribut": '"detail_territory":',
            "value": this.data.epdb.admin.country.territory.detail_territory,
            "name": "Title in Detail Territory"
          },
          {
            "attribut": '"add_new_territory":',
            "value": this.data.epdb.admin.country.territory.add_new_territory,
            "name": "Title in Add New Territory"
          },
          {
            "attribut": '"edit_territory":',
            "value": this.data.epdb.admin.country.territory.edit_territory,
            "name": "Title in Edit Territory"
          },
          {
            "attribut": '"btn_add_territory":',
            "value": this.data.epdb.admin.country.territory.btn_add_territory,
            "name": "Label Button Add Territory"
          },
          {
            "attribut": '"territory_id":',
            "value": this.data.epdb.admin.country.territory.territory_id,
            "name": "Territory ID"
          },
          {
            "attribut": '"territory_code":',
            "value": this.data.epdb.admin.country.territory.territory_code,
            "name": "Territory Code"
          },
          {
            "attribut": '"territory_name":',
            "value": this.data.epdb.admin.country.territory.territory_name,
            "name": "Territory Name"
          }
        ]
    }
    else if (value == "epdb-admin-country-region") {
      this.contentarr =
        [
          {
            "attribut": '"region":',
            "value": this.data.epdb.admin.country.region.region,
            "name": "Region"
          },
          {
            "attribut": '"region_list":',
            "value": this.data.epdb.admin.country.region.region_list,
            "name": "Title in Region List"
          },
          {
            "attribut": '"add_new_region":',
            "value": this.data.epdb.admin.country.region.add_new_region,
            "name": "Title in Add New Region"
          },
          {
            "attribut": '"detail_region":',
            "value": this.data.epdb.admin.country.region.detail_region,
            "name": "Title in Detail Region"
          },
          {
            "attribut": '"edit_region":',
            "value": this.data.epdb.admin.country.region.edit_region,
            "name": "Title in Edit Region"
          },
          {
            "attribut": '"btn_add_region":',
            "value": this.data.epdb.admin.country.region.btn_add_region,
            "name": "Label Button Add Region"
          },
          {
            "attribut": '"region_id":',
            "value": this.data.epdb.admin.country.region.region_id,
            "name": "Region ID"
          },
          {
            "attribut": '"region_code":',
            "value": this.data.epdb.admin.country.region.region_code,
            "name": "Region Code"
          },
          {
            "attribut": '"region_name":',
            "value": this.data.epdb.admin.country.region.region_name,
            "name": "Region Name"
          }
        ]
    }
    else if (value == "epdb-admin-country-sub-region") {
      this.contentarr =
        [
          {
            "attribut": '"sub_region":',
            "value": this.data.epdb.admin.country.sub_region.sub_region,
            "name": "Sub Region"
          },
          {
            "attribut": '"sub_region_list":',
            "value": this.data.epdb.admin.country.sub_region.sub_region_list,
            "name": "Sub Region List"
          },
          {
            "attribut": '"btn_add_sub_region":',
            "value": this.data.epdb.admin.country.sub_region.btn_add_sub_region,
            "name": "Add Sub Region"
          },
          {
            "attribut": '"sub_region_id":',
            "value": this.data.epdb.admin.country.sub_region.sub_region_id,
            "name": "Sub Region ID"
          },
          {
            "attribut": '"sub_region_code":',
            "value": this.data.epdb.admin.country.sub_region.sub_region_code,
            "name": "Sub Region Code"
          },
          {
            "attribut": '"sub_region_name":',
            "value": this.data.epdb.admin.country.sub_region.sub_region_name,
            "name": "Sub Region Name"
          },
          {
            "attribut": '"geo_code":',
            "value": this.data.epdb.admin.country.sub_region.geo_code,
            "name": "Geo Code"
          },
          {
            "attribut": '"territory_code":',
            "value": this.data.epdb.admin.country.sub_region.territory_code,
            "name": "Territory Code"
          },
          {
            "attribut": '"region_code":',
            "value": this.data.epdb.admin.country.sub_region.region_code,
            "name": "Region Code"
          },
          {
            "attribut": '"detail_sub_region":',
            "value": this.data.epdb.admin.country.sub_region.detail_sub_region,
            "name": "Detail Sub Region"
          },
          {
            "attribut": '"created_by":',
            "value": this.data.epdb.admin.country.sub_region.created_by,
            "name": "Created By"
          },
          {
            "attribut": '"created_date":',
            "value": this.data.epdb.admin.country.sub_region.created_date,
            "name": "Created Date"
          },
          {
            "attribut": '"add_new_sub_region":',
            "value": this.data.epdb.admin.country.sub_region.add_new_sub_region,
            "name": "Add New Sub Region"
          },
          {
            "attribut": '"geo":',
            "value": this.data.epdb.admin.country.sub_region.geo,
            "name": "Geo"
          },
          {
            "attribut": '"select_geo":',
            "value": this.data.epdb.admin.country.sub_region.select_geo,
            "name": "-- Select Geo --"
          },
          {
            "attribut": '"territory":',
            "value": this.data.epdb.admin.country.sub_region.territory,
            "name": "Territory"
          },
          {
            "attribut": '"select_territory":',
            "value": this.data.epdb.admin.country.sub_region.select_territory,
            "name": "-- Select Territory --"
          },
          {
            "attribut": '"region":',
            "value": this.data.epdb.admin.country.sub_region.region,
            "name": "Region"
          },
          {
            "attribut": '"select_region":',
            "value": this.data.epdb.admin.country.sub_region.select_region,
            "name": "-- Select Region --"
          },
          {
            "attribut": '"edit_sub_region":',
            "value": this.data.epdb.admin.country.sub_region.edit_sub_region,
            "name": "Edit Sub Region"
          }
        ]
    }
    else if (value == "epdb-admin-country-countries") {
      this.contentarr =
        [
          {
            "attribut": '"country":',
            "value": this.data.epdb.admin.country.countries.country,
            "name": "Country"
          },
          {
            "attribut": '"countries_list":',
            "value": this.data.epdb.admin.country.countries.countries_list,
            "name": "Title in List Countries"
          },
          {
            "attribut": '"add_new_country":',
            "value": this.data.epdb.admin.country.countries.add_new_country,
            "name": "Title in Add New Country"
          },
          {
            "attribut": '"detail_country":',
            "value": this.data.epdb.admin.country.countries.detail_country,
            "name": "Title in Detail Country"
          },
          {
            "attribut": '"edit_country":',
            "value": this.data.epdb.admin.country.countries.edit_country,
            "name": "Title in Edit Country"
          },
          {
            "attribut": '"btn_add_country":',
            "value": this.data.epdb.admin.country.countries.btn_add_country,
            "name": "Label Button Add Countries"
          },
          {
            "attribut": '"country_id":',
            "value": this.data.epdb.admin.country.countries.country_id,
            "name": "Country ID"
          },
          {
            "attribut": '"country_code":',
            "value": this.data.epdb.admin.country.countries.country_code,
            "name": "Country Code"
          },
          {
            "attribut": '"country_name":',
            "value": this.data.epdb.admin.country.countries.country_name,
            "name": "Country Name"
          },
          {
            "attribut": '"country_phone_code":',
            "value": this.data.epdb.admin.country.countries.country_phone_code,
            "name": "Country Phone Code"
          }
        ]
    }

    // sub manage org
    else if (value == "epdb-manager-organization-add") {
      this.contentarr =
        [
          {
            "attribut": '"add_new_organization":',
            "value": this.data.epdb.manage.organization.add_new_organization,
            "name": "Add New Organization"
          },
          {
            "attribut": '"organization_name":',
            "value": this.data.epdb.manage.organization.organization_name,
            "name": "Organization Name"
          },
          {
            "attribut": '"english_organization_name":',
            "value": this.data.epdb.manage.organization.english_organization_name,
            "name": "English Organization Name"
          },
          {
            "attribut": '"commercial_organization_name":',
            "value": this.data.epdb.manage.organization.commercial_organization_name,
            "name": "Commercial Organization Name"
          },
          {
            "attribut": '"atc_global_csn":',
            "value": this.data.epdb.manage.organization.atc_global_csn,
            "name": "ATC Global CSN"
          },
          {
            "attribut": '"var_global_csn":',
            "value": this.data.epdb.manage.organization.var_global_csn,
            "name": "VAR Global CSN"
          },
          {
            "attribut": '"ap_global_csn":',
            "value": this.data.epdb.manage.organization.ap_global_csn,
            "name": "A&P Global CSN"
          },
          {
            "attribut": '"tax_exempt":',
            "value": this.data.epdb.manage.organization.tax_exempt,
            "name": "Tax Exempt?"
          },
          {
            "attribut": '"vat_number":',
            "value": this.data.epdb.manage.organization.vat_number,
            "name": "VAT Number"
          },
          {
            "attribut": '"year_joined":',
            "value": this.data.epdb.manage.organization.year_joined,
            "name": "Year Joined"
          },
          {
            "attribut": '"organization_web_address":',
            "value": this.data.epdb.manage.organization.organization_web_address,
            "name": "Organization Web Address"
          },
          {
            "attribut": '"registered_address":',
            "value": this.data.epdb.manage.organization.registered_address,
            "name": "Registered Address"
          },
          {
            "attribut": '"contact_address":',
            "value": this.data.epdb.manage.organization.contact_address,
            "name": "Contact Address"
          },
          {
            "attribut": '"invoicing_address":',
            "value": this.data.epdb.manage.organization.invoicing_address,
            "name": "Invoicing Address"
          },
          {
            "attribut": '"department":',
            "value": this.data.epdb.manage.organization.department,
            "name": "Department"
          },
          {
            "attribut": '"department":',
            "value": this.data.epdb.manage.organization.department,
            "name": "Department"
          },
          {
            "attribut": '"address_line":',
            "value": this.data.epdb.manage.organization.address_line,
            "name": "Address"
          },
          {
            "attribut": '"city":',
            "value": this.data.epdb.manage.organization.city,
            "name": "City"
          },
          {
            "attribut": '"country":',
            "value": this.data.epdb.manage.organization.country,
            "name": "Country"
          },
          {
            "attribut": '"select_country":',
            "value": this.data.epdb.manage.organization.select_country,
            "name": "-- Select Country --"
          },
          {
            "attribut": '"state":',
            "value": this.data.epdb.manage.organization.state,
            "name": "State / Province / Prefecture"
          },
          {
            "attribut": '"zip":',
            "value": this.data.epdb.manage.organization.zip,
            "name": "Zip / Postal Code"
          },
          {
            "attribut": '"atc_director":',
            "value": this.data.epdb.manage.organization.atc_director,
            "name": "ATC Director"
          },
          {
            "attribut": '"legal_contact":',
            "value": this.data.epdb.manage.organization.legal_contact,
            "name": "Legal Contact"
          },
          {
            "attribut": '"billing_contact":',
            "value": this.data.epdb.manage.organization.billing_contact,
            "name": "Billing Contact"
          },
          {
            "attribut": '"first_name":',
            "value": this.data.epdb.manage.organization.first_name,
            "name": "First Name"
          },
          {
            "attribut": '"last_name":',
            "value": this.data.epdb.manage.organization.last_name,
            "name": "Last Name"
          },
          {
            "attribut": '"email_address":',
            "value": this.data.epdb.manage.organization.email_address,
            "name": "Email Address"
          },
          // {
          //   "attribut": '"telephone":',
          //   "value": this.data.epdb.manage.organization.telephone,
          //   "name": "Telephone"
          // },
          {
            "attribut": '"fax":',
            "value": this.data.epdb.manage.organization.fax,
            "name": "Fax"
          },
          {
            "attribut": '"administrative_notes":',
            "value": this.data.epdb.manage.organization.administrative_notes,
            "name": "Administrative Notes"
          } 
        ]
    }
    else if (value == "epdb-manager-organization-search") {
      this.contentarr =
      [
        {
          "attribut": '"search_organization":',
          "value": this.data.epdb.manage.organization.search_organization,
          "name": "Search Organization"
        },
        {
          "attribut": '"tab_organization":',
          "value": this.data.epdb.manage.organization.tab_organization,
          "name": "Tab Organization"
        },
        {
          "attribut": '"tab_site":',
          "value": this.data.epdb.manage.organization.tab_site,
          "name": "Tab Site"
        },
        {
          "attribut": '"tab_contact":',
          "value": this.data.epdb.manage.organization.tab_contact,
          "name": "Tab Contact"
        },
        {
          "attribut": '"organization_id":',
          "value": this.data.epdb.manage.organization.organization_id,
          "name": "Organization ID"
        },
        {
          "attribut": '"organization_name":',
          "value": this.data.epdb.manage.organization.organization_name,
          "name": "Organization Name"
        },
        {
          "attribut": '"partner_type":',
          "value": this.data.epdb.manage.organization.partner_type,
          "name": "Partner Type"
        },
        {
          "attribut": '"select_partner_type":',
          "value": this.data.epdb.manage.organization.select_partner_type,
          "name": "-- Select Partner Type --"
        },
        {
          "attribut": '"partner_type_status":',
          "value": this.data.epdb.manage.organization.partner_type_status,
          "name": "Partner Type Status"
        },
        {
          "attribut": '"geo":',
          "value": this.data.epdb.manage.organization.geo,
          "name": "Geo"
        },
        {
          "attribut": '"region":',
          "value": this.data.epdb.manage.organization.region,
          "name": "Region"
        },
        {
          "attribut": '"sub_region":',
          "value": this.data.epdb.manage.organization.sub_region,
          "name": "Sub Region"
        },
        {
          "attribut": '"countries":',
          "value": this.data.epdb.manage.organization.countries,
          "name": "Countries"
        },
        {
          "attribut": '"state":',
          "value": this.data.epdb.manage.organization.state,
          "name": "State"
        },
        {
          "attribut": '"search_result":',
          "value": this.data.epdb.manage.organization.search_result,
          "name": "Search Result"
        }
      ]
    }
    else if (value == "epdb-manager-organization-detail") {
      this.contentarr =
      [
        {
          "attribut": '"detail_organization":',
          "value": this.data.epdb.manage.organization.detail.detail_organization,
          "name": "Detail Organization"
        },
        {
          "attribut": '"organization":',
          "value": this.data.epdb.manage.organization.detail.organization.organization,
          "name": "Organization"
        },
        {
          "attribut": '"organization_id":',
          "value": this.data.epdb.manage.organization.detail.organization.organization_id,
          "name": "Organization ID"
        },
        {
          "attribut": '"organization_name":',
          "value": this.data.epdb.manage.organization.detail.organization.organization_name,
          "name": "Organization Name"
        },
        {
          "attribut": '"commercial_name":',
          "value": this.data.epdb.manage.organization.detail.organization.commercial_name,
          "name": "Commercial Name"
        },
        {
          "attribut": '"geo_region":',
          "value": this.data.epdb.manage.organization.detail.organization.geo_region,
          "name": "Geo / Region"
        },
        {
          "attribut": '"partner_type":',
          "value": this.data.epdb.manage.organization.detail.organization.partner_type,
          "name": "Partner Type"
        },
        {
          "attribut": '"email":',
          "value": this.data.epdb.manage.organization.detail.organization.email,
          "name": "Email"
        },
        {
          "attribut": '"phone":',
          "value": this.data.epdb.manage.organization.detail.organization.phone,
          "name": "Phone"
        },
        {
          "attribut": '"fax":',
          "value": this.data.epdb.manage.organization.detail.organization.fax,
          "name": "Fax"
        },
        {
          "attribut": '"organization_web":',
          "value": this.data.epdb.manage.organization.detail.organization.organization_web,
          "name": "Organization Web"
        },
        {
          "attribut": '"date_added":',
          "value": this.data.epdb.manage.organization.detail.organization.date_added,
          "name": "Date Added"
        },
        {
          "attribut": '"last_admin":',
          "value": this.data.epdb.manage.organization.detail.organization.last_admin,
          "name": "Last Admin"
        },
        {
          "attribut": '"admin_note":',
          "value": this.data.epdb.manage.organization.detail.organization.admin_note,
          "name": "Admin Note"
        },
        {
          "attribut": '"address_siebel":',
          "value": this.data.epdb.manage.organization.detail.organization.address_siebel,
          "name": "Address is Siebel"
        },
        {
          "attribut": '"organization_address":',
          "value": this.data.epdb.manage.organization.detail.organization.organization_address,
          "name": "Organization Address"
        },
        {
          "attribut": '"mailing_address":',
          "value": this.data.epdb.manage.organization.detail.organization.mailing_address,
          "name": "Mailing Address"
        },
        {
          "attribut": '"shiping_address":',
          "value": this.data.epdb.manage.organization.detail.organization.shiping_address,
          "name": "Shiping Address"
        },
        {
          "attribut": '"status":',
          "value": this.data.epdb.manage.organization.detail.organization.status,
          "name": "Status"
        }
      ]
    }
    else if (value == "epdb-manager-organization-edit") {
      this.contentarr =
      [
        {
          "attribut": '"edit_organization":',
          "value": this.data.epdb.manage.organization.edit.edit_organization,
          "name": "Edit Organization"
        },
        {
          "attribut": '"organization_name":',
          "value": this.data.epdb.manage.organization.edit.organization_name,
          "name": "Organization Name"
        },
        {
          "attribut": '"english_organization_name":',
          "value": this.data.epdb.manage.organization.edit.english_organization_name,
          "name": "English Organization Name"
        },
        {
          "attribut": '"commercial_organization_name":',
          "value": this.data.epdb.manage.organization.edit.commercial_organization_name,
          "name": "Commercial Organization Name"
        },
        {
          "attribut": '"atc_global_csn":',
          "value": this.data.epdb.manage.organization.edit.atc_global_csn,
          "name": "ATC Global CSN"
        },
        {
          "attribut": '"var_global_csn":',
          "value": this.data.epdb.manage.organization.edit.var_global_csn,
          "name": "VAR Global CSN"
        },
        {
          "attribut": '"ap_global_csn":',
          "value": this.data.epdb.manage.organization.edit.ap_global_csn,
          "name": "A&P Global CSN"
        },
        {
          "attribut": '"tax_exempt":',
          "value": this.data.epdb.manage.organization.edit.tax_exempt,
          "name": "Tax Exempt?"
        },
        {
          "attribut": '"vat_number":',
          "value": this.data.epdb.manage.organization.edit.vat_number,
          "name": "VAT Number"
        },
        {
          "attribut": '"year_joined":',
          "value": this.data.epdb.manage.organization.edit.year_joined,
          "name": "Year Joined"
        },
        {
          "attribut": '"organization_web_address":',
          "value": this.data.epdb.manage.organization.edit.organization_web_address,
          "name": "Organization Web Address"
        },
        {
          "attribut": '"registered_address":',
          "value": this.data.epdb.manage.organization.edit.registered_address,
          "name": "Registered Address"
        },
        {
          "attribut": '"contact_address":',
          "value": this.data.epdb.manage.organization.edit.contact_address,
          "name": "Contact Address"
        },
        {
          "attribut": '"invoicing_address":',
          "value": this.data.epdb.manage.organization.edit.invoicing_address,
          "name": "Invoicing Address"
        },
        {
          "attribut": '"department":',
          "value": this.data.epdb.manage.organization.edit.department,
          "name": "Department"
        },
        {
          "attribut": '"department":',
          "value": this.data.epdb.manage.organization.edit.department,
          "name": "Department"
        },
        {
          "attribut": '"address_line":',
          "value": this.data.epdb.manage.organization.edit.address_line,
          "name": "Address"
        },
        {
          "attribut": '"city":',
          "value": this.data.epdb.manage.organization.edit.city,
          "name": "City"
        },
        {
          "attribut": '"country":',
          "value": this.data.epdb.manage.organization.edit.country,
          "name": "Country"
        },
        {
          "attribut": '"select_country":',
          "value": this.data.epdb.manage.organization.edit.select_country,
          "name": "-- Select Country --"
        },
        {
          "attribut": '"state":',
          "value": this.data.epdb.manage.organization.edit.state,
          "name": "State / Province / Prefecture"
        },
        {
          "attribut": '"zip":',
          "value": this.data.epdb.manage.organization.edit.zip,
          "name": "Zip / Postal Code"
        },
        {
          "attribut": '"atc_director":',
          "value": this.data.epdb.manage.organization.edit.atc_director,
          "name": "ATC Director"
        },
        {
          "attribut": '"legal_contact":',
          "value": this.data.epdb.manage.organization.edit.legal_contact,
          "name": "Legal Contact"
        },
        {
          "attribut": '"billing_contact":',
          "value": this.data.epdb.manage.organization.edit.billing_contact,
          "name": "Billing Contact"
        },
        {
          "attribut": '"first_name":',
          "value": this.data.epdb.manage.organization.edit.first_name,
          "name": "First Name"
        },
        {
          "attribut": '"last_name":',
          "value": this.data.epdb.manage.organization.edit.last_name,
          "name": "Last Name"
        },
        {
          "attribut": '"email_address":',
          "value": this.data.epdb.manage.organization.edit.email_address,
          "name": "Email Address"
        },
        // {
        //   "attribut": '"telephone":',
        //   "value": this.data.epdb.manage.organization.edit.telephone,
        //   "name": "Telephone"
        // },
        {
          "attribut": '"fax":',
          "value": this.data.epdb.manage.organization.edit.fax,
          "name": "Fax"
        },
        {
          "attribut": '"administrative_notes":',
          "value": this.data.epdb.manage.organization.edit.administrative_notes,
          "name": "Administrative Notes"
        }
      ]
    }
    else if(value == "epdb-manager-organization-journal-entries") {
      this.contentarr =
      [
        {
          "attribut": '"organization_journal_entry":',
          "value": this.data.epdb.manage.organization.add_org_journal_entries.organization_journal_entry,
          "name": "Organization Journal Entries"
        },
        {
          "attribut": '"add_org_journal_entries":',
          "value": this.data.epdb.manage.organization.add_org_journal_entries.add_org_journal_entries,
          "name": "Journal Entry Type"
        },
        {
          "attribut": '"journal_entry_type":',
          "value": this.data.epdb.manage.organization.add_org_journal_entries.journal_entry_type,
          "name": "Journal Entry Type"
        },
        {
          "attribut": '"note_value":',
          "value": this.data.epdb.manage.organization.add_org_journal_entries.note_value,
          "name": "Note / Value"
        }
      ]
    }
    else if(value == "epdb-manager-invoice") {
      this.contentarr =
      [
        {
          "attribut": '"add_invoice":',
          "value": this.data.epdb.manage.organization.add_invoice.add_invoice,
          "name": "Add Invoice"
        },
        {
          "attribut": '"invoice_number":',
          "value": this.data.epdb.manage.organization.add_invoice.invoice_number,
          "name": "Invoice Number"
        },
        {
          "attribut": '"po_receive_date":',
          "value": this.data.epdb.manage.organization.add_invoice.po_receive_date,
          "name": "P.O. Receive Date"
        },
        {
          "attribut": '"financial_year":',
          "value": this.data.epdb.manage.organization.add_invoice.financial_year,
          "name": "Financial Year"
        },
        {
          "attribut": '"invoice_description":',
          "value": this.data.epdb.manage.organization.add_invoice.invoice_description,
          "name": "Invoice Decription"
        },
        {
          "attribut": '"sku":',
          "value": this.data.epdb.manage.organization.add_invoice.sku,
          "name": "SKU"
        },
        {
          "attribut": '"license":',
          "value": this.data.epdb.manage.organization.add_invoice.license,
          "name": "License"
        },
        {
          "attribut": '"total_amount":',
          "value": this.data.epdb.manage.organization.add_invoice.total_amount,
          "name": "Total Amount"
        },
        {
          "attribut": '"total_amount_usd":',
          "value": this.data.epdb.manage.organization.add_invoice.total_amount_usd,
          "name": "Total Amount in USD"
        }
      ]
    }

    // sub manage site
    else if (value == "epdb-manager-site-add") {
      this.contentarr =
      [
        {
          "attribut": '"add_new_site":',
          "value": this.data.epdb.manage.site.add_new_site,
          "name": "Add New Site"
        },
        {
          "attribut": '"organization":',
          "value": this.data.epdb.manage.site.organization,
          "name": "Organization"
        },
        {
          "attribut": '"site_name":',
          "value": this.data.epdb.manage.site.site_name,
          "name": "Site Name"
        },
        {
          "attribut": '"english_site_name":',
          "value": this.data.epdb.manage.site.english_site_name,
          "name": "English Site Name"
        },
        {
          "attribut": '"commercial_site_name":',
          "value": this.data.epdb.manage.site.commercial_site_name,
          "name": "Commercial Site Name"
        },
        {
          "attribut": '"workstation":',
          "value": this.data.epdb.manage.site.workstation,
          "name": "Workstation"
        },
        {
          "attribut": '"magellan_id":',
          "value": this.data.epdb.manage.site.magellan_id,
          "name": "Magellan ID"
        },
        // {
        //   "attribut": '"telephone":',
        //   "value": this.data.epdb.manage.site.telephone,
        //   "name": "Telephone"
        // },
        {
          "attribut": '"fax":',
          "value": this.data.epdb.manage.site.fax,
          "name": "Fax"
        },
        {
          "attribut": '"email_address":',
          "value": this.data.epdb.manage.site.email_address,
          "name": "Email Address"
        },
        {
          "attribut": '"siebel_site":',
          "value": this.data.epdb.manage.site.siebel_site,
          "name": "Siebel Site"
        },
        {
          "attribut": '"site_web_address":',
          "value": this.data.epdb.manage.site.site_web_address,
          "name": "Site Web Address"
        },
        {
          "attribut": '"partner_type":',
          "value": this.data.epdb.manage.site.partner_type,
          "name": "Partner Type"
        },
        {
          "attribut": '"head_siebel_address":',
          "value": this.data.epdb.manage.site.head_siebel_address,
          "name": "Siebel Address"
        },
        {
          "attribut": '"head_site_address":',
          "value": this.data.epdb.manage.site.head_site_address,
          "name": "Site Address"
        },
        {
          "attribut": '"head_mailing_address":',
          "value": this.data.epdb.manage.site.head_mailing_address,
          "name": "Mailing Address"
        },
        {
          "attribut": '"head_shipping_address":',
          "value": this.data.epdb.manage.site.head_shipping_address,
          "name": "Shiping Address"
        },
        {
          "attribut": '"head_site_manager":',
          "value": this.data.epdb.manage.site.head_site_manager,
          "name": "Site Manager"
        },
        {
          "attribut": '"head_site_administrator":',
          "value": this.data.epdb.manage.site.head_site_administrator,
          "name": "Site Administrator"
        },
        {
          "attribut": '"department":',
          "value": this.data.epdb.manage.site.department,
          "name": "Department"
        },
        {
          "attribut": '"address":',
          "value": this.data.epdb.manage.site.address,
          "name": "Address"
        },
        {
          "attribut": '"city":',
          "value": this.data.epdb.manage.site.city,
          "name": "City"
        },
        {
          "attribut": '"country":',
          "value": this.data.epdb.manage.site.country,
          "name": "Country"
        },
        {
          "attribut": '"select_country":',
          "value": this.data.epdb.manage.site.select_country,
          "name": "-- Select Country --"
        },
        {
          "attribut": '"state":',
          "value": this.data.epdb.manage.site.state,
          "name": "Site Administrator"
        },
        {
          "attribut": '"postal_code":',
          "value": this.data.epdb.manage.site.postal_code,
          "name": "First Name"
        },
        {
          "attribut": '"first_name":',
          "value": this.data.epdb.manage.site.first_name,
          "name": "First Name"
        },
        {
          "attribut": '"last_name":',
          "value": this.data.epdb.manage.site.last_name,
          "name": "Last Name"
        },
        {
          "attribut": '"administrative_note":',
          "value": this.data.epdb.manage.site.administrative_note,
          "name": "Administrative Note"
        }
      ]
    }
    else if (value == "epdb-manager-site-search") {
      this.contentarr =
      [
        {
          "attribut": '"search_site":',
          "value": this.data.epdb.manage.site.search_site,
          "name": "Search Site"
        },
        {
          "attribut": '"tab_organization":',
          "value": this.data.epdb.manage.site.tab_organization,
          "name": "Tab Organization"
        },
        {
          "attribut": '"tab_site":',
          "value": this.data.epdb.manage.site.tab_site,
          "name": "Tab Site"
        },
        {
          "attribut": '"tab_contact":',
          "value": this.data.epdb.manage.site.tab_contact,
          "name": "Tab Contact"
        },
        {
          "attribut": '"site_id":',
          "value": this.data.epdb.manage.site.site_id,
          "name": "Site ID"
        },
        {
          "attribut": '"site_name":',
          "value": this.data.epdb.manage.site.site_name,
          "name": "Site Name"
        },
        {
          "attribut": '"contact_name":',
          "value": this.data.epdb.manage.site.contact_name,
          "name": "Contact Name"
        },
        {
          "attribut": '"sap_csn":',
          "value": this.data.epdb.manage.site.sap_csn,
          "name": "SAP / CSN"
        },
        {
          "attribut": '"partner_type":',
          "value": this.data.epdb.manage.site.partner_type,
          "name": "Partner Type"
        },
        {
          "attribut": '"partner_type_status":',
          "value": this.data.epdb.manage.site.partner_type_status,
          "name": "Partner Type Status"
        },
        {
          "attribut": '"geo":',
          "value": this.data.epdb.manage.site.geo,
          "name": "Geo"
        },
        {
          "attribut": '"region":',
          "value": this.data.epdb.manage.site.region,
          "name": "Region"
        },
        {
          "attribut": '"sub_region":',
          "value": this.data.epdb.manage.site.sub_region,
          "name": "Sub Region"
        },
        {
          "attribut": '"countries":',
          "value": this.data.epdb.manage.site.countries,
          "name": "Countries"
        },
        {
          "attribut": '"state":',
          "value": this.data.epdb.manage.site.state,
          "name": "State / Province / Prefecture"
        },
        {
          "attribut": '"search_result":',
          "value": this.data.epdb.manage.site.search_result,
          "name": "Search Result"
        },
        {
          "attribut": '"organization_id":',
          "value": this.data.epdb.manage.site.organization_id,
          "name": "Organization ID"
        },
        {
          "attribut": '"type":',
          "value": this.data.epdb.manage.site.type,
          "name": "Type"
        },
        {
          "attribut": '"site_status":',
          "value": this.data.epdb.manage.site.site_status,
          "name": "Site Status"
        },
        {
          "attribut": '"contact":',
          "value": this.data.epdb.manage.site.contact,
          "name": "Contact"
        }
      ]
    }
    else if (value == "epdb-manager-site-detail") {
      this.contentarr =
      [
        {
          "attribut": '"detail_site":',
          "value": this.data.epdb.manage.site.detail.detail_site,
          "name": "Detail Site"
        },
        {
          "attribut": '"site":',
          "value": this.data.epdb.manage.site.detail.site.site,
          "name": "Site"
        },
        {
          "attribut": '"siebel_site_name":',
          "value": this.data.epdb.manage.site.detail.site.siebel_site_name,
          "name": "Siebel Site Name"
        },
        {
          "attribut": '"site_name":',
          "value": this.data.epdb.manage.site.detail.site.site_name,
          "name": "Site Name"
        },
        {
          "attribut": '"site_id":',
          "value": this.data.epdb.manage.site.detail.site.site_id,
          "name": "Site ID"
        },
        {
          "attribut": '"commercial_name":',
          "value": this.data.epdb.manage.site.detail.site.commercial_name,
          "name": "Commercial Name"
        },
        {
          "attribut": '"geo_region":',
          "value": this.data.epdb.manage.site.detail.site.geo_region,
          "name": "Geo / Region"
        },
        {
          "attribut": '"email":',
          "value": this.data.epdb.manage.site.detail.site.email,
          "name": "Email"
        },
        {
          "attribut": '"phone":',
          "value": this.data.epdb.manage.site.detail.site.phone,
          "name": "Phone"
        },
        {
          "attribut": '"fax":',
          "value": this.data.epdb.manage.site.detail.site.fax,
          "name": "Fax"
        },
        {
          "attribut": '"organization_web":',
          "value": this.data.epdb.manage.site.detail.site.organization_web,
          "name": "Organization Web"
        },
        {
          "attribut": '"date_added":',
          "value": this.data.epdb.manage.site.detail.site.date_added,
          "name": "Date Added"
        },
        {
          "attribut": '"last_admin":',
          "value": this.data.epdb.manage.site.detail.site.last_admin,
          "name": "Last Admin"
        },
        {
          "attribut": '"admin_note":',
          "value": this.data.epdb.manage.site.detail.site.admin_note,
          "name": "Admin Note"
        },
        {
          "attribut": '"organization_name":',
          "value": this.data.epdb.manage.site.detail.site.organization_name,
          "name": "Organization Name"
        },
        {
          "attribut": '"organization_id":',
          "value": this.data.epdb.manage.site.detail.site.organization_id,
          "name": "Organization ID"
        },
        {
          "attribut": '"mailing_address":',
          "value": this.data.epdb.manage.site.detail.site.mailing_address,
          "name": "Mailing Address"
        },
        {
          "attribut": '"shiping_address":',
          "value": this.data.epdb.manage.site.detail.site.shiping_address,
          "name": "Shiping Address"
        }
      ]
    }
    else if (value == "epdb-manager-site-edit") {
      this.contentarr =
      [
        {
          "attribut": '"edit_site":',
          "value": this.data.epdb.manage.site.edit.edit_site,
          "name": "Edit Site"
        },
        {
          "attribut": '"organization":',
          "value": this.data.epdb.manage.site.edit.organization,
          "name": "Organization"
        },
        {
          "attribut": '"site_name":',
          "value": this.data.epdb.manage.site.edit.site_name,
          "name": "Site Name"
        },
        {
          "attribut": '"english_site_name":',
          "value": this.data.epdb.manage.site.edit.english_site_name,
          "name": "English Site Name"
        },
        {
          "attribut": '"commercial_site_name":',
          "value": this.data.epdb.manage.site.edit.commercial_site_name,
          "name": "Commercial Site Name"
        },
        {
          "attribut": '"workstation":',
          "value": this.data.epdb.manage.site.edit.workstation,
          "name": "Workstations"
        },
        {
          "attribut": '"magellan_id":',
          "value": this.data.epdb.manage.site.edit.magellan_id,
          "name": "Magellan ID"
        },
        // {
        //   "attribut": '"telephone":',
        //   "value": this.data.epdb.manage.site.edit.telephone,
        //   "name": "Telephone"
        // },
        {
          "attribut": '"fax":',
          "value": this.data.epdb.manage.site.edit.fax,
          "name": "Fax"
        },
        {
          "attribut": '"email_address":',
          "value": this.data.epdb.manage.site.edit.email_address,
          "name": "Email Address"
        },
        {
          "attribut": '"site_web_address":',
          "value": this.data.epdb.manage.site.edit.site_web_address,
          "name": "Site Web Address"
        },
        {
          "attribut": '"siebel_site":',
          "value": this.data.epdb.manage.site.edit.siebel_site,
          "name": "Siebel Site"
        },
        {
          "attribut": '"head_siebel_address":',
          "value": this.data.epdb.manage.site.edit.head_siebel_address,
          "name": "Siebel Address"
        },
        {
          "attribut": '"head_site_address":',
          "value": this.data.epdb.manage.site.edit.head_site_address,
          "name": "Site Address"
        },
        {
          "attribut": '"head_mailing_address":',
          "value": this.data.epdb.manage.site.edit.head_mailing_address,
          "name": "Mailing Address"
        },
        {
          "attribut": '"head_shipping_address":',
          "value": this.data.epdb.manage.site.edit.head_shipping_address,
          "name": "Shiping Address"
        },
        {
          "attribut": '"head_site_manager":',
          "value": this.data.epdb.manage.site.edit.head_site_manager,
          "name": "Site Manager"
        },
        {
          "attribut": '"head_site_administrator":',
          "value": this.data.epdb.manage.site.edit.head_site_administrator,
          "name": "Site Administrator"
        },
        {
          "attribut": '"department":',
          "value": this.data.epdb.manage.site.edit.department,
          "name": "Department"
        },
        {
          "attribut": '"address":',
          "value": this.data.epdb.manage.site.edit.address,
          "name": "Address"
        },
        {
          "attribut": '"city":',
          "value": this.data.epdb.manage.site.edit.city,
          "name": "City"
        },
        {
          "attribut": '"country":',
          "value": this.data.epdb.manage.site.edit.country,
          "name": "Country"
        },
        {
          "attribut": '"select_country":',
          "value": this.data.epdb.manage.site.edit.select_country,
          "name": "-- Select Country --"
        },
        {
          "attribut": '"state":',
          "value": this.data.epdb.manage.site.edit.state,
          "name": "Site Administrator"
        },
        {
          "attribut": '"postal_code":',
          "value": this.data.epdb.manage.site.edit.postal_code,
          "name": "First Name"
        },
        {
          "attribut": '"first_name":',
          "value": this.data.epdb.manage.site.edit.first_name,
          "name": "First Name"
        },
        {
          "attribut": '"last_name":',
          "value": this.data.epdb.manage.site.edit.last_name,
          "name": "Last Name"
        },
        {
          "attribut": '"administrative_note":',
          "value": this.data.epdb.manage.site.edit.administrative_note,
          "name": "Administrative Note"
        }
      ]
    }
    else if(value == "epdb-manager-site-partner-type") {
      this.contentarr = 
      [
        {
          "attribut": '"add_partner_type":',
          "value": this.data.epdb.manage.site.add_partner_type.add_partner_type,
          "name": "Add Partner Type"
        },
        {
          "attribut": '"csn":',
          "value": this.data.epdb.manage.site.add_partner_type.csn,
          "name": "Add Partner Type"
        },
        {
          "attribut": '"external_id":',
          "value": this.data.epdb.manage.site.add_partner_type.external_id,
          "name": "External ID"
        },
        {
          "attribut": '"sub_type":',
          "value": this.data.epdb.manage.site.add_partner_type.sub_type,
          "name": "Sub Type"
        }
      ]
    }

    // sub manage contact
    else if (value == "epdb-manager-contact-add"){
      this.contentarr =
      [
        {
          "attribut": '"add_new_contact":',
          "value": this.data.epdb.manage.contact.add_new_contact,
          "name": "Add New Contact"
        },
        {
          "attribut": '"site_name":',
          "value": this.data.epdb.manage.contact.site_name,
          "name": "Site Name"
        },
        {
          "attribut": '"first_name":',
          "value": this.data.epdb.manage.contact.first_name,
          "name": "First Name"
        },
        {
          "attribut": '"last_name":',
          "value": this.data.epdb.manage.contact.last_name,
          "name": "Last Name"
        },
        {
          "attribut": '"email_address_username":',
          "value": this.data.epdb.manage.contact.email_address_username,
          "name": "Email Address or Username"
        },
        {
          "attribut": '"or":',
          "value": this.data.epdb.manage.contact.or,
          "name": "Email Address or Username"
        },
        {
          "attribut": '"btn_choose_affiliated":',
          "value": this.data.epdb.manage.contact.btn_choose_affiliated,
          "name": "Choose from Affiliated Contacts"
        }
      ]
    }
    else if (value == "epdb-manager-contact-search") {
      this.contentarr =
      [
        {
          "attribut": '"search_contact":',
          "value": this.data.epdb.manage.contact.search_contact,
          "name": "Search Contact"
        },
        {
          "attribut": '"tab_organization":',
          "value": this.data.epdb.manage.contact.tab_organization,
          "name": "Tab Organization"
        },
        {
          "attribut": '"tab_site":',
          "value": this.data.epdb.manage.contact.tab_site,
          "name": "Tab Site"
        },
        {
          "attribut": '"tab_contact":',
          "value": this.data.epdb.manage.contact.tab_contact,
          "name": "Tab Contact"
        },
        {
          "attribut": '"contact_id":',
          "value": this.data.epdb.manage.contact.contact_id,
          "name": "Contact ID"
        },
        {
          "attribut": '"first_name":',
          "value": this.data.epdb.manage.contact.first_name,
          "name": "First Name"
        },
        {
          "attribut": '"last_name":',
          "value": this.data.epdb.manage.contact.last_name,
          "name": "Last Name"
        },
        {
          "attribut": '"email_address":',
          "value": this.data.epdb.manage.contact.email_address,
          "name": "Email Address"
        },
        {
          "attribut": '"partner_type":',
          "value": this.data.epdb.manage.contact.partner_type,
          "name": "Partner Type"
        },
        {
          "attribut": '"partner_type_status":',
          "value": this.data.epdb.manage.contact.partner_type_status,
          "name": "Partner Type Status"
        },
        {
          "attribut": '"geo":',
          "value": this.data.epdb.manage.contact.geo,
          "name": "Geo"
        },
        {
          "attribut": '"region":',
          "value": this.data.epdb.manage.contact.region,
          "name": "Region"
        },
        {
          "attribut": '"sub_region":',
          "value": this.data.epdb.manage.contact.sub_region,
          "name": "Sub Rregion"
        },
        {
          "attribut": '"countries":',
          "value": this.data.epdb.manage.contact.countries,
          "name": "Countries"
        },
        {
          "attribut": '"state":',
          "value": this.data.epdb.manage.contact.state,
          "name": "State"
        },
        {
          "attribut": '"search_result":',
          "value": this.data.epdb.manage.contact.search_result,
          "name": "Search Result"
        },
        {
          "attribut": '"name":',
          "value": this.data.epdb.manage.contact.name,
          "name": "Name"
        },
        {
          "attribut": '"country":',
          "value": this.data.epdb.manage.contact.country,
          "name": "Country"
        }
      ]
    }
    else if (value == "epdb-manager-contact-detail") {
      this.contentarr =
      [
        {
          "attribut": '"detail_contact":',
          "value": this.data.epdb.manage.contact.detail.detail_contact,
          "name": "Detail Contact"
        },
        {
          "attribut": '"contact":',
          "value": this.data.epdb.manage.contact.detail.contact.contact,
          "name": "Contact"
        },
        {
          "attribut": '"name":',
          "value": this.data.epdb.manage.contact.detail.contact.name,
          "name": "Name"
        },
        {
          "attribut": '"eidm_guid":',
          "value": this.data.epdb.manage.contact.detail.contact.eidm_guid,
          "name": "EIDM GUID"
        },
        {
          "attribut": '"title_position":',
          "value": this.data.epdb.manage.contact.detail.contact.title_position,
          "name": "Title / Posistion"
        },
        // {
        //   "attribut": '"telephone":',
        //   "value": this.data.epdb.manage.contact.detail.contact.telephone,
        //   "name": "Telephone"
        // },
        {
          "attribut": '"mobile":',
          "value": this.data.epdb.manage.contact.detail.contact.mobile,
          "name": "Mobile"
        },
        {
          "attribut": '"time_zone":',
          "value": this.data.epdb.manage.contact.detail.contact.time_zone,
          "name": "Time Zone"
        },
        {
          "attribut": '"comment":',
          "value": this.data.epdb.manage.contact.detail.contact.comment,
          "name": "Comments"
        },
        {
          "attribut": '"created":',
          "value": this.data.epdb.manage.contact.detail.contact.created,
          "name": "Created"
        },
        {
          "attribut": '"modified":',
          "value": this.data.epdb.manage.contact.detail.contact.modified,
          "name": "Modified"
        },
        {
          "attribut": '"contact_id":',
          "value": this.data.epdb.manage.contact.detail.contact.contact_id,
          "name": "Contact ID"
        },
        {
          "attribut": '"instructor_id":',
          "value": this.data.epdb.manage.contact.detail.contact.instructor_id,
          "name": "Instructor ID"
        },
        {
          "attribut": '"department":',
          "value": this.data.epdb.manage.contact.detail.contact.department,
          "name": "Department"
        },
        {
          "attribut": '"email":',
          "value": this.data.epdb.manage.contact.detail.contact.email,
          "name": "Email"
        },
        {
          "attribut": '"eidm_user_id":',
          "value": this.data.epdb.manage.contact.detail.contact.eidm_user_id,
          "name": "EIDM User ID"
        },
        {
          "attribut": '"language":',
          "value": this.data.epdb.manage.contact.detail.contact.language,
          "name": "Language"
        },
        {
          "attribut": '"last_login":',
          "value": this.data.epdb.manage.contact.detail.contact.last_login,
          "name": "Last Login"
        },
        {
          "attribut": '"last_valid":',
          "value": this.data.epdb.manage.contact.detail.contact.last_valid,
          "name": "Last Validated"
        },
        {
          "attribut": '"primary_industry":',
          "value": this.data.epdb.manage.contact.detail.contact.primary_industry,
          "name": "Primary Industry"
        },
        {
          "attribut": '"atc_instructor":',
          "value": this.data.epdb.manage.contact.detail.contact.atc_instructor,
          "name": "ATC Instructor"
        },
        {
          "attribut": '"date_questionnaire_complete":',
          "value": this.data.epdb.manage.contact.detail.contact.date_questionnaire_complete,
          "name": "Date Questionnaire Completed"
        },
        {
          "attribut": '"priamry_job_role":',
          "value": this.data.epdb.manage.contact.detail.contact.priamry_job_role,
          "name": "Primary Job Role"
        },
        {
          "attribut": '"dedicated":',
          "value": this.data.epdb.manage.contact.detail.contact.dedicated,
          "name": "Dedicated"
        }
      ]
    }
    else if (value == "epdb-manager-contact-edit") {
      this.contentarr =
      [
        {
          "attribut": '"edit_contact":',
          "value": this.data.epdb.manage.contact.edit.edit_contact,
          "name": "Edit Contact"
        },
        {
          "attribut": '"contact_name":',
          "value": this.data.epdb.manage.contact.edit.contact_name,
          "name": "Contact Name"
        },
        {
          "attribut": '"first_name":',
          "value": this.data.epdb.manage.contact.edit.first_name,
          "name": "First Name"
        },
        {
          "attribut": '"last_name":',
          "value": this.data.epdb.manage.contact.edit.last_name,
          "name": "Last Name"
        },
        {
          "attribut": '"honorific":',
          "value": this.data.epdb.manage.contact.edit.honorific,
          "name": "Honorific"
        },
        {
          "attribut": '"email_address":',
          "value": this.data.epdb.manage.contact.edit.email_address,
          "name": "Email Address"
        },
        {
          "attribut": '"dont_send_email":',
          "value": this.data.epdb.manage.contact.edit.dont_send_email,
          "name": "Don't send email"
        },
        {
          "attribut": '"primary_job_role":',
          "value": this.data.epdb.manage.contact.edit.primary_job_role,
          "name": "Primary Job Role"
        },
        {
          "attribut": '"primary_industry":',
          "value": this.data.epdb.manage.contact.edit.primary_industry,
          "name": "Primary Industry"
        },
        {
          "attribut": '"time_zone":',
          "value": this.data.epdb.manage.contact.edit.time_zone,
          "name": "Time Zone"
        },
        // {
        //   "attribut": '"telephone":',
        //   "value": this.data.epdb.manage.contact.edit.telephone,
        //   "name": "Telephone"
        // },
        {
          "attribut": '"mobile":',
          "value": this.data.epdb.manage.contact.edit.mobile,
          "name": "Mobile"
        },
        {
          "attribut": '"primary_language":',
          "value": this.data.epdb.manage.contact.edit.primary_language,
          "name": "Primary Language"
        },
        {
          "attribut": '"secondary_language":',
          "value": this.data.epdb.manage.contact.edit.secondary_language,
          "name": "Secondary Language"
        },
        {
          "attribut": '"comment":',
          "value": this.data.epdb.manage.contact.edit.comment,
          "name": "Comment"
        }
      ]
    }
    else if (value == "epdb-manager-contact-affiliated-site") {
      this.contentarr =
      [
        {
          "attribut": '"affiliated_site":',
          "value": this.data.epdb.manage.contact.affiliated_site.affiliated_site,
          "name": "Affiliated Sites"
        },
        {
          "attribut": '"site_location":',
          "value": this.data.epdb.manage.contact.affiliated_site.site_location,
          "name": "Site Location"
        }
      ]
    }

    // Distributor
    else if(value == "epdb-manager-distributor") {
      this.menuselect = true;
      this.submenuselect = false;
      this.contentarr =
      [
        {
          "attribut": '"list_distributor":',
          "value": this.data.epdb.manage.distributor.list_distributor,
          "name": "List Distributor"
        },
        {
          "attribut": '"btn_add_distributor":',
          "value": this.data.epdb.manage.distributor.btn_add_distributor,
          "name": "Button Add Distributor"
        },
        {
          "attribut": '"site_id":',
          "value": this.data.epdb.manage.distributor.site_id,
          "name": "Header Table Site ID"
        },
        {
          "attribut": '"site_name":',
          "value": this.data.epdb.manage.distributor.site_name,
          "name": "Site Name"
        },
        {
          "attribut": '"partner_type":',
          "value": this.data.epdb.manage.distributor.partner_type,
          "name": "Partner Type"
        },
        {
          "attribut": '"site_status":',
          "value": this.data.epdb.manage.distributor.site_status,
          "name": "Site Status"
        },
        {
          "attribut": '"add_distributor":',
          "value": this.data.epdb.manage.distributor.add_distributor,
          "name": "Add Distributor"
        },
        {
          "attribut": '"geo":',
          "value": this.data.epdb.manage.distributor.geo,
          "name": "Geo"
        },
        {
          "attribut": '"region":',
          "value": this.data.epdb.manage.distributor.region,
          "name": "Region"
        },
        {
          "attribut": '"sub_region":',
          "value": this.data.epdb.manage.distributor.sub_region,
          "name": "Sub Region"
        },
        {
          "attribut": '"country":',
          "value": this.data.epdb.manage.distributor.country,
          "name": "Country"
        },
        {
          "attribut": '"country_code":',
          "value": this.data.epdb.manage.distributor.country_code,
          "name": "Country Code"
        },
        {
          "attribut": '"market_type":',
          "value": this.data.epdb.manage.distributor.market_type,
          "name": "Market Type"
        },
        {
          "attribut": '"embargoed":',
          "value": this.data.epdb.manage.distributor.embargoed,
          "name": "Embargoed"
        },
        {
          "attribut": '"territory":',
          "value": this.data.epdb.manage.distributor.territory,
          "name": "Territory"
        },
        {
          "attribut": '"master_distributor":',
          "value": this.data.epdb.manage.distributor.master_distributor,
          "name": "Master Distributor"
        },
        {
          "attribut": '"regional_distributor":',
          "value": this.data.epdb.manage.distributor.regional_distributor,
          "name": "Regional Distributor"
        },
        {
          "attribut": '"contact_distributor":',
          "value": this.data.epdb.manage.distributor.contact_distributor,
          "name": "Contact Distributor"
        }
      ]
    }
  }


  datatmp: any;
  saveLanguage(value) {

    let data = value.attribut + ' "' + value.value + '"';
    let dataedit = value.attribut + ' "' + this.changevalue + '"';
    console.log(this._serviceUrl + '/update/' + this.languageselect + '/' + data + '/' + dataedit)
    this.service.httpClientGet(this._serviceUrl + '/update/' + this.languageselect + '/' + data + '/' + dataedit, '')
      .subscribe(result => {
        this.data = result;
      },
      error => {
        this.service.errorserver();
        this.data = '';
      });

    this.changevalue = '';

  }

  inputchange(value, item) {
    this.itemvalue = item;
    this.changevalue = value;
  }

}
