import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as c3 from 'c3';
declare const $: any;
declare var Morris: any;
import '../../../../assets/echart/echarts-all.js';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { Router } from '@angular/router';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import swal from 'sweetalert2';

import { SessionService } from '../../../shared/service/session.service';
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";

@Component({
  selector: 'app-products-add-epdb',
  templateUrl: './products-add-epdb.component.html',
  styleUrls: [
    './products-add-epdb.component.css',
    '../../../../../node_modules/c3/c3.min.css',
  ],
  encapsulation: ViewEncapsulation.None
})

export class ProductsAddEPDBComponent implements OnInit {

  private _serviceUrl = '/api/Product';
  addproduct: FormGroup;
  private productcategory;
  private year;
  public loading = false;
  public showValid = false;
  dropdownListCategory = [];
  selectedItemsCategory = [];
  dropdownSettings = {};
  available: boolean;
  productID;
  public useraccesdata: any;

  constructor(private router: Router, private service: AppService, private formatdate: AppFormatDate, private session: SessionService) {

    //get user level
    let useracces = this.session.getData();
    this.useraccesdata = JSON.parse(useracces);

    //validation
    let productName = new FormControl('', Validators.required);
    let familyId = new FormControl('', Validators.required);
    // let year = new FormControl('', Validators.required);
    let version = new FormControl('', Validators.required);
    let Status = new FormControl('');

    this.addproduct = new FormGroup({
      productName: productName,
      familyId: familyId,
      version: version,
      Status: Status
    });

  }

  // getFY() {
  //   var year = '';
  //   var parent = "FYIndicator";
  //   var status = "A";
  //   this.service.httpClientGet("api/Dictionaries/where/{'Parent':'" + parent + "','Status':'" + status + "'}", year)
  //     .subscribe(res => {
  //       this.year = res;
  //     }, error => {
  //       this.service.errorserver();
  //     });
  // }

  checkAvailable(value) {
    let data: any;
    this.service.httpClientGet(this._serviceUrl + "/CheckAvailable/" + value, data)
      .subscribe(res => {
        data = res;
        // console.log(data);
        if (data.jumlah != '0') {
          this.available = true;
          this.productID = data.productId;
        } else {
          this.available = false;
        }
      });
  }

  getCategory() {
    //get data Product Category
    var data: any;
    this.service.httpClientGet('/api/Product/Family', data)
      .subscribe(result => {
        if (result == "Not found") {
          this.service.notfound();
          this.dropdownListCategory = [];
        }
        else {
          this.productcategory = result;
          this.dropdownListCategory = this.productcategory.map((item) => {
            return {
              id: item.familyId,
              itemName: item.familyId
            }
          });
        }
      });
    this.selectedItemsCategory = [];
  }

  ngOnInit() {
    // this.getFY();
    this.getCategory();
    this.dropdownSettings = {
      singleSelection: false,
      text: "Please Select",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class",
      disabled: false,
      maxHeight: 120,
      badgeShowLimit: 5
    };
  }

  onCategorySelect(item: any) { this.showValid = false; }

  OnCategoryDeSelect(item: any) { }

  onCategorySelectAll(items: any) { this.showValid = false; }

  onCategoryDeSelectAll(items: any) { }

  //submit form
  submitted: boolean;
  onSubmit() {
    this.addproduct.controls['productName'].markAsTouched();
    this.addproduct.controls['familyId'].markAsTouched();
    this.addproduct.controls['version'].markAsTouched();

    this.submitted = true;

    if (this.addproduct.valid) {
      if (this.selectedItemsCategory.length != 0) {
        this.loading = true;
        if (this.available == false) {
          let temp = [];
          for (let i = 0; i < this.selectedItemsCategory.length; i++) {
            temp.push(this.selectedItemsCategory[i].id);
          }

          this.addproduct.value.familyId = temp.join(",");
          this.addproduct.value.Status = "A";
          
          this.addproduct.value.cuid = this.useraccesdata.ContactName;
          this.addproduct.value.UserId = this.useraccesdata.UserId;

          let data = JSON.stringify(this.addproduct.value);

          //Simpan baru ke tabel product,product families dan product versions
          this.service.httpClientPost(this._serviceUrl, data);
        } else {
          let dataVersi = ({
            productId: this.productID,
            version: this.addproduct.value.version,
            Status: 'A'
          });

          //Simpan baru ke tabel product,product families dan product versions
          this.service.httpClientPost(this._serviceUrl + "/ProductVersions", JSON.stringify(dataVersi));
        }

        setTimeout(() => {
          //redirect
          // this.router.navigate(['/admin/products/products-list'], { queryParams: { familyId: this.addproduct.value.familyId } });
          this.router.navigate(['/admin/products/products-list']);
          this.loading = false;
        }, 1000)
      } else {
        this.showValid = true;
      }
    }
  }

  resetForm() {
    this.addproduct.reset({
      'familyId': '',
      'year': '',
      'productName': ''
    });
  }
}
