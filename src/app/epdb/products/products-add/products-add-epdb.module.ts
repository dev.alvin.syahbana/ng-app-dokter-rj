import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsAddEPDBComponent } from './products-add-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';

export const ProductsAddEPDBRoutes: Routes = [
  {
    path: '',
    component: ProductsAddEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.product.add_new_product',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProductsAddEPDBRoutes),
    SharedModule,
    LoadingModule,
    AngularMultiSelectModule
  ],
  declarations: [ProductsAddEPDBComponent],
  providers: [AppService, AppFormatDate]
})
export class ProductsAddEPDBModule { }
