import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsUpdateEPDBComponent } from './products-update-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../../shared/shared.module";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import {AppService} from "../../../shared/service/app.service";
import { LoadingModule } from 'ngx-loading';

export const ProductsUpdateEPDBRoutes: Routes = [
  {
    path: '',
    component: ProductsUpdateEPDBComponent,
    data: {
      breadcrumb: 'epdb.admin.product.edit_product',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProductsUpdateEPDBRoutes),
    SharedModule,
    LoadingModule,
    AngularMultiSelectModule
  ],
  declarations: [ProductsUpdateEPDBComponent],
  providers:[AppService]
})
export class ProductsUpdateEPDBModule { }
