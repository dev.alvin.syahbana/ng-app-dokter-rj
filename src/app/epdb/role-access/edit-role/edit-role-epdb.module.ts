import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditRoleEPDBComponent } from './edit-role-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { AppTranslateLanguage } from '../../../shared/translate-language/app.translateLanguage';
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";

export const EditRoleEPDBRoutes: Routes = [
  {
    path: '',
    component: EditRoleEPDBComponent,
    data: {
      breadcrumb: 'epdb.role.edit_role',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(EditRoleEPDBRoutes),
    SharedModule
  ],
  declarations: [EditRoleEPDBComponent],
  providers: [AppService, AppFormatDate]
})
export class EditRoleEPDBModule { }