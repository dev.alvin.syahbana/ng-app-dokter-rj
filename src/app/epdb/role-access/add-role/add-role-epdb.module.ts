import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddRoleEPDBComponent } from './add-role-epdb.component';
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "../../../shared/shared.module";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { AppTranslateLanguage } from '../../../shared/translate-language/app.translateLanguage';
import { AppService } from "../../../shared/service/app.service";
import { AppFormatDate } from "../../../shared/format-date/app.format-date";
import { LoadingModule } from 'ngx-loading';
import { DataFilterRolePipe } from './add-role-epdb.component';

export const AddRoleEPDBRoutes: Routes = [
  {
    path: '',
    component: AddRoleEPDBComponent,
    data: {
      breadcrumb: 'epdb.role.add_new_role',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AddRoleEPDBRoutes),
    SharedModule,
    LoadingModule
  ],
  declarations: [AddRoleEPDBComponent, DataFilterRolePipe],
  providers: [AppService, AppFormatDate]
})
export class AddRoleEPDBModule { }