import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewUserInformationEPDBComponent } from './view-user-information-epdb.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";

export const ViewUserInformationEPDBRoutes: Routes = [
  {
    path: '',
    component: ViewUserInformationEPDBComponent,
    data: {
      breadcrumb: 'View User Information',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ViewUserInformationEPDBRoutes),
    SharedModule
  ],
  declarations: [ViewUserInformationEPDBComponent]
})
export class ViewUserInformationEPDBModule { }
